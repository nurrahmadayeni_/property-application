<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
if(!isset($_SESSION["username"])){
   echo "<script>window.alert('Anda belum login');
        window.location=('../../index.php')</script>";
}else{

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">  <head>
<title>SUPERVISOR</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Bootstrap -->
<link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- styles -->
<link href="../../css/main.css" rel="stylesheet">
<link href="../../images/pavicon.png" rel="icon" type="image/x-icon" />

<link rel="stylesheet" href="../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css"/>
<link rel="stylesheet" href="../assets/css/bootstrap-theme.min.css">

<script src="../assets/js/jquery-1.11.0.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/jquery.maskMoney.min.js"></script>
<script src="../assets/datatables/jquery.dataTables.js"></script>
<script src="../assets/datatables/dataTables.bootstrap.js"></script>

<script>
//copyright
function copyDate() {
	var cpyrt = document.getElementById("copyright")
	if (cpyrt) {
	cpyrt.firstChild.nodeValue = (new Date()).getFullYear();
	}}
window.onload = copyDate;
</script>
	
</head>
  <body >
  	<div class="header" style="background-color: #5F9EAD">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-5">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="index.html">Supervisor</a></h1>
	              </div>
	           </div>
	           <div class="col-md-5">
	              <div class="row">
	                <div class="col-lg-12">
	                  <div class="input-group form"></div>
	                </div>
	              </div>
	           </div>
	           <div class="col-md-2">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo"$_SESSION[username]"; ?></b></a>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

    <div class="page-content">
    	<div class="row">
		  <div class="col-md-2">
		  	<div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
					<?php
				        $page = $_SERVER['QUERY_STRING'];
				    ?>
                    <li <?php if($page=='mod=data_adm') echo "class='current'" ?>>
						<a href="?mod=data_adm"><i class="glyphicon glyphicon-record"></i>Laporan ADM</a>
					</li>
					<li <?php if($page=='mod=data_teknikal') echo "class='current'" ?>>
						<a href="?mod=data_teknikal"><i class="glyphicon glyphicon-record"></i>Laporan Teknikal</a>
					</li>
					<li <?php if($page=='mod=data_marketing') echo "class='current'" ?>>
						<a href="?mod=data_marketing"><i class="glyphicon glyphicon-record"></i>Laporan Marketing</a>
					</li>
                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-record"></i> Account
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
		                    <li <?php if($page=='mod=data_admin') echo "class='current'" ?>>
								<a href="?mod=data_admin"><i class="glyphicon glyphicon-chevron-right"></i> Profile</a>
							</li>   
							<li><a href="logout.php"><i class="glyphicon glyphicon-chevron-right"></i> Logout</a></li>            
						</ul>
					</li>
                </ul>
             </div>
		  </div>
		  <div class="col-md-10">
			<div class="content-box-large">
				<?php 
					include '../../config/connectdb.php';
					
					if($_GET[mod]=='data_adm'){
						include "../mod_teknikal/show_admin.php";
					}
					elseif($_GET[mod]=='data_marketing'){
						include "../mod_teknikal/show_marketing.php";
					}
					elseif($_GET[mod]=='logout') {
						?>
						<a href='logout.php' onclick="return confirm('Anda Ingin Keluar dari Halaman Ini?')"> Logout </a>
						<?php
					}
				?>
			</div>
		  </div>
		</div>
    </div>
    <footer style="background-color: #5F9EAD">
         <div class="container">
            <div class="copy text-center">
               Copyright&copy; <span id="copyright">...</span> <a href='#'>PT Matahari Cipta. Developed by Lunata IT & Consultant</a>
            </div>        
         </div>
      </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../js/custom.js"></script>

  </body>
</html>
<?php
}
?>