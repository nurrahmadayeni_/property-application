<link rel="stylesheet" href="../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css"/>
<h3 align=center>DETAIL KAVLING PERUMAHAN</h3>
<table id="order" class="table table-bordered table-hover">
<thead>
	<tr>
		<th width="10%">No</th>
		<th width="10%">Tipe Kavling</th>  
		<th width="13%">Nomor Kavling</th>  
		<th width="15%">Luas Tanah</th>
		<th width="23%">Harga Tanah Per Kavling (Rp)</th>
		<th width="15%">Koordinat Mapping</th>
		<th width="20%">Aksi</th>
	</tr>
 </thead>
<tbody>

<?php
		error_reporting(0);
		
		$id= $_GET[id];

		$perumahan = $mysqli->query(
                "SELECT nama_perumahan 
                FROM data_perumahan
                WHERE id_perumahan = '$id'
                ")->fetch_object()->nama_perumahan;

		echo "<h4>Perumahan : ".$perumahan."</h4>";

		$sql = mysqli_query($mysqli,"SELECT dk.kav_id, tr.type_rumah, dk.no_kavling, tr.luas_tanah, tr.harga_kavling, dk.mapping, dp.id_perumahan from type_rumah tr, data_kavling dk, data_perumahan dp where tr.id_type=dk.id_type and dk.id_perumahan=dp.id_perumahan and dp.id_perumahan='$id'");
		
		$no = 1;
		while ($r = mysqli_fetch_array($sql)) {
			echo"
				<tr>	
					<td> $no </td>
					<td> $r[type_rumah] </td>
					<td> $r[no_kavling] </td>
					<td> $r[luas_tanah] </td>
					<td align='right'> ".number_format($r[harga_kavling],0,',','.') ."</td>
					<td> $r[mapping] </td>
					<td>
						<a href='#' class='edit-record' data-toggle='modal' data-id='$r[kav_id]' data-id2='$r[type_rumah]' data-id3='$id' title='edit'><button class='btn btn-primary btn-sm' alt='edit'>
						<span class='glyphicon glyphicon-pencil'></span></button></a>

						<a href='../action/adm/act_edit_teknikal.php?mod=adm&act=hapuskavling&idk=".$r[kav_id]."&idr=".$id."' title='hapus' onclick='return confirm('Anda ingin menghapus data ini?')> 
						<button class='btn btn-danger btn-sm' alt='Hapus'><span class='glyphicon glyphicon-trash'></span></button> </a>
					</td>
				</tr>
			";
			$no++;
		}
		?>
	</tbody>
	
</table>
<span class="container">
	   <a class="btn btn-primary " data-toggle="modal" href="#tambah_kavling"><span class="glyphicon glyphicon-plus"></span> Kavling</a>
	   <a class="btn btn-primary " data-toggle="modal" href="#tipe_kavling"><span class="glyphicon glyphicon-plus"></span> Tipe Kavling</a>
	   <a class="btn btn-primary " data-toggle="modal" href="#stipe_kavling"><span class="glyphicon glyphicon-eye-open"></span> Data Tipe Kavling</a>
</span>

<div id="tambah_kavling" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Tambah Kavling</h4>
			</div>
			<div class="modal-body">
                <form method="post" action='../action/adm/act_perumahan.php?mod=adm&act=tambahkavling' enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="nama-perumahan" class="control-label">Pilih Type Kavling : </label>
                    <input type="hidden" name='id_perumahan' value=<?= $_GET[id] ?>>
                    <select name="type_kavling" id="Perumahan" class="form-control">
			              <option value='' selected disabled='disabled'>- Pilih Type Kavling -</option>
			              <?php
			                  $getdata="SELECT * FROM type_rumah";
			                  $tampil=mysqli_query($mysqli,$getdata);
			                  while($r=mysqli_fetch_array($tampil)){
			                      echo "<option value=$r[id_type]>
			                      $r[type_rumah]</option>";
			                  }
			              ?>
			        </select>
                  </div>
                  <div class="form-group">
                    <label for="banyak_kavling" class="control-label">Pilih Banyak Kavling : </label>
                    <input class="form-control" type='number' id='jlh-input' name="banyak_input"/>
                  </div>
				  <div class="form-group">
				  	<table id='tabel-input'></table>
				  </div>
				<div class="modal-footer">
					<input type="Submit" class="btn btn-primary" value="Submit">
					<input type="reset" class="btn btn-default" value="Reset">
				</div>

                </form>
            </div>
        </div>
	</div>
</div>

<div id="tipe_kavling" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Tambah Tipe Kavling</h4>
			</div>
			<div class="modal-body">
                <form method="post" action='../action/adm/act_perumahan.php?mod=adm&act=tambahtipekavling' enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="nama-perumahan" class="control-label">Input Type Kavling : </label>
                    <input type="text" class="form-control" id="type_rumah" name="type_rumah" required=''>
                  </div>
                  <div class="form-group">
                    <label for="banyak_kavling" class="control-label">Harga
                     Kavling: </label>
                    <input type="text" class="form-control" id="hrga" name="harga_kavling" required=''>
					<script type="text/javascript">
						$(document).ready(function(){
							$('#hrga').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
						});
					</script>
                  </div>
                  <div class="form-group">
                    <label for="luas_tanah" class="control-label">Luas Tanah : </label>
                    <input type="text" class="form-control" id="luas_tanah" name="luas_tanah" required=''>
                  </div>
				<div class="modal-footer">
					<input type="Submit" class="btn btn-primary" value="Submit">
					<input type="reset" class="btn btn-default" value="Reset">
				</div>
                </form>
            </div>
        </div>
	</div>
</div>

<div id="stipe_kavling" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Data Tipe Kavling</h4>
			</div>
			<div class="modal-body">
                <table id="order" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th width="10%">No</th>
							<th width="10%">Tipe Kavling</th>  
							<th width="23%">Harga Tanah Per Kavling (Rp)</th>
							<th width="15%">Luas Tanah</th>
						</tr>
					 </thead>
					<tbody>

					<?php
							error_reporting(0);
							
							$sql = mysqli_query($mysqli,"SELECT *FROM type_rumah");
							
							$no = 1;
							while ($r = mysqli_fetch_array($sql)) {
								echo"
									<tr>	
										<td> $no </td>
										<td> $r[type_rumah] </td>
										<td align='right'> ".number_format($r[harga_kavling],0,',','.') ."</td>
										<td> $r[luas_tanah] </td>
									</tr>
								";
								$no++;
							}
							?>
						</tbody>
						
					</table>
					<script type="text/javascript">
					  $(function() {
							 $("#order").dataTable();
					</script>
            </div>
        </div>
	</div>
</div>
<?php
	echo "<div align='center'>";
	echo "<h3>Denah Lokasi Kavling</h3>";
	$foto_rumah = $mysqli->query(
			"SELECT foto_rumah FROM foto_perumahan
			WHERE id_perumahan = '$id' 
		")->fetch_object()->foto_rumah;
	if(isset($foto_rumah)){
?>

	
	<h4>Keterangan : </h4>

	<canvas id="ready" width="30" height="30"
	style="border:1px solid red">
	</canvas> Ready Sale

	<script>
		var canvas = document.getElementById("ready");
		var ctx = canvas.getContext("2d");
		ctx.fillStyle = "#CCCCCC";
		ctx.fillRect(0,0,150,75);
	</script>
	
	<canvas id="booking" width="30" height="30"
	style="border:0px solid ">
	</canvas> Booking

	<script>
		var canvas = document.getElementById("booking");
		var ctx = canvas.getContext("2d");
		ctx.fillStyle = "#CCCCCC";
		ctx.fillRect(0,0,150,75);
	</script>

	<canvas id="sold" width="30" height="30"
	style="border:0px solid ">
	</canvas> Sold Out

	<script>
		var canvas = document.getElementById("sold");
		var ctx = canvas.getContext("2d");
		ctx.fillStyle = "#FF4C4C";
		ctx.fillRect(0,0,150,75);
	</script>


</div>

<script type="text/javascript" src="http://davidlynch.org/projects/maphilight/jquery.maphilight.js"></script>
<div align="center">
	<img id='ImageMap1' src='gbr_perumahan/<?= $foto_rumah?> ' usemap="#ImageMapmapAreas" >

    <map id="ImageMapmapAreas" name="ImageMapmapAreas">
    	<?php
			$sql = mysqli_query($mysqli,"SELECT * from data_kavling where id_perumahan='$id' ORDER BY status");
			
			$no = 1;
			while ($r = mysqli_fetch_array($sql)) {

				$stas = $r['status'];

				if($stas==0){ // ready sale
					echo"
					<area shape='poly' coords='$r[mapping]' class='aToggle' data-placement='top' href='#' title='$r[no_kavling]' alt='37ee8d' >
					";
				}elseif($stas==1){ // booking
					echo"
					<area shape='poly' coords='$r[mapping]' class='tooltip' data-placement='top' href='#' title='$r[no_kavling]' alt='$r[no_kavling]' data-maphilight='{&quot;stroke&quot;:true}'>
					";
				}elseif($stas==2){ // sold out
					echo "
					<area shape='poly' coords='$r[mapping]' class='tooltip' data-placement='top' href='#' title='$r[no_kavling]' alt='$r[no_kavling]' data-maphilight='{&quot;stroke&quot;:false,&quot;fillOpacity&quot;:0.7,&quot;fade&quot;:false,&quot;fillColor&quot;:&quot;ff0000&quot;}'>
					";
				}				
				$no++;
			}
		?>
    </map>
    </div>
   <?php
		} else {
			echo "Silahkan upload foto perumahan untuk pembuatan denah lokasi";
   }
   ?>

   <p><a href="#" class="aToggle" alt="37ee8d">Go GREEN</a></p> 


<script type="text/javascript">
	$('.aToggle').click(function (e) { 
		var data = $('#ImageMap1').mouseout().data('maphilight') || {}; 
		data.fillColor = $(this).attr('alt'); $('#ImageMap1').data('maphilight', data).trigger('alwaysOn.maphilight'); 
	}); 
	jQuery(function(){
		jQuery('#ImageMap1').maphilight();

		$('[data-toggle="tooltip"]').tooltip(); 

		$('.mapHiLight').maphilight({ stroke: false, fillColor: '009DDF', fillOpacity: 1 });

	});
</script>
<!-- Modal for edit kavling-->
<div id="editkavling" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content modal-lg">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Edit Kavling</h4>
			</div>
			<div class="modal-body">
				
			</div>
		</div>
	</div>
</div>

<script>
$(function(){
	$(document).on('click','.edit-record',function(e){
		e.preventDefault();
		$("#editkavling").modal('show');
		$.post('../action/adm/edit_kavling.php',
			{id:$(this).attr('data-id'), id2: $(this).attr('data-id2'), id3 :$(this).attr('data-id3')},
			function(html){
				$(".modal-body").html(html);
			}   
		);
		$('#editkavling').on('hidden.bs.modal', function () {
            location.reload();
        })
	});
});
</script>
<script type="text/javascript">
  $(function() {
		 $("#order").dataTable();

		 $('#jlh-input').change(function(){
			if($('#jlh-input').val() == ''){
				alert("Tidak boleh kosong!");
			}else{
				for(i = 0; i < $('#jlh-input').val(); i++){
					var tr = $('<tr/>'),
						td_input = $('<td/>');

					tr.append($('<td/>', {
						text: 'Input Nama kavling :'
					}));
					
					td_input.append($('<input/>', {
						type: 'text',
						name: 'no_kavling[' + i + ']',
						class : 'form-control'
					}));
					tr.append(td_input);
					$('#tabel-input').append(tr);
				}
			}
		});
  });
</script>