<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> DAFTAR GUDANG </h1>";
        echo "
            <table id='gudang' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th width='20%'>Nama Gudang</th>
                    <th width='15%'>Penanggung Jawab</th>
                    <th width='15%'>Alamat</th>
                    <th width='15%'>Nomor Telephone</th>
                    <th width='15%'>Aksi</th>
                </tr>
            </thead>
            <tbody>
        ";
       
             
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,'SELECT * FROM data_gudang');
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
            $id = $r['id_gudang'];
        ?>

        <tr align='left'>
            <td>
                <?php echo  $r['nama_gudang']; ?>
            </td>
            <td>
                <?php echo  $r['penanggungjwb_gudang']; ?>
            </td>
            <td>
                <?php echo  $r['alamat_gudang']; ?>
            </td>
			<td>
                <?php echo  $r['tlp_gudang']; ?>
            </td>
			<td>
                <a href="#" title="edit" class="edit-gudang" data-id="<?php echo $id; ?>"><button class='btn btn-primary btn-sm' data-toggle="modal" alt="edit">
				<span class="glyphicon glyphicon-pencil"></span></button></a>
                <a href="../action/adm/act_edit_gd.php?mod=data_gudang&act=hapus&id=<?php echo $id;?>" title="hapus" onclick="return confirm('Anda ingin menghapus data ini?')"> 
				<button class='btn btn-danger btn-sm' alt="Hapus"><span class="glyphicon glyphicon-trash"></span></button> </a>
            </td>
        </tr>
        <?php
            $no++;
        }
        ?>

        </tbody>

    </table>  
    <span class="container">
	   <a class="btn btn-primary btn-md" data-toggle="modal" href="#myModal"><span class="glyphicon glyphicon-plus"></span> Gudang</a>
  	</span>
	
	<!-- Modal for add gudang-->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Gudang</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action='../action/adm/act_gudang.php?mod=data_gudang&act=tambah' enctype="multipart/form-data">
                      <div class="form-group">
                        <label for="nama-gudang" class="control-label">Nama Gudang : </label>
                        <input type="text" class="form-control" id="nama-gudang" name="nama_gudang">
                      </div>
                      <div class="form-group">
                        <label for="penanggung-jawab" class="control-label">Penanggung Jawab :</label>
                        <input type="text" class="form-control" id="penanggung-jawab" name="penanggungjwb_gudang">
                      </div>
                      <div class="form-group">
                        <label for="alamat" class="control-label">Alamat :</label>
                        <textarea class="form-control" id="alamat" name="alamat_gudang"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="tlp-gudang" class="control-label">Nomor Telepon :</label>
                        <input type="text" class="form-control" id="tlp-gudang" name="tlp_gudang">
                      </div>
                      <div class="modal-footer">
                        <input type="Submit" class="btn btn-primary" value="Submit">
                        <input type="reset" class="btn btn-default" value="Reset">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
	
	<!-- Modal for Edit Gudang-->
    <div id="edit_gudang" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Gudang</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
	
  
    <script type="text/javascript">
    $(function() {
        $("#gudang").dataTable();
    });
    </script>
	
	<script>
        $(function(){
            $(document).on('click','.edit-gudang',function(e){
                e.preventDefault();
                $("#edit_gudang").modal('show');
                $.post('../action/adm/edit_gudang.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#edit_gudang').on('hidden.bs.modal', function () {
                 location.reload();
                })
            });
        });
    </script>

    <?php
        break;
	}
	?>
	
<script type="text/javascript">
    $(function() {
        $("#Dgudang").DataTable();
    });
    </script>
