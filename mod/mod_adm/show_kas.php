<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> DATA KAS (PEMASUKAN)</h1>";
        echo "
            <table id='kas' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th width='5%'>No</th>
                    <th width='10%'>Tanggal Masuk</th>
                    <th width='10%'>Asal Cek/Bg</th>
                    <th width='15%'>No.Cek</th>
                    <th width='15%'>Keterangan</th>
                    <th width='15%'><center>Debit (Rp)</center></th>
                </tr>
            </thead>
            <tbody>
        ";    

        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,"SELECT g.jenis_giro,g.no_giro,k.giro_asal,k.tgl_sekarang,k.tgl_cek,k.no_cek,k.nominal,k.keterangan,j.jenis_keuangan,j.id_jeniskeuangan 
                    FROM giro g, data_keuangan k,jenis_bayar j
                    WHERE g.no_giro=k.no_giro AND k.id_jeniskeuangan=j.id_jeniskeuangan 
                    AND g.jenis_giro='ADM' ");
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
            $id=$r[id_kenuangan];
            $rp=number_format($r[nominal],0,',','.');
            $nominal=$r[nominal];
            $saldo+=$nominal;
        ?>
        <tr align='left'>
            <td><?php echo $no;?></td>
            <td><?php echo  $r['tgl_sekarang']; ?></td>
			<td><?php echo  $r['giro_asal']; ?></td>
            <td><?php echo  $r['no_cek']; ?></td>
            <td><?php echo  $r['keterangan']; ?></td>
            <td align='right'><?php echo $rp; ?></td>
        </tr>
    
        <?php    
        $no++;
        }
        ?>

        </tbody>
    </table>  
    <span class="container">
       <a class="btn btn-primary btn-md" data-toggle="modal" href="index.php?mod=laporan_kas"><span class="glyphicon glyphicon-book"></span> Laporan Kas</a>
    </span>

    <div id="Mkas" class="modal fade bs-example-modal-lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Input Data</h4>
                </div>
                <div class="modal-body">
                    <?php include "../action/adm/input_data.php"; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">OK</
                </div>
                </div>
            </div>
        </div>

    <!-- Modal for Edit Giro-->
    <div id="edit_giro" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Giro</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
    
  
    <script type="text/javascript">
    $(function() {
        $("#kas").dataTable();
    });
    </script>

      <?php
        break;
    }
    ?>
    
    <script>
        $(function(){
            $(document).on('click','.edit-giro',function(e){
                e.preventDefault();
                $("#edit_giro").modal('show');
                $.post('../action/adm/edit_giro.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#edit_giro').on('hidden.bs.modal', function () {
                 location.reload();
                })
            });
        });
    </script>

<script>
    function print_d(){
        window.open("../action/adm/printKas.php?id=<?php echo $id;?>","_blank");
    }
</script>