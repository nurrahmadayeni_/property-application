<link rel="stylesheet" href="../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css"/>
<h3 align=center> DETAIL ORDER </h3>
<table id="order" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th width="10%">No</th>
			<th width="10%">ID Order</th>  
			<th width="15%">Tanggal Order</th>
            <th width="15%">Status Pembayaran</th>
            <th width="15%">Pembayaran</th>
        </tr>
     </thead>
            <tbody>
				<?php
                    error_reporting(0);
					
                    //Data mentah yang ditampilkan ke tabel   
                    $sql = mysqli_query($mysqli,"SELECT o.id_order as id_order,o.tgl_order,o.status_pembayaran as status_pembayaran FROM detail_order d,material m,order_material o where o.id_order=d.id_order and m.id_material=d.id_material and o.id_supplier='$_GET[ids]' group by d.id_order ");
                    $no = 1;
                    while ($r = mysqli_fetch_array($sql)) {
                    $id = $r['id_order'];
                    
					if($r['status_pembayaran']=='lunas')
					echo"
                    <tr align='left'>
                        <td>$no</td>
						<td><a href='#' class='edit-record' data-id='$id'>$id</td>
                        <td>$r[tgl_order]</td>
						<td>$r[status_pembayaran]</td>
                        <td>
                        	
                        </td>
                    </tr>";
					else
					echo"
                    <tr align='left'>
                        <td>$no</td>
						<td><a href='#' class='edit-record' data-id='$id'>$id</td>
                        <td>$r[tgl_order]</td>
						<td>belum lunas</td>
                        <td>
                        	<a href='index.php?mod=add_bayar&id=$r[id_order]' class='btn btn-primary btn-sm'> Bayar </a>
                        </td>
                    </tr>";
					
                    $no++;
                   }
                    ?>
                </tbody>
            </table>
		<table><tr>
		<td><a href='index.php?mod=add_order&ids=<?php echo $_GET[ids]; ?>' class='btn btn-primary' ><span class="glyphicon glyphicon-plus"></span>  Order</a>
		</td>
		<td>&nbsp; &nbsp;</td>
		<td>
		<a href='index.php?mod=supplier' class='btn btn-primary'>Back</a>
		</td>
		</table>
		<div class="modal fade" id="detail_ord" tabindex="-1" role="dialog" aria-labelledby="detail_suppLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Detail Order</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
		</div>
		
		<script>
        $(function(){
            $(document).on('click','.edit-record',function(e){
                e.preventDefault();
                $("#detail_ord").modal('show');
                $.post('../action/adm/detail_bon.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#detail_ord').on('hidden.bs.modal', function () {
                 location.reload();
                })
            });
        });
    </script>

    <script src="../assets/js/jquery-1.11.0.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>        
	<script src="../assets/datatables/jquery.dataTables.js"></script>
	<script src="../assets/datatables/dataTables.bootstrap.js"></script>
    <script type="text/javascript">
      $(function() {
             $("#order").dataTable();
      });
    </script>