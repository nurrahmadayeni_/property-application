<link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<link href="../../images/pavicon.png" rel="icon" type="image/x-icon" />

<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
if(!isset($_SESSION["username"])){
	?>
    <!--Modal Session -->
		<script type="text/javascript">
		    $(document).ready(function(){
		        $("#login").modal('show');
		    });
		</script>

		<div id="login" class="modal fade">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header alert-warning">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                <h4 class="modal-title">WARNING</h4>
		            </div>
		            <div class="modal-body">
					  Please Login First
					</div>
		            <div class = "modal-footer">
			            <a href="../../index.php"><button type = "button" class = "btn btn-warning" >OK</button> </a>
			         </div>
		        </div>
		    </div>
		</div>
    <?php
}else{

?>
<!DOCTYPE html>
<head>
	<title>ADM</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- styles -->
	<link href="../../css/styles.css" rel="stylesheet">
	<link href="../../css/main.css" rel="stylesheet">

	<link rel="stylesheet" href="../assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css"/>
	<link rel="stylesheet" href="../assets/css/bootstrap-theme.min.css">

	<script src="../assets/js/jquery-1.11.0.js"></script>
	<script src="../assets/js/bootstrap.min.js"></script>
	<script src="../assets/js/jquery.min.js"></script>
	<script src="../assets/js/jquery.maskMoney.min.js"></script>
	<script src="../assets/datatables/jquery.dataTables.js"></script>
	<script src="../assets/datatables/dataTables.bootstrap.js"></script>

	<script>
	//copyright
	function copyDate() {
		var cpyrt = document.getElementById("copyright")
		if (cpyrt) {
		cpyrt.firstChild.nodeValue = (new Date()).getFullYear();
		}}
	window.onload = copyDate;
	</script>
		
</head>
<body>
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-5">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="index.html">Administrasi</a></h1>
	              </div>
	           </div>
	           <div class="col-md-5">
	              <div class="row">
	                <div class="col-lg-12">
	                  <div class="input-group form"></div>
	                </div>
	              </div>
	           </div>
	           <div class="col-md-2">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <?php echo"$_SESSION[username]"; ?> <b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="?mod=data_admin">Profile</a></li>
	                          <li><a data-toggle = "modal" data-target = "#logout"> Logout </a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	          	</div>
	        </div>
	     </div>
	</div>

    <div class="page-content">
    	<div class="row">
		  <div class="col-md-2">
		  	<div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
					<?php
				        $page = $_SERVER['QUERY_STRING'];
				    ?>
					<li <?php if($page=='mod=supplier') echo "class='current'" ?> > 
						<a href="?mod=supplier"><i class="glyphicon glyphicon-record"></i> Supplier</a>
					</li>
					<li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-record"></i> Keuangan
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
		                    <li <?php if($page=='mod=keuangan_giro') echo "class='current'" ?>>
								<a href="?mod=keuangan_giro"><i class="glyphicon glyphicon-chevron-right"></i> Giro</a>
							</li>   
							<li <?php if($page=='mod=keuangan_alokasi') echo "class='current'" ?>>
								<a href="?mod=keuangan_alokasi"><i class="glyphicon glyphicon-chevron-right"></i> Alokasi</a>
							</li>
							<li <?php if($page=='mod=keuangan_kas') echo "class='current'" ?>>
								<a href="?mod=keuangan_kas"><i class="glyphicon glyphicon-chevron-right"></i> Kas</a>
							</li> 
							<li <?php if($page=='mod=keuangan_kredit') echo "class='current'" ?>>
								<a href="?mod=keuangan_kredit"><i class="glyphicon glyphicon-chevron-right"></i> Kredit</a>
							</li>              
						</ul>
					</li>
					<li class="submenu">
                         <a href="?mod=data_gudang">
                            <i class="glyphicon glyphicon-record"></i> Gudang
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li <?php if($page=='mod=data_gudang') echo "class='current'" ?>>
								<a href="?mod=data_gudang"><i class="glyphicon glyphicon-chevron-right"></i> Data Gudang</a></li>
							<li <?php if($page=='mod=data_material') echo "class='current'" ?>>
								<a href="?mod=data_material"><i class="glyphicon glyphicon-chevron-right"></i> Data Material</a></li>
                            <li <?php if($page=='mod=data_aset') echo "class='current'" ?>>
								<a href="?mod=data_aset"><i class="glyphicon glyphicon-chevron-right"></i> Data Aset</a></li>
                            <li <?php if($page=='mod=data_pengeluaran') echo "class='current'" ?>>
								<a href="?mod=data_pengeluaran"><i class="glyphicon glyphicon-chevron-right"></i> Data Pengeluaran</a></li>
                            <li <?php if($page=='mod=data_retur') echo "class='current'" ?>>
								<a href="?mod=data_retur"><i class="glyphicon glyphicon-chevron-right"></i> Laporan Retur Barang</a></li>
                        </ul>
                    </li>                    
					<li <?php if($page=='mod=inventaris') echo "class='current'" ?>>
						<a href="?mod=inventaris"><i class="glyphicon glyphicon-record"></i> Inventaris</a>
					</li>
					<li <?php if($page=='mod=data_perumahan') echo "class='current'" ?> >
						<a href="?mod=data_perumahan"> <i class="glyphicon glyphicon-record"></i> Data Perumahan</a>
					</li>
					<li <?php if($page=='mod=pegawai') echo "class='current'" ?>>
						<a href="?mod=pegawai"><i class="glyphicon glyphicon-record"></i> Data Pegawai</a></li>
					<li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-record"></i> Account
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
		                    <li <?php if($page=='mod=data_admin') echo "class='current'" ?>>
								<a href="?mod=data_admin"><i class="glyphicon glyphicon-chevron-right"></i> Profile</a>
							</li>   
							<li><a href="#" data-toggle = "modal" data-target = "#logout"><i class="glyphicon glyphicon-chevron-right"></i> Logout</a></li>            
						</ul>
					</li>

                </ul>
             </div>
		  </div>
		  <div class="col-md-10">
			<div class="content-box-large">
				<?php
					include '../../config/connectdb.php';

					if($_GET[mod]=='supplier'){
						include "show_order.php"; 
					}
					elseif($_GET[mod]=='data_admin'){
						include "profile.php";
					}
					elseif($_GET[mod]=='tambah_order'){
						include "tabelbon.php";
					}
					else if($_GET['mod']=='add_order'){
						include "tambah_order.php";
					}
					else if($_GET['mod']=='aksi_saveorder'){
						include "aksi_addorder.php";
					}
					else if($_GET['mod']=='add_bayar'){
						include "../action/adm/pembayaran.php";
					}
					else if($_GET['mod']=='add_newbon'){
						include "../action/adm/newbon.php";
					}
					else if($_GET['mod']=='data_pengeluaran'){
						include "../action/adm/tablepengeluaran.php";
					}
					else if($_GET['mod']=='data_retur'){
						include "../action/adm/tableretur.php";
					}
					else if($_GET['mod']=='pengeluaran'){
						include "../action/adm/pengeluaran.php";
					}
					else if($_GET['mod']=='retur'){
						include "../action/adm/retur.php";
					}
					else if ($_GET[mod]=='keuangan_giro') {
						include "show_giro.php";
					}
					else if($_GET['mod']=='new_giro'){
						include "../action/adm/class_paging.php";
						include "../action/adm/newgiro.php";
					}
					else if ($_GET[mod]=='laporan_giro') {
						include "../action/adm/laporan_giro.php";
					}
					else if ($_GET[mod]=='keuangan_alokasi') {
						include "show_alokasi.php";
					}
					else if ($_GET[mod]=='laporan_alokasi') {
						include "../action/adm/laporan_alokasi.php";
					}
					else if ($_GET[mod]=='keuangan_kas') {
						include "show_kas.php";
					}
					else if ($_GET[mod]=='laporan_kas') {
						include "../action/adm/laporan_kas.php";
					}
					else if ($_GET[mod]=='keuangan_kredit') {
						include "show_kredit.php";
					}
					else if ($_GET[mod]=='laporan_kredit') {
						include "../action/adm/laporan_kredit.php";
					}
					else if ($_GET[mod]=='data_gudang') {
						include "show_gudang.php";
					}
					else if ($_GET[mod]=='data_aset') {
						include "show_aset.php";
					}
					else if ($_GET[mod]=='data_material') {
						include "show_katmaterial.php";
					}
					else if ($_GET[mod]=='material') {
						include "material.php";
					}
					else if($_GET[mod]== 'data_perumahan'){
						include "show_perumahan.php";
					}
					else if ($_GET[mod]=='pegawai') {
						include "pegawai.php"; 
					}
					else if($_GET[mod]=='detail_kavling'){
						include "showdetail_kavling.php";
					}
					else if ($_GET[mod]=='inventaris') {
						include "show_inventaris.php"; 
					} elseif ($_GET[mod]=='logout') {
						?>
						<a href="#" data-toggle = "modal" data-target = "#logout"> Logout</a>
						<?php
					}
					
				?>
			</div>
		  </div>
		</div>
    </div>
    <!-- Modal LOGOUT-->
		<div class = "modal fade" id = "logout" tabindex = "-1" role = "dialog" 
		   aria-labelledby = "myModalLabel" aria-hidden = "true">
		   
		   <div class = "modal-dialog">
		      <div class = "modal-content">
		         
		         <div class = "modal-header">
		            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
		                  &times;
		            </button>
		            
		            <h4 class = "modal-title" id = "myModalLabel">
		               CONFIRM LOGOUT
		            </h4>
		         </div>
		         
		         <div class = "modal-body">
		            Are you sure you want to logout?
		         </div>
		         
		         <div class = "modal-footer">
		            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
		               NO
		            </button>
		            
		            <a href="logout.php"><button type = "button" class = "btn btn-primary" >YES</button> </a>
		         </div>
		         
		      </div><!-- /.modal-content -->
		   </div><!-- /.modal-dialog -->
		  
		</div><!-- /.modal -->
    <footer>
         <div class="container">
            <div class="copy text-center">
               Copyright&copy; <span id="copyright">...</span> <a href='#'>PT Matahari Cipta | Developed by Lunata IT & Consultant</a>
            </div>         
         </div>
      </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../../js/custom.js"></script>

</body>
</html>
<?php
}
?>