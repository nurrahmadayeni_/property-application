<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> DAFTAR MATERIAL </h1>";
        echo "
            <table id='material' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th width='5%'>No</th>
                    <th width='15%'>Nama Material</th>
                    <th width='15%'>Satuan Material</th>
                    <th width='22%'>Spesifikasi</th>
                    <th width='10%'>Foto</th>
                    <th width='18%'>Harga Material (Rp)</th>
                    <th width='10%'>Aksi</th>
                </tr>
            </thead>
            <tbody>
        ";
       
             
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,"SELECT * FROM material WHERE id_kategorimaterial='$_GET[ids]'");
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
            $id = $r['id_material'];
            $idk = $r['id_kategorimaterial'];
        ?>

        <tr align='left'>
			<td align="center"> <?php echo  $no;?> </td>
			<td>
                <b><a href="#" class="detail-record" data-id="<?php echo $id ?>">
                <?php echo  $r['nama_material']; ?></a></b>
            </td>
            <td>
                <?php echo  $r['satuan_material']; ?>
            </td>
            <td>
                <?php echo  $r['spesifikasi']; ?>
            </td>
			<td>
                <img src="gbr_material/<?php echo  $r['gambar_material']; ?>" width='70%'>
            </td>
			<td align='right'>
				<?php 
                    $rupiah=number_format($r['harga_material'],0,',','.'); 
                    echo $rupiah; 
                ?>
            </td>
			<td>
                <a href="#edit_material" title="edit" data-toggle="modal" class="edit-record" data-id="<?php echo $id; ?>"><button class='btn btn-primary btn-sm' alt="edit">
				<span class="glyphicon glyphicon-pencil"></span></button></a>
                <a href="../action/adm/act_material.php?mod=material&act=hapus&id=<?php echo $id;?>" title="hapus" onclick="return confirm('Anda ingin menghapus data ini?')"> 
				<button class='btn btn-danger btn-sm' alt="Hapus"><span class="glyphicon glyphicon-trash"></span></button> </a>
            </td>
        </tr>
        <?php
            $no++;
        }
		}
        ?>

        </tbody>

    </table>  
    <span class="container">
	   <a class="btn btn-primary btn-md" data-toggle="modal" href="#myModal"><span class="glyphicon glyphicon-plus"></span> Material</a>
	</span>
	
	<!-- Modal for add gudang-->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Material</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action='../action/adm/act_material.php?mod=material&act=tambah'  enctype="multipart/form-data">
                      <div class="form-group">
                        <label for="id-katmaterial" class="control-label">Kategori Material : </label>
                        <select name="kategori_material" class="form-control">
                                <?php
                                    $getdata="SELECT * FROM kategori_material WHERE id_kategorimaterial='$_GET[ids]'";
                                    $tampil=mysqli_query($mysqli,$getdata);
                                    while($r=mysqli_fetch_array($tampil)){
                                        echo "<option value=$r[id_kategorimaterial]>
                                        $r[kategori_material]</option>";
                                    }
                                ?>
                          </select>
					  </div>
					  <div class="form-group">
                        <label for="nama-material" class="control-label">Nama Material : </label>
                        <input type="text" class="form-control" id="nama-material" name="nama_material">
                      </div>
                      <div class="form-group">
                        <label for="satuan-material" class="control-label">Satuan Material :</label>
                        <input type="text" class="form-control" id="satuan-material" name="satuan_material">
                      </div>
                      <div class="form-group">
                        <label for="spesifikasi-mat" class="control-label">Spesifikasi :</label>
                        <textarea class="form-control" id="spesifikasi-mat" name="spesifikasi"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="gambar_material" class="control-label">Gambar Material :</label>
                        <input type="file" class="form-control" id="gambar_material" name="gambar_material" required>
                      </div>
                      <div class="form-group">
                        <label for="hrg" class="control-label">Harga Material :</label>
                        <input type="text" class="form-control" id="harga_material" name="harga_material">
						<script type="text/javascript">
                                $(document).ready(function(){
                                    $('#harga_material').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
                                });
                            </script>
                      </div>
                      <div class="modal-footer">
                        <input type="Submit" class="btn btn-primary" value="Submit" name="submit">
                        <input type="reset" class="btn btn-default" value="Reset">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
	
	<!-- Modal for Edit Gudang-->
    <div id="edit_material" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Matrial</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for detail supplier-->
    <div class="modal fade" id="detail_material" tabindex="-1" role="dialog" aria-labelledby="detail_material" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Detail Material</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
  
    <script type="text/javascript">
    $(function() {
        $("#material").dataTable();
		$('#harga_material').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
    });
    </script>
	
	<script>
        $(function(){
            $(document).on('click','.edit-record',function(e){
                e.preventDefault();
                $("#edit_material").modal('show');
                $.post('../action/adm/edit_material.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#edit_material').on('hidden.bs.modal', function () {
                 location.reload();
                })
            });
        });
    </script>

    <script>
        $(function(){
            $(document).on('click','.detail-record',function(e){
                e.preventDefault();
                $("#detail_material").modal('show');
                $.post('../action/adm/detail_material.php',
                    {ids:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#detail_material').on('hidden.bs.modal', function () {
                 location.reload();
                })
            });
        });
    </script>
	
	<script type="text/javascript">
    $(function() {
        $("#Dmaterial").DataTable();
    });
    </script>
