<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> DATA KREDIT (PENGELUARAN)</h1>";
        echo "
            <table id='kredit' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th width='5%'>No</th>
                    <th width='15%'>Tanggal Keluar</th>
                    <th width='15%'>Tujuan Cek/Bg</th>
                    <th width='15%'>No.Cek</th>
                    <th width='20%'>Keterangan</th>
                    <th width='20%'><center>Kredit (Rp)</center></th>
                    <th width='10%'>Aksi</th>
                </tr>
            </thead>
            <tbody>
        ";
       
             
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,"SELECT g.jenis_giro,g.no_giro,k.tgl_sekarang,k.id_jeniskeuangan,k.nominal,k.keterangan,k.id_keuangan,jb.jenis_keuangan
                        FROM data_keuangan k, giro g,jenis_bayar jb
                        WHERE k.no_giro=g.no_giro  and jb.id_jeniskeuangan=k.id_jeniskeuangan
                        and k.giro_asal='ADM' AND k.id_jeniskeuangan='2'");
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
            $id=$r[id_keuangan];
            $row1=number_format($r[nominal],0,',','.');
        ?>

        <tr align='left'>
            <td><?php echo $no;?></td>
            <td><?php echo  $r['tgl_sekarang']; ?></td>
            <td><?php echo  $r['jenis_giro']; ?></td>
            <td><?php echo  $r['no_cek']; ?></td>
            <td><?php echo  $r['keterangan']; ?></td>
            <td align='right'><?php echo  $row1; ?></td>
            <td align='center'>
                <a href="#" title="edit" class="edit-nominal" data-toggle="modal" data-id="<?php echo $id ; ?>"><button class='btn btn-primary btn-sm' alt="edit">
                <span class="glyphicon glyphicon-pencil"></span></button></a>
            </td>
        </tr>
        <?php
            $no++;
        }
        ?>

        </tbody>
    </table>  

    <span class="container">
      <a class="btn btn-primary btn-md" data-toggle="modal" href="#myModal"><span class="glyphicon glyphicon-pencil"></span> Input Data</a>
     <a class="btn btn-primary btn-md" data-toggle="modal" href="index.php?mod=laporan_kredit"><span class="glyphicon glyphicon-book"></span> Laporan Kas</a>
    </span>
    
    <!-- Modal for add giro-->
    <div id="myModal" class="modal fade bs-example-modal-lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Input Data</h4>
                </div>
                <div class="modal-body">
                    <?php include "../action/adm/input_datakredit.php"; ?>
                </div>
                
                </div>
            </div>
        </div>

    <!-- Modal for Edit Giro-->
    <div id="edit_nominal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Kredit ADM</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
    $(function() {
        $("#kredit").dataTable();
    });
    </script>

      <?php
        break;
    }
    ?>
    
    <script>
        $(function(){
            $(document).on('click','.edit-nominal',function(e){
                e.preventDefault();
                $("#edit_nominal").modal('show');
                $.post('../action/adm/edit_kredit.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#edit_nominal').on('hidden.bs.modal', function () {
                 location.reload();
                })
            });
        });
    </script>

<script>
    function print_d(){
        <?php
        
        echo "window.open('../action/adm/printLaporankredit.php','_blank');";
        ?>
    }
</script>
