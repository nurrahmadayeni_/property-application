<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> DAFTAR BON </h1>";
        echo "
            <table id='order' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th width='10%'>ID Order</th>
                    <th width='20%'>Nama Perusahaan</th>
                    <th width='15%'>Total Orderan</th>
                    <th width='15%'>Sisa Orderan</th>
					<th width='15%'>Total Harga (Rp)</th>
                    <th width='15%'>Detail Order</th>
                </tr>
            </thead>
            <tbody>
        ";
       
             
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,'SELECT * FROM bon_2');
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
            $id = $r['id_supplier'];
			if($r['sisa']==NULL){
        ?>
        <tr>
            <td align='center'>
                <?php echo  $no;?>
            </td>
            <td>
            <a href="#" class="edit-record" data-id="<?php echo $id; ?>">
                <?php echo  $r['nama_supplier']; ?>
            </a>
            </td>
            <td align='center'>
                <?php echo  $r['total_order']; ?>
            </td>
            <td align='center'>
                <?php echo  "0"; ?>
            </td>
			<td align='right'>
               
			   <?php 
                    $rupiah=number_format($r['total_harga'],0,',','.'); 
                    echo $rupiah; 
                ?>
			   
            </td>
            <td align='center'>
                <a href="?mod=tambah_order&ids=<?php echo $r['id_supplier']; ?>"> <button class='btn btn-primary btn-sm'>Detail</button> </a>
            </td>
        </tr>
			<?php
			}else{
			?>
			<tr>
            <td align='center'>
                <?php echo  $no;?>
            </td>
            <td>
            <a href="#" class="edit-record" data-id="<?php echo $id; ?>">
                <?php echo  $r['nama_supplier']; ?>
            </a>
            </td>
            <td align='center'>
                <?php echo  $r['total_order']; ?>
            </td>
            <td align='center'>
               <?php echo  $r['sisa']; ?>
            </td>
			<td align='right'>
               
			   <?php 
                    $rupiah=number_format($r['total_harga'],0,',','.'); 
                    echo $rupiah; 
                ?>
			   
            </td>
            <td align='center'>
                <a href="?mod=tambah_order&ids=<?php echo $r['id_supplier']; ?>"> <button class='btn btn-primary btn-sm'>Detail</button> </a>
            </td>
        </tr>
		
		<?php
		}
            $no++;
        }
        ?>

        </tbody>

    </table>  
    <span class="container">
       <a class="btn btn-primary " data-toggle="modal" href="#myModal"><span class="glyphicon glyphicon-plus"></span> Supplier</a>
		<a class="btn btn-primary " data-toggle="modal" href="#daftar_supplier">Daftar Supplier</a>
		<a class="btn btn-primary " data-toggle="modal" href="index.php?mod=add_newbon"><span class="glyphicon glyphicon-plus"></span> Bon Baru</a>
    </span>

    <!-- Modal for add supplier-->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Supplier</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action='../action/adm/act_supplier.php?mod=supplier&act=tambah'>
                      <div class="form-group">
                        <label for="nama-perusahaan" class="control-label">Nama Perusahaan : </label>
                        <input type="text" class="form-control" id="nama-perusahaan" name="nama_perusahaan">
                      </div>
                      <div class="form-group">
                        <label for="nama-pemilik" class="control-label">Nama Pemilik :</label>
                        <input type="text" class="form-control" id="nama-pemilik" name="nama_pemilik">
                      </div>
                      <div class="form-group">
                        <label for="alamat" class="control-label">Alamat :</label>
                        <textarea class="form-control" id="alamat" name="alamat"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="norek-supplier" class="control-label">Nomor Rekening :</label>
                        <input type="text" class="form-control" id="norek-supplier" name="norek_supplier">
                      </div>
                      <div class="form-group">
                        <label for="tlp-supplier" class="control-label">Nomor Telepon :</label>
                        <input type="text" class="form-control" id="tlp-supplier" name="tlp_supplier">
                      </div>
                      <div class="modal-footer">
                        <input type="Submit" class="btn btn-primary" value="Submit">
                        <input type="reset" class="btn btn-default" value="Reset">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for Daftar supplier-->
    <div id="daftar_supplier" class="modal fade bs-example-modal-lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Daftar Supplier</h4>
                </div>
                <div class="modal-body">
                    <?php include "../action/adm/daftar_supplier.php"; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">OK</
                </div>
                      
                </div>

            </div>
        </div>
    </div>

    <!-- Modal for detail supplier-->
    <div class="modal fade" id="detail_supp" tabindex="-1" role="dialog" aria-labelledby="detail_suppLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Detail Supplier</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

    </div>
    
    <script type="text/javascript">
    $(function() {
        $("#order").dataTable();
    });
    </script>

    <script>
        $(function(){
            $(document).on('click','.edit-record',function(e){
                e.preventDefault();
                $("#detail_supp").modal('show');
                $.post('../action/adm/detail_supplier.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#detail_supp').on('hidden.bs.modal', function () {
                    location.reload();
                })
            });
        });
    </script>

    <?php
        break;

}
?>
<script type="text/javascript">
    $(function() {
        $("#Dsupllier").DataTable();
    });
    </script>
