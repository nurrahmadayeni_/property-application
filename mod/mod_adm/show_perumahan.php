<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> DATA PERUMAHAN </h3>";
        echo "
            <table id='perumahan' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th width='3%'>No</th>
                    <th width='15%'>Nama perumahan</th>
					<th width='35%'>Deskripsi Perumahan</th>
                    <th width='25%'>Spesifikasi Teknis</th>
					<th width='3%'>Jumlah Kavling</th>
                    <th width='10%'>Aksi</th>
					<th width='10%'>Gambar Perumahan</th>
					<th width='10%'>Detail Kavling</th>
                </tr>
            </thead>
            <tbody>
        ";
       
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,'SELECT * FROM data_perumahan');
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
		$id=$r['id_perumahan'];
			?>
			<tr align='left'>
            <td>
                <?php echo  $no;?>
            </td>
            <td>
				<?php echo  $r['nama_perumahan']; ?>
            </td>
            <td>
                <?php echo  $r['deskripsi']; ?>
            </td>
			<td>
                <?php echo  $r['spesifikasi_teknik']; ?>
            </td>
            <?php
                $j_kavling = $mysqli->query(
                "SELECT count(kav_id) as 'jumlah'
                FROM data_kavling
                WHERE id_perumahan = '$r[id_perumahan]'
                ")->fetch_object()->jumlah;
            ?>
			<td>
                <?php echo  $j_kavling; ?>
            </td>
            <td>
				<a href="#editrumah" class="edit-record" title="edit" data-toggle="modal" data-id="<?php echo $id; ?>"><button class='btn btn-primary btn-sm' alt="edit">
                <span class="glyphicon glyphicon-pencil"></span></button></a>

                <a href="../action/adm/aksi_bataldaftar.php?mod=adm&act=hapusperumahan&idr=<?php echo $r['id_perumahan'];?>" title="hapus" onclick="return confirm('Anda ingin menghapus data ini?')"> 
                <button class='btn btn-danger btn-sm' alt="Hapus"><span class="glyphicon glyphicon-trash"></span></button> </a>
			</td>
			<td>
				<a href="#showimage" class="edit-record3" data-toggle="modal" data-id="<?php echo $r['id_perumahan']; ?>"><button class='btn btn-primary btn-sm' alt="showimage">
                Show Image</button></a>
			</td>
			<td>
                <a href="?mod=detail_kavling&id=<?php echo $r['id_perumahan'];?>"> <button class='btn btn-primary btn-sm'>Detail</button> </a>
            </td>
        </tr>
		
		<?php
            $no++;
        }
        ?>

        </tbody>

    </table>  
    <span class="container">
	   <a class="btn btn-primary " data-toggle="modal" href="#tambahrumah"><span class="glyphicon glyphicon-plus"></span> Perumahan</a>
	</span>
	
	<!-- Modal for Show Image Perumahan-->
    <div id="showimage" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Gambar Perumahan</h4>
                </div>
                <div class="modal-body">
				
				</div>
				
            </div>
        </div>
    </div>
	
	<!-- Modal for Edit Perumahan-->
    <div id="editrumah" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Perumahan</h4>
                </div>
                <div class="modal-body">
                    
                </div>
            </div>
        </div>
    </div>
	
	<!-- Modal for Edit image Perumahan-->
    <div id="tambahimage" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Image Perumahan</h4>
                </div>
                <div class="modal-body">
                    
                </div>
            </div>
        </div>
    </div>
	
	 <!-- Modal for add perumahan-->
    <div id="tambahrumah" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Perumahan</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action='../action/adm/act_perumahan.php?mod=adm&act=tambahadm' enctype="multipart/form-data">
                      <div class="form-group">
                        <label for="nama-perumahan" class="control-label">Nama Perumahan : </label>
                        <input type="text" class="form-control" id="nama-perumahan" name="nama_perumahan" required=''>
                      </div>
                      <div class="form-group">
                        <label for="spesifikasi" class="control-label">Deskripsi Rumah :</label>
                        <textarea name="deskripsi" class="form-control" id="deskripsi" required=''> </textarea>
                      </div>
					  <div class="form-group">
                        <label for="spekteknik" class="control-label">Spesifikasi Teknis Perumahan:</label>
                        <textarea name="spekteknikrmh" class="form-control" id="spekteknikrmh" required=''> </textarea>
                      </div>
					  <div class="form-group">
                        <label for="hrga" class="control-label">Harga Perumahan:</label>
                        <input type="text" class="form-control" id="hrga" name="hrga" required=''>
						<script type="text/javascript">
							$(document).ready(function(){
								$('#hrga').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
							});
						</script>
                      </div>
					  <div class="form-group">
                        <label for="upimage" class="control-label">Upload Gambar Perumahan:</label>
                        <input type="file" class="form-control" id="foto" name="foto">
                      </div>
                      <div class="modal-footer">
                        <input type="Submit" class="btn btn-primary" value="Submit">
                        <input type="reset" class="btn btn-default" value="Reset">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    $(function() {
        $("#perumahan").dataTable();
    });
    </script>

    <?php

	}
	?>

	<script>
        $(function(){
            $(document).on('click','.edit-record',function(e){
                e.preventDefault();
                $("#editrumah").modal('show');
                $.post('../action/adm/edit_rumah.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#editrumah').on('hidden.bs.modal', function () {
                    location.reload();
                })
            });
        });
    </script>

	<script>
        $(function(){
            $(document).on('click','.edit-record3',function(e){
                e.preventDefault();
                $("#showimage").modal('show');
                $.post('../action/adm/showimage.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#showimage').on('hidden.bs.modal', function () {
                    location.reload();
                })
            });
        });
    </script>
	
	<script>
        $(function(){
            $(document).on('click','.edit-record5',function(e){
                e.preventDefault();
                $("#tambahimage").modal('show');
                $.post('../action/adm/add_image.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#tambahimage').on('hidden.bs.modal', function () {
                    location.reload();
                })
            });
        });
    </script>
