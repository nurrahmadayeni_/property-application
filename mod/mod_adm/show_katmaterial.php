<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> DAFTAR KATEGORI MATERIAL</h1>";
        echo "
            <table id='kategorimaterial' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th width='2%'>No</th>
                    <th width='20%'>Nama Kategori Material</th>
                    <th width='5%'>Detail Material</th>
                    <th width='5%'>Aksi</th>
                </tr>
            </thead>
            <tbody>
        ";
       
             
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,'SELECT * FROM kategori_material');
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
            $id = $r['id_kategorimaterial'];
        ?>
		
        <tr align='left'>
			<td>
                <b><?php echo  $no; ?></b>
            </td><td>
				<b><?php echo  $r['kategori_material']; ?>
				</b>
            </td>
            <td>
                <a href="?mod=material&ids=<?php echo $r['id_kategorimaterial']; ?>"><button class='btn btn-primary btn-sm' alt="detail">Detail</button> </a>
            </td>
			<td>
                <a href="#" class="edit-record" title="edit" data-id="<?php echo $id; ?>"><button class='btn btn-primary btn-sm' data-toggle="modal" alt="edit">
				<span class="glyphicon glyphicon-pencil"></span></button></a>
                <a href="../action/adm/act_kategori_material.php?mod=data_material&act=hapus&id=<?php echo $id;?>" title="hapus" onclick="return confirm('Anda ingin menghapus data ini?')"> 
				<button class='btn btn-danger btn-sm' alt="hapus"><span class="glyphicon glyphicon-trash"></span></button> </a>
                
            </td>
        </tr>
        <?php
            $no++;
        }
        ?>

        </tbody>

    </table>  
    <span class="container">
	   <a class="btn btn-primary btn-md" data-toggle="modal" href="#myModal"><span class="glyphicon glyphicon-plus"></span> Kategori Material</a>
	</span>
	
	<!-- Modal for add kategori material-->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Kategori Material</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action='../action/adm/act_kategori_material.php?mod=data_material&act=tambah'>
                      <div class="form-group">
                        <label for="nama-katmaterial" class="control-label">Nama Kategori Material : </label>
                        <input type="text" class="form-control" id="nama-katmaterial" name="kategori_material">
                      </div>
                      <div class="modal-footer">
                        <input type="Submit" class="btn btn-primary" value="Submit">
                        <input type="reset" class="btn btn-default" value="Reset">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
	
	<!-- Modal for Edit kategori material-->
    <div id="edit_kategori_material" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Kategori Material</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
	
  
    <script type="text/javascript">
    $(function() {
        $("#kategorimaterial").dataTable();
    });
    </script>
	
	<script>
        $(function(){
            $(document).on('click','.edit-record',function(e){
                e.preventDefault();
                $("#edit_kategori_material").modal('show');
                $.post('../action/adm/edit_kategori_material.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#edit_kategori_material').on('hidden.bs.modal', function () {
                 location.reload();
                })
            });
        });
    </script>

    <?php
        break;
	}
	?>
	
<script type="text/javascript">
    $(function() {
        $("#Dmaterial").DataTable();
    });
    </script>
