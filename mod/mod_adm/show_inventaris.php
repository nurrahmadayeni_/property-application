<?php
    error_reporting(0);

    switch($_GET[act]){
        default:
        echo "<h3 align=center> INVENTARIS KANTOR</h1>";
        echo "
            <table id='inventaris' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Merk / Type</th>
                    <th>Warna</th>
                    <th>Thn Perolehan</th>
                    <th>Jlh</th>
                    <th>Kondisi</th>
                    <th>Harga (Rp)</th>
                    <th>Total (Rp)</th>
                    <th>PTJ</th>
                    <th>Lokasi</th>
                    <th>Foto</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
        ";
       
             
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,'SELECT * FROM inventaris');
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
            $id = $r['id_inventaris'];
            $idk = $r['id_kategori'];

            $jumlah = $r['jumlah'];
            $hrg = $r['harga_barang'];
            $total = $jumlah * $hrg;        
        ?>
        <tr align='left'>
            <td align="center"> <?php echo  $no;?> </td>
            <td><b><a href="#" class="detail-record" data-id="<?php echo $id ?>">
                <?php echo  $r['nama_barang']; ?> </a></b>
            </td>
            <td> <?php echo  $r['merk']; ?> </td>
            <td> <?php echo  $r['warna']; ?> </td>
            <td> <?php echo  $r['thn_peroleh']; ?> </td>
            <td> <?php echo  $r['jumlah']; ?> </td>
            <td> <?php echo  $r['kondisi']; ?></td>
            <td align='right'> 
                <?php 
                    $rupiah=number_format($r['harga_barang'],0,',','.'); 
                    echo $rupiah; 
                ?>
            </td>
            <td align='right'>
                <?php
                    echo number_format($total,0,',','.'); 
                ?>
            </td>
            <td> <?php echo  $r['ptj']; ?> </td>
            <td> <?php echo  $r['lokasi']; ?> </td>
            <td> <img src="gbr_inventaris/<?php echo  $r['foto']; ?>" width='100%'> </td>
            <td>
                <a href="#edit_inventaris" class="edit-record" data-toggle="modal" data-id="<?php echo $id; ?>"><button class='btn btn-primary btn-sm' alt="edit">
                <span class="glyphicon glyphicon-pencil"></span></button></a>

                <a href="../action/adm/act_edit_inventaris.php?mod=inventaris&act=hapus&id=<?php echo $id;?>" onclick="return confirm('Anda ingin menghapus data ini?')"> 
                <button class='btn btn-danger btn-sm' alt="Hapus"><span class="glyphicon glyphicon-trash"></span></button> </a>
            </td>
        </tr>
            
        <?php
        $no++;
        }
            
        }
        ?>

        </tbody>

    </table>  
    <span class="container">
        <a class="btn btn-primary" data-toggle="modal" href="#myModal"><span class="glyphicon glyphicon-plus"></span> Inventaris</a>
        <a class="btn btn-primary" data-toggle="modal" href="#mKategori"><span class="glyphicon glyphicon-plus"></span> Kategori Inventaris</a>
        <a class="btn btn-primary" data-toggle="modal" href="#mTotal">Total Keseluruhan Harga Inventaris</a>
    </span>

    <!-- Modal for add inventaris-->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Inventaris</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action='../action/adm/act_inventaris.php?mod=inventaris&act=tambah' enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="nama-barang" class="control-label">Nama Barang :</label>
                            <input type="text" class="form-control" id="nama-barang" name="nama_barang">
                        </div>
                        <div class="form-group">
                            <label for="kategori" class="control-label">Kategori Barang : </label>
                            <select name="kategori" class="form-control">
                                <option value='' selected disabled='disabled'>- Pilih Kategori -</option>
                                <?php
                                    $getdata="SELECT * FROM kategori_inventaris";
                                    $tampil=mysqli_query($mysqli,$getdata);
                                    while($r=mysqli_fetch_array($tampil)){
                                        echo "<option value=$r[id_kategori]>
                                        $r[kategori_inventaris]</option>";
                                    }
                                ?>
                          </select>
                        </div>
                        <div class="form-group">
                            <label for="merk/type" class="control-label">Merk/Type :</label>
                            <input type="text" class="form-control" id="merk" name="merk">
                        </div>
                        <div class="form-group">
                            <label for="warna" class="control-label">Warna :</label>
                            <input type="text" class="form-control" id="warna" name="warna">
                        </div>
                        <div class="form-group">
                            <label for="thn" class="control-label">Tahun Perolehan :</label>
                            <input type="text" class="form-control" id="thn_peroleh" name="thn_peroleh">
                        </div>
                        <div class="form-group">
                            <label for="jumlah" class="control-label">Jumlah :</label>
                            <input type="number" class="form-control" id="jumlah" name="jumlah">
                        </div>
                        <div class="form-group">
                            <label for="kondisi" class="control-label">Kondisi :</label>
                            <input type="text" class="form-control" id="kondisi" name="kondisi">
                        </div>
                        <div class="form-group">
                            <label for="hrg" class="control-label">Harga :</label>
                            <input type="text" class="form-control" name="harga_barang" id="harga_barang">
                            <script type="text/javascript">
                                $(document).ready(function(){
                                    $('#harga_barang').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
                                });
                            </script>
                            
                        </div>
                        <div class="form-group">
                            <label for="ptj" class="control-label">Penganggung Jawab :</label>
                            <input type="text" class="form-control" id="ptj" name="ptj">
                        </div>
                        <div class="form-group">
                            <label for="lokasi" class="control-label">Lokasi :</label>
                            <input type="text" class="form-control" id="lokasi" name="lokasi">
                        </div>
                        <div class="form-group">
                            <label for="foto" class="control-label">Upload Foto :</label>
                            <input type="file" class="form-control" id="foto" name="foto" required />
                        </div>
                     
                      <div class="modal-footer">
                        <input type="Submit" class="btn btn-primary" value="Submit" name="submit">
                        <input type="reset" class="btn btn-default" value="Reset">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for Edit Inventaris-->
    <div id="edit_inventaris" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Inventaris</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for add kategori inventaris-->
    <div id="mKategori" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Kategori Inventaris</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action='../action/adm/act_inventaris.php?mod=inventaris&act=tambahKategori'>
                        <div class="form-group">
                            <label for="nama-kategori" class="control-label">Nama Kategori :</label>
                            <input type="text" class="form-control" id="nama-kategori" name="nama_kategori">
                        </div>
                        <div class="modal-footer">
                            <input type="Submit" class="btn btn-primary" value="Submit" name="submit">
                            <input type="reset" class="btn btn-default" value="Reset">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for Total Harga Inventaris-->
    <div id="mTotal" class="modal fade" width='1366px'>
        <div class="modal-dialog" width='1366px'>
            <div class="modal-content" style="width:1000px; margin-left:-100px; margin-right:-100px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Total Keseluruhan Harga Inventaris</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <select name='kategori_inventaris' id='kInventaris' class='form-control' required=''>
                            <option value='' selected disabled="">- Pilih Kategori Inventaris -</option>
                            <?php
                                $getdata="SELECT * FROM kategori_inventaris";
                                $tampil=mysqli_query($mysqli,$getdata);
                                while($r=mysqli_fetch_array($tampil)){
                                    echo "<option value=$r[id_kategori]>
                                    $r[kategori_inventaris]</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div id="total_harga" style="width:100%;">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="detail_inventaris" tabindex="-1" role="dialog" aria-labelledby="detail_inventaris" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Detail Inventaris</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

    <script>
     $(document).on('change','#kInventaris',function(){
             var val = $(this).val();
             $.ajax({
                   url: '../action/adm/show_total_Hinventaris.php',
                   data: {kategori_inventaris:val},
                   type: 'GET',
                   dataType: 'html',
                   success: function(result){
                        $('#total_harga').html();  
                        $('#total_harga').html(result); 
                   }
              });
       });
    </script>
        
    <script type="text/javascript">
        $(function() {
            $("#inventaris").dataTable();
            $('#harga_barang').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
        });
    </script>

    <script>
        $(function(){
            $(document).on('click','.edit-record',function(e){
                e.preventDefault();
                $("#edit_inventaris").modal('show');
                $.post('../action/adm/edit_inventaris.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $("#edit_inventaris").on("hidden.bs.modal", function () {        
                        location.reload();                   
                });
            });
        });
    </script>

    <script>
        $(function(){
            $(document).on('click','.detail-record',function(e){
                e.preventDefault();
                $("#detail_inventaris").modal('show');
                $.post('../action/adm/detail_inventaris.php',
                    {ids:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#detail_inventaris').on('hidden.bs.modal', function () {
                 location.reload();
                })
            });
        });
    </script>


