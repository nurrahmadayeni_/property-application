<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> DATA ASET (LAPORAN) </h1>";
?>
	<div class="panel-heading"><p></div>
	<div class="navbar">
		<ul class="nav nav-pills">
			<li class="active"><a href="#tab1" data-toggle="tab">Laporan Gudang</a></li>
			<li><a href="#tab2" data-toggle="tab">Laporan Gudang Periodik</a></li>
			<li><a href="#tab3" data-toggle="tab">Laporan Keseluruhan</a></li>
			<li><a href="#tab4" data-toggle="tab">Laporan Kebutuhan Material</a></li>
		</ul>
	</div>

	<div class="tab-content">
		<div class="tab-pane active" id="tab1">
			<div class="form-group">
				<select name='laporan_gudang' id='klgudang' class='form-control' required=''>
                    <option value='' selected disabled="">- Pilih Gudang -</option>
                    <?php
                        $getdata="SELECT * FROM data_gudang ORDER BY nama_gudang ASC";
                        $tampil=mysqli_query($mysqli,$getdata);
                        while($r=mysqli_fetch_array($tampil)){
                            echo "<option value=$r[id_gudang]>
                            $r[nama_gudang]</option>";
                        }
                    ?>
                </select>
			</div>
			<div id="Lgudang"></div>
		</div>
		
		<div class="tab-pane" id="tab2">
			<div class="form-group">
				<table class='table table-bordered table-hover'>
					<tr align='center'>
						<td>
							<select name='kategori_material' id='kmaterial' class='form-control' required=''>
							<option value='' selected disabled="">- Pilih Kategori Material -</option>
							<?php
								$getdata="SELECT * FROM kategori_material";
								$tampil=mysqli_query($mysqli,$getdata);
									while($r3=mysqli_fetch_array($tampil)){
										echo "<option value=$r3[id_kategorimaterial]>
										$r3[kategori_material]</option>";
									}
							?>
							</select>
						</td>
						<td>
							<select name='data_gudang' id='kgudang' class='form-control' required=''>
							<option value='' selected disabled="">- Pilih Gudang -</option>
								<?php
									$getdata="SELECT * FROM data_gudang";
									$tampil=mysqli_query($mysqli,$getdata);
									while($r=mysqli_fetch_array($tampil)){
										echo "<option value=$r[id_gudang]>
										$r[nama_gudang]</option>";
									}

								$tglsekarang=date('Y-m-d');
								?>
							</select>
						</td><td>
						<!--<td><input type='date' name='tgl_masuk' id="tgl" class='form-control' required=''>-->
						<input type='date' name='tgl' id="tgl" class='form-control' required='' value="<?php echo $tglsekarang ; ?>" disabled='disabled'></td>
						<td><input type="Submit" class="btn btn-primary btn-sm" id="cari" value="Cari"></td>
					</tr>
				</table>
			</div>
			<div id="tampil">
			</div>
		</div>
		
		<div class="tab-pane" id="tab3">
			<div class="form-group">
				<table class='table table-bordered table-hover'>
					<tr align='center'>
						<td><input type='date' name='tgl1' class='form-control' id="tgl1" required=''></td>
						<td><input type='date' name='tgl2' id="tgl2" class='form-control' required=''></td>
						<td><input type="Submit" id="submit" class="btn btn-primary btn-sm" value="Cari"></td>
					</tr>
				</table>
			</div>
			<div id="hasil">
			</div>
		</div>

		<div class="tab-pane" id="tab4">
			<div class="form-group">
				<table class='table table-bordered table-hover'>
					<tr align='center'>
						<td>
							<select name='data_perumahan' id='kperumahan' class='form-control' required=''>
							<option value='' selected disabled="">- Pilih Perumahan -</option>
							<?php
								$getdata="SELECT * FROM data_perumahan";
								$tampil=mysqli_query($mysqli,$getdata);
									while($r3=mysqli_fetch_array($tampil)){
										echo "<option value=$r3[id_perumahan]>
										$r3[nama_perumahan]</option>";
									}
							?>
							</select>
						</td>
						<td>
							<select name='data_kavling' id='Kkavling' class='form-control' required=''>
							<option value='' selected disabled="">- Pilih Kavling -</option>
								<?php
									$getdata="SELECT * FROM data_kavling";
									$tampil=mysqli_query($mysqli,$getdata);
									while($r=mysqli_fetch_array($tampil)){
										echo "<option value=$r[no_kavling]>
										$r[no_kavling]</option>";
									}
								?>
							</select>
						</td>
						<td><input type="Submit" class="btn btn-primary btn-sm" id="search" value="Cari"></td>
					</tr>
				</table>
			</div>
			<div id="LKebutuhan"></div>
		</div>

	</div>
		
<?php
 	}
?>
<script>
    $(document).on('change','#klgudang',function(){
            var val = $(this).val();
			$.ajax({

                url: '../action/adm/show_Lgudang.php',
                data: {laporan_gudang:val},
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#Lgudang').html();  
                    $('#Lgudang').html(result); 
                }
            });
    });
</script>

<script>
    $(document).on('click','#submit',function(){
            var tgl_awal = $('#tgl1').val();
            var tgl_akhir = $('#tgl2').val();
			$.ajax({

                url: '../action/adm/show_Lseluruh.php',
                data: {
                	tgl_1:tgl_awal,
                	tgl_2:tgl_akhir
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#hasil').html();  
                    $('#hasil').html(result); 
                }
            });
    });
</script>

<script>
    $(document).on('click','#cari',function(){
            var val1 = $('#kmaterial').val();
            var val2 = $('#kgudang').val();
            var val3 = $('#tgl').val();

			$.ajax({

                url: '../action/adm/show_Lperiodik.php',
                data: {
                	km:val1,
                	kg:val2,
                	tg:val3
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#tampil').html();  
                    $('#tampil').html(result); 
                }
            });
    });
</script>

<script>
    $(document).on('click','#search',function(){
            var perum = $('#kperumahan').val();
            var kavl = $('#Kkavling').val();

			$.ajax({

                url: '../action/adm/show_LKebutuhan.php',
                data: {
                	per:perum,
                	kav:kavl
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#LKebutuhan').html();  
                    $('#LKebutuhan').html(result); 
                }
            });
    });
</script>