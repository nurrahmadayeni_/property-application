<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> PENGAJUAN ALOKASI </h1>";
        echo "
            <table id='alokasi' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th width='5%'><center>No</center></th>
                    <th width='15%'>Jenis Alokasi</th>
                    <th width='15%'>Nama Alokasi</th>
                    <th width='15%'>Tangga Alokasi</th>
                    <th width='10%'><center>Jumlah</center></th>
                    <th width='15%'><center>Nilai/Harga (Rp)</center></th>
                    <th width='15%'><center>Total (Rp)</center></th>
                    <th width='15%'>Aksi</th></center>
                </tr>
            </thead>
            <tbody>
        ";
       
             
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,'SELECT * FROM pengajuan_alokasi');
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
            $id = $r['no_alokasi'];
        ?>

        <tr >
            <td align='center'><?php echo $no;?></td>
            <td>
                <?php echo  $r['jenis_alokasi']; ?>
            </td>
            <td>
                <?php echo  $r['nama_alokasi']; ?>
            </td>
            <td align='center'>
                <?php echo  $r['waktu']; ?>
            </td>
			<td align='center'>
                <?php echo  $r['jml_alokasi']; ?>
            </td>
            <td align='right'>
                <?php 
                    $rupiah=number_format($r['harga_alokasi'],0,',','.'); 
                    echo $rupiah; 
                ?>
            </td>
            <td align='right'>
                <?php 
                    $total = $r['jml_alokasi'] * $r['harga_alokasi'];
                    echo number_format($total,0,',','.'); 
                ?>
            </td>
			<td>
                <a href="#" title="edit" class="edit-alokasi" data-id="<?php echo $id; ?>"><button class='btn btn-primary btn-sm' data-toggle="modal" alt="edit">
				<span class="glyphicon glyphicon-pencil"></span></button></a>
                <a href="../action/adm/act_alokasi.php?mod=keuangan_alokasi&act=hapusalokasi&id=<?php echo $id;?>" title="hapus" onclick="return confirm('Anda ingin menghapus data ini?')"> 
				<button class='btn btn-danger btn-sm' alt="Hapus"><span class="glyphicon glyphicon-trash"></span></button> </a>
            </td>
        </tr>
        <?php
            $no++;
        }
        ?>
        </tbody>

    </table>  
    <span class="container">
	   <a class="btn btn-primary btn-md" data-toggle="modal" href="#myModal"><span class="glyphicon glyphicon-plus"></span> Pengajuan Alokasi</a>
       <button type="button" class="btn btn-primary" onclick="print_d()" ><span class="glyphicon glyphicon-print"></span> Print Laporan Pangajuan</button>
       <a class="btn btn-primary btn-md" data-toggle="modal" href="index.php?mod=laporan_alokasi"><span class="glyphicon glyphicon-book"></span> Laporan Alokasi</a>
       
    </span>
    
    <!-- Modal for add Alokasi-->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Pengajuan Alokasi</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action='../action/adm/act_alokasi.php?mod=keuangan_aloksi&act=tambahalokasi'>
                      <div class="form-group">
                        <label for="jenis-alokasi" class="control-label">Jenis Alokasi: </label>
                        <input type="text" class="form-control" id="jenis_alokasi" name="jenis_alokasi">
                      </div>
                      <div class="form-group">
                        <label for="nama" class="control-label">Nama Alokasi:</label>
                        <input type="text" class="form-control" id="nama" name="nama">
                      </div>
                      <div class="form-group">
                        <label for="waktu" class="control-label">Tanggal Alokasi :</label>
                         <input type="date" class="form-control" id="waktu" name="waktu">
                      </div>
                      <div class="form-group">
                        <label for="jml" class="control-label">Jumlah :</label>
                         <input type="text" class="form-control" id="jml" name="jml">
                      </div>
                      <div class="form-group">
                        <label for="harga_alokasi" class="control-label">Nilai/Harga :</label>
                        <input type="text" class="form-control" id="harga_alokasi" name="harga_alokasi">
                        <script type="text/javascript">
                            $(document).ready(function(){
                                $('#harga_alokasi').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
                            });
                        </script>
                      </div>

                      <div class="modal-footer">
                        <input type="Submit" class="btn btn-primary" value="Submit">
                        <input type="reset" class="btn btn-default" value="Reset">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Modal for Edit Giro-->
    <div id="edit_alokasi" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Pengajuan Alokasi</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
  
    <script type="text/javascript">
    $(function() {
        $("#alokasi").dataTable();
    });
    </script>

      <?php
        break;
    }
    ?>
    
    <script>
        $(function(){
            $(document).on('click','.edit-alokasi',function(e){
                e.preventDefault();
                $("#edit_alokasi").modal('show');
                $.post('../action/adm/edit_alokasi.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#edit_alokasi').on('hidden.bs.modal', function () {
                 location.reload();
                })
            });
        });
    </script>

<script>
    function print_d(){
        window.open("../action/adm/printPengajuan.php?id=<?php echo $id;?>","_blank");
    }
</script>