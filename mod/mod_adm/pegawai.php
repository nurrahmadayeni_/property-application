<?php
    error_reporting(0);

    switch($_GET[act]){
        default:
        echo "<h3 align=center>DATA PEGAWAI</h3>";
        echo "
            <table id='pegawai' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Pegawai</th>
                    <th>No Handphone</th>
                    <th>Jenis Kelamin</th>
                    <th>Email</th>
                    <th>Alamat</th>
                    <th>Divisi</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            
        ";

        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,'SELECT * FROM data_pegawai');
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
            $id = $r['id_pegawai'];
            $idu = $r['id_user'];
        ?>
        <tr align='left'>
            <td align="center"> <?php echo  $no;?> </td>
            <td> <?php echo  $r['nama_pegawai']; ?> </td>
            <td> <?php echo  $r['nope']; ?> </td>
            <td> <?php echo  $r['jenis_kelamin']; ?> </td>
            <td> <?php echo  $r['email']; ?> </td>
            <td> <?php echo  $r['alamat']; ?> </td>
            <td> <?php echo  $r['level']; ?> </td>
            <td>
                <a href="#" class="edit-record" data-toggle="modal" data-id="<?php echo $id; ?>"><button class='btn btn-primary btn-sm' alt="edit">
                <span class="glyphicon glyphicon-pencil"></span></button></a>

                <a href="../action/adm/act_pegawai.php?mod=pegawai&act=hapus&id=<?php echo $id;?>" onclick="return confirm('Anda yakin ingin menghapus data ini?')"> 
                <button class='btn btn-danger btn-sm' alt="Hapus"><span class="glyphicon glyphicon-trash"></span></button> </a>
            </td>

        </tr>
            
        <?php
        $no++;
        }
            
        }
        ?>

        </tbody>

    </table>  
    <span class="container">
        <a class="btn btn-primary" data-toggle="modal" href="#myModal"><span class="glyphicon glyphicon-plus"></span> Pegawai</a>
        
    </span>

    <!-- Modal for add pegawai-->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Pegawai</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action='../action/adm/act_pegawai.php?mod=pegawai&act=tambah' >
                        <div class="form-group">
                            <label for="nama-pegawai" class="control-label">Nama pegawai :</label>
                            <input type="text" class="form-control" id="nama-pegawai" name="nama_pegawai" style="text-transform: capitalize">
                        </div>
                        <div class="form-group">
                            <label for="nope" class="control-label">No Handphone :</label>
                            <input type="handphone" class="form-control" id="nope" name="nope">
                        </div>
                        <div class="form-group">
                            <label for="jenis_kelamin" class="control-label">Jenis Kelamin : </label>
                            <input name="jenis_kelamin" class="form" type="radio" value="Pria"/> Pria
                            <input name="jenis_kelamin" class="form" type="radio" value="Wanita" /> Wanita
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label">Email :</label>
                            <input type="email" class="form-control" id="email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="alamat" class="control-label">Alamat : </label>
                            <textarea class="form-control" name="alamat"> </textarea>
                        </div>
                        <div class="form-group">
                            <label for="level" class="control-label">Level : </label>
                            <select name="level" class="form-control" id="level">
                                <option selected="true" value="-" disabled="disabled" style="padding-right: 20px;">--Select Level--</option>
                                <option value="ADM">ADM</option>
                                <option value="Teknikal">Teknikal</option>
                                <option value="Marketing">Marketing</option>
                            </select>
                        </div>
                        
                      <div class="modal-footer">
                        <input type="Submit" class="btn btn-primary" value="Submit" name="submit">
                        <input type="reset" class="btn btn-default" value="Reset">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for Edit Pegawai-->
    <div id="edit_pegawai" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Pegawai</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(document).ready(function(){
            $('#pegawai').dataTable();
        });

    </script>



    <script>
        $(function(){
            $(document).on('click','.edit-record',function(e){
                e.preventDefault();
                $("#edit_pegawai").modal('show');
                $.post('../action/adm/edit_pegawai.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $("#edit_pegawai").on("hidden.bs.modal", function () {        
                        location.reload();                   
                });
            });
        });
    </script>

