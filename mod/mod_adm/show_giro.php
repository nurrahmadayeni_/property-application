<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> DAFTAR GIRO </h1>";
        echo "
            <table id='giro' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th width='5%'>No</th>
                    <th width='15%'>Nama Giro</th>
                    <th width='20%'>Div.Penanggung Jawab</th>
                    <th width='15%'>No.Rek</th>
                    <th width='15%'>Bank</th>
                    <th width='15%'>Specimen</th>
                    <th width='15%'>Aksi</th>
                </tr>
            </thead>
            <tbody>
        ";
       
             
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,"SELECT * FROM giro");
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
            $id = $r['no_giro'];
        ?>

        <tr align='left'>
            <td><?php echo $no;?></td>
            <td>
                <b><a href="#" class="detail-record" data-id="<?php echo $id ?>">
                <?php echo  $r['jenis_giro']; ?></a></b>
            </td>
            <td>
                <?php echo  $r['penaggungjawab']; ?>
            </td>
            <td>
                <?php echo  $r['no_rek']; ?>
            </td>
			<td>
                <?php echo  $r['nama_bank']; ?>
            </td>
            <td>
                <?php echo  $r['spesimen']; ?>
            </td>
			<td>
                <a href="#" title="edit" class="edit-giro" data-id="<?php echo $id; ?>"><button class='btn btn-primary btn-sm' data-toggle="modal" alt="edit">
				<span class="glyphicon glyphicon-pencil"></span></button></a>
                <a href="../action/adm/act_giro.php?mod=keuangan_giro&act=hapusgiro&id=<?php echo $id;?>" title="hapus" onclick="return confirm('Anda ingin menghapus data ini?')"> 
				<button class='btn btn-danger btn-sm' alt="Hapus"><span class="glyphicon glyphicon-trash"></span></button> </a>
            </td>
        </tr>
        <?php
            $no++;
        }
        ?>

        </tbody>

    </table>  
    <span class="container">
	   <a class="btn btn-primary btn-md" data-toggle="modal" href="#myModal"><span class="glyphicon glyphicon-plus"></span> Giro</a>
       <a class="btn btn-primary btn-md" data-toggle="modal" href="index.php?mod=new_giro"><span class="glyphicon glyphicon-pencil"></span> Input Data</a>
       <a class="btn btn-primary btn-md" data-toggle="modal" href="index.php?mod=laporan_giro"><span class="glyphicon glyphicon-book"></span> Laporan Giro</a>
       
    </span>
    
    <!-- Modal for add giro-->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Input Data Giro</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action='../action/adm/act_giro.php?mod=keuangan_giro&act=tambahgiro'>
                      <div class="form-group">
                        <label for="jenis-giro" class="control-label">Nama Giro : </label>
                        <input type="text" class="form-control" id="jenis_giro" name="jenis_giro">
                      </div>
                      <div class="form-group">
                        <label for="penanggung-jawab" class="control-label">Div.Penanggung Jawab :</label>
                        <input type="text" class="form-control" id="penaggungjawab" name="penaggungjawab">
                      </div>
                      <div class="form-group">
                        <label for="no_rek" class="control-label">No.Rek :</label>
                         <input type="text" class="form-control" id="no_rek" name="no_rek">
                      </div>
                      <div class="form-group">
                        <label for="bank" class="control-label">Bank :</label>
                        <input type="text" class="form-control" id="bank" name="bank">
                      </div>
                      <div class="form-group">
                        <label for="specimen" class="control-label">Specimen :</label>
                        <input type="text" class="form-control" id="specimen" name="specimen">
                      </div>
                      <div class="modal-footer">
                        <input type="Submit" class="btn btn-primary" value="Submit">
                        <input type="reset" class="btn btn-default" value="Reset">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for Edit Giro-->
    <div id="edit_giro" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Giro</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
    

    <div id="Mgiro" class="modal fade bs-example-modal-lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Dana Masing - Masing giro</h4>
                </div>
                <div class="modal-body">
                    <?php include "../action/adm/listdana_giro.php"; ?>
                </div>
                
            </div>
        </div>
    </div>

    <div  class="modal fade bs-example-modal-lg" id="detail_giro">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Detail Giro</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    
  
    <script type="text/javascript">
    $(function() {
        $("#giro").dataTable();
    });
    </script>

      <?php
        break;
    }
    ?>
    
    <script>
        $(function(){
            $(document).on('click','.edit-giro',function(e){
                e.preventDefault();
                $("#edit_giro").modal('show');
                $.post('../action/adm/edit_giro.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#edit_giro').on('hidden.bs.modal', function () {
                 location.reload();
                })
            });
        });
    </script>

        <script>
        $(function(){
            $(document).on('click','.detail-record',function(e){
                e.preventDefault();
                $("#detail_giro").modal('show');
                $.post('../action/adm/list_dana.php',
                    {ids:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#detail_giro').on('hidden.bs.modal', function () {
                 location.reload();
                })
            });
        });
    </script>
    
<script type="text/javascript">
    $(function() {
        $("#DGiro").DataTable();
    });
    </script>
<script type="text/javascript">
    $(function() {
        $("#Dmasuk_keluar").DataTable();
    });
    </script>