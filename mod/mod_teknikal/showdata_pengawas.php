<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> DATA PENGAWAS</h3>";
        echo "
            <table id='Dpengawas' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th width='10%'>ID Pengawas</th>
                    <th width='20%'>Nama Pengawas</th>
                    <th width='20%'>Alamat </th>
                    <th width='20%'>No.telp/Hp</th>
                    <th width='10%'>email</th>
					<th width='10%'>Aksi</th>
                </tr>
            </thead>
            <tbody>
        ";
       
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,'SELECT * FROM data_pengawas');
        $no = 1;
        while ($q = mysqli_fetch_array($sql)) {
        ?>

        <tr align='left'>
            <td>
                <?php echo  $q['id_pengawas'];?>
            </td>
            <td>
                <?php echo $q['nama_pengawas']; ?>
            </a>
            </td>
            <td>
                <?php echo  $q['alamat_pengawas']; ?>
            </td>
            <td>
                <?php echo  $q['no_telp'];?>
            </td>
             <td>
                <?php echo  $q['email'];?>
            </td>
			<td>
				<a href="#" class="edit-record" data-toggle="modal" data-id="<?php echo $q['id_pengawas']; ?>"><button class='btn btn-primary btn-sm' alt="edit">
                <span class="glyphicon glyphicon-pencil"></span></button></a>

                <a href="../action/teknikal/aksi_bataldaftar.php?mod=teknikal&act=hapuspengawas&idp=<?php echo $q['id_pengawas'];?>" onclick="return confirm('Anda ingin menghapus data ini?')"> 
                <button class='btn btn-danger btn-sm' alt="Hapus"><span class="glyphicon glyphicon-trash"></span></button> </a>

			</td>
        </tr>
        <?php
            $no++;
        }
        ?>
        </tbody>

    </table>  

    <span class="container">
	   <a class="btn btn-primary " data-toggle="modal" href="#tambahpengawas"><span class="glyphicon glyphicon-plus"></span> Pengawas</a>
 	</span>
		
	 <!-- Modal for Tambah Pengawas-->
    <div id="tambahpengawas" class="modal fade bs-example-modal-lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Pengawas</h4>
                </div>
                <div class="modal-body">    
					  <form method="post" action='../action/teknikal/act_perumahan.php?mod=teknikal&act=tambahpengawas'>
                      <div class="form-group">
                        <label for="nama-pengawas" class="control-label">Nama Pengawas : </label>
                        <input type="text" class="form-control" id="nama-pengawas" name="nama_pengawas" required=''>
                      </div>
                      <div class="form-group">
                        <label for="alamat" class="control-label">Alamat Pengawas :</label>
                        <input type="text" class="form-control" id="alamat" name="alamat" required=''>
                      </div>
					  <div class="form-group">
                        <label for="telp" class="control-label">No.Telp/HP :</label>
                        <input type="text" class="form-control" id="telp" name="telp" required=''>
                      </div>
					  <div class="form-group">
                        <label for="email" class="control-label">Email Pengawas :</label>
                        <input type="text" class="form-control" id="email" name="email">
                      </div>
                      <div class="modal-footer">
                        <input type="Submit" class="btn btn-primary" value="Submit">
                        <input type="reset" class="btn btn-default" value="Reset">
                    </div>
                    </form>
                </div>
                </div>
            </div>
    </div>
	
	<!-- Modal for Edit Pengawas-->
    <div id="edit_pengawas" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Data Pengawas</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>

	<script type="text/javascript">
    $(function() {
        $("#Dpengawas").DataTable();
    });
    </script>

    <?php
	}
	?>

	<script>
        $(function(){
            $(document).on('click','.edit-record',function(e){
                e.preventDefault();
                $("#edit_pengawas").modal('show');
                $.post('../action/teknikal/edit_pengawas.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#edit_pengawas').on('hidden.bs.modal', function () {
                    location.reload();
                })
            });
        });
    </script>
	