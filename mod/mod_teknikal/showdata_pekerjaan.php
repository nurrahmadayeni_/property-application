<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> DATA PEKERJAAN</h3>";
        echo "
            <table id='Dpekerjaan' class='table table-bordered table-hover'>
            <thead>
                <tr>
                   <th width='10%'>No</th>
                    <th width='20%'>Tipe Pekerjaan</th>
                    <th width='20%'>Nama Pekerjaan</th>
                    <th width='20%'>Harga Upah Pekerjaan(Rp.)</th>
					<th width='20%'>Menu</th>
                </tr>
            </thead>
            <tbody>
        ";
       
		include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,'SELECT pk.id_pekerjaan,tp.nama_kategorikerja,pk.nama_pekerjaan,pk.harga_pekerjaan 
							FROM kategori_pekerjaan tp,data_pekerjaan pk 
							WHERE pk.id_kategorikerja=tp.id_kategorikerja');
        $no = 1;
        while ($q = mysqli_fetch_array($sql)) {
			$id=$q['id_pekerjaan'];
        ?>

        <tr align='left'>
            <td>
                <?php echo  $no;?>
            </td>
            <td>
                <?php echo $q['nama_kategorikerja']; ?>
            </a>
            </td>
            <td>
                <?php echo  $q['nama_pekerjaan']; ?>
            </td>
            <td align='right'>
				<?php 
                    $rupiah=number_format($q['harga_pekerjaan'],0,',',','); 
                    echo $rupiah; 
                ?>
            </td>
			<td align='center'>
			<a href="#" title="edit" class="edit-record" data-id="<?php echo $id; ?>" data-toggle="modal">
                <i class="material-icons btn btn-primary">edit</i></a>               
			<a href="../action/teknikal/aksi_bataldaftar.php?mod=teknikal&act=hapuskerja&idd=<?php echo $q['id_pekerjaan'];?>" title="delete" onclick="return confirm('Anda ingin menghapus data ini?')"> 
                <i class="material-icons btn btn-danger">delete</i></a>
			</td>
		
        </tr>
        <?php
            $no++;
        }
        ?>
        </tbody>
    </table>   
    <span class="container">
	   <a class="btn btn-primary " data-toggle="modal" href="#tambahpekerjaan"><span class="glyphicon glyphicon-plus"></span> Pekerjaan</a>
 	</span>
		
	 <!-- Modal for Tambah Pekerjaan-->
    <div id="tambahpekerjaan" class="modal fade bs-example-modal-lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Pekerjaan</h4>
                </div>
                <div class="modal-body">    
					  <form method="post" action='../action/teknikal/act_perumahan.php?mod=teknikal&act=tambahpekerjaan'>
                      <div class="form-group">
                        <label for="tipe-pekerjaan" class="control-label">Tipe Pekerjaan : </label>
						<select name='tipe_krj' id='tipe_krj' class='form-control' required=''>
						<option value='' selected>- Pilih Tipe Pekerjaan -</option>
						<?php
						$getkategori="SELECT * FROM kategori_pekerjaan";
						$tampil=mysqli_query($mysqli,$getkategori);
						while($r=mysqli_fetch_assoc($tampil))
						{
							echo "<option value=$r[id_kategorikerja]>
							$r[nama_kategorikerja]</option>";
						}
						?>
						</select>
					  </div>
                      <div class="form-group">
                        <label for="nama-pekerjaan" class="control-label">Nama Pekerjaan :</label>
						<input type="text" class="form-control" id="nama" name="nama" required=''>
					  </div>
					  <div class="form-group">
                        <label for="harga" class="control-label">Harga Upah per Pekerjaan :</label>
                        <input type="text" class="form-control" id="hrg" name="hrg" required=''>
						<script type="text/javascript">
							$(document).ready(function(){
								$('#hrg').maskMoney({prefix:'Rp. ', thousands:',', decimal:',', precision:0});
							});
						</script>
					  </div>
			
                      <div class="modal-footer">
                        <input type="Submit" class="btn btn-primary" value="Submit">
                        <input type="reset" class="btn btn-default" value="Reset">
                    </div>
                    </form>
                </div>
                </div>
            </div>
    </div>
	
	<!-- Modal for Edit Pekerjaan-->
    <div id="edit_pekerjaan" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Data Pekerjaan</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>

	<script type="text/javascript">
    $(function() {
        $("#Dpekerjaan").DataTable();
    });
    </script>

    <?php

	}
	?>
	
	<script type="text/javascript">
    $(function() {
        $("#Dsupllier").DataTable();
    });
    </script>

	<script>
        $(function(){
            $(document).on('click','.edit-record',function(e){
                e.preventDefault();
                $("#edit_pekerjaan").modal('show');
                $.post('../action/teknikal/edit_pekerjaan.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#edit_pekerjaan').on('hidden.bs.modal', function () {
                    location.reload();
                })
            });
        });
    </script>
	