<!DOCTYPE html>
<head>
	<title>Menu Teknikal </title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="../../images/pavicon.png" rel="icon" type="image/x-icon" />

	<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=PT+Serif:100,200,400,300'>
	<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/icon?family=Material+Icons'>

	<link rel="stylesheet" href="../../css/bootstrap.min.css"/>	

	<script src="../assets/js/jquery.min.js"></script>
	<script src="../assets/js/bootstrap.min.js"></script>
</head>

<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
if(!isset($_SESSION["username"])){
	?>
    <!--Modal Session -->
		<script type="text/javascript">
		    $(document).ready(function(){
		        $("#login").modal('show');
		    });
		</script>

		<div id="login" class="modal fade">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header alert-warning">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                <h4 class="modal-title">WARNING</h4>
		            </div>
		            <div class="modal-body">
					  Please Login First
					</div>
		            <div class = "modal-footer">
			            <a href="../../index.php"><button type = "button" class = "btn btn-warning" >OK</button> </a>
			         </div>
		        </div>
		    </div>
		</div>
    <?php
}else{
	include '../../config/connectdb.php';

	$nexttujuh = mktime(0, 0, 0, date("m"), date("d") + 7, date("Y"));
	$nexttiga = mktime(0, 0, 0, date("m"), date("d") + 3, date("Y"));
	$nextsatu = mktime(0, 0, 0, date("m"), date("d") + 1, date("Y"));
	
	$semingu = date("Y-m-d", $nexttujuh);
	$tiga = date("Y-m-d", $nexttiga);
	$satu = date("Y-m-d", $nextsatu);

	$tempo = "SELECT d.nama_perumahan, d.id_perumahan, k.no_kavling, kp.id_kategorikerja, kp.nama_kategorikerja, k.tgl_selesai 
	from data_perumahan d, kontrak_pekerjaan k, kategori_pekerjaan kp 
	WHERE d.id_perumahan=k.id_perumahan and k.id_kategorikerja=kp.id_kategorikerja limit 1";

	$t = mysqli_query($mysqli,$tempo);

	echo "<div class='alert alert-warning fade in form-group' >
			    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Kontrak telah jatuh tempo pada perumahan :";
	while ($r = mysqli_fetch_array($t)) {
		$sql1 = "SELECT COUNT(pp.progress) as 'siap' 
				FROM data_perumahan p, progress_pembangunan pp, kategori_pekerjaan k , data_pekerjaan d 
				WHERE p.id_perumahan=pp.id_perumahan 
				AND k.id_kategorikerja=d.id_kategorikerja 
				AND d.id_pekerjaan=pp.id_pekerjaan 
				AND pp.id_perumahan='$r[id_perumahan]' 
				AND pp.no_kavling='$r[no_kavling]' 
				AND pp.progress='1' 
				GROUP BY k.id_kategorikerja limit 1
		";
		$tampil=mysqli_query($mysqli,$sql1);

		$ro = mysqli_num_rows($tampil);
		$sql2 = "SELECT id_pekerjaan FROM data_pekerjaan WHERE id_kategorikerja=$r[id_kategorikerja]";
		$result=mysqli_query($mysqli,$sql2);
		$h = mysqli_num_rows($result);

		$persen = round(($r['siap'] / $h) *100);

		if($semingu==$r['tgl_selesai'] || $tiga==$r['tgl_selesai'] || $satu == $r['tgl_selesai'] && $persen < 100){
			echo "
	    		<b>".$r['nama_perumahan'].", kavling: </b>
	    		<b>".$r['no_kavling']." ,</b>
	    		<b> pekerjaan :".$r['nama_kategorikerja']."
	    		dan tanggal selesai : ".$r['tgl_selesai']."</b>
	    		<a href='?mod=reminder' class='btn btn-warning'> see more </a>
			";
		}
	}
	echo "</div>";

?>

	<link href="../../css/styles.css" rel="stylesheet">
	
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css"/>
	<link rel="stylesheet" href="../assets/css/bootstrap-theme.min.css">

	<script src="../assets/js/jquery-1.11.0.js"></script>
	<script src="../assets/js/bootstrap.min.js"></script>
	
	<script src="../assets/js/jquery.maskMoney.min.js"></script>
	<script src="../assets/datatables/jquery.dataTables.js"></script>
	<script src="../assets/datatables/dataTables.bootstrap.js"></script>

	<script>
	//copyright
	function copyDate() {
		var cpyrt = document.getElementById("copyright")
		if (cpyrt) {
		cpyrt.firstChild.nodeValue = (new Date()).getFullYear();
		}}
	window.onload = copyDate;
	</script>
	

<body>
  	<div class="header" style="background-color: #795548">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-5">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="?mod=show_progress">Teknikal</a></h1>
	              </div>
	           </div>
	           <div class="col-md-5">
	              <div class="row">
	                <div class="col-lg-12">
	                  <div class="input-group form"></div>
	                </div>
	              </div>
	           </div>
	           	<div class="col-md-2">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                    <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <?php echo"$_SESSION[username]"; ?> <b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="?mod=profile">Profile</a></li>
	                          <li><a href="#" data-toggle = "modal" data-target = "#logout">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	          	</div>
	        </div>
	     </div>
	</div>

    <div class="page-content">
    	<div class="row">
		  <div class="col-md-3">
		  	<div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
					<?php
				        $page = $_SERVER['QUERY_STRING'];
				    ?>
                     <li class="submenu">
                           <a href="?mod=kontrak_pekerjaan">
                          	  <div class="row">
                          	  	<div class="col-lg-2">
	                          	  <i class="material-icons">description</i> 
	                            </div>
	                            <div class="col-lg-8"> 
	                              Data Pembangunan
	                              <span class="caret pull-right"></span>
	                            </div>
                              </div>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                         	<li <?php if($page=='mod=kontrak_pekerjaan') echo "class='current'" ?>>
								<a href="?mod=kontrak_pekerjaan">
									<i class="material-icons"><i class="material-icons">insert_drive_file</i></i> 
								Kontrak Pekerjaan</a>
							</li>
							<li <?php if($page=='mod=update_progress') echo "class='current'" ?> > <a href="?mod=update_progress">
								<i class="material-icons">update</i>
								 Update Progress Pembangunan </a>
							</li> 
		                    <li <?php if($page=='mod=show_progress') echo "class='current'" ?>>
								<a href="?mod=show_progress">
									<i class="material-icons">remove_red_eye</i>
								 Show Progress Pembangunan 
								</a>
							</li>   
							
						</ul>
					</li>

					<li class="submenu">
                         <a href="?mod=data_teknis">
                         	<div class="row">
                          	  	<div class="col-lg-2">
                            		<i class="material-icons">settings</i>
                            	</div>
                            	<div class="col-lg-8">
                            		Data Teknis
                            		<span class="caret pull-right"></span>
                            	</div>
                            </div>

                        </a>
                         <!-- Sub menu -->
                         <ul>
                            <li <?php if($page=='mod=data_teknis') echo "class='current'" ?>>
								<a href="?mod=data_teknis">
									<div class="row">
                          	  			<div class="col-lg-2">
											<i class="material-icons">description</i>
										</div>
										<div class="col-lg-8">
											Perumahan
										</div>
									</div>
								</a>
							</li>
							<li <?php if($page=='mod=data_kavling') echo "class='current'" ?>>
								<a href="?mod=data_kavling">
									<div class="row">
                          	  			<div class="col-lg-2">
											<i class="material-icons">description</i>
										</div>
										<div class="col-lg-8">
											Kavling
										</div>
									</div>
								</a>
							</li>
                            <li <?php if($page=='mod=data_pekerjaan') echo "class='current'" ?>>
								<a href="?mod=data_pekerjaan">
									<div class="row">
                          	  			<div class="col-lg-2">
											<i class="material-icons">description</i>
										</div>
										<div class="col-lg-8">
											Pekerjaan
										</div>
									</div>
								</a>
							</li>                           
                        </ul>
                    </li>

					<li class="submenu">
                         <a href="?mod=pembayaran">
                          	  <div class="row">
                          	  	<div class="col-lg-2">
	                          	  <i class="material-icons">attach_money</i> 
	                            </div>
	                            <div class="col-lg-8"> 
	                              Keuangan
	                              <span class="caret pull-right"></span>
	                            </div>
                              </div>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                         	<li <?php if($page=='mod=pembayaran') echo "class='current'" ?>>
								<a href="?mod=pembayaran">
									<i class="material-icons"><i class="material-icons">attach_money</i></i> 
								Bayar Pekerjaan</a>
							</li>	
							<li <?php if($page=='mod=pengeluaran') echo "class='current'" ?>>
								<a href="?mod=pengeluaran">
									<i class="material-icons"><i class="material-icons">attach_money</i></i> 
								Pengeluaran</a>
							</li>	
							 <li <?php if($page=='mod=alokasi') echo "class='current'" ?>>
								<a href="?mod=alokasi">
									<i class="material-icons">remove_red_eye</i>
								 Alokasi
								</a>
							</li> 
							<li <?php if($page=='mod=penarikan_cek') echo "class='current'" ?> > <a href="?mod=penarikan_cek">
								<i class="material-icons">update</i>
								 Penarikan Cek </a>
							</li>
							<li <?php if($page=='mod=kas') echo "class='current'" ?>>
								<a href="?mod=kas">
									<i class="material-icons">remove_red_eye</i>
								 Kas
								</a>
							</li>  
						</ul>
					</li>
                    
                    
                    <li class="submenu">
                         <a href="?mod=data_teknis">
	                         <div class="row">
	                          	  	<div class="col-lg-2">
                            			<i class="material-icons">find_in_page</i>
                            		</div>
                            		<div class="col-lg-8">
                            			<p> Laporan 
                            			<span class="caret pull-right"></span> </p>
                            		</div>
                            </div>
                         </a>
                         <ul>	
                         	<li <?php if($page=='mod=data_admin') echo "class='current'" ?>>
								<a href="?mod=data_admin">
									<div class="row">
                          	  			<div class="col-lg-2">
											<i class="material-icons">label</i>
										</div>
										<div class="col-lg-8">
											Data Adm
										</div>
									</div>
								</a>
							</li>   
							<li style="display:none;"<?php if($page=='mod=data_marketing') echo "class='current'" ?>>
								<a href="?mod=data_marketing">
									<div class="row">
                          	  			<div class="col-lg-2">
											<i class="material-icons">label</i>
										</div>
										<div class="col-lg-8">
											Data Marketing
										</div>
									</div>
								</a>
							</li>      
							<li <?php if($page=='mod=laporan_teknikal') echo "class='current'" ?>>
								<a href="?mod=laporan_teknikal">
									<div class="row">
                          	  			<div class="col-lg-2">
											<i class="material-icons">label</i>
										</div>
										<div class="col-lg-8">
											Laporan Teknikal
										</div>
									</div>
								</a>
							</li>  
                         </ul>
                         <!-- Sub menu -->
                         <ul style="display:none;">
		                    <li <?php if($page=='mod=data_admin') echo "class='current'" ?>>
								<a href="?mod=data_admin">
									<div class="row">
                          	  			<div class="col-lg-2">
											<i class="material-icons">label</i>
										</div>
										<div class="col-lg-8">
											Data Admin
										</div>
									</div>
								</a>
							</li>   
							<li <?php if($page=='mod=data_marketing') echo "class='current'" ?>>
								<a href="?mod=data_marketing">
									<div class="row">
                          	  			<div class="col-lg-2">
											<i class="material-icons">label</i>
										</div>
										<div class="col-lg-8">
											Data Marketing
										</div>
									</div>
								</a>
							</li>            
						</ul>
					</li>
					<li class="submenu">
                         <a href="#">
                         	<div class="row">
	                          	  	<div class="col-lg-2">
                            			<i class="material-icons">account_circle</i> 
                            		</div>
                            		<div class="col-lg-8">
                            			Account
                            			<span class="caret pull-right"></span>
                            		</div>
                            </div>
                         </a>
                         <!-- Sub menu -->
                         <ul>
		                    <li <?php if($page=='mod=profile') echo "class='current'" ?>>
								<a href="?mod=profile">
									<div class="row">
	                          	  		<div class="col-lg-2">
											<i class="material-icons">label</i>
										</div>
										<div class="col-lg-8">
											Profile
										</div>
									</div>
								</a>
							</li>   
							<li>
								<a href="#" data-toggle = "modal" data-target = "#logout">
										<div class="row">
											<div class="col-lg-2">
												<i class="material-icons">label</i>
											</div>
											<div class="col-lg-8">
												Logout
											</div>
										</div>
								</a>
							</li>            
						</ul>
					</li>
					
                </ul>
             </div>
		  </div>
		  <div class="col-md-9">
			<div class="content-box-large">
				<?php

					if($_GET[mod]=='show_progress'){
						include "show_prog.php"; 
					}
					elseif($_GET[mod]=='update_progress'){
						include "update_prog.php"; 
					}
					elseif($_GET[mod]=='kontrak_pekerjaan'){
						include "show_kontrak.php";
					}
					elseif($_GET[mod]=='detail_kontrak'){
						include "detail_kontrak.php";
					}
					elseif($_GET[mod]=='bayar_upah'){
						include "bayar_upah.php";
					}					
					elseif($_GET[mod]=='bayar_upah_luar'){
						include "bayar_upah_luar.php";
					}
					elseif($_GET[mod]=='pembayaran'){
						include "pembayaran.php";
					}
					elseif($_GET[mod]=='pengeluaran'){
						include "pengeluaran.php";
					}
					elseif($_GET[mod]=='penarikan_cek'){
						include "penarikan_cek.php";
					}
					elseif($_GET[mod]=='alokasi'){
						include "alokasi.php";
					}
					elseif($_GET[mod]=='kas'){
						include "kas.php";
					}
					elseif($_GET[mod]=='reminder'){
						include "reminder.php";
					}
					elseif($_GET[mod]=='data_teknis'){
						include "showdata_teknis.php";
					}
					elseif($_GET[mod]=='data_kavling'){
						include "kavling.php";
					}
					elseif($_GET[mod]=='detail_kavling'){
						include "showdetail_kavling.php";
					}
					elseif($_GET[mod]=='data_pekerjaan'){
						include "showdata_pekerjaan.php";
					}
					elseif($_GET[mod]=='data_admin'){
						include "show_admin.php";
					}
					elseif($_GET[mod]=='laporan_teknikal'){
						include "laporan_teknikal1.php";
					}
					elseif($_GET[mod]=='data_marketing'){
						include "show_marketing.php";
					}
					elseif($_GET[mod]=='profile'){
						include "profile.php";
					}
					elseif ($_GET[mod]=='logout') {
						?>
						<a href="#" data-toggle = "modal" data-target = "#logout"> Logout</a>
						<?php
					}
					
				?>
			</div>
		  </div>
		</div>
    </div>
    <!-- Modal LOGOUT-->
		<div class = "modal fade" id = "logout" tabindex = "-1" role = "dialog" 
		   aria-labelledby = "myModalLabel" aria-hidden = "true">
		   
		   <div class = "modal-dialog">
		      <div class = "modal-content">
		         
		         <div class = "modal-header">
		            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
		                  &times;
		            </button>
		            
		            <h4 class = "modal-title" id = "myModalLabel">
		               CONFIRM LOGOUT
		            </h4>
		         </div>
		         
		         <div class = "modal-body">
		            Are you sure you want to logout?
		         </div>
		         
		         <div class = "modal-footer">
		            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
		               NO
		            </button>
		            
		            <a href="logout.php"><button type = "button" class = "btn btn-primary" >YES</button> </a>
		         </div>
		         
		      </div><!-- /.modal-content -->
		   </div><!-- /.modal-dialog -->
		  
		</div><!-- /.modal -->
    <footer style="background-color: #795548">
         <div class="container">
            <div class="copy text-center">
               Copyright&copy; <span id="copyright">...</span> <a href='#'>PT Matahari Cipta | Developed by Lunata IT & Consultant</a>
            </div>          
         </div>
      </footer>

    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../js/custom.js"></script>
	
  </body>
</html>
<?php
}
?>