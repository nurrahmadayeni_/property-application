<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> Riwayat Alokasi </h1>";
        echo "
            <table id='gudang' class='table table-bordered table-hover'>
            <thead>
                <tr>  
                    <th width='10%'>No</th>
                    <th width='20%'>Tanggal Alokasi</th>
                    <th width='15%'>Jumlah Alokasi(Rp.)</th>
                    <th width='15%'>Keterangan</th>
                </tr>
            </thead>
            <tbody>
        ";
       
             
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,'SELECT * FROM alokasi_teknikal WHERE debit!=0' );
        $no = 1;
        $tot="";
        while ($r = mysqli_fetch_array($sql)) {
        ?>

        <tr align='center'>

            <td><?= $no; ?></td>            
            <td>
                <?php echo  $r['tgl']; ?>
            </td>
            <td align="right">
                <?php echo  $rupiah=number_format($r['debit'],0,',',','); 
                $tot += $r['debit']; ?>

            </td>
            <td>
                <?php echo  $r['keterangan']; ?>
            </td>
        </tr>
        <?php
            $no++;
        }
        ?>
        </tbody>

    </table> 
    <table class="table"> 
        <tr>  
          <td>Total Alokasi</td>
          <td></td>
          <td></td>
          <td>: <b><?= number_format($tot,0,',',',');?></b></td>
        </tr>
        <tr>  
          <!--<td>Sisa Dana = Dana dari ADM - Total dana biaya Pembangunan</td> -->
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
  
    <script type="text/javascript">
    $(function() {
        $("#gudang").dataTable();
    });
    </script>
  
  <script>
        $(function(){
            $(document).on('click','.edit-gudang',function(e){
                e.preventDefault();
                $("#edit_gudang").modal('show');
                $.post('../action/adm/edit_gudang.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#edit_gudang').on('hidden.bs.modal', function () {
                 location.reload();
                })
            });
        });
    </script>

    <?php
        break;
  }
  ?>
  
<script type="text/javascript">
    $(function() {
        $("#Dgudang").DataTable();
    });
    </script>
