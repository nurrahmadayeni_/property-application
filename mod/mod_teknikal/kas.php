<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> SISA KAS TEKNIKAL </h1>";
        echo "
            <table id='kas' class='table table-bordered table-hover'>
            <thead>
                <tr>  
                    <th width='10%'>No</th>
                    <th width='20%'>Tanggal Transaksi</th>
                    <th width='15%'>Debit(Rp.)</th>
                    <th width='15%'>Kredit(Rp.)</th>
                    <th width='15%'>Sisa Saldo(Rp.)</th>
                    <th width='15%'>Keterangan</th>
                </tr>
            </thead>
            <tbody>
        ";
       
             
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,"SELECT * from saldo_teknikal ");

        $no = 1;

        while ($r = mysqli_fetch_array($sql)) {
            $id = $r['id'];
        ?>

        <tr align='center'>

            <td align="center"><?= $no; ?></td>            
            <td>
                <?php echo  $r['tgl']; ?>
            </td>
            <td align="right">
                <?php echo  $rupiah=number_format($r['debit'],0,',',','); ?>
            </td>
            <td align="right">
                <?php echo  $rupiah=number_format($r['kredit'],0,',',','); ?>
            </td>
            <td align="right">
                <?php echo  $rupiah=number_format($r['sisa_saldo'],0,',',','); ?>
            </td>
            <td>
                <?php echo  $r['keterangan']; ?>
            </td>
        </tr>
        <?php
            $no++;
        }
        ?>
        </tbody>

    </table> 
    
  
    <script type="text/javascript">
    $(function() {
        $("#kas").dataTable();
    });
    </script>
 

    <?php
        break;
  }
 