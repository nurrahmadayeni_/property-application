<?php
	include '../../config/connectdb.php';
	echo "<h3 align='center'> REMINDER </h3>";

	$nexttujuh = mktime(0, 0, 0, date("m"), date("d") + 7, date("Y"));
	$nexttiga = mktime(0, 0, 0, date("m"), date("d") + 3, date("Y"));
	$nextsatu = mktime(0, 0, 0, date("m"), date("d") + 1, date("Y"));
	
	$semingu = date("Y-m-d", $nexttujuh);
	$tiga = date("Y-m-d", $nexttiga);
	$satu = date("Y-m-d", $nextsatu);

	$tempo = "SELECT d.nama_perumahan, d.id_perumahan, k.no_kavling, kp.id_kategorikerja, kp.nama_kategorikerja, k.tgl_selesai 
	from data_perumahan d, kontrak_pekerjaan k, kategori_pekerjaan kp 
	WHERE d.id_perumahan=k.id_perumahan and k.id_kategorikerja=kp.id_kategorikerja";

	$t = mysqli_query($mysqli,$tempo);

	echo "Kontrak telah jatuh tempo pada perumahan :";
	while ($r = mysqli_fetch_array($t)) {
		$sql1 = "SELECT COUNT(pp.progress) as 'siap' 
				FROM data_perumahan p, progress_pembangunan pp, kategori_pekerjaan k , data_pekerjaan d 
				WHERE p.id_perumahan=pp.id_perumahan 
				AND k.id_kategorikerja=d.id_kategorikerja 
				AND d.id_pekerjaan=pp.id_pekerjaan 
				AND pp.id_perumahan='$r[id_perumahan]' 
				AND pp.no_kavling='$r[no_kavling]' 
				AND pp.progress='1' 
				GROUP BY k.id_kategorikerja
		";
		$tampil=mysqli_query($mysqli,$sql1);

		$ro = mysqli_num_rows($tampil);
		$sql2 = "SELECT id_pekerjaan FROM data_pekerjaan WHERE id_kategorikerja=$r[id_kategorikerja]";
		$result=mysqli_query($mysqli,$sql2);
		$h = mysqli_num_rows($result);

		$persen = round(($r['siap'] / $h) *100);

		if($semingu==$r['tgl_selesai'] || $tiga==$r['tgl_selesai'] || $satu == $r['tgl_selesai'] && $persen < 100){
			echo "
			    	<li>
			    		<b>".$r['nama_perumahan'].", kavling : </b>
			    		<b>".$r['no_kavling']." ,</b>
			    		<b> pekerjaan : ".$r['nama_kategorikerja']."
			    		dan tanggal selesai : ".$r['tgl_selesai']."</b>
			    	</li>
			    
			";
		}
	}
?>