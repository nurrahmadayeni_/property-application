<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> Laporan Teknikal </h1>";
?>
	<div class="panel-heading"><p></div>
	<div class="navbar">
		<ul class="nav nav-pills">
			<li class='active'><a href="#cek" data-toggle="tab">Pencairan Cek Giro</a></li>
			<li><a href="#pengeluaran" data-toggle="tab">Laporan Pengeluaran Dana</a></li>
		</ul>
	</div>

	<div class="tab-content">
		<div class="tab-pane active" id="cek">
				<div class="form-group">
					<table class='table table-bordered table-hover'>
						<tr align='center'>
							<td><input type='date' name='tgl1' class='form-control' id="tgl1" required=''></td>
							<td><input type='date' name='tgl2' id="tgl2" class='form-control' required=''></td>
							<td><input type="Submit" id="cek" class="btn btn-primary btn-sm" value="Cari"></td>
						</tr>
					</table>
				</div>
				<div id="penarikan" style="display:none;">
				</div>
		</div>
		<div class="tab-pane" id="pengeluaran">
			<div class="form-group">
				<table class='table table-bordered table-hover'>
					<tr align='center'>
						<td>
							<select name='data_perumahan' id='kperumahan' class='form-control' required=''>
							<option value='' selected disabled="">- Pilih Perumahan -</option>
							<?php
								$getdata="SELECT * FROM data_perumahan";
								$tampil=mysqli_query($mysqli,$getdata);
									while($r3=mysqli_fetch_array($tampil)){
										echo "<option value=$r3[id_perumahan]>
										$r3[nama_perumahan]</option>";
									}
							?>
							</select>
						</td>
						<td><input type='date' name='tgl11' class='form-control' id="tgl11" required=''></td>
							<td><input type='date' name='tgl22' id="tgl22" class='form-control' required=''></td>
						<td><input type="Submit" class="btn btn-primary btn-sm" id="keluar" value="Cari"></td>
					</tr>
				</table>
			</div>
			<div id="Lpengeluaran"></div>
		</div>

	</div>
		
<?php
 	}
?>
<script>
    $(document).on('click','#cek',function(){
            var tgl_awal = $('#tgl1').val();
            var tgl_akhir = $('#tgl2').val();
			$.ajax({

                url: '../action/teknikal/rekap_cek.php',
                data: {
                	tgl_1:tgl_awal,
                	tgl_2:tgl_akhir
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#penarikan').html();  
                    $('#penarikan').show(); 
                    $('#penarikan').html(result); 
                }
            });
    });
</script>

<script>
    $(document).on('click','#keluar',function(){
            var perum = $('#kperumahan').val();
            var tgl_awal = $('#tgl11').val();
            var tgl_akhir = $('#tgl22').val();

			$.ajax({

                url: '../action/teknikal/pengeluaran.php',
                data: {
                	v1:perum,
                	v3:tgl_awal,
                	v4:tgl_akhir
                },
                type :'GET',
                dataType: 'html',
                success: function(result){
                    $('#Lpengeluaran').html();  
                    $('#Lpengeluaran').html(result); 
                }
            });
    });
</script>