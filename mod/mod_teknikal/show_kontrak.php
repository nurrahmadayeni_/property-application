<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h4 align=center> KONTRAK PEKERJAAN</h4>";
?>
    <script> 
        $(document).ready(function() {
                $('#perumahann').change(function(){
                var val = $("#perumahann").val();
                    $.ajax({
                      url: '../action/teknikal/show_kavling2.php',
                      data: {id_perumahan:val},
                      type: 'GET',
                      dataType: 'html',
                      success: function(result){
                        $('#no_kavling').html(); 
                        $('#no_kavling').html(result); 
                      }
                    });
                });

                $('#submitt').click(function(){
                    var val1 = $('#perumahann').val();
                    var val2 = $('#no_kavling').val();
                    $("#submitt").val("LOADING. . . .");
                    $.ajax({
                        url: '../action/teknikal/show_kontrak.php',
                        data: {id1:val1,id2:val2},
                        type: 'GET',
                        success: function(r){
                            $("#submitt").val("SEARCH");

                            $("#kontrak").slideUp(500, function(){
                               $("#kontrak").html(r);
                               $("#kontrak").slideDown(500); 
                            });
                        }
                    });
                  });
            });
    </script>
    <div class="form-group">
        <label for="perumahan" class="control-label">Pilih Perumahan : </label>
        <select name="perumahan" id="perumahann" class="form-control">
            <option value='' selected disabled='disabled'>- Pilih Perumahan -</option>
                <?php
                    $getdata="SELECT * FROM data_perumahan";
                    $tampil=mysqli_query($mysqli,$getdata);
                    
                    while($r=mysqli_fetch_array($tampil)){
                        echo "<option value=$r[id_perumahan]>$r[nama_perumahan]</option>";
                }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="no_kavling" class="control-label">Pilih Kavling :</label>
        <select name="no_kavling" class="form-control" id="no_kavling">
        <option value='' selected disabled='disabled'>- Pilih Kavling -</option></select>
    </div>     
    <div class="form-group">
        <input type='submit' value='SEARCH' class="form-control btn-primary" id="submitt">
    </div>

    <div id="kontrak"></div>

    <span class="container">
       <a class="btn btn-primary " data-toggle="modal" href="#tambahkontrak" alt='tambah'><span class="glyphicon glyphicon-plus"></span> Kontrak Pekerjaan</a>
    </span>
        
     <!-- Modal for Tambah Kontrak-->
    <div id="tambahkontrak" class="modal fade bs-example-modal-lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Kontrak</h4>
                </div>
                <div class="modal-body">    
                    <form method="post" action='../action/teknikal/act_kontrak.php?mod=teknikal&act=tambahkontrak'>
                    <div class="form-group">
                        <label for="perumahan" class="control-label">Pilih Perumahan : </label>
                        <select name="perumahan" id="Perumahan" class="form-control">
                            <option value='' selected disabled='disabled'>- Pilih Perumahan -</option>
                            <?php
                                $getdata="SELECT * FROM data_perumahan";
                                $tampil=mysqli_query($mysqli,$getdata);
                                while($r=mysqli_fetch_array($tampil)){
                                    echo "<option value='$r[id_perumahan]''>$r[nama_perumahan]</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="no_kavling" class="control-label">Pilih Kavling :</label>
                        <select name="no_kavling" class="form-control" id="no_kavling2">
                            <option value='' selected disabled='disabled'>- Pilih Kavling -</option>
                        </select>
                    </div>      
                    <div class="form-group">
                        <label for="kontraktor" class="control-label">Kontraktor :</label>
                        <input type="text" class="form-control" id="kontraktor" name="kontraktor" required=''>
                      </div>
                    <div class="form-group">
                        <label for="nama_pegawai" class="control-label">Pengawas :</label>
                         <input type="text" class="form-control" id="nama_pegawai" name="nama_pegawai" required=''>
                    </div>
                    
                      <div class="form-group">
                        <label for="tipe-pekerjaan" class="control-label">Tipe Pekerjaan : </label>
                        <select name='tipe_krj' id='tipe_krj' class='form-control' required=''>
                        <option value='' selected>- Pilih Tipe Pekerjaan -</option>
                        <?php
                        $getkategori="SELECT * FROM kategori_pekerjaan";
                        $tampil=mysqli_query($mysqli,$getkategori);
                        while($r=mysqli_fetch_assoc($tampil))
                        {
                            echo "<option value=$r[id_kategorikerja]>
                            $r[nama_kategorikerja]</option>";
                        }
                        ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="nilai" class="control-label">Nilai Kontrak :</label>
                        <input type="text" class="form-control" id="nilai" name="nilai" required=''>
                        <script type="text/javascript">
                            $(document).ready(function(){
                                $('#nilai').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
                            });
                        </script>
                      </div>
                      <div class="form-group">
                        <label for="tanggal" class="control-label">Tanggal Siap :</label>
                        <input type="date" class="form-control" id="tgl" name="tgl" required=''>
                      </div>
            
                      <div class="modal-footer">
                        <input type="Submit" class="btn btn-primary" value="Submit">
                        <input type="reset" class="btn btn-default" value="Reset">
                    </div>
                    </form>
                </div>
                </div>
            </div>
    </div>

    <script>
	  $(document).on('change','#Perumahan',function(){
				 var val = $(this).val();
				 $.ajax({
					   url: '../action/teknikal/show_kavling.php',
					   data: {id_perumahan:val},
					   type: 'GET',
					   dataType: 'html',
					   success: function(result){
							$('#no_kavling2').html(); 
							$('#no_kavling2').html(result); 
					   }
				  });
	  });
    </script>
<?php
	}
?>