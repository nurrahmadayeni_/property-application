<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center>PROGRESS AKUMALASI SETIAP PERUMAHAN</h3>
        ";

        include "../action/teknikal/show_akumulasi.php";
        echo "<h3 align=center>PROGRESS PEMBANGUNAN KAVLING</h3>";
?>

<script>
    $(document).on('change','#Perumahan',function(){
        var val = $("#Perumahan").val();
        $.ajax({
          url: '../action/teknikal/show_kavling.php',
          data: {id_per:val},
          type: 'GET',
          dataType: 'html',
          success: function(result){
            $('#no_kavling').html(); 
            $('#no_kavling').html(result); 
          }
        });
    });

    $(document).on('click','#submitt',function(){
            var val1 = $('#Perumahan').val();
            var val2 = $('#no_kavling').val();
            $.ajax({
                url: '../action/teknikal/show_prog.php',
                data: {
                  id_perumahan:val1,
                  no_kavling:val2
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#show_prog').html();  
                    $('#show_prog').html(result); 
                    $('[data-toggle="tooltip"]').tooltip({trigger: 'manual'}).tooltip('show');
                    $(".progress-bar").each(function(){
                      each_bar_width = $(this).attr('aria-valuenow');
                      $(this).width(each_bar_width + '%');
                    });
                }
            });
    });
    
</script>

      <div class="form-group">
          <label for="perumahan" class="control-label">Pilih Perumahan : </label>
          <select name="perumahan" id="Perumahan" class="form-control">
              <option value='' selected disabled='disabled'>- Pilih Perumahan -</option>
              <?php
                  $getdata="SELECT * FROM data_perumahan";
                  $tampil=mysqli_query($mysqli,$getdata);
                  while($r=mysqli_fetch_array($tampil)){
                      echo "<option value=$r[id_perumahan]>
                      $r[nama_perumahan]</option>";
                  }
              ?>
        </select>
      </div>
      <div class="form-group">
          <label for="no_kavling" class="control-label">Pilih Kavling :</label>
          <select name="no_kavling" class="form-control" id="no_kavling">
              <option value='' selected disabled='disabled'>- Pilih Kavling -</option>
          </select>
      </div>     
      <div class="form-group">
        <input type='submit' value='SHOW' class="form-control" id="submitt">
      </div>

        <table class="table table-hover" id="show_prog" width="100%"> </table> 
        
<?php
  }
?>

