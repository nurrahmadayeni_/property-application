<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h4 align=center> DAFTAR PERUMAHAN </h4>";
        echo "
            <table id='order' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th width='3%'>No</th>
                    <th width='15%'>Nama perumahan</th>
					<th width='40%'>Deskripsi Perumahan</th>
                    <th width='25%'>Spesifikasi Teknis</th>
                    <th width='5%'>Aksi Edit</th>
					<th width='10%'>Tampil Gambar Perumahan</th>
                </tr>
            </thead>
            <tbody>
        ";
       
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,'SELECT * FROM data_perumahan');
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
		$id=$r['id_perumahan'];
			?>
			<tr align='left'>
            <td>
                <?php echo  $no;?>
            </td>
            <td>
				<?php echo  $r['nama_perumahan']; ?>
            </td>
            <td>
                <?php echo  $r['deskripsi']; ?>
            </td>
			<td>
                <?php echo  $r['spesifikasi_teknik']; ?>
            </td>
            <td align='center'>
				<a href="#editrumah" class="edit-record" data-toggle="modal" title="edit" data-id="<?php echo $id; ?>">
                <i class="material-icons btn btn-primary">edit</i></a>
            </td>
			<td align='center'>
				<a href="#showimage" class="edit-record3" data-toggle="modal" data-id="<?php echo $r['id_perumahan']; ?>">
                <i class="material-icons btn btn-primary">remove_red_eye</i></a>
			</td>
        </tr>
		
		<?php
            $no++;
        }
        ?>

        </tbody>

    </table>  
   
	<!-- Modal for Edit Perumahan-->
    <div class="modal fade" id="editrumah" tabindex="-1" role="dialog" aria-labelledby="detail" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Perumahan</h4>
                </div>
                <div class="modal-body">
                    
                </div>
            </div>
        </div>
    </div>
	
	<!-- Modal for Show Image Perumahan-->
    <div id="showimage" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Gambar Perumahan</h4>
                </div>
                <div class="modal-body">
				
				</div>
				
            </div>
        </div>
    </div>
	
	<!-- Modal for Edit image Perumahan-->
    <div id="tambahimage" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Image Perumahan</h4>
                </div>
                <div class="modal-body">
                    
                </div>
            </div>
        </div>
    </div>
	
    <script type="text/javascript">
    $(function() {
        $("#order").dataTable();
    });
    </script>

    <?php

	}
	?>

	<script>
        $(function(){
            $(document).on('click','.edit-record',function(e){
                e.preventDefault();
                $("#editrumah").modal('show');
                $.post('../action/teknikal/edit_rumah.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                
            });
        });
    </script>

	<script>
        $(function(){
            $(document).on('click','.edit-record3',function(e){
                e.preventDefault();
                $("#showimage").modal('show');
                $.post('../action/teknikal/showimage.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                
            });
        });
    </script>
	
	<script>
        $(function(){
            $(document).on('click','.edit-record5',function(e){
                e.preventDefault();
                $("#tambahimage").modal('show');
                $.post('../action/teknikal/add_image.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                
            });
        });
    </script>
