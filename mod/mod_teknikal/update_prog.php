<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center>UPDATE PROGRESS</h3>";
?>
        <script>
			
            $(document).on('change','#Perumahan',function(){
                var val = $(this).val();
                $.ajax({
                  url: '../action/teknikal/show_kavling.php',
                  data: {id_per:val},
                  type: 'GET',
                  dataType: 'html',
                  success: function(result){
                    $('#no_kavling').html(); 
                    $('#no_kavling').html(result); 
                  }
                });
            });
			
            $(document).ready(function() {
				$('#Perumahan, #no_kavling').change(function(){
					var val2 = $("#Perumahan").val();
					var val = $("#no_kavling").val();
					$.ajax({
					  url: '../action/teknikal/show_kategori_kerja.php',
					  data: {no_kvl:val, id_per:val2},
					  type: 'GET',
					  dataType: 'html',
					  success: function(result){
						$('#kategori_kerja').html(); 
						$('#kategori_kerja').html(result); 
					  }
					});
				});
				
                $('#kategori_kerja').change(function(){
                    var val1 = $('#kategori_kerja').val();
                    var val2 = $('#Perumahan').val();
                    var val3 = $('#no_kavling').val();

                    $('.tmpil').show();
                    $('#foto').show();

                    $.ajax({
                        url: '../action/teknikal/show_pekerjaan.php',
                        data: {id1:val1,id2:val2,id3:val3},
                        type: 'GET',
                        dataType    : 'json',
                        success: function(response){
                          $("#tabel-input").html("");

                          var tbl_pekerjaan = $("#tabel-input");

                          var trHTML = '';
                          var string = '';
                          var progresss ='';

                          var head = "<tr><td><b>Jenis Pekerjaan</b></td><td><b>Progress</b></td></tr>";
                          $('#tabel-input').append(head);
                          $.each(response, function(key, item) {

                            var tr = $('<tr/>'),
                              td_input = $('<td/>');

                            tr.append($('<td/>', {
                              text: item.nama_pekerjaan
                            }));

                            var td_input = $("<td/>");
                            td_input.append($('<input/>', {
                              type: 'hidden',
                              name: 'id_progress',
                              value: item.id_progress
                            }));

                            if(item.progress == 0){
                              td_input.append($('<input/>', {
                                type: 'checkbox',
                                name: 'progress[' + item.id_progress + ']',
                                value: 1,
                                center:true,
                                checked: false
                              }));
                            }else{
                              td_input.append($('<input/>', {
                                type: 'checkbox',
                                name: 'progress[' + item.id_progress + ']',
                                value: 1,
                                disabled:true,
                                center:true,
                                checked: true
                              }));
                            }

                            tr.append(td_input);
							$("#tabel-input").show().slideUp(100, function(){
							   $("#tabel-input").append(tr);
							   $("#tabel-input").slideDown(300); 
							});
                          });
						  
						 
						  
                        }
                    });
              });
            })
        </script>
        <form action="../action/teknikal/act_progress.php" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <label for="perumahan" class="control-label">Pilih Perumahan : </label>
            <select name="perumahan" id="Perumahan" class="form-control">
                  <option value='' selected disabled='disabled'>- Pilih Perumahan -</option>
                  <?php
                      $getdata="SELECT * FROM data_perumahan";
                      $tampil=mysqli_query($mysqli,$getdata);
                      while($r=mysqli_fetch_array($tampil)){
                          echo "<option value=$r[id_perumahan]>
                          $r[nama_perumahan]</option>";
                      }
                  ?>
            </select>
          </div>

          <div class="form-group">
            <label for="no_kavling" class="control-label">Pilih Kavling :</label>
            <select name="no_kavling" class="form-control" id="no_kavling">
              <option value='' selected disabled='disabled'>- Pilih Kavling -</option>
            </select>
          </div>

          <div class="form-group">
            <label for="kategori_kerja" class="control-label">Pilih Kategori Kerja :</label>
            <select name="kategori_kerja" id="kategori_kerja" class=" form-control" required>
                <option value='' selected disabled='disabled'>- Pilih Bagian Pekerjaan -</option>
            </select>
          </div>
          
          <div class="form-group">
              <table class='form-group table table-hover' id='tabel-input' style="display:none; width:100%;">
                <tr><b><td><label class='label-control'>Jenis Pekerjaan</label></td>
                  <td align='center' ><label class='label-control'>Progress </label></td></b></tr>
              </table>
              
              <input type="file" class="form-control" style='display:none;' id="foto" id="foto" name="foto" required/>   
          </div> 
          <div class="form-group">
            <input type='submit' class="form-control btn-primary" value='UPDATE' name="sub" width="50%"> 
            
            <a href='?mod=show_progress'> <button class='form-control btn btn-default' value='CANCEL'> CANCEL </button></a>
          </div>
      </form>

<?php
  }
?>