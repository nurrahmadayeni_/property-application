<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center>Pembayaran Upah Kerja</h3><table width='100%'' align='center'>";
?>

<script>
	$(document).ready(function() {
		$('#Perumahan').change(function(){
			var val = $("#Perumahan").val();
			$.ajax({
			  url: '../action/teknikal/show_kavling.php',
			  data: {id_per:val},
			  type: 'GET',
			  dataType: 'html',
			  success: function(result){
				$('#no_kavling').html(); 
				$('#no_kavling').html(result); 
			  }
			});
		});
		
		$('#Perumahan, #no_kavling').change(function(){
			var val2 = $("#Perumahan").val();
			var val = $("#no_kavling").val();
			$.ajax({
			  url: '../action/teknikal/show_kategori_kerja.php',
			  data: {no_kvl:val, id_per:val2},
			  type: 'GET',
			  dataType: 'html',
			  success: function(result){
				$('#kategori_kerja').html(); 
				$('#kategori_kerja').html(result); 
			  }
			});
		});
		
		$('#submitt').click(function(){
			var val1 = $('#Perumahan').val();
			var val2 = $('#no_kavling').val();
			var val3 = $('#kategori_kerja').val();
			
			$("#submitt").val("LOADING. . . .");
			$.ajax({
				url: '../action/teknikal/detail_bayar_upah.php',
				data: {
					v1:val1,
					v2:val2,
					v3:val3
					},
				type: 'GET',
				success: function(r){
					$("#submitt").val("SEARCH");
					$("#form-bayar").slideUp(500, function(){
					   $("#form-bayar").html(r);
					   $("#form-bayar").slideDown(500); 
					});
				}
			});
		});
	});
	
</script>

	<div class="form-group">
		<label for="perumahan" class="control-label">Pilih Perumahan : </label>
		<select name="perumahan" id="Perumahan" class="form-control">
		  <option value='' selected disabled='disabled'>- Pilih Perumahan -</option>
		  <?php
			  $getdata="SELECT * FROM data_perumahan";
			  $tampil=mysqli_query($mysqli,$getdata);
			  while($r=mysqli_fetch_array($tampil)){
				  echo "<option value=$r[id_perumahan]>
				  $r[nama_perumahan]</option>";
			  }
		  ?>
		</select>
	</div>
	<div class="form-group">
		<label for="no_kavling" class="control-label">Pilih Kavling :</label>
		<select name="no_kavling" class="form-control" id="no_kavling">
		  <option value='' selected disabled='disabled'>- Pilih Kavling -</option>
		</select>
	</div>     
	<div class="form-group">
		<label for="kategori_kerja" class="control-label">Pilih Kategori Kerja :</label>
		<select name="kategori_kerja" id="kategori_kerja" class=" form-control">
			<option value='' selected disabled='disabled'>- Pilih Bagian Pekerjaan -</option>
			
		</select>
	</div>
	  
	<div class="form-group">
		<input type='submit' value='SEARCH' class="form-control btn-primary" id="submitt">
	</div>
	<div id='form-bayar'> </div>
<?php
  }
?>

