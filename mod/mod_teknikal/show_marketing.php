<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> DATA MARKETING </h1>";
?>
	<div class="panel-heading"><p></div>
	<div class="navbar">
		<ul class="nav nav-pills">
			<li class="active"><a href="#tab1" data-toggle="tab">Jadwal Akad dan Kavling</a></li>
			<li><a href="#tab2" data-toggle="tab">Tanggal Serah Terima</a></li>
			<li><a href="#tab3" data-toggle="tab">Data Konsumen</a></li>
			<li><a href="#tab4" data-toggle="tab">Data Kavling</a></li>
		</ul>
	</div>

	<div class="tab-content">
		<div class="tab-pane active" id="tab1">
			<div class="form-group">
				<select name='laporan_gudang' id='klgudang' class='form-control' required=''>
                    <option value='' selected disabled="">- Pilih Gudang -</option>
                    <?php
                        $getdata="SELECT * FROM data_gudang ORDER BY nama_gudang ASC";
                        $tampil=mysqli_query($mysqli,$getdata);
                        while($r=mysqli_fetch_array($tampil)){
                            echo "<option value=$r[id_gudang]>
                            $r[nama_gudang]</option>";
                        }
                    ?>
                </select>
			</div>
			<div id="Lgudang"></div>
		</div>
		
		<div class="tab-pane" id="tab2">
			<div class="form-group">
				<select name='kategori_inventaris' id='kInventaris' class='form-control' required=''>
                    <option value='' selected disabled="">- Pilih Kategori Inventaris -</option>
                    <?php
                        $getdata="SELECT * FROM kategori_inventaris";
                        $tampil=mysqli_query($mysqli,$getdata);
                        while($r=mysqli_fetch_array($tampil)){
                            echo "<option value=$r[id_kategori]>
                            $r[kategori_inventaris]</option>";
	                    }
                    ?>
               	</select>
			</div>
			<div id="Linventaris">
			</div>
		</div>
		
		<div class="tab-pane" id="tab3">
			<div class="form-group">
				<div class="row">
			  		<div class="col-md-12 panel-warning">
			  			<div class="content-box-header">
		  					<div class="panel-title ">Data Konsumen</div>
			  			</div>
			  			<div class="content-box box-with-header">
			  				<table id='Dpengawas' class='table table-bordered table-hover'>
				            <thead>
				                <tr>
				                    <th width='10%'>No</th>
				                    <th width='20%'>Nama Pembeli</th>
				                    <th width='20%'>Alamat </th>
				                    <th width='20%'>No.telp/Hp</th>
				                </tr>
				            </thead>
				            <?php
				            	include '../../config/connectdb.php';

						        $sql = mysqli_query($mysqli,'SELECT * FROM data_pembeli');
						        $no = 1;
						        while ($q = mysqli_fetch_array($sql)) {
						        	$id = $q['id_pembeli'];
						    ?>
				            <tbody>
								<tr align='left'>		
									<td align="center"> <?php echo  $no;?> </td>
									<td><?php echo  $q['nama_pembeli']; ?></td>
									<td><?php echo  $q['alamat_pembeli']; ?></td>
									<td><?php echo  $q['notelp_pembeli']; ?></td>			
								</tr>
							<?php
								$no++;
							} 
							?>
								<tr align="right">
									<td colspan='4'><button type="button" class="btn btn-primary" onclick="print_d()" >
									<span class="glyphicon glyphicon-print"></span> Print</button></td>
								</tr>
				            </tbody>
				            </table>

						</div>
			  		</div>
			  	</div>
			</div>
		</div>
		<div class="tab-pane" id="tab4">
			<div class="form-group">
				<table class='table table-bordered table-hover'>
					<tr align='center'>
						<td>
							<select name='data_perumahan' id='kperumahan' class='form-control' required=''>
							<option value='' selected disabled="">- Pilih Perumahan -</option>
							<?php
								$getdata="SELECT * FROM data_perumahan";
								$tampil=mysqli_query($mysqli,$getdata);
									while($r3=mysqli_fetch_array($tampil)){
										echo "<option value=$r3[id_perumahan]>
										$r3[nama_perumahan]</option>";
									}
							?>
							</select>
						</td>
						<td>
							<select name='data_kavling' id='Kkavling' class='form-control' required=''>
							<option value='' selected disabled="">- Pilih Kavling -</option>
								<?php
									$getdata="SELECT * FROM data_kavling";
									$tampil=mysqli_query($mysqli,$getdata);
									while($r=mysqli_fetch_array($tampil)){
										echo "<option value=$r[no_kavling]>
										$r[no_kavling]</option>";
									}
								?>
							</select>
						</td>
						<td><input type="Submit" class="btn btn-primary btn-sm" id="search" value="Cari"></td>
					</tr>
				</table>
			</div>
			<div id="LKebutuhan"></div>
		</div>

	</div>
		
<?php
 	}
?>
<script>
    $(document).on('change','#klgudang',function(){
            var val = $(this).val();
			$.ajax({

                url: '../action/adm/show_Lgudang.php',
                data: {laporan_gudang:val},
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#Lgudang').html();  
                    $('#Lgudang').html(result); 
                }
            });
    });
</script>

<script>
    $(document).on('change','#kInventaris',function(){
            var val = $(this).val();
			$.ajax({

                url: '../action/teknikal/show_DInventaris.php',
                data: {kategori_inventaris:val},
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#Linventaris').html();  
                    $('#Linventaris').html(result); 
                }
            });
    });
</script>

<script>
    $(document).on('click','#search',function(){
            var perum = $('#kperumahan').val();
            var kavl = $('#Kkavling').val();

			$.ajax({

                url: '../action/adm/show_LKebutuhan.php',
                data: {
                	per:perum,
                	kav:kavl
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#LKebutuhan').html();  
                    $('#LKebutuhan').html(result); 
                }
            });
    });
</script>

<script>
	function print_d(){
		<?php
			$id = $q['id_pembeli'];
		
		echo "window.open('../action/adm/printlaporan2.php?id=$id','_blank');";
		?>
	}
</script>