<?php
    error_reporting(0);
    switch($_GET[act]){
        default: ?>
<h3 align=center> DETAIL KONTRAK </h3>
<table id="detail_kontrak" class="table table-bordered table-hover">
    <thead>
        <tr align='center'>
            <th width="5%">No</th>
            <th width="10%">Kontraktor</th>  
			<th width="10%">Pengawas</th> 
            <th width="10%">Pekerjaan</th>
            <th width="15%">Nilai Kontrak(Rp.)</th>
            <th width="12%">Upah Cair(Rp.)</th>
            <th width="12%">Sisa Upah(Rp.)</th>
            <th width="12%">Tanggal Siap </th>
            <th width="15%">Detail</th>
        </tr>
     </thead>
        <tbody align="center">
            <?php
                error_reporting(0);

                $id_perumahan = $_GET['idp'];
                $no_kavling = $_GET['idk'];
                $pengawas = $_GET['idpp'];

                $sql = mysqli_query($mysqli,"SELECT k.id_kontrak, k.pengawas, p.nama_perumahan,p.id_perumahan, k.no_kavling, kp.nama_kategorikerja, k.nilai_kontrak, k.kontraktor, k.tgl_selesai, k.id_kategorikerja
                    FROM kontrak_pekerjaan k, data_perumahan p, 
                    kategori_pekerjaan kp 
                    WHERE k.id_perumahan=p.id_perumahan 
                    AND k.id_kategorikerja=kp.id_kategorikerja 
                    AND k.id_perumahan='$id_perumahan' AND k.no_kavling='$no_kavling' ");

                $rumah = $mysqli->query(
                        "SELECT nama_perumahan 
                        FROM data_perumahan
                        WHERE id_perumahan = '$_GET[idp]'
                        ")->fetch_object()->nama_perumahan;

                echo "Perumahan : <b>".$rumah."</b><br/>";
                echo "No Kavling : <b>".$_GET[idk]."</b><br>";
                //echo "Pengawas : <b>".$pengawas."</b><br><br>";
                
                $total="";
                $totals="";
                $totalss="";

                $no = 1;
                while ($r = mysqli_fetch_array($sql)) {
                    $id = $r['id_kontrak'];

                    echo"
                        <tr>
                        <td>$no</td>
                        <td>$r[kontraktor]</td>
						<td>$r[pengawas]</td>
                        <td>$r[nama_kategorikerja]</td>";
                        
                        $nilai = number_format($r[nilai_kontrak],0,',',',');

                    echo "
                    <td align='right'>$nilai</td>
                    ";

                    $upah = $mysqli->query(
                            "SELECT sum(harga_upah) as 'upah' FROM progress_pembayaran_kontrak
                            WHERE id_kontrak = '$id' 
                            ")->fetch_object()->upah;

                    if(isset($upah)){
                        echo "<td align='right'>".number_format($upah,0,',',',')."</td>";    
                    } else{
                        echo "<td align='right'>0</td>";    
                    }
                    
                    $sisa = $r[nilai_kontrak] - $upah;
                    $sisaa = number_format($sisa,0,',',',');


                    echo "
                        <td align='right'> $sisaa </td>
                        <td>$r[tgl_selesai]</td>";


                    ?>
			
					
                    <td><a class='tambah-record btn btn-primary btn-sm' data-idp="<?php echo $_GET['idp']; ?>" data-idk="<?php echo $_GET['idk'] ?>" data-idpp="<?php echo $_GET['idpp']; ?>">
						<i class="material-icons">fiber_new</i>
						</a>
                        <a class='next-record btn btn-primary btn-sm' data-idp="<?php echo $_GET['idp']; ?>" data-idk="<?php echo $_GET['idk'] ?>" data-idkp="<?php echo $id; ?>" 
						data-idkk="<?php echo $r['nama_kategorikerja']; ?>" data-sisa="<?php echo $sisa; ?>"><i class="material-icons">next_week</i></a>
                    </td>
                    <?php
                    
                    $total+=$r['nilai_kontrak'];
                    $totals+=$upah;
                    $totalss+=$sisa;

                    $no++;
               }

                ?>
        </tbody>
                <tr>
                    <td colspan="4" align="center">Total</td>
                    <td align="right"><?php echo number_format($total,0,',',',') ?></td>
                    <td align="right"><?php echo number_format($totals,0,',',',') ?></td>
                    <td align="right"><?php echo number_format($totalss,0,',',',') ?></td>
                    
                   
                    <td></td>
                    <td></td>
                </tr>
           
            </table>
    
        <tr>

        <td>&nbsp; &nbsp;</td>
        <td>
        <a href='index.php?mod=kontrak_pekerjaan' class='btn btn-primary'>Back</a>
        </td>
        </table>

    <script type="text/javascript">        
             $("#detail_kontrak").dataTable();
			 $(document).on('click','.tambah-record',function(e){
                e.preventDefault();

                var val1=$(this).attr('data-idp');
                var val2 = $(this).attr('data-idk');
                var val3 = $(this).attr('data-idpp');

                $("#tambah_kontrak").modal('show');
                $.post('../action/teknikal/act_kontrak_new.php',
                    {v1: val1, v2: val2, v3:val3},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
				$('#tambah_kontrak').on('hidden.bs.modal', function () {
                    location.reload();
                })
            });
			
			$(document).on('click','.next-record',function(e){
                e.preventDefault();

                var val1 = $(this).attr('data-idp');
                var val2 = $(this).attr('data-idk');
                var val3 = $(this).attr('data-idkp');
				var val4 = $(this).attr('data-idkk');
				var val5 = $(this).attr('data-sisa');

                $("#tambah_kontrak").modal('show');
                $.post('../action/teknikal/act_kontrak_lanjutan.php',
                    {v1: val1, v2: val2, v3:val3, v4: val4, v5: val5},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
				$('#tambah_kontrak').on('hidden.bs.modal', function () {
                    location.reload();
                })
            });
    </script>
	<div class="modal fade" id="tambah_kontrak" tabindex="-1" role="dialog" aria-labelledby="detail" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Form Kontrak</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
	<div class="modal fade" id="tambah_kontrak_lanjutan" tabindex="-1" role="dialog" aria-labelledby="detail" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Kontrak Lanjutan</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
<?php

    }
    ?>