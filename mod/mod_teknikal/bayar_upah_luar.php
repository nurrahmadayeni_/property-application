<?php
    //database connection 
    
    $id = $_POST["perumahan"]; // perumahan
    $id2 = $_GET["no_kavling"]; // kavling
    $id_kategorikerja = $_POST['kategori_kerja'];    

    echo "<h3 align=center> Pembayaran Upah </h3><br/>";
?>
<script>
    $(function(){
        $("#bayar").click(function(event){
            event.preventDefault();

            $.ajax({
                    url:'../action/teknikal/act_kontrak.php?mod=teknikal&act=bayar_upah',
                    type:'POST',
                    data:$('#form').serialize(),
                    success:function(result){
                        $("#response").show();
                        $("#response").html(result);
                        $("#response").fadeIn(3000);
                        //$("#div3").fadeIn(3000);
                        $( "#bayar" ).prop( "disabled", true );
                        $( "#upah" ).prop( "disabled", true );
                        $( "#keterangan" ).prop( "disabled", true );
                        $( "#cetak" ).prop( "disabled", false );
                        $( "#cetak" ).focus();
                    }

            });
        });
    });

</script>

    <form id='form' action="#" method="post">
    <style>
        .table.no-border tr td, .table.no-border tr th {
          border-width: 0;
        }
    </style>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6" >
                <div class="row">
                    <div class="col-xs-4">Kontraktor</div>
                    <div class="col-xs-4"><b>:<?= $id4;?></b></div>
                </div>
            </div>
            <div class="col-md-6" >
                <div class="row">
                    <div class="col-xs-4">Pekerjaan</div>
                    <div class="col-xs-4"><b>:<?= $id5; ?></b></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" >
                <div class="row">
                    <div class="col-xs-4">Pekerjaan Siap </div>
                    <div class="col-xs-4">
                    <?php
                        $id_k = $mysqli->query(
                            "SELECT id_kategorikerja 
                            FROM kategori_pekerjaan
                            WHERE nama_kategorikerja = '$id5'
                            ")->fetch_object()->id_kategorikerja;

                        $kesiapan = "SELECT pp.id_pekerjaan, d.nama_pekerjaan from data_pekerjaan d, progress_pembangunan pp WHERE d.id_kategorikerja in (select id_kategorikerja from kontrak_pekerjaan ) and pp.id_pekerjaan=d.id_pekerjaan and pp.progress='1' and pp.id_perumahan='$id' AND pp.no_kavling='$id2' AND d.id_kategorikerja='$id_k'";

                        $t = mysqli_query($mysqli,$kesiapan);
                        $siap = mysqli_num_rows($t);
                        
                        while ($r = mysqli_fetch_array($t)) {
                            echo "<b><li>".$r['nama_pekerjaan']."</li></b>";
                        }
                    ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6" >
                <div class="row">
                    <div class="col-xs-4">Progress Pembangunan</div>
                    <div class="col-xs-4">
                        <?php
                            $tot = "SELECT pp.id_pekerjaan, d.nama_pekerjaan from data_pekerjaan d, progress_pembangunan pp WHERE d.id_kategorikerja in (select id_kategorikerja from kontrak_pekerjaan ) and pp.id_pekerjaan=d.id_pekerjaan and pp.id_perumahan='$id' AND pp.no_kavling='$id2' AND d.id_kategorikerja='$id_k'";

                            $total = mysqli_query($mysqli,$tot);
                            $siap_t = mysqli_num_rows($total);

                            $persen = round(($siap / $siap_t) *100);
                            $persens = "$persen"."%";
                            echo "<b>:".$persens."</b>";
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" >
                <div class="row">
                    <div class="col-xs-4">Nilai Kontrak</div>
                    <div class="col-xs-4">
                        <?php
                            $nilai_kontrak = $mysqli->query(
                            "SELECT nilai_kontrak FROM kontrak_pekerjaan
                            WHERE id_perumahan = '$id' 
                            AND no_kavling = '$id2' AND id_kategorikerja='$id_k'
                            ")->fetch_object()->nilai_kontrak;

                            $nilai_k = number_format($nilai_kontrak,0,',','.');
                            echo "<b>:".$nilai_k."</b>";
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6" >
                <div class="row">
                    <div class="col-xs-4">Uang sesuai progress</div>
                    <div class="col-xs-4">
                        <?php
                            $biaya_progress = ($persen/100) * $nilai_kontrak;
                            $nilai = number_format($biaya_progress,0,',','.');

                            echo "<b>:".$nilai."</b>";
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" >
                <div class="row">
                    <div class="col-xs-4">Upah yang telah dibayar</div>
                    <div class="col-xs-4">
                        <?php
                            $tot = $mysqli->query(
                            "SELECT total_ambilan FROM progress_pembayaran_kontrak 
                            WHERE id_kontrak='$id3' 
                            ORDER BY id_progress_pembayaran DESC limit 1
                            ")->fetch_object()->total_ambilan;

                            $upah_tot = number_format($tot,0,',','.');
                            echo "<b>:".$upah_tot."</b>";
                        ?>
                        
                    </div>
                </div>
            </div>
        </div>
        
    </div>

    <div class="form-group">
        <input type="hidden" name="kontraktor" class="form-control"
         value= <?= $id4; ?> >
         <input type="hidden" name="id_kontrak" class="form-control
         " value= <?= $id3; ?> >
      </div>
  
    <div class="form-group">
        <input type="hidden" name="pekerjaan" class="form-control" value= <?= $id5; ?> >
    </div>

    <div class="form-group">
        <input type="hidden" name="progress" value=<?= $persen ?> >
    </div>

    <div class="form-group">
        <input type="hidden" name="nilai_kontrak" value=<?= $nilai_kontrak ?> >
    </div>

    <div class="form-group">
        <input type="hidden" name="biaya_progress" value=<?= $biaya_progress ?> >
    </div>

    <div class="form-group">
        <label for="keterangan" class="control-label">Keterangan/Kemajuan Pekerjaan</label>
        <textarea class="form-control" name="keterangan" id="keterangan"></textarea>
    </div>  

    <div class="form-group">
        <label for="nilai" class="control-label">Bayar Upah</label>
        <input type="text" class="form-control" id="upah" name="upah_bayar" required=''>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#nilai').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
            });
        </script>
    </div>      
    <div id="response"></div>
    
    <div class="modal-footer">
        <button id='cetak' class="btn btn-primary" onclick="print_u()" disabled>Print</button>
        <input type="Submit" class="btn btn-primary" value="Bayar" id='bayar'>
        <input type="reset" class="btn btn-default" value="Reset" id='batal'>
    </div>
   </form>
   <script>
    function print_u(){
        <?php        
            echo "window.open('../action/teknikal/print_upah.php?id=$id3&id2=$id5&id3=$id&id4=$id2','_blank');";
        ?>
        }
    </script>