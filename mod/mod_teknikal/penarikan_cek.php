<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> PENARIKAN CEK </h1>";
        echo "
            <table id='gudang' class='table table-bordered table-hover'>
            <thead>
                <tr>  
                    <th width='10%'>No</th>
                    <th width='20%'>Tanggal Penarikan</th>
                    <th width='15%'>Jumlah Penarikan(Rp.)</th>
                    <th width='15%'>Keterangan</th>
                </tr>
            </thead>
            <tbody>
        ";
       
             
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,"SELECT id, tgl, debit, keterangan from saldo_teknikal ");

        $no = 1;

        while ($r = mysqli_fetch_array($sql)) {
            $id = $r['id'];
        ?>

        <tr align='left'>

            <td align="center"><?= $no; ?></td>            
            <td>
                <?php echo  $r['tgl']; ?>
            </td>
            <td align="right">
                <?php echo  $rupiah=number_format($r['debit'],0,',',','); 
                $tot += $r['debit']; ?>

            </td>
            <td>
                <?php echo  $r['keterangan']; ?>
            </td>
        </tr>
        <?php
            $no++;
        }
        ?>
        </tbody>

    </table> 
    <table class="table"> 
        <tr>  
        <?php
            $tot = $mysqli->query(
            "SELECT sum(debit) as 'jumlah_tarik'
            from  saldo_teknikal
            ")->fetch_object()->jumlah_tarik;
        ?>
          <td>Total Penarikan</td>
          <td>: <b><?= number_format($tot,0,',',',');?></b></td>
        </tr>
        <tr>  
            <?php
                $sisa2 = $mysqli->query(
                    "SELECT sisa_saldo
                    from  alokasi_teknikal
                    order by id desc limit 1 
                    ")->fetch_object()->sisa_saldo;
            ?>
          <td>Sisa Alokasi </td> 
          <td>:<b><?= number_format($sisa2,0,',',',');?></b></td>
        </tr>
      </table>
    <span class="container">
     <a class="btn btn-primary btn-md" data-toggle="modal" href="#myModal"><span class="glyphicon glyphicon-plus"></span> Penarikan Cek</a>
    </span>
  
  <!-- Modal for add gudang-->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Biaya Pembangunan</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action='../action/teknikal/act_biayabangun.php?mod=biaya_pembangunan&act=tambah'>
                      
                      <div class="form-group">
                        <label for="tanggal_penarikan" class="control-label">Tanggal Penarikan : </label>
                        <input type="date" class="form-control" id="tanggal_penarikan" name="tanggal_penarikan">
                      </div>
                      <div class="form-group">
                        <label for="hrg" class="control-label">Jumlah :</label>
                        <input type="text" class="form-control" name="jumlah" id="jumlah">
                        <script type="text/javascript">
                            $(document).ready(function(){
                                $('#jumlah').maskMoney({prefix:'Rp. ', thousands:',', decimal:',', precision:0});
                            });
                        </script>
                            
                        </div>
                      <div class="form-group">
                        <label for="keterangan" class="control-label">Keterangan :</label>
                        <textarea class="form-control" id="keterangan" name="keterangan"></textarea>
                      </div>
                      
                      <div class="modal-footer">
                        <input type="Submit" class="btn btn-primary" value="Submit">
                        <input type="reset" class="btn btn-default" value="Reset">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  
  
    <script type="text/javascript">
    $(function() {
        $("#gudang").dataTable();
    });
    </script>
  
  <script>
        $(function(){
            $(document).on('click','.edit-gudang',function(e){
                e.preventDefault();
                $("#edit_gudang").modal('show');
                $.post('../action/adm/edit_gudang.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#edit_gudang').on('hidden.bs.modal', function () {
                 location.reload();
                })
            });
        });
    </script>

    <?php
        break;
  }
  ?>
  
<script type="text/javascript">
    $(function() {
        $("#Dgudang").DataTable();
    });
    </script>
