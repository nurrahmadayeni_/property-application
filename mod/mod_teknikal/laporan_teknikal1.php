<script>
	 $(document).on('change','#tipe_rmh2',function(){
			 var val = $(this).val();
			 $.ajax({
				   url: '../action/marketing/show_perumahantransaksi.php',
				   data: {tipe_rmh2:val},
				   type: 'GET',
				   dataType: 'html',
				   success: function(result){
						$('#nama_prm2').html();  
						$('#nama_prm2').html(result); 
				   }
			  });
	   });
</script>

<script type="text/javascript" src="http://davidlynch.org/projects/maphilight/jquery.maphilight.js"></script>

<script>
	 $(document).on('change','#tipe_rmh',function(){
			 var val = $(this).val();
			 $.ajax({
				   url: '../action/marketing/show_type.php',
				   data: {tipe_rmh:val},
				   type: 'GET',
				   dataType: 'html',
				   success: function(result){
						$('#typ').html();  
						$('#typ').html(result); 
				   }
			  });
	   });
</script>

<script>
	 $(document).on('change','#typ',function(){
			 var val1 = $('#tipe_rmh').val();
			 var val = $('#typ').val();
			 
			 $.ajax({
				   url: '../action/marketing/show_kav.php',
				   data: {typ:val, prm: val1},
				   type: 'GET',
				   dataType: 'html',
				   success: function(result){
						$('#nama_prm').html();  
						$('#nama_prm').html(result); 
				   }
			  });
	   });
</script>

<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> LAPORAN TEKNIKAL </h3><hr>";
?>
	<div class="modal-body">
    	<div class="form-group">
    		<label for="jenis_laporan" class="control-label">Jenis Laporan : </label>
            <table width="100%" class='table'>
            	<tr>
		            <td><select name='jenis_laporan' id='jenis_laporan' class='form-control' required=''>
		                <option value='' selected disabled="">- Pilih Jenis Laporan -</option>
		                <option value='LCek_giro'>Laporan Pencairan Cek/Giro</option>
		                <option value='LPengeluaran'>Laporan Pengeluaran Dana</option>
		                <option value='LResapan'>Laporan Resapan Dana</option>
		            </select></td>
		        </tr>
		    </table>
        </div>
       	<div id="perumahan">
       		<label for="perumahan" class="control-label">Perumahan : </label>
       		<table width="100%" class='table'>
            	<tr>
            		<td><select name='tipe_rmh' id='tipe_rmh' class='form-control' required=''>
					<option value='' selected disabled="">- Pilih Perumahan -</option>
					<?php
						$getkategori="SELECT * FROM data_perumahan";
						$tampil=mysqli_query($mysqli,$getkategori);
							while($r=mysqli_fetch_assoc($tampil))
							{
								echo "<option value=$r[id_perumahan]>
								$r[nama_perumahan]</option>";
							}
					?>
					</select></td>
				</tr>
			</table>
		</div>
		<div id="kavling">
			<label for="kavling" class="control-label">Kavling : </label>
			<table width="100%" class='table'>
				<tr>
					<td width="40%" style="padding-right:5%;"><select name='typ' id='typ' class="form-control"  required=''>
						<option value='' selected>- Pilih Type Rumah -</option>
						</select>
					</td>
					<td width="40%" style="padding-right:5%;"><select name='nama_prm' class="form-control" id='nama_prm'  required=''>
						<option value='' selected>- All Kavling -</option>";
						</select>
					</td>
				</tr>
			</table>
		</div>
		<div id="time">
			<label for="time" class="control-label">Time :</label>
			<table width="100%" class='table'>
				<tr>
					<td width="15%">
						<label for="range" class="control-label" style="font-size:14px;">Pilih Range Tanggal :</label>
					</td>
					<td width="30%">
						<input type='date' name='tgl1' id="tgl1" class='form-control' required=''>
					</td>
					<td width="10%" align="center"><b style="font-size:14px;">s/d</b></td>
					<td width="30%">
						<input type='date' name='tgl2' id="tgl2" class='form-control' required=''>
					</td>
				</tr>
				<tr>
					<td>
						<label for="tanggal" class="control-label" style="font-size:14px;">Pilih Tanggal :</label>
					</td>
					<td width="30%">
						<select name="day" id="day" onchange=""  class='form-control' >
						    <option value=" " selected disabled="">- Tanggal -</option>
						    <option value="01">01</option>
						    <option value="02">02</option>
						    <option value="03">03</option>
						    <option value="04">04</option>
						    <option value="05">05</option>
						    <option value="06">06</option>
						    <option value="07">07</option>
						    <option value="08">08</option>
						    <option value="09">09</option>
						    <option value="10">10</option>
						    <option value="11">11</option>
						    <option value="12">12</option>
						    <option value="13">13</option>
						    <option value="14">14</option>
						    <option value="15">15</option>
						    <option value="16">16</option>
						    <option value="17">17</option>
						    <option value="18">18</option>
						    <option value="19">19</option>
						    <option value="20">20</option>
						    <option value="21">21</option>
						    <option value="22">22</option>
						    <option value="23">23</option>
						    <option value="24">24</option>
						    <option value="25">25</option>
						    <option value="26">26</option>
						    <option value="27">27</option>
						    <option value="28">28</option>
						    <option value="29">29</option>
						    <option value="30">30</option>
						    <option value="31">31</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<label for="bulan" class="control-label" style="font-size:14px;">Pilih Bulan :</label>
					</td>
					<td width="30%">
						<select name="month" id="month" onchange="" size="1" class='form-control' >
						    <option value=" " selected disabled="">- Bulan -</option>
						    <option value="01">January</option>
						    <option value="02">February</option>
						    <option value="03">March</option>
						    <option value="04">April</option>
						    <option value="05">May</option>
						    <option value="06">June</option>
						    <option value="07">July</option>
						    <option value="08">August</option>
						    <option value="09">September</option>
						    <option value="10">October</option>
						    <option value="11">November</option>
						    <option value="12">December</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<label for="tahun" class="control-label" style="font-size:14px;">Pilih Tahun :</label>
					</td>
					<td width="30%">
						<select name="year" id="year" onchange="" size="1" class='form-control' >
						    <option value=" " selected>- Tahun -</option>
						</select>
					</td>
				</tr>
				
			</table>
		</div>
		<div id="kcari">			
			<input type="Submit" id="cari1" class="btn btn-primary btn-sm" value="Search">
		</div>
		<div id="search">
       		<input type="Submit" id="cari2" class="btn btn-primary btn-sm" value="Search">
		</div>
		<div id="key">
			<input type="Submit" id="cari3" class="btn btn-primary btn-sm" value="Search">
		</div>
		<hr>
	    <div id="tampil"></div>
    </div>
<?php
 	}
?>

<script type="text/javascript">
    $('#jenis_laporan').change(function(){
    	if($(this).val() == 'LCek_giro'){
        	$('#perumahan').hide();
        	$('#kavling').hide();
        	$('#time').show();
        	$('#kcari').show();
        	$('#search').hide();
        	$('#key').hide();
    	}
    	else if($(this).val() == 'LPengeluaran'){
    		$('#perumahan').show();
        	$('#kavling').show();
        	$('#time').show();
        	$('#kcari').hide();
        	$('#search').show();
        	$('#key').hide();
    	}
    	else if($(this).val() == 'LResapan'){
    		$('#perumahan').show();
        	$('#kavling').show();
        	$('#time').hide();
        	$('#kcari').hide();
        	$('#search').hide();
        	$('#key').show();
    	}
    	
   
    });
</script>

<script>
   $(document).on('click','#cari1',function(){
            var tgl_1 = $('#tgl1').val();
            var tgl_2 = $('#tgl2').val();
            var month1=$('#month').val();
            var day1=$('#day').val();
            var year1=$('#year').val();
			$.ajax({

                 url: '../action/teknikal/rekap_cek.php',
                data: {
                   	tgl_awal:tgl_1,
                	tgl_akhir:tgl_2,
                	month:month1,
                	day:day1,
                	year:year1
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#tampil').html();  
                    $('#tampil').html(result); 
                }
            });
    });
</script>
<script>
   $(document).on('click','#cari2',function(){
            var prm = $('#tipe_rmh').val();
            var kav = $('#nama_prm').val();
            var tgl_1 = $('#tgl1').val();
            var tgl_2 = $('#tgl2').val();
            var month1=$('#month').val();
            var day1=$('#day').val();
            var year1=$('#year').val();
			$.ajax({

               url: '../action/teknikal/pengeluaran.php',
                data: {
                	prm1:prm,
                	kav1:kav,
                	tgl_awal:tgl_1,
                	tgl_akhir:tgl_2,
                	month:month1,
                	day:day1,
                	year:year1
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#tampil').html();  
                    $('#tampil').html(result); 
                }
            });
    });
</script>
<script>
   $(document).on('click','#cari3',function(){
           	var prm = $('#tipe_rmh').val();
            var kav = $('#nama_prm').val();
            var tgl_1 = $('#tgl1').val();
            var tgl_2 = $('#tgl2').val();
            var month1=$('#month').val();
            var day1=$('#day').val();
            var year1=$('#year').val();
			$.ajax({

                url: '../action/teknikal/Lresapan_dana.php',
                data: {
                	prm1:prm,
                	kav1:kav,
                	tgl_awal:tgl_1,
                	tgl_akhir:tgl_2,
                	month:month1,
                	day:day1,
                	year:year1
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#tampil').html();  
                    $('#tampil').html(result); 
                }
            });
    });
</script>

