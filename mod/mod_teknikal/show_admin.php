<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center>DATA ADMIN</h3>";
?>
	<div class="panel-heading"></div>
	<div class="navbar">
		<ul class="nav nav-pills">
			<li class="active"><a href="#tab1" data-toggle="tab">Data Stok Gudang</a></li>
			<li><a href="#tab2" data-toggle="tab">Data Iventaris</a></li>
			<li><a href="#tab3" data-toggle="tab">Data Keluar Barang</a></li>
			<li><a href="#tab4" data-toggle="tab">Data Masuk Barang</a></li>
			<li><a href="#tab5" data-toggle="tab">Data Penggunaan Material</a></li>
		</ul>
	</div>

	<div class="tab-content">
		<div class="tab-pane active" id="tab1">
			<div class="form-group">
				<select name='laporan_gudang' id='klgudang' class='form-control' required=''>
                    <option value='' selected disabled="">- Pilih Gudang -</option>
                    <?php
                        $getdata="SELECT * FROM data_gudang ORDER BY nama_gudang ASC";
                        $tampil=mysqli_query($mysqli,$getdata);
                        while($r=mysqli_fetch_array($tampil)){
                            echo "<option value=$r[id_gudang]>
                            $r[nama_gudang]</option>";
                        }
                    ?>
                </select>
			</div>
			<div id="Lgudang"></div>
		</div>
		
		<div class="tab-pane" id="tab2">
			<div class="form-group">
				<select name='kategori_inventaris' id='kInventaris' class='form-control' required=''>
                    <option value='' selected disabled="">- Pilih Kategori Inventaris -</option>
                    <?php
                        $getdata="SELECT * FROM kategori_inventaris";
                        $tampil=mysqli_query($mysqli,$getdata);
                        while($r=mysqli_fetch_array($tampil)){
                            echo "<option value=$r[id_kategori]>
                            $r[kategori_inventaris]</option>";
	                    }
                    ?>
               	</select>
			</div>
			<div id="Linventaris">
			</div>
		</div>
		
		<div class="tab-pane" id="tab3">
			<div class="form-group">
				<table class='table table-bordered table-hover'>
					<tr align='center'>
						<td><input type='date' name='tgl1' class='form-control' id="tgl1" required=''></td>
						<td><input type='date' name='tgl2' id="tgl2" class='form-control' required=''></td>
						<td><input type="Submit" id="submit" class="btn btn-primary btn-sm" value="Cari"></td>
					</tr>
				</table>
			</div>
			<div id="hasil">
			</div>
		</div>

		<div class="tab-pane" id="tab4">
			<div class="form-group">
				<table class='table table-bordered table-hover'>
					<tr align='center'>
						<td><input type='date' name='tgl_1' class='form-control' id="tgl_1" required=''></td>
						<td><input type='date' name='tgl_2' class='form-control' id="tgl_2"  required=''></td>
						<td><input type="Submit" id="sub" class="btn btn-primary btn-sm" value="Cari"></td>
					</tr>
				</table>
			</div>
			<div id="tampil">
			</div>
		</div>


		<div class="tab-pane" id="tab5">
			<div class="form-group">
				<table class='table table-bordered table-hover'>
					<tr align='center'>
						<td>
							<select name='data_perumahan' id='kperumahan' class='form-control' required=''>
							<option value='' selected disabled="">- Pilih Perumahan -</option>
							<?php
								$getdata="SELECT * FROM data_perumahan";
								$tampil=mysqli_query($mysqli,$getdata);
									while($r3=mysqli_fetch_array($tampil)){
										echo "<option value=$r3[id_perumahan]>
										$r3[nama_perumahan]</option>";
									}
							?>
							</select>
						</td>
						<td>
							<select name='data_kavling' id='Kkavling' class='form-control' required=''>
							<option value='' selected disabled="">- Pilih Kavling -</option>
								<?php
									$getdata="SELECT * FROM data_kavling";
									$tampil=mysqli_query($mysqli,$getdata);
									while($r=mysqli_fetch_array($tampil)){
										echo "<option value=$r[no_kavling]>
										$r[no_kavling]</option>";
									}
								?>
							</select>
						</td>
						<td><input type="Submit" class="btn btn-primary btn-sm" id="search" value="Cari"></td>
					</tr>
				</table>
			</div>
			<div id="LKebutuhan"></div>
		</div>

	</div>
		
<?php
 	}
?>
<script>
    $(document).on('change','#klgudang',function(){
            var val = $(this).val();
			$.ajax({

                url: '../action/adm/show_Lgudang.php',
                data: {laporan_gudang:val},
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#Lgudang').html();  
                    $('#Lgudang').html(result); 
                }
            });
    });
</script>

<script>
    $(document).on('change','#kInventaris',function(){
            var val = $(this).val();
			$.ajax({

                url: '../action/teknikal/show_DInventaris.php',
                data: {kategori_inventaris:val},
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#Linventaris').html();  
                    $('#Linventaris').html(result); 
                }
            });
    });
</script>

<script>
    $(document).on('click','#search',function(){
            var perum = $('#kperumahan').val();
            var kavl = $('#Kkavling').val();

			$.ajax({

                url: '../action/adm/show_LKebutuhan.php',
                data: {
                	per:perum,
                	kav:kavl
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#LKebutuhan').html();  
                    $('#LKebutuhan').html(result); 
                }
            });
    });
</script>

<script>
    $(document).on('click','#submit',function(){
            var tgl_aw = $('#tgl1').val();
            var tgl_ak = $('#tgl2').val();
			$.ajax({

                url: '../action/teknikal/show_pengeluaran.php',
                data: {
                	tgl_a:tgl_aw,
                	tgl_b:tgl_ak
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#hasil').html();  
                    $('#hasil').html(result); 
                }
            });
    });
</script>

<script>
    $(document).on('click','#sub',function(){
            var tg1 = $('#tgl_1').val();
            var tg2 = $('#tgl_2').val();
			$.ajax({

                url: '../action/teknikal/show_pemasukan.php',
                data: {
                	v1:tg1,
                	v2:tg2
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#tampil').html();  
                    $('#tampil').html(result); 
                }
            });
    });
</script>