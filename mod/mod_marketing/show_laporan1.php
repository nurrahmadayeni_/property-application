
<script>
	 $(document).on('change','#tipe_rmh2',function(){
			 var val = $(this).val();
			 $.ajax({
				   url: '../action/marketing/show_perumahantransaksi.php',
				   data: {tipe_rmh2:val},
				   type: 'GET',
				   dataType: 'html',
				   success: function(result){
						$('#nama_prm2').html();  
						$('#nama_prm2').html(result); 
				   }
			  });
	   });
</script>

<script type="text/javascript" src="http://davidlynch.org/projects/maphilight/jquery.maphilight.js"></script>

<script>
	 $(document).on('change','#tipe_rmh',function(){
			 var val = $(this).val();
			 $.ajax({
				   url: '../action/marketing/show_type.php',
				   data: {tipe_rmh:val},
				   type: 'GET',
				   dataType: 'html',
				   success: function(result){
						$('#typ').html();  
						$('#typ').html(result); 
				   }
			  });
	   });
</script>

<script>
	 $(document).on('change','#typ',function(){
			 var val1 = $('#tipe_rmh').val();
			 var val = $('#typ').val();
			 
			 $.ajax({
				   url: '../action/marketing/show_kav.php',
				   data: {typ:val, prm: val1},
				   type: 'GET',
				   dataType: 'html',
				   success: function(result){
						$('#nama_prm').html();  
						$('#nama_prm').html(result); 
				   }
			  });
	   });
</script>

<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> LAPORAN </h3><hr>";
?>
	<div class="modal-body">
    	<div class="form-group">
    		<label for="jenis_laporan" class="control-label">Jenis Laporan : </label>
            <table width="100%" class='table'>
            	<tr>
		            <td><select name='jenis_laporan' id='jenis_laporan' class='form-control' required=''>
		                <option value='' selected disabled="">- Pilih Jenis Laporan -</option>
		                <option value='LPemasukan'>Laporan Pemasukan</option>
		                <option value='LPembatalan'>Laporan Pembatalan</option>
		                <option value='LPengembalian'>Laporan Pengembalian </option>
		                <option value='LHangus'>Laporan Dana Hangus</option>
		                <option value='LHarga'>Laporan Harga Rumah</option>
		                <option value='LKonsumen'>Laporan Data Konsumen</option>
		                <option value='LRetensi'>Laporan Retensi</option>
		                <option value='LBerkas'>Laporan Progres Berkas</option>
		                <option value='LKas'>Laporan Kas</option>
		                <option value='LNotaris'>Laporan Notaris</option>
		            </select></td>
		        </tr>
		    </table>
        </div>
       	<div id="perumahan">
       		<label for="perumahan" class="control-label">Perumahan : </label>
       		<table width="100%" class='table'>
            	<tr>
            		<td><select name='tipe_rmh' id='tipe_rmh' class='form-control' required=''>
					<option value='' selected disabled="">- Pilih Perumahan -</option>
					<?php
						$getkategori="SELECT * FROM data_perumahan";
						$tampil=mysqli_query($mysqli,$getkategori);
							while($r=mysqli_fetch_assoc($tampil))
							{
								echo "<option value=$r[id_perumahan]>
								$r[nama_perumahan]</option>";
							}
					?>
					</select></td>
				</tr>
			</table>
		</div>
		<div id="kavling">
			<label for="kavling" class="control-label">Kavling : </label>
			<table width="100%" class='table'>
				<tr>
					<td width="40%" style="padding-right:5%;"><select name='typ' id='typ' class="form-control"  required=''>
						<option value='' selected>- Pilih Type Rumah -</option>
						</select>
					</td>
					<td width="40%" style="padding-right:5%;"><select name='nama_prm' class="form-control" id='nama_prm'  required=''>
						<option value='' selected>- All Kavling -</option>";
						</select>
					</td>
				</tr>
			</table>
		</div>
		<div id="time">
			<label for="time" class="control-label">Time :</label>
			<table width="100%" class='table'>
				<tr>
					<td width="15%">
						<label for="range" class="control-label" style="font-size:14px;">Pilih Range Tanggal :</label>
					</td>
					<td width="30%">
						<input type='date' name='tgl1' id="tgl1" class='form-control' required=''>
					</td>
					<td width="10%" align="center"><b style="font-size:14px;">s/d</b></td>
					<td width="30%">
						<input type='date' name='tgl2' id="tgl2" class='form-control' required=''>
					</td>
				</tr>
                <tr>
                    <td colspan="4" style="color:red;">*) Untuk melihat data persatu hari set tanggal awal sama dengan tanggal akhir</td>
                </tr>
			</table>
		</div>
		<div id="spek">
			<label for="spek" class="control-label" id="spek">Spesifikasi :</label>
			<table width="100%" class='table'>
            	<tr>
            		<td><select name='trans' id='trans' class='form-control' required=''>
					<option value='' selected disabled="">- Pilih Jenis Pembayaran -</option>
					<?php
						$getkategori="SELECT * FROM kategori_jenistransaksi";
						$tampil=mysqli_query($mysqli,$getkategori);
							while($r=mysqli_fetch_assoc($tampil))
							{
								echo "<option value=$r[id_jenisbayar]>
								$r[jenis_transaksi]</option>";
							}
					?>
					</select></td>
				</tr>
			</table>
		</div>
		<div id="cari" style="display:none;">			
			<input type="Submit" id="cari1" class="btn btn-primary btn-sm" value="Search">
       		<input type="Submit" id="cari2" class="btn btn-primary btn-sm" value="Search">
			<input type="Submit" id="cari3" class="btn btn-primary btn-sm" value="Search">
			<input type="Submit" id="cari4" class="btn btn-primary btn-sm" value="Search">
			<input type="Submit" id="cari5" class="btn btn-primary btn-sm" value="Search">
			<input type="Submit" id="cari6" class="btn btn-primary btn-sm" value="Search">
			<input type="Submit" id="cari7" class="btn btn-primary btn-sm" value="Search">
			<input type="Submit" id="cari8" class="btn btn-primary btn-sm" value="Search">
			<input type="Submit" id="cari9" class="btn btn-primary btn-sm" value="Search">
			<input type="Submit" id="cari10" class="btn btn-primary btn-sm" value="Search">
		</div>
        <hr>
	        <div id="tampil"></div>
    </div>
<?php
 	}
?>

<script>
   $(document).on('click','#cari1',function(){
            $('#tampil').show();
            var tipe = $('#tipe_rmh').val();
            var tp = $('#typ').val();
            var prm = $('#nama_prm').val();
            var tgl_1 = $('#tgl1').val();
            var tgl_2 = $('#tgl2').val();
            var trans1 =$('#trans').val();
			$.ajax({

                url: '../action/marketing/show_Lpemasukan.php',
                data: {
                	tipe1:tipe,
                	typ1:tp,
                	nm:prm,
                	tgl_a:tgl_1,
                	tgl_aa:tgl_2,
                	trs:trans1
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#tampil').html();  
                    $('#tampil').html(result); 
                }
            });
    });
</script>
<script>
   $(document).on('click','#cari2',function(){
            $('#tampil').show();
            var tipe = $('#tipe_rmh').val();
            var tp = $('#typ').val();
            var prm = $('#nama_prm').val();
            var tgl_1 = $('#tgl1').val();
            var tgl_2 = $('#tgl2').val();
			$.ajax({

                url: '../action/marketing/show_Lbatal.php',
                data: {
                	tipe1:tipe,
                	typ1:tp,
                	nm:prm,
                	tgl_a:tgl_1,
                	tgl_aa:tgl_2
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#tampil').html();  
                    $('#tampil').html(result); 
                }
            });
    });
</script>
<script>
   $(document).on('click','#cari3',function(){
            $('#tampil').show();
            var tipe = $('#tipe_rmh').val();
            var tp = $('#typ').val();
            var prm = $('#nama_prm').val();
            var tgl_1 = $('#tgl1').val();
            var tgl_2 = $('#tgl2').val();
			$.ajax({

                url: '../action/marketing/show_Pdp.php',
                data: {
                	tipe1:tipe,
                	typ1:tp,
                	nm:prm,
                	tgl_a:tgl_1,
                	tgl_aa:tgl_2
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#tampil').html();  
                    $('#tampil').html(result); 
                }
            });
    });
</script>

<script>
    $(document).on('click','#cari4',function(){
            $('#tampil').show();
            var tipe = $('#tipe_rmh').val();
            var tp = $('#typ').val();
            var prm = $('#nama_prm').val();
             var tgl_1 = $('#tgl1').val();
            var tgl_2 = $('#tgl2').val();
			$.ajax({

                url: '../action/marketing/show_Ldanahangus.php',
                data: {
                	tipe1:tipe,
                	typ1:tp,
                	nm:prm,
                	tgl_a:tgl_1,
                	tgl_aa:tgl_2
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#tampil').html();  
                    $('#tampil').html(result); 
                }
            });
    });
</script>

<script>
    $(document).on('click','#cari5',function(){
            $('#tampil').show();
            var tipe = $('#tipe_rmh').val();
            var tp = $('#typ').val();
            var prm = $('#nama_prm').val();
            
			$.ajax({

                url: '../action/marketing/show_Lperumahanharga.php',
                data: {
                	tipe1:tipe,
                	typ1:tp,
                	nm:prm
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#tampil').html();  
                    $('#tampil').html(result); 
                }
            });
    });
</script>
<script>
    $(document).on('click','#cari6',function(){
            $('#tampil').show();
            var tipe = $('#tipe_rmh').val();
            var tp = $('#typ').val();
            
			$.ajax({

                url: '../action/marketing/show_Lkonsumen.php',
                data: {
                	tipe1:tipe,
                	typ1:tp
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#tampil').html();  
                    $('#tampil').html(result); 
                }
            });
    });
</script>

<script>
    $(document).on('click','#cari7',function(){
            $('#tampil').show();
            var tipe = $('#tipe_rmh').val();
            var tp = $('#typ').val();
            var prm = $('#nama_prm').val();
             var tgl_1 = $('#tgl1').val();
            var tgl_2 = $('#tgl2').val();
			$.ajax({

                url: '../action/marketing/show_Lretensi.php',
                data: {
                	tipe1:tipe,
                	typ1:tp,
                	nm:prm,
                	tgl_a:tgl_1,
                	tgl_aa:tgl_2
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#tampil').html();  
                    $('#tampil').html(result); 
                }
            });
    });
</script>
<script>
    $(document).on('click','#cari8',function(){
            $('#tampil').show();
            var tgl_1 = $('#tgl1').val();
            var tgl_2 = $('#tgl2').val();
			$.ajax({

                url: '../action/marketing/show_Lkas.php',
                data: {
               
                	tgl_a:tgl_1,
                	tgl_aa:tgl_2
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#tampil').html();  
                    $('#tampil').html(result); 
                }
            });
    });
</script>

<script>
    $(document).on('click','#cari9',function(){
            $('#tampil').show();
            var tipe = $('#tipe_rmh').val();
            var tp = $('#typ').val();
            var prm = $('#nama_prm').val();
            var tgl_1 = $('#tgl1').val();
            var tgl_2 = $('#tgl2').val();
			$.ajax({

                url: '../action/marketing/show_Lnotaris.php',
                data: {
                	tipe1:tipe,
                	typ1:tp,
                	nm:prm,
                	tgl_a:tgl_1,
                	tgl_aa:tgl_2
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#tampil').html();  
                    $('#tampil').html(result); 
                }
            });
    });
</script>

<script>
    $(document).on('click','#cari10',function(){
            $('#tampil').show();
            var tipe = $('#tipe_rmh').val();
            var tp = $('#typ').val();
            
			$.ajax({

                url: '../action/marketing/show_Lberkas.php',
                data: {
                	tipe1:tipe,
                	typ1:tp
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#tampil').html();  
                    $('#tampil').html(result); 
                }
            });
    });
</script>

<script type="text/javascript">
    $('#jenis_laporan').change(function(){
     if($(this).val() == 'LPemasukan'){
            $('#cari').show();
        	$('#perumahan').show();
        	$('#kavling').show();
        	$('#time').show();
        	$('#spek').show();
        	$('#cari1').show();
        	$('#cari2').hide();
        	$('#cari3').hide();
        	$('#cari4').hide();
        	$('#cari5').hide();
        	$('#cari6').hide();
        	$('#cari7').hide();
        	$('#cari8').hide();
        	$('#cari9').hide();
        	$('#cari10').hide();
        }
      else if($(this).val() == 'LPembatalan'){
        	$('#tampil').hide();
            $('#perumahan').show();
        	$('#kavling').show();    
        	$('#time').show();   
        	$('#spek').hide();
        	$('#cari1').hide();
        	$('#cari2').show();
        	$('#cari3').hide();
        	$('#cari4').hide();
        	$('#cari5').hide();
        	$('#cari6').hide();
        	$('#cari7').hide();
        	$('#cari8').hide();
        	$('#cari9').hide();
        	$('#cari10').hide();
        }else if($(this).val() == 'LPengembalian'){ 
            $('#tampil').hide();  
            $('#perumahan').show();
        	$('#kavling').show();    
        	$('#time').show();       
        	$('#spek').hide();
        	$('#cari1').hide();
        	$('#cari2').hide();
        	$('#cari3').show();
        	$('#cari4').hide();
        	$('#cari5').hide();
        	$('#cari6').hide();
        	$('#cari7').hide();
        	$('#cari8').hide();
        	$('#cari9').hide();
        	$('#cari10').hide();
        }else if($(this).val() == 'LHangus'){
             $('#tampil').hide();
            $('#perumahan').show();
        	$('#kavling').show();    
        	$('#time').show();       
        	$('#spek').hide();
        	$('#cari1').hide();
        	$('#cari2').hide();
        	$('#cari3').hide();
        	$('#cari4').show();
        	$('#cari5').hide();
        	$('#cari6').hide();
        	$('#cari7').hide();
        	$('#cari8').hide();
        	$('#cari9').hide();
        	$('#cari10').hide();
        }else if($(this).val() == 'LHarga'){ 
             $('#tampil').hide();
        	$('#perumahan').show();
        	$('#kavling').show();   
        	$('#time').hide();   
        	$('#spek').hide();
        	$('#cari1').hide();
        	$('#cari2').hide();
        	$('#cari3').hide();
        	$('#cari4').hide();
        	$('#cari5').show();
        	$('#cari6').hide();
        	$('#cari7').hide();
        	$('#cari8').hide();
        	$('#cari9').hide();
        	$('#cari10').hide();
        }else if($(this).val() == 'LKonsumen'){
             $('#tampil').hide();
        	$('#perumahan').hide();
        	$('#kavling').hide();    
        	$('#time').hide();   
        	$('#spek').hide();
        	$('#cari1').hide();
        	$('#cari2').hide();
        	$('#cari3').hide();
        	$('#cari4').hide();
        	$('#cari5').hide();
        	$('#cari6').show();
        	$('#cari7').hide();
        	$('#cari8').hide();
        	$('#cari9').hide();
        	$('#cari10').hide();
        }else if($(this).val() == 'LRetensi'){
             $('#tampil').hide();
        	$('#perumahan').show();
        	$('#kavling').show();    
        	$('#time').hide();   
        	$('#spek').hide();
        	$('#cari1').hide();
        	$('#cari2').hide();
        	$('#cari3').hide();
        	$('#cari4').hide();
        	$('#cari5').hide();
        	$('#cari6').hide();
        	$('#cari7').show();
        	$('#cari8').hide();
        	$('#cari9').hide();
        	$('#cari10').hide();
        }else if($(this).val() == 'LKas'){
             $('#tampil').hide();
        	$('#perumahan').hide();
        	$('#kavling').hide();    
        	$('#time').show();   
        	$('#spek').hide();
        	$('#cari1').hide();
        	$('#cari2').hide();
        	$('#cari3').hide();
        	$('#cari4').hide();
        	$('#cari5').hide();
        	$('#cari6').hide();
        	$('#cari7').hide();
        	$('#cari8').show();
        	$('#cari9').hide();
        	$('#cari10').hide();
        }else if($(this).val() == 'LNotaris'){
             $('#tampil').hide();
        	$('#perumahan').show();
        	$('#kavling').show();    
        	$('#time').show();   
        	$('#spek').hide();
        	$('#cari1').hide();
        	$('#cari2').hide();
        	$('#cari3').hide();
        	$('#cari4').hide();
        	$('#cari5').hide();
        	$('#cari6').hide();
        	$('#cari7').hide();
        	$('#cari8').hide();
        	$('#cari9').show();
        	$('#cari10').hide();
        }
        else if($(this).val() == 'LBerkas'){
            $('#tampil').hide();
        	$('#perumahan').hide();
        	$('#kavling').hide();    
        	$('#time').hide();   
        	$('#spek').hide();
        	$('#cari1').hide();
        	$('#cari2').hide();
        	$('#cari3').hide();
        	$('#cari4').hide();
        	$('#cari5').hide();
        	$('#cari6').hide();
        	$('#cari7').hide();
        	$('#cari8').hide();
        	$('#cari9').hide();
        	$('#cari10').show();
        }
   
    });
</script>



