
<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> LAPORAN MARKETING </h1>";
?>
	<div class="panel-heading"><p></div>
	<div class="navbar">
		<ul class="nav nav-pills">
			<li class="active"><a href="#tab1" data-toggle="tab">Laporan Pemasukan</a></li>
			<li><a href="#tab2" data-toggle="tab">Laporan Pengembalian DP</a></li>
			<li><a href="#tab3" data-toggle="tab">Laporan Dana Hangus</a></li>
			<li><a href="#tab4" data-toggle="tab">Laporan Harga Rumah</a></li>
			<li><a href="#tab5" data-toggle="tab">Laporan Pembatalan</a></li>
		</ul>
	</div>

	<div class="tab-content">
		<div class="tab-pane active" id="tab1">
			<div class="form-group">
				<table class='table table-bordered table-hover'>
					<tr align='center'>
						<td><input type='date' name='tgl8' class='form-control' id="tgl8" required=''></td>
						<td><input type='date' name='tgl9' id="tgl9" class='form-control' required=''></td>
						<td><input type="Submit" id="submit" class="btn btn-primary btn-sm" value="Cari"></td>
					</tr>
				</table>
			</div>
			<div id="Lpemasukan"></div>
		</div>
		
		<div class="tab-pane" id="tab2">
			<div class="form-group">
				<table class='table table-bordered table-hover'>
					<tr align='center'>
						<td><input type='date' name='tgl5' class='form-control' id="tgl5" required=''></td>
						<td><input type='date' name='tgl6' id="tgl6" class='form-control' required=''></td>
						<td><input type="Submit" id="submit1" class="btn btn-primary btn-sm" value="Cari"></td>
					</tr>
				</table>
			</div>
			<div id="hasil3">
			</div>
		</div>
		
		<div class="tab-pane" id="tab3">
			<div class="form-group">
				<table class='table table-bordered table-hover'>
					<tr align='center'>
						<td><input type='date' name='tgl1' class='form-control' id="tgl1" required=''></td>
						<td><input type='date' name='tgl2' id="tgl2" class='form-control' required=''></td>
						<td><input type="Submit" id="submit2" class="btn btn-primary btn-sm" value="Cari"></td>
					</tr>
				</table>
			</div>
			<div id="hasil">
			</div>
		</div>

		<div class="tab-pane" id="tab4">
			<div class="form-group">
				<table class='table table-bordered table-hover'>
					<tr align='center'>
						<td>
							<select name='data_perumahan' id='kperumahan' class='form-control' required=''>
							<option value='' selected disabled="">- Pilih Perumahan -</option>
							<?php
								$getdata="SELECT * FROM data_perumahan";
								$tampil=mysqli_query($mysqli,$getdata);
									while($r3=mysqli_fetch_array($tampil)){
										echo "<option value=$r3[id_perumahan]>
										$r3[nama_perumahan]</option>";
									}
							?>
							</select>
						</td>
						<td>
							<select name='data_kavling' id='Kkavling' class='form-control' required=''>
							<option value='' selected disabled="">- Pilih type -</option>
								<?php
									$getdata="SELECT * FROM type_rumah";
									$tampil=mysqli_query($mysqli,$getdata);
									while($r=mysqli_fetch_array($tampil)){
										echo "<option value=$r[id_type]>
										$r[type_rumah]</option>";
									}
								?>
							</select>
						</td>
						<td><input type="Submit" class="btn btn-primary btn-sm" id="search" value="Cari"></td>
					</tr>
				</table>
			</div>
			<div id="Lhargarumah"></div>
		</div>
		
		<div class="tab-pane" id="tab5">
			<div class="form-group">
				<table class='table table-bordered table-hover'>
					<tr align='center'>
						<td><input type='date' name='tgl3' class='form-control' id="tgl3" required=''></td>
						<td><input type='date' name='tgl4' id="tgl4" class='form-control' required=''></td>
						<td><input type="Submit" id="cari" class="btn btn-primary btn-sm" value="Cari"></td>
					</tr>
				</table>
			</div>
			<div id="hasil2">
			</div>
		</div>

	</div>
		
<?php
 	}
?>
<script>
    $(document).on('click','#submit',function(){
            var tgl_awal = $('#tgl8').val();
            var tgl_akhir = $('#tgl9').val();
			$.ajax({

                url: '../action/marketing/show_Lpemasukan.php',
                data: {
                	tgl_1:tgl_awal,
                	tgl_2:tgl_akhir
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#Lpemasukan').html();  
                    $('#Lpemasukan').html(result); 
                }
            });
    });
</script>

<script>
    $(document).on('click','#search',function(){
            var perum = $('#kperumahan').val();
            var kavl = $('#Kkavling').val();

			$.ajax({

                url: '../action/marketing/show_Lperumahanharga.php',
                data: {
                	per:perum,
                	kav:kavl
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#Lhargarumah').html();  
                    $('#Lhargarumah').html(result); 
                }
            });
    });
</script>

<script>
    $(document).on('click','#submit2',function(){
            var tgl_awal = $('#tgl1').val();
            var tgl_akhir = $('#tgl2').val();
			$.ajax({

                url: '../action/marketing/show_Ldanahangus.php',
                data: {
                	tgl_1:tgl_awal,
                	tgl_2:tgl_akhir
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#hasil').html();  
                    $('#hasil').html(result); 
                }
            });
    });
</script>

<script>
    $(document).on('click','#submit1',function(){
            var tgl_awal = $('#tgl5').val();
            var tgl_akhir = $('#tgl6').val();
			$.ajax({

                url: '../action/marketing/show_Pdp.php',
                data: {
                	tgl_1:tgl_awal,
                	tgl_2:tgl_akhir
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#hasil3').html();  
                    $('#hasil3').html(result); 
                }
            });
    });
</script>

<script>
    $(document).on('click','#cari',function(){
            var tgl_awal = $('#tgl3').val();
            var tgl_akhir = $('#tgl4').val();
			$.ajax({

                url: '../action/marketing/show_Lbatal.php',
                data: {
                	tgl_1:tgl_awal,
                	tgl_2:tgl_akhir
                },
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#hasil2').html();  
                    $('#hasil2').html(result); 
                }
            });
    });
</script>
