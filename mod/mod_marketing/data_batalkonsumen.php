<link rel="stylesheet" href="../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css"/>

<?php   
	include '../../config/connectdb.php';
	error_reporting(0);
	
	echo "<p><h3 align=center> Data Pembatalan Transaksi</h3>
		<table id='batal' class='table table-bordered table-hover'>
        <thead>
            <tr>
                <th width='5%' align='center'>No</th>
				<th width='10%'>Nama Konsumen</th>
                <th width='10%'>Nama Perumahan</th>
                <th width='15%'>No Kavling</th>
                <th width='15%'>Tanggal Pembatalan</th>
            </tr>
        </thead>
        <tbody>
        ";
		
	$sql = mysqli_query($mysqli,"SELECT bt.nik_ktp,p.nama_pembeli,dp.nama_perumahan,bt.no_kavling,bt.tgl_pembatalan
		FROM batal_transaksi bt,data_perumahan dp,data_pembeli p
		WHERE bt.id_perumahan=dp.id_perumahan and bt.nik_ktp=p.nik_ktp   
		ORDER BY bt.tgl_pembatalan DESC");
		$no = 1;
		while ($r = mysqli_fetch_array($sql)) {
			$nakons= $r['nama_pembeli'];
			$nokav= $r['no_kavling'];
			$narum= $r['nama_perumahan'];
			$tgl= $r['tgl_pembatalan'];
		echo"
		<tr align='left'>
			<td align='center'>$no</td>
			<td>$nakons</td>
			<td>$narum</td>
			<td>$nokav</td>
			<td>$tgl
            </td>
		</tr>
		";
		   
		$no++;
	} 
?>
</tbody>
</table>

<script type="text/javascript">
    $(function() {
        $("#batal").dataTable();
    });
</script>
	

