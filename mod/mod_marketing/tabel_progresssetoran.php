<link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../../assets/css/dataTables.bootstrap.css"/>

<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> SETORAN KONSUMEN</h3>";
        echo "
            <table id='konsumen' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th width='5%'>No</th>
					<th width='15%'>ID Transaksi</th>
                    <th width='20%'>Nama Konsumen</th>
					<th width='20%'>Tanggal Transaksi</th>
					<th width='20%'>Detail Setoran Angsuran Rumah</th>
                </tr>
            </thead>
            <tbody>
        ";
       
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,'select tr.tgl_booking, p.nik_ktp, p.nama_pembeli, p.notelp_pembeli, p.email, dt.id_transaksi from data_pembeli p,detail_transaksirumah dt,transaksi_perumahan tr where dt.id_transaksi=tr.id_transaksi and tr.nik_ktp=p.nik_ktp order by tr.tgl_booking,tr.id_transaksi DESC');
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
		$idtr = $r['id_transaksi'];
		$id = $r['nik_ktp'];
			?>
			<tr align='left'>
            <td>
                <?php echo  $no;?>
            </td>
			<td>
				<?php echo  $r['id_transaksi']; ?>
            </td>
            <td>
				<?php echo  $r['nama_pembeli']; ?>
            </td>
			 <td>
                <?php echo  $r['tgl_booking']; ?>
            </td>
			<td align="center">
                <a href="?mod=detail_setoran2&id=<?php echo $id;?>&idtr=<?php echo $idtr; ?>"> <button class='btn btn-primary btn-sm'>Detail</button> </a>
            </td>
			
        </tr>
		<?php
            $no++;
        }
        ?>
        </tbody>
    </table>

	
    <?php

	}
	?>

    <script type="text/javascript">
    $(function() {
        $("#konsumen").dataTable();
    });
    </script>
