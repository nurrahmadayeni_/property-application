<link rel="stylesheet" href="../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css"/>
<script>
		$(document).on('change','#tipe_rmh2',function(){
			 var val = $(this).val();
			 $.ajax({
				   url: '../action/marketing/show_perumahantransaksi.php',
				   data: {tipe_rmh2:val},
				   type: 'GET',
				   dataType: 'html',
				   success: function(result){
						$('#nama_prm2').html();  
						$('#nama_prm2').html(result); 
				   }
			  });
		});
</script>

<script type="text/javascript" src="http://davidlynch.org/projects/maphilight/jquery.maphilight.js"></script>

<script>
		function formatThousands(n, dp) {
			  var s = ''+(Math.floor(n)), d = n % 1, i = s.length, r = '';
			  while ( (i -= 3) > 0 ) { r = ',' + s.substr(i, 3) + r; }
			  return s.substr(0, i + 3) + r + (d ? '.' + Math.round(d * Math.pow(10,dp||2)) : '');
		}


		$(document).on('change','#tipe_rmh',function(){
			 var val = $(this).val();
			 $.ajax({
				   url: '../action/marketing/show_type.php',
				   data: {tipe_rmh:val},
				   type: 'GET',
				   dataType: 'html',
				   success: function(result){
						$('#typ').html();  
						$('#typ').html(result); 
				   }
			  });
		});

		$(document).on('change','#typ',function(){
			 var val1 = $('#tipe_rmh').val();
			 var val = $('#typ').val();
			 
			 $.ajax({
				   url: '../action/marketing/show_kavling.php',
				   data: {typ:val, prm: val1},
				   type: 'GET',
				   dataType: 'html',
				   success: function(result){
						$('#nama_prm').html();  
						$('#nama_prm').html(result); 
				   }
			  });
		});

		$(document).on('change','#tipe_rmh',function(){
			 var val = $(this).val();
			 $.ajax({
				   url: '../action/marketing/show_imagermh.php',
				   data: {tipe_rmh:val},
				   type: 'GET',
				   dataType: 'html',
				   success: function(result){
						$('#foto').html();  
						$('#foto').html(result); 
				   }
			  });
		});

		$(document).ready(function() {
		    $('#hrgatanah').keyup(function(){
		    	var harga = $('#hrgatanah').val();
			    var hargatanah=parseInt(harga.replace(/,/g,''), 10);
				var tambahtnahn=parseInt($('#pntanah').val());

				var total=hargatanah*tambahtnahn;
			   	var hargatmabh = formatThousands(total);

			   	$('#hargatmbhtanah').val(total);
			    $('#hargatmbhtanahh').val(hargatmabh);
		    });

		    $('#bookingcash').keyup(function(){
		    	var bookingg = $('#bookingcash').val();
			    var booking=parseFloat(bookingg.replace(/,/g,''));

				var totalrumah=parseInt($('#totalrumah').val());

				var uangdp=totalrumah-booking;

			   	var uangdpp = formatThousands(uangdp);

			   	$('#dpcashh').val(uangdpp);
			    $('#dpcash').val(uangdp);
		    });

		    $('#bookingkpr').keyup(function(){
		    	var bookingkprr = $('#bookingkpr').val();
			    var bookingkpr=parseInt(bookingkprr.replace(/,/g,''), 10);

				var totalrumah=parseInt($('#totalrumah').val());

				var uangdpkprr=totalrumah-bookingkpr;

			   	var uangdpkpr = formatThousands(uangdpkprr);

			   	$('#kprdpp').val(uangdpkpr);
			    $('#kprdp').val(uangdpkprr);
		    });

		    $('#typ').change(function(){
				var reg_number = $(this).val();
				var data_String;
				data_String = 'id_type='+reg_number;
				$.post('cek_harga.php',data_String,function(data){
				console.log(data);
				var data= jQuery.parseJSON(data);
					var val = data.harga_kavling;
					var hrg = formatThousands(val);
					
					$('#hrgarmhh').val(hrg);
					$('#hrgarmh').val(val);

					var tanahh = data.luas_tanah + "m²";
					$('#ltanahh').val(tanahh);

					var tanah = data.luas_tanah;
					$('#ltanah').val(tanah);					

				});
			});

			$("#nikktp").keyup(function() {
		        var nisp = $('#nikktp').val();		
				$.post('load_data.php', // request ke file load_data.php
				{parent_id: nisp},
				function(data){
					 $('#nama_pembeli').val(data[0].nama_pembeli);
					 $('#alamat_pembeli').val(data[0].alamat_pembeli);
					 $('#notelp_pembeli').val(data[0].notelp_pembeli);
					 $('#email').val(data[0].email);
					 $('#catatan_khusus').val(data[0].catatan_khusus);	
				},'json'
		      );
		   });

			$('#potongnhrga').keyup(function(){
				var hargarumahh = $('#hrgarmh').val();
				var hargarumah=parseInt(hargarumahh.replace(/,/g,''), 10);

				var tambahtnahnh = $('#hargatmbhtanah').val();
				var tambahtnahn = parseInt(tambahtnahnh.replace(/,/g,''), 10);

				var hookk = $('#hook').val();
				var hook = parseInt(hookk.replace(/,/g,''), 10);

				var tmbahanlainnyaa = $('#hargatmbhlain').val();
				var tmbahanlainnya = parseInt(tmbahanlainnyaa.replace(/,/g,''), 10);
				
				var total=hargarumah+tambahtnahn+hook;

				var diskonn = $('#potongnhrga').val();
	            var diskon = parseInt(diskonn.replace(/,/g,''), 10);
	 
				var total_bayar = total - diskon;
				
				var nilait = formatThousands(total_bayar);
	            $('#totalrumahh').val(nilait);
	            $('#totalrumah').val(total_bayar);
            });
		});

		function enabledisabletext(){
			if(document.myform.pmbayaran.value=='1'){
				document.myform.bookingkpr.disabled=true;
				document.myform.kprdp.disabled=true;
				document.myform.bookingcash.disabled=false;
				document.myform.dpcash.disabled=false;
			}
			if(document.myform.pmbayaran.value=='2'){
				document.myform.bookingkpr.disabled=false;
				document.myform.kprdp.disabled=false;
				document.myform.bookingcash.disabled=true;
				document.myform.dpcash.disabled=true;
			}
		}
</script>

<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center>TRANSAKSI KONSUMEN</h3>";
        echo "
            <table id='order' class='table-bordered table-hover table-condensed'>
            <thead>
                <tr>
                	<th width='2%'>No</th>
					<th width='5%'>ID Transaksi</th>
					<th width='5%'>NIK</th>
                    <th width='20%'>Nama </th>
					<th width='10%'>Perumahan</th>
                    <th width='2%'>Kavling</th>
					<th width='10%'>Harga Rumah</th>
					<th width='10%'>Tanggal Booking</th>
					<th width='4%'>Jenis Pembayaran</th>
					<th width='10%'>Pembayaran DP (Rp) </th>
					<th width='10%'>Total Transaksi (Rp)</th>
					<b><td width='10%' align='center'>Menu</td></b>
                </tr>
            </thead>
            <tbody>
        ";
       
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,'SELECT tr.nik_ktp,dt.harga_tambahtanah,dt.hook,dt.total_hrgarumah,ty.harga_kavling,tr.id_transaksi,jb.jenis_pembayaran, dt.jlh_bayarbooking , dt.jlh_DP1, dt.nominal_kpr, p.nama_pembeli,dp.nama_perumahan,dk.no_kavling,tr.tgl_booking 
			from data_pembeli p,data_perumahan dp,data_kavling dk,transaksi_perumahan tr,detail_transaksirumah dt,jenis_pembayaranrumah jb,type_rumah ty
			where p.nik_ktp=tr.nik_ktp and dt.id_transaksi=tr.id_transaksi and dt.id_perumahan=dp.id_perumahan and ty.id_type=dk.id_type and jb.id_jenispembayaran=dt.id_jenispembayaran
			and dt.no_kavling=dk.no_kavling and dt.status_batal="" order by tr.tgl_booking DESC');
        $satu = 1;
        while ($r = mysqli_fetch_array($sql)) {
		$id = $r['id_transaksi'];
		
			?>
			<tr align='left'>
			<td>
				<?php echo $satu; ?>
			</td>
			<td>
				<?php echo $r['id_transaksi'];?>
			</td>
			<td>
				<?php echo $r['nik_ktp'];?>
			</td>
            <td>
				<?php echo  $r['nama_pembeli']; ?>
            </td>
			<td>
                <?php echo  $r['nama_perumahan']; ?>
            </td>
			<td>
                <?php echo  $r['no_kavling']; ?>
            </td>
			<td><div align='right'> 
                <?php 
                    $rupiah=number_format(($r['total_hrgarumah']),0,',',','); 
                    echo $rupiah; 
                ?>
				</div>
            </td>
			<td>
                <?php echo  $r['tgl_booking']; ?>
            </td>
			<td>
				<?php $jnis = $r['jenis_pembayaran'];
				echo $jnis; ?>
			</td>
			<td><div align='right'> 
				<?php 
					if($jnis=='Tunai(cash)'){
                    	$rupiah=number_format($r['total_hrgarumah'],0,',',','); 
                    	echo $rupiah; 
                    }elseif($jnis=='Kredit(KPR)'){
                    	$rupiah=number_format($r['jlh_DP1'],0,',',','); 
                    	echo $rupiah; 
                    }
                ?>
				</div>
			</td>
			<td><div align='right'> 
				<?php 
					$queryid = mysqli_query($mysqli,"SELECT sum(jlh_setoran) AS jlh_setoran FROM bayar_rumah where id_transaksi='$id'");
					$data = mysqli_fetch_array($queryid);
					$jlh = $data['jlh_setoran'];

					$rupiah=number_format($jlh,0,',',','); 
                    
                    echo $rupiah; 
                ?>
				</div>
			</td>
            <td>
				<a href="#edittransaksi" class="edit-record" data-toggle="modal" data-id="<?php echo $id; ?>"> 
					<i class="material-icons btn btn-primary" title="Edit transaksi">edit</i>
                </a>

                <a href="#bataltransaksi" class="batal-record" data-toggle="modal" data-id="<?php echo $id; ?>"> 
					<i class="material-icons btn btn-primary" title="Batal Transaksi">cancel</i>
                </a>
			</td>
        </tr>
		<?php
            $satu++;
        }
        ?>

        </tbody>

    </table>  
	<?php
		 $sql = mysqli_query($mysqli,'select * from transaksi_perumahan');
         $r = mysqli_fetch_array($sql);
		 $idtr = $r['id_transaksi'];
		 if(empty($idtr)){
	?>
		<span class="container">
		<a class="btn btn-primary " data-toggle="modal" href="#tambahtransaksi"><span class="glyphicon glyphicon-plus"></span>Transaksi Perumahan</a>
		<a href="#cetak" class="btn btn-primary " data-toggle="modal">
			<i class="material-icons btn btn-primary" title="Print Consumen Card"></i>Print Consumen Card
        </a>
		</span>
	<?php
		}else{
	?>
    <span class="container">
	   <a class="btn btn-primary " data-toggle="modal" href="#tambahtransaksi"><span class="glyphicon glyphicon-plus"></span> Transaksi</a>
		<a href="#cetak" class="btn btn-primary " data-toggle="modal">
			<i class="material-icons btn btn-primary"></i>Print Consumen Card
        </a>
	</span>
	<?php
		}
	?>
	
	<!-- Modal for Edit transaksi Perumahan-->
    <div id="edittransaksi" class="modal fade ">
        <div class="modal-dialog modal-lg">
            <div class="modal-content modal-lg" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Transaksi Perumahan</h4>
                </div>
                <div class="modal-body">
                    
                </div>
            </div>
        </div>
    </div>
	
	<div id="cetak" class="modal fade ">
        <div class="modal-dialog modal-lg">
            <div class="modal-content modal-lg" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Cetak Kartu Konsumen</h4>
                </div>
                <div class="modal-body">
                      <div class="form-group">
                        <label for="idtran3" class="control-label">ID Transaksi: </label>
                        <select name='idtran' id='idtran' class='form-control' required=''>
						<option value='' selected>- Pilih ID Transaksi -</option>
							<?php
								$getkategori="SELECT * FROM detail_transaksirumah where status_batal=''";
								$tampil=mysqli_query($mysqli,$getkategori);
								while($r=mysqli_fetch_assoc($tampil))
								{
									echo "<option value=$r[id_transaksi]>
										$r[id_transaksi]</option>";
								}
							?>
						</select>
                      </div>
					   <div class="modal-footer">
                        <input type="Submit" class="btn btn-primary" value="Print" onclick='print_d2()'>
					  </div>
                </div>
            </div>
        </div>
    </div>
	
	 <!-- Modal for add transaksi perumahan-->
    <div id="tambahtransaksi" class="modal fade lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content modal-lg">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Tambah Transaksi Pembelian Rumah</h4>
                </div>
                <div class="modal-body">
                    <form method="post" name="myform" action='../action/marketing/act_transaksi.php?mod=marketing&act=tambahtransaksi' enctype="multipart/form-data">
                      <div class="form-group">
                        <label for="tgl_booking" class="control-label">Tanggal Transaksi : </label>
						<input type="text" class="form-control" id="tglbook" name="tglbook" value="<?php echo date('Y-m-d');?>" disabled>
                      </div>
					  <div class="form-group">
                        <label for="nik" class="control-label">NIK : </label>
                        <input type="text" class="form-control" id="nikktp" name="nikktp" required=''>
                      </div>
					  <div class="form-group">
                        <label for="nama-konsumen" class="control-label">Nama Konsumen : </label>
                        <input type="text" class="form-control" id="nama_pembeli" name="nama_konsumen" required=''>
                      </div>
					  <div class="form-group">
                        <label for="notelp" class="control-label">No.telp/HP : </label>
                        <input type="text" class="form-control" id="notelp_pembeli" name="notelp" required=''>
                      </div>
					  <div class="form-group">
                        <label for="alamat" class="control-label">Alamat Konsumen: </label>
                        <input type="text" class="form-control" id="alamat_pembeli" name="alamatpm" required=''>
                      </div>
					  <div class="form-group">
                        <label for="email" class="control-label">Email Konsumen: </label>
                        <input type="email" class="form-control" id="email" name="emailpm" required=''>
                      </div>
					  <div class="form-group">
                        <label for="ket" class="control-label">Catatan/Keterangan Lainnya: </label>
                        <input type="text" class="form-control" id="catatan_khusus" name="ket">
                      </div>
					  <div class="form-group">
                        <hr><label for="datarumah" class="control-label"><h4><b>Data Rumah</b></h4></label><hr>
                      </div>
					  <div class="form-group">
                        <label for="nmarmh" class="control-label">Nama Perumahan: </label>
                        <select name='tipe_rmh' id='tipe_rmh' class='form-control' required=''>
						<option value='' selected>- Pilih Perumahan -</option>
							<?php
								$getkategori="SELECT * FROM data_perumahan";
								$tampil=mysqli_query($mysqli,$getkategori);
								while($r=mysqli_fetch_assoc($tampil))
								{
									echo "<option value=$r[id_perumahan]>
										$r[nama_perumahan]</option>";
								}
							?>
						</select>
                      </div>
					  
					  <div class="form-group">
                        <label for="typ" class="control-label">Type Rumah : </label>
						<select name='typ' id='typ' class='form-control' required=''>
						<option value='' selected>- Pilih Type Rumah -</option>";
						</select>
					  </div>
					  
					  <div class="form-group">
                        <label for="namakav" class="control-label">Kavling: </label>
						<select name='nama_prm' id='nama_prm' class='form-control' required=''>
						<option value='' selected>- Pilih Kavling -</option>";
						</select>
					  </div>
					  
					  <div id='foto' width='100%' height='100%'>
                      </div>
					   
					  <div class="form-group">
                        <label for="hargarmh" class="control-label">Harga Rumah Standar (Rp.): </label>
						<input type="text" class="form-control" id="hrgarmhh" name="hrgarmhh" disabled>
						<input type="hidden" class="form-control" id="hrgarmh" name="hrgarmh">
					  </div>
					  
					  <div class="form-group">
			  			<label for="ltanah">Luas Tanah: </label>
			  			<input type="text" class='form-control'  id="ltanahh" name="ltanahh" disabled> 
			  			<input type="hidden" class='form-control'  id="ltanah" name="ltanah" >
					  </div>
					  
					  <div class="form-group">
					  		<label for="pntanah" class="control-label">Penambahan Tanah: </label>
						  	<input type="text" class='form-control' id="pntanah" name="pntanah" placeholder="meter" required>
							<input type="text" class='form-control' id="hrgatanah" name="hrgatanah" placeholder="Rp." required>
					  </div>                        
						
						<script type="text/javascript">
	                        $(document).ready(function(){
	                            $('#hrgatanah, #hook, #hargatmbhlain, #potongnhrga, #bookingcash, #bookingkpr').maskMoney({thousands:',', decimal:',', precision:0});
	                        });
	                    </script>
					  
					 	<div class="form-group">
					 		<label for="pntanah" class="control-label">Total harga Tambah Tanah (Rp.): </label>
					 		<input type="text" class="form-control" id="hargatmbhtanahh" name="hargatmbhtanahh"  disabled >
							<input type="hidden" class='form-control' id="hargatmbhtanah" name="hargatmbhtanah" >
					 	</div>
					  
					  <div class="form-group">
                        <label for="hook3" class="control-label">Hook: </label>
						<input type="text" class="form-control" id="hook" name="hook" placeholder="Rp." required>
					  </div>

					  <div class="form-group">
                        <label for="hargalain" class="control-label">Biaya Penambahan Lainnya: </label>
						<input type="text" class="form-control" id="hargatmbhlain" name="hargatmbhlain" placeholder="Rp.">
					  </div>
					  
					  <div class="form-group">
                        <label for="potongnhrga" class="control-label">Potongan Harga : </label> 
						<input type="text" class="form-control" id="potongnhrga" name="potongnhrga" placeholder="Rp." required> 
					  </div>
					  
					  <div class="form-group">
						<label for="potongnhrga" class="control-label">Total Harga Rumah(Rp.) : </label> 
						<input type="text" class="form-control" id="totalrumahh" name="totalrumahh" disabled>
						<input type="hidden" class="form-control" id="totalrumah" name="totalrumah" >
					  </div>
					  
					  <div class="form-group">
                        <label for="pmbayaran" class="control-label">Pembayaran secara: </label>
						<select name='pmbayaran' id='pmbayaran' onchange="enabledisabletext()" class='form-control' required=''>
						<option value='' selected>- Pilih Cara Pembayaran-</option>";
						    <?php
								$getkategori="SELECT * FROM jenis_pembayaranrumah";
								$tampil=mysqli_query($mysqli,$getkategori);
								while($r=mysqli_fetch_assoc($tampil))
								{
									echo "<option value=$r[id_jenispembayaran]>
										$r[jenis_pembayaran]</option>";
								}
							?>
						</select>
					  </div>

					  <div class="form-group">
                        <label for="pembayaran" class="control-label"> >Pembayaran Tunai/Cash</label>
				      </div>
				      <div class="form-group">
                        <label for="tgl_booking" class="control-label">Tanggal Booking : </label>
						<input type="date" class="form-control" id="tglbook" name="tgl_booking" required>
                      </div>
					  <div class="form-group">
                        <label for="booking" class="control-label">Nominal Booking : </label>
						<input type="text" class="form-control" id="bookingcash" name="bookingcash" required>
					  </div>
					  <div class="form-group">
                        <label for="dp" class="control-label">Sisa Pembayaran : </label>
						<input type="text" class="form-control" id="dpcashh" name="dpcashh" disabled>
						<input type="hidden" class="form-control" id="dpcash" name="dpcash">
					  </div>

					  <div class="form-group">
                        <label for="pembayarankpr" class="control-label"> >Pembayaran Kredit/KPR</label>
				      </div>
					  <div class="form-group">
                        <label for="bookingkpr" class="control-label">Nominal KPR: </label>
						<input type="text" class="form-control" id="bookingkpr" name="bookingkpr" required>
					  </div>
					  <div class="form-group">
                        <label for="dp" class="control-label">Jumlah DP : </label>
						<input type="text" class="form-control" id="kprdpp" name="kprdpp" disabled>
						<input type="hidden" class="form-control" id="kprdp" name="kprdp">
					  </div>
					  <div class="form-group">
                        <label for="keterangntr" class="control-label">Keterangan Lainnya: </label>
						<input type="text" class="form-control" id="keterangntr" name="keterangntr">
					  </div>
                      <div class="modal-footer">
                        <input type="Submit" class="btn btn-primary" value="Save">
						<input type="button" class="btn btn-primary" value="Print PSJB" onclick='print_PSJB()'>
						<a href='?mod=data_transaksi'> <input type='' class='btn btn-primary' value='Cancel'> </a>
					  </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

	 <!-- Modal for batal transaksi perumahan-->
    <div id="bataltransaksi" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Batal Transaksi Pembelian Rumah</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
	
	
    <script type="text/javascript">
    $(function() {
        $("#order").dataTable();
    });
    </script>

    <?php

	}
	?>

	<script>
        $(function(){
            $(document).on('click','.edit-record',function(e){
                e.preventDefault();
                $("#edittransaksi").modal('show');
                $.post('../action/marketing/edit_transaksi.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#edittransaksi').on('hidden.bs.modal', function () {
                    location.reload();
                })
            });
            $(document).on('click','.batal-record',function(e){
                e.preventDefault();
                $("#bataltransaksi").modal('show');
                $.post('../action/marketing/batal_transaksi.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#bataltransaksi').on('hidden.bs.modal', function () {
                    location.reload();
                })
            });
        });
    </script>
	<script>
		function print_d2(){
			window.open("../action/marketing/print2.php?id="+document.getElementById('idtran').value+"&mod=full","_blank");
		}
	</script>
	
	<script>
		function print_PSJB(){
			window.open("../action/marketing/printpsjb.php?namapembeli="+document.getElementById('nama_pembeli').value+"&idr="+document.getElementById('tipe_rmh').value+"&ty="+document.getElementById('typ').value+"&kav="+document.getElementById('nama_prm').value+"&alamat="+document.getElementById('alamat_pembeli').value+"&notelp="+document.getElementById('notelp_pembeli').value+"&hargarumah="+document.getElementById('hrgarmh').value+"&tambahtanah="+document.getElementById('hargatmbhtanah').value+"&hook="+document.getElementById('hook').value+"&totalrmh="+document.getElementById('totalrumah').value+"","_blank");
		}
	</script>