<link rel="stylesheet" href="../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css"/>
<h3 align=center>DETAIL KAVLING PERUMAHAN</h3>
<table id="order" class="table table-bordered table-hover">
<thead>
	<tr>
		<th width="10%">No</th>
		<th width="25%">Nomor Kavling</th>  
		<th width="25%">Luas Tanah</th>
		<th width="25%">Harga Tanah Per Kavling(Rp)</th>
		<th>Type Rumah</th>
		<th>Update Kavling</th>
	</tr>
 </thead>
<tbody>
<?php
		error_reporting(0);
		
		
		//Data mentah yang ditampilkan ke tabel   
		$sql = mysqli_query($mysqli,"SELECT ty.type_rumah,ty.harga_kavling, ty.luas_tanah, dk.id_perumahan,dk.no_kavling from data_kavling dk,data_perumahan dp,type_rumah ty where dk.id_perumahan=dp.id_perumahan and ty.id_type=dk.id_type and dk.id_perumahan='$_GET[id]'");
		$no = 1;
		while ($r = mysqli_fetch_array($sql)) {
			echo"
			<tr align='left'>
				<td>$no</td>
				<td>$r[no_kavling]</td>
				<td>$r[luas_tanah]</td>
				<td><div align='right'>
					";?>
					<?php 
                    $rupiah=number_format($r['harga_kavling'],0,',','.'); 
                    echo $rupiah; 
                ?>
				<?php
				echo"
				</div>
				</td>
				<td>$r[type_rumah]</td>
				<td>
					<a href='#editkavling' class='edit-record' data-toggle='modal' data-id='$r[no_kavling]' title='edit'><button class='btn btn-primary btn-sm' alt='edit'>
					<span class='glyphicon glyphicon-pencil'></span></button></a>
				</td>
			</tr>";
		
			$no++;
		}
		?>
	</tbody>
</table>
<!-- Modal for edit kavling-->
<div id="editkavling" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Edit Kavling</h4>
			</div>
			<div class="modal-body">
				
			</div>
		</div>
	</div>
</div>

<script>
$(function(){
	$(document).on('click','.edit-record',function(e){
		e.preventDefault();
		$("#editkavling").modal('show');
		$.post('../action/marketing/edit_kavling.php',
			{id:$(this).attr('data-id')},
			function(html){
				$(".modal-body").html(html);
			}   
		);
		$('#editkavling').on('hidden.bs.modal', function () {
			location.reload();
		})
	});
});
</script>

<script src="../assets/js/jquery-1.11.0.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>        
<script src="../assets/datatables/jquery.dataTables.js"></script>
<script src="../assets/datatables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
  $(function() {
		 $("#order").dataTable();
  });
</script>