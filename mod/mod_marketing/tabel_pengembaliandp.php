<link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../../assets/css/dataTables.bootstrap.css"/>

<script>
	 $(document).on('change','#tipe_rmh',function(){
			 var val = $(this).val();
			 $.ajax({
				   url: '../action/marketing/show_type.php',
				   data: {tipe_rmh:val},
				   type: 'GET',
				   dataType: 'html',
				   success: function(result){
						$('#typ').html();  
						$('#typ').html(result); 
				   }
			  });
	   });
</script>

<script>
	 $(document).on('change','#typ',function(){
			 var val1 = $('#tipe_rmh').val();
			 var val = $('#typ').val();
			 
			 $.ajax({
				   url: '../action/marketing/show_kavling3.php',
				   data: {typ:val, prm: val1},
				   type: 'GET',
				   dataType: 'html',
				   success: function(result){
						$('#nama_prm').html();  
						$('#nama_prm').html(result); 
				   }
			  });
	   });
</script>

<script>
  $(document).ready(function() {
    $("#namakons").change(function() {
        var reg_number = $(this).val();
		var data_String;
		data_String = 'nik_ktp='+reg_number;		
		$.post('cek_dp.php', 
		data_String,
		function(data){
		console.log(data);
		var data= jQuery.parseJSON(data);
			 $('#dp2').val(data.jlh_DP1);
			 $('#nominalkembali').keyup(function(){
				var nominalkembali = parseInt($('#nominalkembali').val());
				var dp = parseInt($('#dp2').val());
				var danahngus=dp - nominalkembali;
				$('#danahngus').val(danahngus);
		});
   });
  });
  });
</script>

<script type="text/javascript">
        $(document).ready(function() {
            $('#nominalkembali').keyup(function(){
            var hargarumah=parseInt($('#hrgarmh').val());
			var tambahtnahn=parseInt($('#hargatmbhtanah').val());
			var hook=parseInt($('#hook').val());
			var tmbahanlainnya=parseInt($('#hargatmbhlain').val());
			
			var total=hargarumah+tambahtnahn+hook+tmbahanlainnya;
            var diskon=parseInt($('#potongnhrga').val());
 
			var total_bayar=total - diskon;
			
            $('#totalrumah').val(total_bayar);
            });
        });
</script>
		

<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> DAFTAR PENGEMBALIAN DP KONSUMEN</h3>";
        echo "
            <table id='dp' class='table table-bordered table-hover'>
            <thead>
                <tr>
					<th>ID Transaksi</th>
                    <th>Nama Konsumen</th>
					<th>Nama Perumahan</th>
                    <th>Nomor Kavling</th>
					<th>Tanggal Booking Rumah</th>
					<th>Dana Hangus</th>
					<th>Jumlah DP</th>
					<th>Nominal Pengembalian</th>
					<th>Kartu Konsumen</th>
					<th>Kwitansi Pengembalian</th>
					<th>Hapus Laporan</th>
                </tr>
            </thead>
            <tbody>
        ";
       
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,'select bp.nominal_pengembalian, bp.id_pembatalan,bp.foto_kartukonsumen,bp.foto_kwitansikembali, p.nama_pembeli,p.notelp_pembeli,dp.nama_perumahan,dt.no_kavling,dt.id_transaksi,
		dt.jlh_DP1,bp.tgl_pembatalan,bp.dana_hangus FROM transaksi_perumahan tr,batal_pembelian bp,detail_transaksirumah dt,data_pembeli p,data_perumahan dp
		where bp.id_transaksi=dt.id_transaksi and p.nik_ktp=tr.nik_ktp and tr.id_transaksi=dt.id_transaksi and dp.id_perumahan=dt.id_perumahan
		');
        
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
		$id = $r['id_pembatalan'];
		$filekons = $r['foto_kartukonsumen'];
		$filekwint = $r['foto_kwitansikembali'];
			?>
			<tr align='left'>
			<td>
				<?php echo $no;?>
			</td>
            <td>
				<?php echo  $r['nama_pembeli']; ?>
            </td>
			<td>
                <?php echo  $r['nama_perumahan']; ?>
            </td>
			<td>
                <?php echo  $r['no_kavling']; ?>
            </td>
			<td>
                <?php echo  $r['tgl_pembatalan']; ?>
            </td>
			<td>
                <?php echo  $r['dana_hangus']; ?>
            </td>
			<td>
                <?php echo  $r['jlh_DP1']; ?>
            </td>
			<td>
                <?php echo  $r['nominal_pengembalian']; ?>
            </td>
			<td>
				<?php if(empty($filekons)){  ?>
				
				<?php }
				else{?>
				<a href="../action/marketing/act_dp.php?mod=marketing&act=download&file=<?php echo $filekons;?>">
				<img src="kwitansipengembaliandp/download.png" width='50px' height='50px' title="download file">
				</a>
				<?php }?>
            </td>
			<td>
				<?php if(empty($filekwint)){  ?>
				
				<?php }else{?>
                <a href="../action/marketing/act_dp.php?mod=marketing&act=download&file=<?php echo $filekwint;?>">
				<img src="kwitansipengembaliandp/download.png" width='50px' height='50px' title="download file">
				</a>
				<?php }?>
            </td>
			<td>
                <a href="../action/marketing/act_dp.php?mod=marketing&act=hapusdp&idp=<?php echo $id;?>" title="hapus" onclick="return confirm('Anda ingin menghapus data ini?')"> 
                <button class='btn btn-danger btn-sm' alt="Hapus"><span class="glyphicon glyphicon-trash"></span></button> </a>
			</td>
        </tr>
		
		<?php
            $no++;
        }
        ?>
        </tbody>
    </table> 
	<span class="container">
	   <a class="btn btn-primary " data-toggle="modal" href="#tambahDP"><span class="glyphicon glyphicon-plus"></span>Pengembalian DP</a>
	</span>
	
	
	 <!-- Modal for add transaksi perumahan-->
    <div id="tambahDP" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Form Pengembalian DP</h4>
                </div>
                <div class="modal-body">
					<form method="post" name="myform" action="../action/marketing/act_dp.php?mod=marketing&act=tambahDP" enctype="multipart/form-data">
					  <div class="form-group">
                        <label for="tgl-setoran" class="control-label">Tanggal Input : </label>
                        <input type="text" class="form-control" id="tglinput" name="tglinput" value="<?php echo date('Y-m-d');?>" disabled>
                      </div>
					  <div class="form-group">
						<label for="tgl_kembali" class="control-label">Tanggal Pengembalian : </label>
						<input type="date" class="form-control" id="tgl_kembali" name="tgl_kembali">
					  </div>
					  <div class="form-group">
						<label for="nmarmh" class="control-label">Nama Perumahan: </label>
						<select name="tipe_rmh" id="tipe_rmh" class="form-control" required="">
						<option value="" selected>- Pilih Perumahan -</option>
						<?php
							$getkategori="SELECT * FROM data_perumahan";
							$tampil=mysqli_query($mysqli,$getkategori);
							while($r=mysqli_fetch_assoc($tampil))
							{
								echo "<option value=$r[id_perumahan]>
									$r[nama_perumahan]</option>";
							}
						?>
						</select>
					  </div>
					  
					  <div class="form-group">
                        <label for="typ" class="control-label">Type Rumah : </label>
						<select name='typ' id='typ' class='form-control' required=''>
						<option value='' selected>- Pilih Type Rumah -</option>
						</select>
					  </div>
					  
					  <div class="form-group">
						<label for="namakav" class="control-label">Kavling: </label>
						<select name="nama_prm" id="nama_prm" class="form-control" required="">
						<option value="" selected>- Pilih Kavling -</option>
						</select>
					  </div>
					  
					  <div class="form-group">
						<label for="nmakons" class="control-label">Nama Konsumen: </label>
						<select name="namakons" id="namakons" class="form-control" required="">
						<option value="" selected>- Pilih Konsumen -</option>
						<?php
							$sql_cek = $mysqli->query(
								"SELECT t.nik_ktp FROM batal_pembelian b,transaksi_perumahan t where t.id_transaksi=b.id_transaksi"
							);

							$cek = mysqli_num_rows($sql_cek);
							if($cek > 0){
								$getkategori="SELECT dp.nik_ktp,dp.nama_pembeli FROM data_pembeli dp,transaksi_perumahan t
											where t.nik_ktp=dp.nik_ktp and t.id_transaksi not in (SELECT t.id_transaksi FROM batal_pembelian b,transaksi_perumahan t where t.id_transaksi=b.id_transaksi)";
								$tampil=mysqli_query($mysqli,$getkategori);
								while($r=mysqli_fetch_assoc($tampil))
								{
									echo "<option value=$r[nik_ktp]>
										$r[nama_pembeli]</option>";
								}
							}
							else{
								$getkategori="SELECT dp.nik_ktp,dp.nama_pembeli FROM data_pembeli dp,transaksi_perumahan t
											where t.nik_ktp=dp.nik_ktp";
								$tampil=mysqli_query($mysqli,$getkategori);
								while($r=mysqli_fetch_assoc($tampil))
								{
									echo "<option value=$r[nik_ktp]>
										$r[nama_pembeli]</option>";
								}
							}
						?>
						</select>
					  </div>
					 
					 <div class="form-group">
						<input type="hidden" class="form-control" id="dp2" name="dp">
					  </div>
					  
					 <div class="form-group">
						<label for="nominalkembali" class="control-label">Nominal Pengembalian: </label>
						<input type="text" class="form-control" id="nominalkembali" name="nominalkembali">
					  </div>
					  
					   <div class="form-group">
                            <label for="danahngus" class="control-label">Dana Hangus :</label>
                            <input type="text" class="form-control" id="danahngus" name="danahngus">
                        </div>
					  
					  <div class="form-group">
						<label for="kartukons" class="control-label">Upload Kartu Konsumen: </label>
						<input type="file" class="form-control" id="kartukons" name="kartukons">
					  </div>
					  
					   <div class="form-group">
						<label for="kwintansikembali" class="control-label">Upload Kwitansi Pengembalian: </label>
						<input type="file" class="form-control" id="kwintansikembali" name="kwintansikembali">
					  </div>
					  
					  <div class="modal-footer">
						<input type="Submit" class="btn btn-primary" value="Save">
						<input type="reset" class="btn btn-primary" value="Reset">
					  </div>
					</form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    $(function() {
        $("#dp").dataTable();
    });
    </script>
	<?php

	}
	?>
	