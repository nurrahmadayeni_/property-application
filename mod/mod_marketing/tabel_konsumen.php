<link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../../assets/css/dataTables.bootstrap.css"/>

<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> DAFTAR KONSUMEN</h3>";
        echo "
            <table id='konsumen' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th width='5%'>No</th>
					 <th width='5%'>NIK</th>
                    <th width='20%'>Nama Konsumen</th>
					<th width='15%'>No.telp/HP</th>
                    <th width='20%'>Email</th>
					<th width='20%'>Keterangan</th>
					<th width='5%'>Aksi</th>
                </tr>
            </thead>
            <tbody>
        ";
       
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,'select * from data_pembeli order by nama_pembeli ASC');
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
		$id = $r['nik_ktp'];
		$idtr = $r['id_transaksi'];
			?>
			<tr align='left'>
            <td>
                <?php echo  $no;?>
            </td>
			<td>
                <?php echo  $id;?>
            </td>
            <td>
				<?php echo  $r['nama_pembeli']; ?>
            </td>
            <td>
                <?php echo  $r['notelp_pembeli']; ?>
            </td>
			<td>
                <?php echo  $r['email']; ?>
            </td>
			<td>
                <?php echo  $r['catatan_khusus']; ?>
            </td>
            <td>
				<a href="#editkonsumen" class="edit-record" data-toggle="modal" data-id="<?php echo $id; ?>"><button class='btn btn-primary btn-sm' alt="edit">
                <span class="glyphicon glyphicon-pencil"></span></button></a>
            </td>
        </tr>
		<?php
            $no++;
        }
        ?>
        </tbody>
    </table>

    <div id="editkonsumen" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Konsumen</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>  
	
    <?php

	}
	?>

	<script>
        $(function(){
            $(document).on('click','.edit-record',function(e){
                e.preventDefault();
                $("#editkonsumen").modal('show');
                $.post('../action/marketing/edit_konsumen.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#editkonsumen').on('hidden.bs.modal', function () {
                    location.reload();
                })
            });
        });
    </script>

    <script type="text/javascript">
    $(function() {
        $("#konsumen").dataTable();
    });
    </script>
