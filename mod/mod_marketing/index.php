<link rel="stylesheet" href="../assets/css/bootstrap.min.css"/>
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<link href="../../images/pavicon.png" rel="icon" type="image/x-icon" />

<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
if(!isset($_SESSION["username"])){
	?>
    <!--Modal Session -->
		<script type="text/javascript">
		    $(document).ready(function(){
		        $("#login").modal('show');
		    });
		</script>

		<div id="login" class="modal fade">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header alert-warning">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                <h4 class="modal-title">WARNING</h4>
		            </div>
		            <div class="modal-body">
					  Please Login First
					</div>
		            <div class = "modal-footer">
			            <a href="../../index.php"><button type = "button" class = "btn btn-warning" >OK</button> </a>
			         </div>
		        </div>
		    </div>
		</div>
    <?php
}else{
?>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">  <head>
<title>MARKETING</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Bootstrap -->
<link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- styles -->
<link href="../../css/main.css" rel="stylesheet">
<link href="../../images/pavicon.png" rel="icon" type="image/x-icon" />

<link rel="stylesheet" href="../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css"/>
<link rel="stylesheet" href="../assets/css/bootstrap-theme.min.css">

<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=PT+Serif:100,200,400,300'>
<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/icon?family=Material+Icons'>

<script src="../assets/js/jquery-1.11.0.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/jquery.maskMoney.min.js"></script>
<script src="../assets/datatables/jquery.dataTables.js"></script>
<script src="../assets/datatables/dataTables.bootstrap.js"></script>

<script>
//copyright
function copyDate() {
	var cpyrt = document.getElementById("copyright")
	if (cpyrt) {
	cpyrt.firstChild.nodeValue = (new Date()).getFullYear();
	}}
window.onload = copyDate;
</script>
	
</head>
  <body>
  	<div class="header" style="background:#01579B;">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-5">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="index.html">Marketing</a></h1>
	              </div>
	           </div>
	           <div class="col-md-5">
	              <div class="row">
	                <div class="col-lg-12">
	                  <div class="input-group form"></div>
	                </div>
	              </div>
	           </div>
	           <div class="col-md-2">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <?php echo"$_SESSION[username]"; ?> <b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                           <li <?php if($page=='mod=data_admin') echo "class='current'" ?>>
									<a href="?mod=data_admin"><i class="glyphicon glyphicon-chevron-right"></i> Profile</a>
								</li> 
	                          <li><a data-toggle = "modal" data-target = "#logout"> Logout </a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	          	</div>
	        </div>
	     </div>
	</div>

    <div class="page-content">
    	<div class="row">
		  <div class="col-md-2">
		  	<div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
					<?php
				        $page = $_SERVER['QUERY_STRING'];
				    ?>
                   <li <?php if($page=='mod=home') echo "class='current'" ?>>
						<a href="?mod=home"><i class="glyphicon glyphicon-record"></i> Home</a>
					</li>
					
				   <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-record"></i> Input
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
							 <li <?php if($page=='mod=data_transaksi') echo "class='current'" ?>>
								<a href="?mod=data_transaksi"><i class="glyphicon glyphicon-chevron-right"></i> Transaksi</a></li>
							 <li <?php if($page=='mod=berkas_konsumen') echo "class='current'" ?>>
								<a href="?mod=berkas_konsumen"><i class="glyphicon glyphicon-chevron-right"></i> Berkas Konsumen</a></li>
							 <li <?php if($page=='mod=data_progressetoran') echo "class='current'" ?>>
								<a href="index.php?mod=data_progressetoran"><i class="glyphicon glyphicon-chevron-right"></i> Pemasukan</a>
							 </li>
							 <li <?php if($page=='data_pengembaliandp') echo "class='current'" ?>>
								<a href="index.php?mod=data_pengembaliandp"><i class="glyphicon glyphicon-chevron-right"></i> Pengembalian DP</a>
							 </li>
						 </ul>
					</li>
                   <li class="submenu">
						<a href="">
                            <i class="glyphicon glyphicon-record"></i> Data
                            <span class="caret pull-right"></span>
                         </a>
						<ul>
							<li <?php if($page=='mod=data_konsumen') echo "class='current'" ?>>
								<a href="?mod=data_konsumen"><i class="glyphicon glyphicon-chevron-right"></i> Data Konsumen</a></li>
							
                            <li <?php if($page=='data_perumahan') echo "class='current'" ?>>
								<a href="index.php?mod=data_perumahan"><i class="glyphicon glyphicon-chevron-right"></i> Data Perumahan</a>
							</li>
							<li <?php if($page=='data_pembayaran') echo "class='current'" ?>>
								<a href="index.php?mod=data_pembayaran"><i class="glyphicon glyphicon-chevron-right"></i> Data Progress Pembayaran</a>
							</li>
							<li <?php if($page=='data_berkaskpr') echo "class='current'" ?>>
								<a href="index.php?mod=data_berkaskpr"><i class="glyphicon glyphicon-chevron-right"></i> Data berkas KPR</a>
							</li>
							<li <?php if($page=='data_batalkonsumen') echo "class='current'" ?>>
								<a href="index.php?mod=data_batalkonsumen"><i class="glyphicon glyphicon-chevron-right"></i> Data Batal Transaksi</a>
							</li>
							<li <?php if($page=='data_notrefund') echo "class='current'" ?>>
								<a href="index.php?mod=data_notrefund"><i class="glyphicon glyphicon-chevron-right"></i> Data Konsumen Batal dan Belum Refund</a>
							</li>
                        </ul>
					</li>
					
					<li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-record"></i> Output
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
		                    <li <?php if($page=='data_laporan') echo "class='current'" ?>>
								<a href="index.php?mod=data_laporan"><i class="glyphicon glyphicon-record"></i> Laporan</a>
							</li>
							<li <?php if($page=='data_rekap') echo "class='current'" ?>>
								<a href="index.php?mod=data_rekap"><i class="glyphicon glyphicon-record"></i> Rekap</a>
							</li>
							<li <?php if($page=='data_closing') echo "class='current'" ?>>
								<a href="index.php?mod=data_closing"><i class="glyphicon glyphicon-record"></i> Closing</a>
							</li>         
						</ul>
					</li>
					
					<li <?php if($page=='data_kas') echo "class='current'" ?>>
						<a href="index.php?mod=data_kas"><i class="glyphicon glyphicon-record"></i> Kas</a>
					</li>
                     
					<li <?php if($page=='data_keluar') echo "class='current'" ?>>
						<a href="index.php?mod=data_keluar"><i class="glyphicon glyphicon-record"></i> Pengeluaran Marketing</a>
					</li>
					
					<li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-record"></i> Account
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
		                    <li <?php if($page=='mod=data_admin') echo "class='current'" ?>>
								<a href="?mod=data_admin"><i class="glyphicon glyphicon-chevron-right"></i> Profile</a>
							</li>   
							<li><a href="#" data-toggle = "modal" data-target = "#logout"><i class="glyphicon glyphicon-chevron-right"></i> Logout</a></li>            
						</ul>
					</li>
                 </ul>
             </div>
		  </div>
		  <div class="col-md-10">
			<div class="content-box-large">
				<?php 
					include '../../config/connectdb.php';
					
					if($_GET[mod]=='logout') {
						?>
						<a href="#" data-toggle = "modal" data-target = "#logout"> Logout</a>
						<?php
					}
					elseif($_GET[mod]=='data_admin'){
						include "profile.php";
					}
					else if($_GET[mod]=='home'){
						include "home.php";
					}
					else if($_GET[mod]=='data_transaksi'){
						include "tabel_transaksi.php";
					}
					else if($_GET[mod]=='data_konsumen'){
						include "tabel_konsumen.php";
					}
					else if($_GET[mod]=='data_pengembaliandp'){
						include "tabel_pengembaliandp.php";
					}
					else if($_GET[mod]=='data_progressetoran'){
						include "tabel_progresssetoran.php";
					}
					else if($_GET[mod]=='detail_setoran2'){
						include "tabel_setoran2.php";
					}
					else if($_GET[mod]=='berkas_konsumen'){
						include "berkas_konsumen.php";
					}
					else if($_GET[mod]=='add_berkas'){
						include "add_berkas.php";
					}
					else if($_GET[mod]=='data_perumahan'){
						include "tabel_perumahan.php";
					}
					else if($_GET[mod]=='detail_kavling'){
						include "showdetail_kavling.php";
					}
					else if($_GET[mod]=='detail_kavlingrumah'){
						include "showdetail_kavlingrumah.php";
					}
					else if($_GET[mod]=='data_pembayaran'){
						include "showdata_pembayaran.php";
					}
					else if($_GET[mod]=='data_detailperumahan'){
						include "data_detailperumahan.php";
					}
					else if($_GET[mod]=='data_laporan'){
						include "show_laporan1.php";
					}
					else if($_GET[mod]=='data_rekap'){
						include "show_rekap.php";
					}
					else if($_GET[mod]=='data_closing'){
						include "show_closing.php";
					}
					else if($_GET[mod]=='data_kas'){
						include "data_kas.php";
					}
					else if($_GET[mod]=='data_berkaskpr'){
						include "data_berkaskpr.php";
					}
					else if($_GET[mod]=='data_batalkonsumen'){
						include "data_batalkonsumen.php";
					}
					else if($_GET[mod]=='data_notrefund'){
						include "data_notrefund.php";
					}
					else if($_GET[mod]=='data_keluar'){
						include "data_keluar.php";
					}
				?>
			</div>
		  </div>
		</div>
    </div>
    <!-- Modal LOGOUT-->
		<div class = "modal fade" id = "logout" tabindex = "-1" role = "dialog" 
		   aria-labelledby = "myModalLabel" aria-hidden = "true">
		   
		   <div class = "modal-dialog">
		      <div class = "modal-content">
		         
		         <div class = "modal-header">
		            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
		                  &times;
		            </button>
		            <h4 class = "modal-title" id = "myModalLabel">
		               CONFIRMASI LOGOUT
		            </h4>
		         </div>
		         <div class = "modal-body">
		            Apakah Anda yakin ingin keluar dari halaman marketing?
		         </div>
		         <div class = "modal-footer">
		            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
		               Tidak
		            </button>
		            <a href="logout.php"><button type = "button" class = "btn btn-primary" >Ya</button> </a>
		         </div>
		      </div><!-- /.modal-content -->
		   </div><!-- /.modal-dialog -->
		  
		</div><!-- /.modal -->
    <footer style="background:#01579B;">
         <div class="container">
            <div class="copy text-center">
               Copyright&copy; <span id="copyright">...</span> <a href='#'>PT Matahari Cipta | Developed by Lunata IT & Consultant</a>
            </div>          
         </div>
      </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../js/custom.js"></script>

  </body>
</html>

<?php
}
?>