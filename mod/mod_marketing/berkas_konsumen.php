<link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../../assets/css/dataTables.bootstrap.css"/>

<script>
  $(document).ready(function() {
    $("#idtran").change(function() {
        var idtr3 = $('#idtran').val();		
		$.post('../action/marketing/show_rumah.php', // request ke file load_data.php
		{parent_id: idtr3},
		function(data){
			 $('#nama_pembeli').val(data[0].nama_pembeli);
			 $('#nama_perumahan').val(data[0].nama_perumahan);
			 $('#no_kavling').val(data[0].no_kavling);
			 $('#jenis_pembayaran').val(data[0].jenis_pembayaran);

			 if( $('#jenis_pembayaran').val() == 'Tunai(cash)'){
                $('#cash').show();
				$('#kpr').hide();
            }else if( $('#jenis_pembayaran').val() == 'Kredit(KPR)'){
				$('#cash').hide();
                $('#kpr').show();
            }

			 // tanggl
			 var now = new Date();
			 var day = ("0" + now.getDate()).slice(-2);
			 var month = ("0" + (now.getMonth() + 1)).slice(-2);
			 var today = now.getFullYear() + "-" + (month) + "-" + (day);
			
			 var tgl_jatuhtempo=data[0].tgl_jatuhtempo;
			 var entry_file = data[0].entry_file;
			 var entry = data[0].entry;
			 var analis = data[0].analis;
			 var ots = data[0].ots;
			 var sp3k = data[0].sp3k;
			 var akad = data[0].akad;
			 var serah = data[0].serah_terima;
			 var closing = data[0].closing;

			  if(entry_file == '' && entry==''){			 	
			  	$("#cashkprfile1").prop( "disabled", false );
			  	$("#entry_file").prop( "disabled", false );
			  	$("#entry").prop("disabled", false );
			  }
			  else if(analis == ''){
			  	$("#analis").prop("disabled", false );
			  }else if(ots == ''){
			  	$("#ots").prop( "disabled", false );
			  	$("#analis").attr("checked", true );
			  }else if(sp3k ==''){
			  	$("#sp3k").prop( "disabled", false );
			  	$("#analis").attr("checked", true );
			  	$("#ots").attr("checked", true );
			  }else if(akad ==''){
			  	$("#akad").prop( "disabled", false );
			  	$("#analis").attr("checked", true );
			  	$("#ots").attr("checked", true );
			  	$("#sp3k").attr("checked", true );
			  }else if(serah ==''){
			  	$("#serahterima").prop( "disabled", false );
			  	$("#analis").attr("checked", true );
			  	$("#ots").attr("checked", true );
			  	$("#sp3k").attr("checked", true );
			  	$("#akad").attr("checked", true );
			  }else if (closing==''){
			  	$("#closing").prop( "disabled", false );
			  	$("#analis").attr("checked", true );
			  	$("#ots").attr("checked", true );
			  	$("#sp3k").attr("checked", true );
			  	$("#akad").attr("checked", true );
			  	$("#serahterima").attr("checked", true );
			  }
			  else{
			  	$("#analis").attr("checked", true );
			  	$("#ots").attr("checked", true );
			  	$("#sp3k").attr("checked", true );
			  	$("#akad").attr("checked", true );
			  	$("#serahterima").attr("checked", true );
			  	$("#closing").attr("checked", true );
			  }
		},'json'
      );
   });
  });
</script>

<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center>BERKAS TRANSAKSI KONSUMEN</h3>";
        echo "
            <table id='konsumen' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th width='5%'>No</th>
					<th width='15%'>ID Transaksi</th>
                    <th width='20%'>Nama Konsumen</th>
                    <th width='20%'>Email</th>
					<th width='5%'>Berkas Konsumen</th>
                </tr>
            </thead>
            <tbody>
        ";
		
		$sql = mysqli_query($mysqli,'select p.nama_pembeli,p.nik_ktp,p.email,b.id_transaksi from data_pembeli p,transaksi_perumahan t ,berkas_kons b where t.nik_ktp=p.nik_ktp and b.id_transaksi=t.id_transaksi group by t.id_transaksi order by p.nama_pembeli ASC ');
        
		$no = 1;
        while ($r = mysqli_fetch_array($sql)) {
		$id = $r['nik_ktp'];
		$idtr = $r['id_transaksi'];
			?>
			<tr align='left'>
            <td>
                <?php echo  $no;?>
            </td>
			<td>
                <?php echo  $r['id_transaksi']; ?>
            </td>
            <td>
				<?php echo  $r['nama_pembeli']; ?>
            </td>
			<td>
                <?php echo  $r['email']; ?>
            </td>
            <td>
				<a href="index.php?mod=add_berkas&id=<?php echo $id; ?>&idtr=<?php echo $idtr; ?>" class="btn btn-primary" data-toggle="modal">
                Detail
				</a>
            </td>
        </tr>
		<?php
            $no++;
        }
        ?>
        </tbody>
    </table>
	 <span class="container">
	   <a class="btn btn-primary " data-toggle="modal" href="#tambahberkas"><span class="glyphicon glyphicon-plus"></span>Berkas</a>
	 </span>

	  <div id="tambahberkas" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Form Input Berkas Konsumen</h4>
                </div>
                <div class="modal-body">
				<form method='post' action='../action/marketing/aksi_addberkas.php?mod=marketing&act=tambahberkas' enctype='multipart/form-data'>
                     <div class="form-group">
                        <label for="idtran3" class="control-label">ID Transaksi: </label>
                        <select name='idtran' id='idtran' class='form-control' required=''>
						<option value='' selected>- Pilih ID Transaksi -</option>
							<?php
								$getkategori="SELECT dt.id_transaksi,dt.id_jenispembayaran FROM detail_transaksirumah dt,berkas_kons bk where dt.id_transaksi=bk.id_transaksi and dt.status_batal=''";
								$tampil=mysqli_query($mysqli,$getkategori);
								while($r=mysqli_fetch_assoc($tampil))
								{
									echo "<option value=$r[id_transaksi]>
										$r[id_transaksi]</option>";
								}
							?>
						</select>
                      </div>

					<div class='form-group'>
						<label for="nmakons" class="control-label">Nama Konsumen: </label>
						<input type="text" class="form-control" name="namakons" id="nama_pembeli" disabled>
					</div>
					<div class='form-group'>
						<label class='control-label'>Perumahan : </label>
						<input type="text" class="form-control" name='tipe_rmh' id='nama_perumahan'  disabled>
					</div>
					<div class='form-group'>
						<label for='namakav' class='control-label'> Kavling: </label>
						<input type="text" class="form-control" name='nama_prm' id='no_kavling' disabled>
					</div>
					
					<div class='form-group'>
						<label for='jenispm' class='control-label'> Jenis Pembayaran: </label>
						<input type="text" class="form-control" name='jenispm' id='jenis_pembayaran'  disabled>
					</div>
		
					<div id="cash">
					<div class='form-group'><label for='cash3' class='control-label'> >>Cash </label></div>
					
					<div class='form-group'>
					<label for='cash' class='control-label'> Upload Data .pdf</label>
					<input type='file' class='form-control' id='cashkprfile1' name='cashfile' accept='application/pdf' disabled>
					</div>
					</div>
					
					<div id="kpr">
					<div class='form-group'><label for='KPR3' class='control-label'> >>KPR </label></div>
					
					<div class='form-group'>
						<label for='kpr1' class='control-label'>Upload Data .pdf</label>
						<input type='file' class='form-control' id="entry_file" name='cashkprfile1' accept='application/pdf' disabled>
					</div>

					<div class='form-group' id="status">
						<label for='status' class='control-label'> Status :</label>
					</div>
					<table style="font-size: 13px;">
					<tr>
					<td>
						<div class='form-group'>
						<label for='entry' class='control-label'>Entry</label></div>
						</td>
						<td>
						<input type="checkbox" name="entry" id="entry" checked disabled>
						</td>
					</tr>
					<tr>
						<td>
						<div class='form-group' >
						<label for='analis' class='control-label'>Analis</label></div>
						</td>
						<td>
						<input type="checkbox" name="analis" id="analis" disabled>
						</td>
					</tr>
					<tr>
						<td>
						<div class='form-group' >
							<label for='analis' class='control-label'>OTS</label></div></td>
						<td>
							<input type="checkbox" name="ots" id="ots" disabled>
						</td>
					</tr>
					<tr>
						<td>
						<div class='form-group' >
							<label for='sp3k' class='control-label'>SPK3K</label></div></td>
						<td>
							<input type="checkbox" name="sp3k" id="sp3k" disabled>
						</td>
					</tr>
					<tr>
						<td>
						<div class='form-group'>
							<label for='akadd' class='control-label'>Akad</label></div></td>
						<td>
							<input type="checkbox" name="akad" id="akad" disabled>
						</td>
					</tr>
					<tr>
						<td>
						<div class='form-group'>
							<label for='srahterima' class='control-label'>Serah Terima</label></div></td>
						<td>
							<input type="checkbox" name="serahterima" id="serahterima" disabled>
						</td>
					</tr>
					<tr>
						<td>
						<div class='form-group'>
							<label for='closing' class='control-label'>Closing</label></div></td>
						<td>
							<input type="checkbox" name="closing" id="closing" disabled>
						</td>
					</tr>
					</table>
					</div>
					<div class='modal-footer'>
						<input type='submit' value='save' class='btn btn-primary'>
					</div>
				</form>
                </div>
            </div>
        </div>
    </div>  
	
    <div id="addberkas" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Berkas Konsumen</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>  
	
    <?php

	}
	?>
    <script type="text/javascript">
    $(function() {
        $("#konsumen").dataTable();
    });
    </script>