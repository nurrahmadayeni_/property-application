<link rel="stylesheet" href="../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css"/>

<?php   
	include '../../config/connectdb.php';
	error_reporting(0);
	
	echo "<h3 align=center style='margin-bottom:90px;'> Data Konsumen Batal Transaksi dan Belum Pengembalian DP</h3>
		
		<table id='batal' class='table table-bordered table-hover'>
        <thead>
            <tr>
                <th width='5%' align='center'>No</th>
				<th width='10%'>Nama Konsumen</th>
                <th width='10%'>Nama Perumahan</th>
                <th width='15%'>No Kavling</th>
                <th width='15%'>Tanggal Pembatalan Transaksi</th>
            </tr>
        </thead>
        <tbody>
        ";
		
	$sql = mysqli_query($mysqli,"SELECT df.id_transaksi,p.nama_pembeli,dp.no_kavling,dr.nama_perumahan,bt.tgl_pembatalan
	from data_konsumenrefund df,transaksi_perumahan tr,data_pembeli p,detail_transaksirumah dp,data_perumahan dr,batal_transaksi bt
	where tr.id_transaksi=df.id_transaksi and p.nik_ktp=tr.nik_ktp and dp.id_transaksi=df.id_transaksi and dr.id_perumahan=dp.id_perumahan
	and bt.id_transaksi=df.id_transaksi order by p.nama_pembeli ASC");
		$no = 1;
		while ($r = mysqli_fetch_array($sql)) {
			$nakons= $r['nama_pembeli'];
			$nokav= $r['no_kavling'];
			$narum= $r['nama_perumahan'];
			$tgl= $r['tgl_pembatalan'];
		echo"
		<tr align='left'>
			<td align='center'>$no</td>
			<td>$nakons</td>
			<td>$narum</td>
			<td>$nokav</td>
			<td>$tgl
            </td>
		</tr>
		";
		   
		$no++;
	} 
?>
</tbody>
</table>

<script type="text/javascript">
    $(function() {
        $("#batal").dataTable();
    });
</script>
	

