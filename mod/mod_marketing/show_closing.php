
<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center>DAFTAR CLOSING</h3><br>";
        echo "
            <table id='closing' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th>No</th>
					<th>ID Transaksi</th>
                    <th>Nama Konsumen</th>
					<th>Perumahan</th>
                    <th>Kavling</th>
					<th>Harga Total Rumah (Rp)</th>
                    <th>Total Setoran (Rp)</th>
                    <th>Tanggal Lunas</th>
					<th>Upload Kartu Konsumen</th>
                    <th>Menu</th>
                </tr>
            </thead>
            <tbody>
        ";
       
        include '../../config/connectdb.php';


        $sql = mysqli_query($mysqli,"SELECT p.nama_pembeli, r.nama_perumahan,dt.total_hrgarumah, dt.no_kavling,t.id_transaksi,b.tgl_setoran,sum(b.jlh_setoran) as jlh_setoran
        FROM data_pembeli p, data_perumahan r,detail_transaksirumah dt,transaksi_perumahan t,data_kavling dk, bayar_rumah b
        WHERE t.nik_ktp=p.nik_ktp and b.nik_ktp=p.nik_ktp 
        AND dt.id_perumahan=r.id_perumahan
        AND dt.no_kavling=dk.no_kavling 
        AND dt.id_transaksi=t.id_transaksi GROUP BY t.id_transaksi ORDER BY b.tgl_setoran  DESC");

        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
		$id = $r['id_transaksi'];
        $htr= $r['total_hrgarumah'];
        $setr=$r['jlh_setoran'];
	   
        if($htr==$setr ){
        ?>
		<tr align='left'>
            <td><?php echo  $no;?></td>
			<td><?php echo  $id;?></td>
            <td><?php echo  $r['nama_pembeli']; ?></td>
            <td><?php echo  $r['nama_perumahan']; ?></td>
			<td><?php echo  $r['no_kavling']; ?></td>
			<td align="right"><?php $rupiah=number_format($htr,0,',','.'); 
                    echo $rupiah; 
                ?>  
            </td>
            <td align="right"><?php $rupiah=number_format($r['jlh_setoran'],0,',','.'); 
                    echo $rupiah; 
                ?> 
            </td>
            <td><?php echo $r['tgl_setoran']; ?></td>
            <td></td>

            <td><a href="#edit_kartu" title="edit" data-toggle="modal" class="edit-record" data-id="<?php echo $id; ?>"><button class='btn btn-primary btn-sm' alt="edit">
                <span class="glyphicon glyphicon-pencil"></span></button></a></td>
        </tr>
		<?php

            $no++;
        }else{
            echo" "; 
        }

        }
    }
        ?>
        </tbody>
    </table>

    <div id="edit_kartu" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Upoad Kartu Konsumen</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>

    <span class="container">
        <button type="button" class="btn btn-primary" onclick="print_b()" >
        <span class="glyphicon glyphicon-print"></span> Print Closing</button>
    </span>

    <script type="text/javascript">
    $(function() {
        $("#closing").dataTable();
    });
    </script>



    <script>
        $(function(){
            $(document).on('click','.edit-record',function(e){
                e.preventDefault();
                $("#edit_kartu").modal('show');
                $.post('../action/marketing/upload_kartu.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#edit_kartu').on('hidden.bs.modal', function () {
                 location.reload();
                })
            });
        });
    </script>

<script>
    function print_b(){
        <?php
        echo "window.open('../action/marketing/printclosing.php','_blank');";
        ?>
    }
</script>
