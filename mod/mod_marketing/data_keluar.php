<link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../../assets/css/dataTables.bootstrap.css"/>
<?php
	error_reporting(0);
	include '../../config/connectdb.php';
	$tglsekarang=date('Y-m-d');
	echo"
		<form method='post' action='../action/marketing/act_kredit.php?mod=marketing&act=tambahkredit'>
			<h3 align='center'>Form Pengeluaran Marketing</h3><br><br>
			<table border=0 width='80%'>
				<tr>
					<td><label class='control-label' style='padding:5%;'>Tanggal Sekarang: </label></td>
					<td><input type='date' name='tgl' required='' value='$tglsekarang' class='form-control' disabled></td>
					<td>&nbsp &nbsp</td>
					<td width='15%'><label class='control-label' style='padding:0%;'>Tanggal Keluar: </label></td>
					<td><input type='date' name='tgl_cek' required=''  class='form-control'></td>
				</tr>
				<tr>
					<td width='20%'><label class='control-label' style='padding:5%;'>Jenis : </label> </td>
					<td><input type='text' name='jenis' value='Kredit' class='form-control' disabled>
					</td>
				</tr>
				<tr>
					<td width='13%'><label class='control-label' style='padding:5%;'>Giro : </label></td>
					<td><select name='cek' id='cek' class='form-control' required=''>
							<option value='' selected disabled=''>- Pilih -</option>
							<option value='ADM'> ADM</option>
							<option value='Dan Lain-Lain'> Dan Lain-Lain</option>
				</select>
					</td>
				</tr>
				<tr><div class='form-group'>
					<td><label class='control-label' style='padding:5%;'>Keterangan : </label></td>
					<td><textarea name='keterangan' class='form-control'></textarea></td>
					</div>
				</tr>
				<tr><div class='form-group'>
					<td><label class='control-label' style='padding:5%;'>Nominal : </label></td>
					<td><input type='text' name='nominal' id='nominal' class='form-control'>";
					?>
						<script type='text/javascript'>
							$(document).ready(function(){
								$('#nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
							});
						</script>						
					<?php
				echo"
					</td>
					</div>
				</tr>
				<tr>
					<td><br><input type='submit' value='Input' class='btn btn-primary btn-sm'>&nbsp;
					<input type='reset' value='Reset' class='btn btn-primary btn-sm'></td>
				</tr>
			</table>
		</form>
		";	

	echo "<br><br>
		";
		?>
		
		<table id='Dkeuangan' class='table table-bordered table-hover'>
			<tr bgcolor=#cfcfcf align='center'>
				<td>No</td>
				<td>Tanggal Keluar</td>
				<td>Tujuan Giro</td>
				<td>Keterangan</td>
                <th><center>Nominal Kredit (Rp)</center></th>
				<th>Aksi</th>
			</tr>
			<tbody>
			<?php
			$tampil = "SELECT g.jenis_giro,g.no_giro,k.tgl_sekarang,k.id_jeniskeuangan,k.nominal,k.keterangan,k.id_keuangan,jb.jenis_keuangan
						FROM data_keuangan k, giro g,jenis_bayar jb
						WHERE k.no_giro=g.no_giro  and jb.id_jeniskeuangan=k.id_jeniskeuangan
						and k.giro_asal='Marketing'";
			
			$queri=mysqli_query($mysqli,$tampil);
			$no=1;
			while($row = mysqli_fetch_array($queri)){
				$id=$row['id_keuangan'];
				$row1=number_format($row[nominal],0,',','.');
            
			echo "<tr align='center'>
				<td>$no</td>
				<td>$row[tgl_sekarang]</td>
				<td>$row[jenis_giro]</td>
				<td>$row[keterangan]</td>
				<td align='right'>".$row1."</td>
				<td>
					<a href='#edit_nominal' class='edit-record' data-toggle='modal' data-id='$id'><button class='btn btn-primary btn-sm' alt='edit'>
					<span class='glyphicon glyphicon-pencil'></span></button></a>
				</td>
			</tr>";
			$no++;
			}
			?>
			</tbody>
		</table>

<!-- Modal for Edit Giro-->
    <div id="edit_nominal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Kredit Marketing</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function(){
            $(document).on('click','.edit-record',function(e){
                e.preventDefault();
                $("#edit_nominal").modal('show');
                $.post('../action/marketing/edit_kredit.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#edit_nominal').on('hidden.bs.modal', function () {
                 location.reload();
                })
            });
        });
    </script>
		        
	<script src="../../assets/js/jquery-1.11.0.js"></script>
	<script src="../../assets/js/bootstrap.min.js"></script>
	<script src="../../assets/datatables/jquery.dataTables.js"></script>
	<script src="../../assets/datatables/dataTables.bootstrap.js"></script>
