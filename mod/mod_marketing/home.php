<?php
	include '../../config/connectdb.php';
    error_reporting(0);
	session_start();
    switch($_GET[act]){
        default:
		$sqlprogress = "SELECT pp.id_perumahan, p.nama_perumahan, pp.no_kavling, pp.id_pekerjaan, k.id_kategorikerja, 
			k.nama_kategorikerja, pp.tgl_laporan, COUNT(pp.progress) as 'siap' 
			FROM data_perumahan p, progress_pembangunan pp, kategori_pekerjaan k , data_pekerjaan d 
			WHERE p.id_perumahan=pp.id_perumahan 
			AND k.id_kategorikerja=d.id_kategorikerja 
			AND d.id_pekerjaan=pp.id_pekerjaan 
			AND pp.id_perumahan='$namarmh' 
			AND pp.no_kavling='$nokav' 
			AND pp.progress='1' 
			GROUP BY k.id_kategorikerja
		";
		$tampilp=mysqli_query($mysqli,$sqlprogress);
		$rowp = mysqli_fetch_array($tampilp);
		
		$sqlpkerjaan = "SELECT id_pekerjaan FROM data_pekerjaan WHERE id_kategorikerja=$rowp[id_kategorikerja]";
		$resultkerja=mysqli_query($mysqli,$sqlpkerjaan);
		$h = mysqli_num_rows($resultkerja);
		
		$persen = round(($rowp['siap'] / $h) *100);
		
		$tglsekarang=date("Y-m-d");
		$sql = mysqli_query($mysqli,"select tgl_jatuhtempo,entry,analis,sp3k,akad,serah_terima,closing from berkas_kons where id_jenispembayaran='2'");	
		$r = mysqli_fetch_array($sql);
		$jatuhtempo = $r['tgl_jatuhtempo'];
				
		$mod_date=strtotime($tglsekarang."+1day");
		$newdate=date("Y-m-d",$mod_date);
			
		if($jatuhtempo==$tglsekarang and $r['analis']==''){
?>
			<script>
				alert("silahkan upload berkas analis")
			</script>
			<div id="home">
				<posttitle><center><h3>Reminder Berkas</h3><br/></center></posttitle>
				<p style="color:red;">*Berikut konsumen yang harus upload berkas analis</p>
				<br/>
					<div id="batas2">
						<div>
							<?php
								echo "
									<table id='home' style='color:red;' class='table table-bordered table-hover'>
									<thead>
										<tr>
											<th width='5%'><b>No</b></th>
											<th width='5%'><b>NIK</b></th>
											<th width='20%'><b>Nama Konsumen</b></th>
											<th width='15%'><b>Tanggal Upload Berkas Awal</b></th>
											<th width='15%'><b>Tanggal Jatuh Tempo</b></th>
										</tr>
									</thead>
									<tbody>
								";
								$no = 1;
								echo "<div style='float:left;'>";
								$q = mysqli_query($mysqli,"select t.nik_ktp,p.nama_pembeli,t.tgl_booking,b.tgl_upload,b.tgl_jatuhtempo from transaksi_perumahan t,berkas_kons b,data_pembeli p where p.nik_ktp=t.nik_ktp and  b.id_transaksi=t.id_transaksi and b.tgl_jatuhtempo='".date("Y-m-d")."' and b.id_jenispembayaran='2'");
								while ($row = mysqli_fetch_array($q)) {
									echo"<tr align='left'>
										<td>$no</td>
										<td>
											$row[nik_ktp]
										</td>
										<td>
											$row[nama_pembeli]
										</td>
										<td>
											$row[tgl_upload]
										</td>
										<td>
											$row[tgl_jatuhtempo]
										</td>
										";
								$no++;
								}
								echo"
								</tbody>
								</table>
								";
							?>						
							</div>
						</div>
					</div>
				</div>
		<?php
			}
			else if($jatuhtempo==$tglsekarang and $r['sp3k']==''){
?>
			<script>
				alert("silahkan upload file sp3k")
			</script>
			<div id="home">
				<posttitle><center><h3>Reminder Berkas</h3><br/></center></posttitle>
				<p style="color:red;">*Berikut konsumen yang harus upload berkas sp3k</p>
				<br/>
					<div id="batas2">
						<div>
							<?php
								echo "
									<table id='home' style='color:red;' class='table table-bordered table-hover'>
									<thead>
										<tr>
											<th width='5%'><b>No</b></th>
											<th width='5%'><b>NIK</b></th>
											<th width='20%'><b>Nama Konsumen</b></th>
											<th width='15%'><b>Tanggal Upload Berkas Awal</b></th>
											<th width='15%'><b>Tanggal Jatuh Tempo</b></th>
										</tr>
									</thead>
									<tbody>
								";
								$no = 1;
								echo "<div style='float:left;'>";
								$q = mysqli_query($mysqli,"select t.nik_ktp,p.nama_pembeli,t.tgl_booking,b.tgl_upload,b.tgl_jatuhtempo from transaksi_perumahan t,berkas_kons b,data_pembeli p where p.nik_ktp=t.nik_ktp and  b.id_transaksi=t.id_transaksi and b.tgl_jatuhtempo='".date("Y-m-d")."' and b.id_jenispembayaran='2'");
								while ($row = mysqli_fetch_array($q)) {
									echo"<tr align='left'>
										<td>$no</td>
										<td>
											$row[nik_ktp]
										</td>
										<td>
											$row[nama_pembeli]
										</td>
										<td>
											$row[tgl_upload]
										</td>
										<td>
											$row[tgl_jatuhtempo]
										</td>
										";
								$no++;
								}
								echo"
								</tbody>
								</table>
								";
							?>						
							</div>
						</div>
					</div>
				</div>
		<?php
			}
			else if($jatuhtempo==$tglsekarang and $r['akad']=='' and $persen=='80'){
?>
			<script>
				alert("Progress Rumah Sudah 80% silahkan upload file akad,seraah terima dan closing")
			</script>
			<div id="home">
				<posttitle><center><h3>Reminder Berkas</h3><br/></center></posttitle>
				<p style="color:red;">*Berikut daftar konsumen yang progress perumahannya sudah 80%</p>
				<br/>
					<div id="batas2">
						<div>
							<?php
								echo "
									<table id='home' style='color:red;' class='table table-bordered table-hover'>
									<thead>
										<tr>
											<th width='5%'><b>No</b></th>
											<th width='5%'><b>NIK</b></th>
											<th width='20%'><b>Nama Konsumen</b></th>
											<th width='15%'><b>Tanggal Upload Berkas Awal</b></th>
											<th width='15%'><b>Tanggal Jatuh Tempo</b></th>
										</tr>
									</thead>
									<tbody>
								";
								$no = 1;
								echo "<div style='float:left;'>";
								$q = mysqli_query($mysqli,"select t.nik_ktp,p.nama_pembeli,t.tgl_booking,b.tgl_upload,b.tgl_jatuhtempo from transaksi_perumahan t,berkas_kons b,data_pembeli p where p.nik_ktp=t.nik_ktp and  b.id_transaksi=t.id_transaksi and b.tgl_jatuhtempo='".date("Y-m-d")."' and b.id_jenispembayaran='2'");
								while ($row = mysqli_fetch_array($q)) {
									echo"<tr align='left'>
										<td>$no</td>
										<td>
											$row[nik_ktp]
										</td>
										<td>
											$row[nama_pembeli]
										</td>
										<td>
											$row[tgl_upload]
										</td>
										<td>
											$row[tgl_jatuhtempo]
										</td>
										";
								$no++;
								}
								echo"
								</tbody>
								</table>
								";
							?>						
							</div>
						</div>
					</div>
				</div>
		<?php
			}
			else if($jatuhtempo==$tglsekarang and $r['analis']=='' and $r['sp3k']==''){
?>
			<script>
				alert("silahkan upload file analis dan sp3k berkas konsumen")
			</script>
			<div id="home">
				<posttitle><center><h3>Reminder Berkas</h3><br/></center></posttitle>
				<p style="color:red;">*Berikut konsumen yang harus upload berkas analis dan sp3k</p>
				<br/>
					<div id="batas2">
						<div>
							<?php
								echo "
									<table id='home' style='color:red;' class='table table-bordered table-hover'>
									<thead>
										<tr>
											<th width='5%'><b>No</b></th>
											<th width='5%'><b>NIK</b></th>
											<th width='20%'><b>Nama Konsumen</b></th>
											<th width='15%'><b>Tanggal Upload Berkas Awal</b></th>
											<th width='15%'><b>Tanggal Jatuh Tempo</b></th>
										</tr>
									</thead>
									<tbody>
								";
								$no = 1;
								echo "<div style='float:left;'>";
								$q = mysqli_query($mysqli,"select t.nik_ktp,p.nama_pembeli,t.tgl_booking,b.tgl_upload,b.tgl_jatuhtempo from transaksi_perumahan t,berkas_kons b,data_pembeli p where p.nik_ktp=t.nik_ktp and  b.id_transaksi=t.id_transaksi and b.tgl_jatuhtempo='".date("Y-m-d")."' and b.id_jenispembayaran='2'");
								while ($row = mysqli_fetch_array($q)) {
									echo"<tr align='left'>
										<td>$no</td>
										<td>
											$row[nik_ktp]
										</td>
										<td>
											$row[nama_pembeli]
										</td>
										<td>
											$row[tgl_upload]
										</td>
										<td>
											$row[tgl_jatuhtempo]
										</td>
										";
								$no++;
								}
								echo"
								</tbody>
								</table>
								";
							?>						
							</div>
						</div>
					</div>
				</div>
		<?php
			}
			else{
			?>
			<div id="homecontainer">
				<div class='text'><h3 align=center>Reminder Marketing</h3>
					<br/><br/><br/>* Tidak Ada Reminder Apapun Saat Ini</div>
			</div>
			<?php
		}
	}	
?>