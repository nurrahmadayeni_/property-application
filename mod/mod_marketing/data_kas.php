<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> KAS MARKETING </h3><br><br><br><br>";
        echo "
            <table class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th width='3%'>No</th>
                    <th width='15%'>Debet (Rp)</th>
					<th width='35%'>Kredit (Rp)</th>
                    <th width='25%'>Saldo (Rp)</th>
                </tr>
            </thead>
            <tbody>
        ";
       
        include '../../config/connectdb.php';
		
		$sql = mysqli_query($mysqli,"select sum(dk.nominal) as nominal from data_keuangan dk,giro g where g.no_giro=dk.no_giro and g.jenis_giro='marketing'");
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
		$nom=$r['nominal'];
		
		$sql6 = mysqli_query($mysqli,"select sum(dk.nominal) as nominal from data_keuangan dk,giro g where g.no_giro=dk.no_giro and g.jenis_giro='Dan Lain-Lain' and dk.giro_asal='Marketing'");
        $row = mysqli_fetch_array($sql6);
		$kredit = $row['nominal'];
		
		$saldo=$saldo+$nom-$kredit;
		
		?>
			<tr align='left'>
            <td>
                <?php echo  $no;?>
            </td>
            <td><div align='right'>
				<?php echo number_format(($nom),0,',','.');  
				?>
				</div>
            </td>
            <td> 
				<div align='right'>
				<?php echo number_format(($kredit),0,',','.');  
				?>
				</div>
            </td>
			<td>
				<div align='right'>
                <?php
					$s= $saldo;
					echo number_format(($s),0,',','.');  
				?>
				</div>
            </td>
        </tr>
		
		<?php
            $no++;
        }
        ?>

        </tbody>

    </table>  

    <script type="text/javascript">
    $(function() {
        $("#kas").dataTable();
    });
    </script>

    <?php

	}
	?>

	