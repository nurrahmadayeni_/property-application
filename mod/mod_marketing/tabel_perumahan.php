<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> DATA PERUMAHAN </h3>";
        echo "
            <table id='perumahan' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th width='3%'>No</th>
                    <th width='15%'>Nama perumahan</th>
					<th width='35%'>Deskripsi Perumahan</th>
                    <th width='25%'>Spesifikasi Teknis</th>
					<th width='3%'>Jumlah Kavling</th>
					<th width='10%'>Gambar Perumahan</th>
					<th width='10%'>Detail Kavling</th>
                </tr>
            </thead>
            <tbody>
        ";
       
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,'SELECT * FROM data_perumahan');
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
		$id=$r['id_perumahan'];
			?>
			<tr align='left'>
            <td>
                <?php echo  $no;?>
            </td>
            <td>
				<?php echo  $r['nama_perumahan']; ?>
            </td>
            <td>
                <?php echo  $r['deskripsi']; ?>
            </td>
			<td>
                <?php echo  $r['spesifikasi_teknik']; ?>
            </td>
			 <?php
                $j_kavling = $mysqli->query(
                "SELECT count(kav_id) as 'jumlah'
                FROM data_kavling
                WHERE id_perumahan = '$r[id_perumahan]'
                ")->fetch_object()->jumlah;
            ?>
            <td>
                <?php echo  $j_kavling; ?>
            </td>
			<td>
				<a href="#showimage" class="edit-record3" data-toggle="modal" data-id="<?php echo $r['id_perumahan']; ?>"><button class='btn btn-primary btn-sm' alt="showimage">
                Show Image</button></a>
			</td>
			<td>
                <a href="?mod=detail_kavling&id=<?php echo $r['id_perumahan'];?>"> <button class='btn btn-primary btn-sm'>Detail</button> </a>
            </td>
        </tr>
		
		<?php
            $no++;
        }
        ?>

        </tbody>

    </table>  
    
	<!-- Modal for Show Image Perumahan-->
    <div id="showimage" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Gambar Perumahan</h4>
                </div>
                <div class="modal-body">
				
				</div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    $(function() {
        $("#perumahan").dataTable();
    });
    </script>

    <?php

	}
	?>

	<script>
        $(function(){
            $(document).on('click','.edit-record3',function(e){
                e.preventDefault();
                $("#showimage").modal('show');
                $.post('../action/marketing/showimage.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#showimage').on('hidden.bs.modal', function () {
                    location.reload();
                })
            });
        });
    </script>