<link rel="stylesheet" href="../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css"/>

<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> Data Progress Pembayaran</h3>";
        echo "
            <table id='konsumen' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th width='5%'>No</th>
					<th width='20%'>Tanggal Transaksi</th>
					<th width='20%'>Nama Konsumen</th>
					<th width='20%'>Nama Perumahan</th>
					<th width='10%'>No Kavling</th>
					<th width='10%'>Layout Perumahan</th>
					<th width='10%'>Harga Rumah (Rp)</th>
					<th width='20%'>Total Transaksi (Rp) </th>
					<th width='20%'>Total Setoran (Rp) </th>
					<th width='20%'>Sisa Setoran (Rp) </th>
                </tr>
            </thead>
            <tbody>
        ";
       
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,'select t.id_transaksi,t.tgl_booking,b.nik_ktp,b.id_perumahan,dp.nama_perumahan,b.no_kavling,p.nama_pembeli,sum(b.jlh_setoran) as jlh_setoran,dt.harga_tambahtanah,dt.hook,dt.biaya_tambahan,ty.harga_kavling
							from bayar_rumah b,data_pembeli p,detail_transaksirumah dt ,data_kavling k,transaksi_perumahan t,data_perumahan dp,type_rumah ty
							where p.nik_ktp=b.nik_ktp and dt.id_perumahan=b.id_perumahan and dt.no_kavling=b.no_kavling and dt.id_perumahan=b.id_perumahan 
							and k.no_kavling=dt.no_kavling and t.id_transaksi=dt.id_transaksi and dp.id_perumahan=b.id_perumahan and ty.id_type=k.id_type
							group by b.nik_ktp,b.id_perumahan,b.no_kavling');
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
		$ipm = $r['id_perumahan'];
		$idtr = $r['id_transaksi'];
		$id = $r['nik_ktp'];
		$total= $r['harga_tambahtanah']+$r['hook']+$r['biaya_tambahan']+$r['harga_kavling'];
		$sisa = $total-$r['jlh_setoran'];
			?>
			<tr align='left'>
            <td>
                <?php echo  $no;?>
            </td>
			<td>
                <?php echo  $r['tgl_booking']; ?>
            </td>
            <td>
				<?php echo  $r['nama_pembeli']; ?>
            </td>
			<td>
				<?php echo  $r['nama_perumahan']; ?>
            </td>
			<td>
				<?php echo  $r['no_kavling']; ?>
            </td>
			<td><?php
					$sqlrm = mysqli_query($mysqli,"select f.foto_rumah from foto_perumahan f,bayar_rumah b where b.id_perumahan=f.id_perumahan and b.id_perumahan='$ipm'");
					$r2 = mysqli_fetch_array($sqlrm);
					$foto = $r2['foto_rumah'];
					if(empty($foto)){ 
						echo "<img src='../mod_adm/gbr_perumahan/noimage.jpg' width='80px' height='60px'>";
					}
					else{
						echo "<img src='../mod_adm/gbr_perumahan/$r2[foto_rumah]' width='80px' height='60px'>";
					}
				?>
			</td>
			<td>
				<?php echo  number_format($r['harga_kavling'],0,',','.'); ?>
            </td>
			<td><div align='right'>
				<?php 
                    $rupiah=number_format($total,0,',','.'); 
                    echo $rupiah; 
                ?>
				</div>
            </td>
			 <td><div align='right'>
				<?php 
                    $rupiah=number_format($r['jlh_setoran'],0,',','.'); 
                    echo $rupiah; 
                ?>
				</div>
            </td>
			 <td><div align='right'>
				<?php 
                    $rupiah=number_format($sisa,0,',','.'); 
                    echo $rupiah; 
                ?>
				</div>
            </td>
			
        </tr>
		<?php
            $no++;
        }
        ?>
        </tbody>
    </table>

	
    <?php

	}
	?>

    <script type="text/javascript">
    $(function() {
        $("#konsumen").dataTable();
    });
    </script>
