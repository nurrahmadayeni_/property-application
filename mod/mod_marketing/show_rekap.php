<link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../../assets/css/dataTables.bootstrap.css"/>

<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center>REKAP LAPORAN</h3><br>";
        echo "
            <table id='closing' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th>No</th>
					<th>ID Transaksi</th>
                    <th>Nama Konsumen</th>
					<th>Perumahan</th>
                    <th>Kavling</th>
					<th>Harga Total Rumah (Rp)</th>
                    <th>Total Setoran (Rp)</th>
                    <th>Tanggal Lunas</th>
					<th>Upload Kartu Konsumen</th>
                </tr>
            </thead>
            <tbody>
        ";
       
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,"SELECT tr.id_transaksi, n.nama_pembeli, pr.nama_perumahan, dt.no_kavling, dt.total_hrgarumah,
            FROM detail_transaksirumah dt, data_pembeli n, perumahan pr ,tr  ");
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
		$id = $r['id_transaksi'];
        $htr= $r['total_hrgarumah'];
			?>
		<tr align='left'>
            <td><?php echo  $no;?></td>
			<td><?php echo  $id;?></td>
            <td><?php echo  $r['nama_pembeli']; ?></td>
            <td><?php echo  $r['nama_perumahan']; ?></td>
			<td><?php echo  $r['no_kavling']; ?></td>
			<td><?php $rupiah=number_format($htr,0,',','.'); 
                    echo $rupiah; 
                ?>  
            </td>
            <td><?php $rupiah=number_format($htr,0,',','.'); 
                    echo $rupiah; 
                ?> 
            </td>
            <td><?php echo"" ; ?></td>
            <td><?php echo"" ; ?></td>
        </tr>
		<?php
            $no++;
        }
    }
        ?>
        </tbody>
    </table>

    <span class="container">
        <button type="button" class="btn btn-primary" onclick="print_b()" >
        <span class="glyphicon glyphicon-print"></span> Print Rekap</button>
    </span>

    <script type="text/javascript">
    $(function() {
        $("#closing").dataTable();
    });
    </script>

<script>
    function print_b(){
        <?php
        echo "window.open('../action/marketing/printclosing.php?id=','_blank');";
        ?>
    }
</script>
