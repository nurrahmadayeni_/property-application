<html>
<head>
	<title>Print Data Konsumen</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
	<script src="../../assets/js/jquery.min.js"></script>
	<script src="../../assets/js/bootstrap.min.js"></script>

</head>
<body><br>
	<img src="../../../images/logo.png" align="right"><br>
	<font size="5%"><b>Kartu Bukti Setoran Angsuran Konsumen</b></font><br><hr>
		<?php
			error_reporting(0);
			include '../../../config/connectdb.php';

			$id=$_GET['id'];
			$sql = mysqli_query($mysqli,"SELECT p.nama_pembeli,ty.type_rumah, p.notelp_pembeli, r.nama_perumahan, ty.harga_kavling, dt.no_kavling, dt.harga_tambahtanah,dt.biaya_tambahan,dt.hook, dt.nominal_kpr, dt.jlh_DP1, t.id_transaksi
										FROM data_pembeli p, data_perumahan r,detail_transaksirumah dt,transaksi_perumahan t,data_kavling dk,type_rumah ty
										WHERE t.nik_ktp=p.nik_ktp and ty.id_type=dk.id_type
										AND dt.id_perumahan=r.id_perumahan
										AND dt.no_kavling=dk.no_kavling
										and dt.status_batal=''
										AND dt.id_transaksi=t.id_transaksi AND dt.id_transaksi='$id'");

			$no = 1;
			while ($r = mysqli_fetch_array($sql)) {
			$hrg = $r['harga_kavling'];
			$tanah = $r['harga_tambahtanah'];
			$hook = $r['hook'];
			$biayalain = $r['biaya_tambahan'];
			$tombak2 = $hrg + $tanah + $hook + $biayalain;
        
		?>
		<br><br>
		<table width="100%" align="center">   
			<thead style="font-size:12px; text-align:right;" >
				<tr>
                <th width='15%'>Nama Pemilik</th><th>:</th>
                <th width='15%'><?php echo $r[nama_pembeli]; ?></th>
                <th width='15%'>Harga Rumah</th><th>: Rp.</th>
                <th width='15%'><div align='right' style='padding-right: 100;'> 
                    <?php 
                        $rupiah=number_format($r['harga_kavling'],0,',','.'); 
                        echo $rupiah; 
                    ?></div>
                </th>
                <th width='15%'>Jumlah KPR</th><th>: Rp.</th>
                <th width='15%'><div align='right' style='padding-right: 100;'>
                    <?php 
                        $rupiah=number_format($r['nominal_kpr'],0,',','.'); 
                        echo $rupiah; 
                    ?></div>
                </th>
            </tr>
            <tr>
                <th>Type Rumah</th><th>:</th>
                <th>  <?php echo $r[type_rumah]; ?></th>
                <th>Penambahan Tanah</th><th>: Rp.</th>
                <th> <div align='right' style='padding-right: 100;'>
                <?php 
                    $rupiah=number_format($r['harga_tambahtanah'],0,',','.'); 
                    echo $rupiah; 
                ?></div>
                </th>
                <th>Jumlah DP</th><th>: Rp.</th>
                <th> <div align='right' style='padding-right: 100;'>
                <?php 
                    $rupiah=number_format($r['jlh_DP1'],0,',','.'); 
                    echo $rupiah; 
                ?></div>
                </th>
            </tr>
            <tr>
				<th>Kavling</th><th>:</th>
                <th>  <?php echo $r[no_kavling]; ?></th>
                <th>Hook</th><th>: Rp.</th>
                <th> <div align='right' style='padding-right: 100;'>
                <?php 
                    $rupiah=number_format($r['hook'],0,',','.'); 
                    echo $rupiah; 
                ?></div>
                </th>
                <th>Total</th><th>: Rp.</th>
                <th> <div align='right' style='padding-right: 100;'>
                <?php
                    $kpr = $r['nominal_kpr'];
                    $dp = $r['jlh_DP1'];
                    $total = $kpr + $dp;
                    echo number_format($total,0,',','.'); 
                ?></div>
                </th>
            </tr>
            <tr>
				<th>Lokasi</th><th>:</th>
                <th> <?php echo $r[nama_perumahan]; ?></th>
                <th>Penambahan Lain</th><th>: Rp. </th>
                <th><div align='right' style='padding-right: 100;'>
				<?php 
                    $rupiah=number_format($r['biaya_tambahan'],0,',','.'); 
                    echo $rupiah; 
                ?>
				</div>
				</th>
                <th></th>
                <th></th>
            </tr>
            <tr>
				<th>HP</th><th>:</th>
                <th> <?php echo $r[notelp_pembeli]; ?></th>
                <th>Total Harga Rumah</th><th>: Rp.</th>
                <th> <div align='right' style='padding-right: 100;'>
                <?php
                    $hrg = $r['harga_kavling'];
                    $tanah = $r['harga_tambahtanah'];
                    $hook = $r['hook'];
					$biayalain = $r['biaya_tambahan'];
                    $tombak = $hrg + $tanah + $hook + $biayalain;
                    echo number_format($tombak,0,',','.'); 
                ?></div>
                </th>
                <th></th>
                <th></th>
            </tr>
			<tr>
				<th>Minimal Angsuran/Bln</th><th>:</th>
                <th>  </th>
				<th></th>
                <th></th>
				<th></th>
                <th></th>
			</tr>
        </thead>
    </table><hr>

    <?php
		$no++;
    }
	?>
	<table id='setoran' class='table table-bordered table-hover' border="1">
	<thead>
		<tr>
			<th>No</th>
			<th>Tanggal</th>
			<th>Uraian</th>
			<th>Jumlah Setoran (Rp)</th>
			<th>Total Setoran (Rp)</th>
			<th>Sisa Setoran (Rp)</th>
			<th>TTD/Paraf Penyetor</th>
			<th>TTD/Paraf Marketing</th>
			<th>TTD/Paraf Keuangan</th>
		</tr>
	</thead>
	<tbody>
	<?php
		$query2 = mysqli_query($mysqli,"SELECT tr.nik_ktp,dt.id_perumahan,dt.no_kavling FROM transaksi_perumahan tr,detail_transaksirumah dt where tr.id_transaksi=dt.id_transaksi and tr.id_transaksi='".$_GET['id']."'");
		$data2 = mysqli_fetch_array($query2);

		$nik = $data2['nik_ktp'];
		$idrumah = $data2['id_perumahan'];
		$no_kavling = $data2['no_kavling'];
		
        $sql2 = mysqli_query($mysqli,"select b.id_bayarrumah,b.nik_ktp,k.jenis_transaksi,b.tgl_setoran,b.jlh_setoran
            from kategori_jenistransaksi k, bayar_rumah b
            where k.id_jenisbayar=b.id_jenisbayar
            AND b.nik_ktp='$nik' AND b.id_perumahan='$idrumah' AND b.no_kavling='$no_kavling'");

        $no = 1;
		$jlh= 0;
        while ($r2 = mysqli_fetch_array($sql2)) {
        $id = $r2['id_bayarrumah'];
		
    ?>
        <tr>
            <td> <?php echo  $no;?> </td>
            <td>
                <?php echo  $r2['tgl_setoran']; ?>
            </td>
            <td>
                <?php echo  $r2['jenis_transaksi']; ?>
            </td>
            <td><div align='right' style='padding-right: 100;'>
                <?php 
                    $rupiah=number_format($r2['jlh_setoran'],0,',','.'); 
                    echo $rupiah; 
                ?>
				</div>
            </td>
            <td><div align='right' style='padding-right: 100;'>
                <?php
                    $jlh=$r2['jlh_setoran'];
                    $jlh1+=$jlh;
                    $totalsetor= $jlh1;
                    echo number_format($totalsetor,0,',','.'); 
					$jlh++;
                
                ?>
				</div>
            </td>
            <td>
			<div align='right' style='padding-right: 100;'>
                <?php

                    $sisa1=$tombak2;
                    $sisa = $sisa1-$totalsetor;
                    echo number_format($sisa,0,',','.'); 
                ?>
			</div>
            </td>
			<td></td>
			<td></td>
			<td></td>
        </tr>
    <?php
            $no++;
        }
    ?>
    </tbody>
    </table>  
	
	<table width="100%" align="center" style="margin-top:140px;">   
			<thead style="font-size:15px; text-align:right;" >
				<tr>
				<th></th>
				<th></th>
				<th>Mengetahui :</th>
				</tr>
				<tr>
				<th><b>Pj.Konsumen</b></th>
				<th><b>Konsumen</b></th>
				<th><b>Asisten Manager Marketing</b></th>
				<th><b>Manager Keuangan</b></th>
				<th><b>Direktur Utama</b></th>
				</tr>
			</thead>
	</table>
	
	<script>
		window.load = print_d();
		function print_d(){
			window.print();
		}
	</script>