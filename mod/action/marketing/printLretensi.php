<html>
<head>
	<title>Print Laporan</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="../../assets/css/bootstrap-theme.min.css">
	<link href="../../../images/pavicon.png" rel="icon" type="image/x-icon" />
</head>
<body><br><br>
	<img src="../../../images/logo.png" align="right"><br>
	<font size="4%">Laporan Retensi</font><br>
	<hr>
	<table border="1" width="100%" style="border-collapse:collapse;" align="center">
    	<tr class="tableheader">
    		 <th width='5%' align='center'>No</th>
				<th width='10%'>ID.Transaksi</th>
				<th width='10%'>Nama Konsumen</th>
                <th width='10%'>Nama Perumahan</th>
				<th width='10%'>Type Rumah</th>
                <th width='15%'>No.Kavling</th>
                <th width='15%'>Nilai KPR (Rp)</th>
				<th width='15%'>Jumlah Pembayaran KPR</th>
				<th width='15%'>Nilai Retensi(Rp)</th>
        </tr>
     </thead>
            <tbody>
				<?php
                    error_reporting(0);
					include '../../../config/connectdb.php';
					
                   $a=$_GET['id'];
					$b=$_GET['id2'];
					$c=$_GET['id3'];
					
					$sql = mysqli_query($mysqli,"SELECT t.id_transaksi,ty.type_rumah,j.id_jenisbayar,p.nama_pembeli,dp.nama_perumahan,dt.no_kavling,b.jlh_setoran, dt.nominal_kpr, dt.id_jenispembayaran
							FROM type_rumah ty,detail_transaksirumah dt ,data_perumahan dp,transaksi_perumahan t,data_pembeli p,bayar_rumah b,data_kavling k,kategori_jenistransaksi j
							WHERE j.id_jenisbayar=b.id_jenisbayar AND ty.id_type=k.id_type AND
				                k.no_kavling=b.no_kavling AND
				                b.nik_ktp=t.nik_ktp
				                AND b.id_perumahan=dt.id_perumahan
				                AND b.no_kavling=dt.no_kavling
				                AND dp.id_perumahan=dt.id_perumahan 
								AND t.id_transaksi=dt.id_transaksi AND p.nik_ktp=t.nik_ktp
								AND dp.id_perumahan='$a' AND ty.id_type='$b' AND dt.no_kavling='$c'
								AND dt.id_jenispembayaran='2'
								ORDER BY b.tgl_setoran DESC");
					$no = 1;
					while ($r = mysqli_fetch_array($sql)) {
						$id=$r['id_transaksi'];
						$akons= $r['nama_pembeli'];
						$type= $r['type_rumah'];
						$nokav= $r['no_kavling'];
						$narum= $r['nama_perumahan'];
						$nom=$r['nominal_kpr'];
						$jlh= $r['jlh_setoran'];
						$retensi = $nom - $jlh;
			 	?>
					<tr align='left'>
						<td align="center"> <?php echo  $no;?> </td>
						<td><?php echo  $id; ?></td>
						<td><?php echo  $akons; ?></td>
						<td><?php echo  $narum; ?></td>
						<td><?php echo  $type; ?></td>
						<td><?php echo  $nokav; ?></td>
						<td align="right">
							<?php $rupiah=number_format($nom,0,',','.'); 
			                   	echo $rupiah; 
			                ?>
			            </td>
						<td align="right"><?php $rupiah=number_format($jlh,0,',','.'); 
			                   	echo $rupiah; 
			                ?></td>
						<td align="right"><?php echo  number_format($retensi,0,',','.'); ?></td>
					</tr>
					   
				<?php
					$no++;
				} 
				?>
                	
                </tbody>
            </table>

</body>
</html>
		
	<script>
		window.load = print_b();
		function print_b(){
			window.print();
		}
	</script>