<html>
<head>
	<title>Print Laporan</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="../../assets/css/bootstrap-theme.min.css">
	<link href="../../../images/pavicon.png" rel="icon" type="image/x-icon" />
</head>
<body><br><br>
	<img src="../../../images/logo.png" align="right"><br>
	<font size="4%">Laporan Pemasukan</font><br>
	<font size="2%">Tanggal <font color='red'><b><?php echo $_GET['id3'] ; ?></b></font> 
						s/d <font color='red'><b><?php echo $_GET['id4']; ?></b></font></font><hr>
	<table border="1" width="100%" style="border-collapse:collapse;" align="center">
    	<tr class="tableheader">
    		<th width='5%' align='center'>No</th>
				<th width='10%'>Tgl Setoran</th>
				<th width='10%'>Nama Konsumen</th>
                <th width='10%'>Nama Perumahan</th>
				<th width='10%'>Type Rumah</th>
                <th width='10%'>No.Kavling</th>
                <th width='15%'>Harga Rumah (Rp)</th>
				<th width='15%'>Keterangan</th>
				<th width='15%'>Jumlah Setoran(Rp)</th>
        </tr>
     </thead>
            <tbody>
				<?php
                    error_reporting(0);
					include '../../../config/connectdb.php';
					
                   	$a=$_GET['id'];
					$b=$_GET['id2'];
					$d=$_GET['id3'];
					$e=$_GET['id4'];
					$i=$_GET['id5'];
					
					
					$sql = mysqli_query($mysqli,"SELECT b.tgl_setoran,j.jenis_transaksi,ty.type_rumah,p.nama_pembeli,dp.nama_perumahan,dt.no_kavling,ty.harga_kavling,b.jlh_setoran, dt.total_hrgarumah, dt.keterangan_transaksi 
						FROM type_rumah ty,detail_transaksirumah dt ,data_perumahan dp,transaksi_perumahan t,data_pembeli p,bayar_rumah b,data_kavling k, kategori_jenistransaksi j
						WHERE j.id_jenisbayar=b.id_jenisbayar and ty.id_type=k.id_type 
						and k.no_kavling=b.no_kavling and b.id_perumahan=dt.id_perumahan 
						and b.no_kavling=dt.no_kavling and dp.id_perumahan=dt.id_perumahan 
						and t.id_transaksi=dt.id_transaksi and p.nik_ktp=t.nik_ktp
						and dp.id_perumahan='$a' and ty.id_type='$b' and j.id_jenisbayar='$i'
						and b.tgl_setoran between '$d' and '$e'
						ORDER BY b.tgl_setoran DESC");

					$no = 1;
					while ($r = mysqli_fetch_array($sql)) {
						$akons= $r['nama_pembeli'];
						$type= $r['type_rumah'];
						$nokav= $r['no_kavling'];
						$narum= $r['nama_perumahan'];
						$hrgakav= $r['harga_kavling'];
						$jns= $r['jenis_transaksi'];
						$jlh= $r['jlh_setoran'];
						$tgl= $r['tgl_setoran'];
						$total = $total + $jlh;

				?>
                    <tr align='left'>
						<td align="center"> <?php echo  $no;?> </td>
						<td><?php echo  $tgl; ?></td>
						<td><?php echo  $akons; ?></td>
						<td><?php echo  $narum; ?></td>
						<td align='center'><?php echo  $type; ?></td>
						<td align='center'><?php echo  $nokav; ?></td>
						<td><div align="right">
							<?php $rupiah=number_format($hrgakav,0,',','.'); 
			                   	echo $rupiah; 
			                ?>
							</div>
			            </td>
						<td><?php echo  $jns; ?></td>
						<td><div align="right">
						<?php echo  number_format($jlh,0,',','.'); ?>
						</div>
						</td>
					</tr>
                <?php
                    $no++;
					}
                ?>
                	<tr>
						<td colspan='8'><b>Total (Rp)</b></td>
						<td align="right"><b><?php echo number_format($total,0,',','.');?></b></td>
					</tr>	
                </tbody>
            </table>

</body>
</html>
		
	<script>
		window.load = print_dc();
		function print_dc(){
			window.print();
		}
	</script>