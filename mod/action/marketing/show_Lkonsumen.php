<?php   
	include '../../../config/connectdb.php';
	error_reporting(0);
	
	echo "<p><h3 align=center> Laporan Data Konsumen</h3>
				
    <table id='DLseluruh' class='table table-bordered table-hover'>
        <thead>
            <tr>
                <th width='5%'>No</th>
				<th width='5%'>NIK</th>
				<th width='20%'>Nama Konsumen</th>
				<th width='15%'>No.telp/HP</th>
                <th width='20%'>Email</th>
				<th width='20%'>Keterangan</th>
            </tr>
        </thead>
        <tbody>
        ";
	
		$id = $_GET['jenis_laporan'];
		$sql = mysqli_query($mysqli,"SELECT * FROM data_pembeli ORDER BY nama_pembeli DESC");

		$no = 1;
		while ($r = mysqli_fetch_array($sql)) {
			$id = $r['nik_ktp'];
			$idtr = $r['id_transaksi'];
 	?>
		<tr align='left'>
			<td align="center"> <?php echo  $no;?> </td>
			<td><?php echo  $id;?></td>
			<td><?php echo  $r['nama_pembeli']; ?></td>
			<td><?php echo  $r['notelp_pembeli']; ?></td>
			<td><?php echo  $r['email']; ?></td>
			<td><?php echo  $r['catatan_khusus']; ?></td>
		</tr>
		   
	<?php
		$no++;
	} 
	?>
	<tr>
		<td colspan="6" align="right"><button type="button" class="btn btn-primary" onclick="print_dc()" >
				<span class="glyphicon glyphicon-print"></span> Print</button</td>
	</tr>
	</tbody>
</table>
	

<script>
	function print_dc(){
		<?php
		
		echo "window.open('../action/marketing/printLkonsumen.php','_blank');";
		?>
	}
</script>