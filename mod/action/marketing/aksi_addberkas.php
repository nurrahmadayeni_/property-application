<?php
	include '../../../config/connectdb.php';
	error_reporting(0);
	
	$mod=$_GET["mod"];
	$act=$_GET["act"];
	
	if(isset($mod) AND $act=='tambahberkas'){
		$idtransaksi=$_POST['idtran'];
		$namakons=$_POST['namakons'];
		$namarmh=$_POST['tipe_rmh'];
		$nokav= $_POST['nama_prm'];
		$tgl_sekarang=date('Y-m-d');
		
		$cashkprfile = $_FILES['cashfile']['name'];
		
		$cashkprfile1 = $_FILES['cashkprfile1']['name'];
		$entry= $_POST['entry'];
		$analis= $_POST['analis'];
		$ots= $_POST['ots'];
		$sp3k= $_POST['sp3k'];
		$akad= $_POST['akad'];
		$serahterima= $_POST['serahterima'];
		$closing= $_POST['closing'];
		
		$query2 = mysqli_query($mysqli,"SELECT id_jenispembayaran FROM detail_transaksirumah where id_transaksi='$idtransaksi'");
		$data2 = mysqli_fetch_array($query2);
		$jenis_pembayaran = $data2['id_jenispembayaran'];
		
		$mod_dateentry=strtotime($tgl_sekarang."+15day");
		$newdateentry=date("Y-m-d",$mod_dateentry);
			
		$mod_dateanalis=strtotime($tgl_sekarang."+5day");
		$newdateanalis=date("Y-m-d",$mod_dateanalis);

		$mod_dateots=strtotime($tgl_sekarang."+5day");
		$newdateots=date("Y-m-d",$mod_dateots);
		
		$mod_datesp3k=strtotime($tgl_sekarang."+90day");
		$newdatesp3k=date("Y-m-d",$mod_datesp3k);
		
		$mod_datesrh=strtotime($tgl_sekarang."+1day");
		$newdatesrh=date("Y-m-d",$mod_datesrh);
		
		$sqlprogress = "SELECT pp.id_perumahan, p.nama_perumahan, pp.no_kavling, pp.id_pekerjaan, k.id_kategorikerja, 
			k.nama_kategorikerja, pp.tgl_laporan, COUNT(pp.progress) as 'siap' 
			FROM data_perumahan p, progress_pembangunan pp, kategori_pekerjaan k , data_pekerjaan d 
			WHERE p.id_perumahan=pp.id_perumahan 
			AND k.id_kategorikerja=d.id_kategorikerja 
			AND d.id_pekerjaan=pp.id_pekerjaan 
			AND pp.id_perumahan='$namarmh' 
			AND pp.no_kavling='$nokav' 
			AND pp.progress='1' 
			GROUP BY k.id_kategorikerja
		";
		$tampilp=mysqli_query($mysqli,$sqlprogress);
		$rowp = mysqli_fetch_array($tampilp);
		
		$sqlpkerjaan = "SELECT id_pekerjaan FROM data_pekerjaan WHERE id_kategorikerja=$rowp[id_kategorikerja]";
		$resultkerja=mysqli_query($mysqli,$sqlpkerjaan);
		$h = mysqli_num_rows($resultkerja);
		
		$persen = round(($rowp['siap'] / $h) *100);
		$persens = "$persen"."%";
		

		//PEMBAYARAN CASH
		if($jenis_pembayaran=='1'){
			if (file_exists("../../mod_marketing/kwitansipengembaliandp/" . $_FILES["cashfile"]["name"]))
            {
            	echo $_FILES["cashfile"]["name"] . " already exists. ";
            }
			else{
				$temp = explode(".", $_FILES["cashfile"]["name"]);
				$newfilename = round(microtime(true)) . '.' . end($temp);
				move_uploaded_file($_FILES["cashfile"]["tmp_name"], "../../mod_marketing/kwitansipengembaliandp/". $newfilename);
				if($_FILES['cashfile']['type']=="application/pdf")
				{
					$sql_update= $mysqli->query(
						"UPDATE berkas_kons SET entry_file='$newfilename',tgl_upload='$tgl_sekarang',tgl_jatuhtempo='$tgl_sekarang'
						   WHERE id_transaksi='$idtransaksi'");
				}
				else{
					?>
						<script>
							alert("hanya bisa upload file pdf")
						</script>
					<?php
				}
			}
		}
		
		//PEMBAYARAN KPR
		else if($jenis_pembayaran=='2'){
			$sql3 = mysqli_query($mysqli,"SELECT b.id_berkaskons,d.id_transaksi,b.tgl_upload,b.tgl_jatuhtempo, p.nik_ktp,p.nama_pembeli,d.id_jenispembayaran,j.jenis_pembayaran,b.entry,b.analis,b.OTS,b.sp3k,b.akad,b.serah_terima,b.closing FROM detail_transaksirumah d,transaksi_perumahan t,data_pembeli p,berkas_kons b,jenis_pembayaranrumah j where j.id_jenispembayaran=b.id_jenispembayaran and b.id_transaksi=d.id_transaksi and d.id_transaksi=t.id_transaksi and p.nik_ktp=t.nik_ktp and d.id_transaksi='$idtransaksi'");
			$r = mysqli_fetch_array($sql3);
			
			if($r['entry_file']=='' and $r['entry']==''){
				$temp = explode(".", $_FILES["cashkprfile1"]["name"]);
				$newfilename = round(microtime(true)) . '.' . end($temp);
				move_uploaded_file($_FILES["cashkprfile1"]["tmp_name"], "../../mod_marketing/kwitansipengembaliandp/". $newfilename);
				if($_FILES['cashkprfile1']['type']=="application/pdf")
				{
					$sql_update= $mysqli->query(
						"UPDATE berkas_kons SET entry_file='$newfilename',entry='1',tgl_upload='$tgl_sekarang',tgl_jatuhtempo='$newdateentry'
						   WHERE id_transaksi='$idtransaksi'");
				}
				else{
					?>
						<script>
							alert("hanya bisa upload file pdf")
						</script>
					<?php
				}
			}
			
			else if($r['analis'] == '' and empty($cashkprfile1)){
				$sql_update= $mysqli->query(
					"UPDATE berkas_kons SET analis='1',tgl_jatuhtempo='$newdateanalis'
						   WHERE id_transaksi = '$idtransaksi'");
			}

			else if($r['OTS'] == '' and empty($cashkprfile1)){
				$sql_update= $mysqli->query(
					"UPDATE berkas_kons SET OTS='1',tgl_jatuhtempo='$newdateots'
						   WHERE id_transaksi = '$idtransaksi'");
			}
			
			else if($r['sp3k']=='' and empty($cashkprfile1)){
					$sql_update= $mysqli->query(
					"UPDATE berkas_kons SET sp3k='1',tgl_jatuhtempo='$newdatesp3k'	
						   WHERE id_transaksi = '$idtransaksi'");
			}
			
			else if($r['akad'] == '' and empty($cashkprfile1)){
				$sql_update= $mysqli->query(
					"UPDATE berkas_kons SET akad='1',tgl_jatuhtempo='$newdatesrh'
						   WHERE id_transaksi = '$idtransaksi'");
			}
			else if($r['serah_terima'] == '' and empty($cashkprfile1)){
				$sql_update= $mysqli->query(
					"UPDATE berkas_kons SET serah_terima='1',tgl_jatuhtempo='$newdatesrh'
						   WHERE id_transaksi = '$idtransaksi'");
			}
			else if($r['closing'] == '' and empty($cashkprfile1)){
				$sql_update= $mysqli->query(
					"UPDATE berkas_kons SET closing='1',tgl_jatuhtempo='$tgl_sekarang'
						   WHERE id_transaksi = '$idtransaksi'");
			}
		}

		 echo "<script type='text/javascript'>
		 document.location='../../mod_marketing/index.php?mod=berkas_konsumen' 
		 </script>";

	}
	
	else if(isset($mod) AND $act=='editberkas'){
		$idtrans = $_POST['id'];
		$berkas =  $_FILES['upfile']['name'];
		$query = mysqli_query($mysqli,"SELECT nik_ktp FROM transaksi_perumahan where id_transaksi='$idtrans'");
		$data = mysqli_fetch_array($query);
		$nik_ktp = $data['nik_ktp'];

		if(!empty($berkas)){
			if (file_exists("../../mod_marketing/kwitansipengembaliandp/" . $_FILES["upfile"]["name"]))
            {
            	echo $_FILES["upfile"]["name"] . " already exists. ";
            }
			else{
				$temp = explode(".", $_FILES["upfile"]["name"]);
				$newfilename = round(microtime(true)) . '.' . end($temp);
				move_uploaded_file($_FILES["upfile"]["tmp_name"], "../../mod_marketing/kwitansipengembaliandp/". $newfilename);
					if($_FILES['upfile']['type']=="application/pdf")
					{
						$sql_update= $mysqli->query(
							"UPDATE berkas_kons SET entry_file='$newfilename'
							   WHERE id_transaksi='$idtrans'");
					}
					else{
						?>
							<script>
								alert("hanya bisa upload file pdf")
							</script>
						<?php
					}
			}
		}


		echo "<script type='text/javascript'>
		document.location='../../mod_marketing/index.php?mod=add_berkas&id=$nik_ktp&idtr=$idtrans' 
		</script>";
	}
?>