<?php   
	include '../../../config/connectdb.php';
	error_reporting(0);
	
	echo "<p><h3 align=center> Laporan Pemasukan</h3>
		<p><h5 align=center> Dari Tanggal <font color='red'><b>".$_GET['tgl_a']."</b></font>
		sampai <font color='red'><b>".$_GET['tgl_aa']."</b></font> </h5> 		
    <table id='DLseluruh' class='table table-bordered table-hover'>
        <thead>
            <tr>
                <th width='5%' align='center'>No</th>
				<th width='10%'>Tgl Setoran</th>
				<th width='10%'>Nama Konsumen</th>
                <th width='10%'>Nama Perumahan</th>
				<th width='10%'>Type Rumah</th>
                <th width='15%'>No.Kavling</th>
                <th width='15%'>Jenis Transaksi</th>
                <th width='15%'>Harga Rumah (Rp)</th>
				<th width='15%'>Keterangan</th>
				<th width='15%'>Jumlah Setoran(Rp)</th>
            </tr>
        </thead>
        <tbody>
        ";

    $a=$_GET['tipe1'];
	$b=$_GET['typ1'];
	$c=$_GET['nm'];
	$d=$_GET['tgl_a'];
	$e=$_GET['tgl_aa'];
	$i=$_GET['trs'];
	
	$sql = mysqli_query($mysqli,"SELECT b.tgl_setoran,j.jenis_transaksi,ty.type_rumah,p.nama_pembeli,dp.nama_perumahan,dt.no_kavling,ty.harga_kavling,b.jlh_setoran, dt.total_hrgarumah, dt.keterangan_transaksi  
		FROM type_rumah ty,detail_transaksirumah dt ,data_perumahan dp,transaksi_perumahan t,data_pembeli p,bayar_rumah b,data_kavling k, kategori_jenistransaksi j
		WHERE j.id_jenisbayar=b.id_jenisbayar and ty.id_type=k.id_type 
		and k.no_kavling=b.no_kavling and b.id_perumahan=dt.id_perumahan 
		and b.no_kavling=dt.no_kavling and dp.id_perumahan=dt.id_perumahan 
		and t.id_transaksi=dt.id_transaksi and p.nik_ktp=t.nik_ktp
		and dp.id_perumahan='$a' and ty.id_type='$b' and j.id_jenisbayar='$i'
		and b.tgl_setoran between '$d' and '$e'
		GROUP BY t.id_transaksi DESC");

		$no = 1;
		while ($r = mysqli_fetch_array($sql)) {
			$akons= $r['nama_pembeli'];
			$type= $r['type_rumah'];
			$nokav= $r['no_kavling'];
			$ket=$r['keterangan_transaksi'];
			$narum= $r['nama_perumahan'];
			$hrgakav= $r['total_hrgarumah'];
			$jns= $r['jenis_transaksi'];
			$jlh= $r['jlh_setoran'];
			$tgl= $r['tgl_setoran'];
			$total = $total + $jlh;
 	?>
		<tr align='left'>
			<td align="center"> <?php echo  $no;?> </td>
			<td><?php echo  $tgl; ?></td>
			<td><?php echo  $akons; ?></td>
			<td><?php echo  $narum; ?></td>
			<td><?php echo  $type; ?></td>
			<td><?php echo  $nokav; ?></td>
			<td><?php echo  $jns; ?></td>
			<td align="right">
				<?php $rupiah=number_format($hrgakav,0,',','.'); 
                   	echo $rupiah; 
                ?>
            </td>
			<td><?php echo  $ket; ?></td>
			<td align="right"><?php echo  number_format($jlh,0,',','.'); ?></td>
		</tr>
		   
	<?php
		$no++;
	} 
	?>
	<tr>
		<td colspan="9"><b>TOTAL </b></td>
		<td align="right"><?php echo number_format($total,0,',','.'); ?></td>
	</tr>
	<tr>
		<td colspan="10" align="right"><button type="button" class="btn btn-primary" onclick="print_dc()" >
				<span class="glyphicon glyphicon-print"></span> Print</button</td>
	</tr>
	</tbody>
</table>
	

<script>
	function print_dc(){
		<?php
			$a=$_GET['tipe1'];
			$b=$_GET['typ1'];
			$d=$_GET['tgl_a'];
			$e=$_GET['tgl_aa'];
			$i=$_GET['trs'];
	
		echo "window.open('../action/marketing/printLpemasukan.php?id=$a&id2=$b&id3=$d&id4=$e&id5=$i','_blank');";
		?>
	}
</script>