<?php   
	include '../../../config/connectdb.php';

	//$idt = $_GET['idtran'];
	//$tglsekarang = date('Y-m-d');
	
	$idt = 'TRS-00004';
	$tglsekarang = date('Y-m-d');
	
	$get_material = "select b.status,b.tgl_upload,b.tgl_jatuhtempo from detail_transaksirumah dt, transaksi_perumahan t, berkas_kons b
	where t.id_transaksi=dt.id_transaksi and b.id_transaksi=dt.id_transaksi
	and dt.id_transaksi='$idt'";

	$tampil=mysqli_query($mysqli,$get_material);
	
	while($r=mysqli_fetch_assoc($tampil)){
		$status = $r['status'];
		$tglupload = $r['tgl_upload'];
		$tgljtempo = $r['tgl_jatuhtempo'];
		
		$mod_dateentry=strtotime($tglupload."+15day");
		$newdateentry=date("Y-m-d",$mod_dateentry);
			
		$mod_dateanalis=strtotime($tglupload."+5day");
		$newdateanalis=date("Y-m-d",$mod_dateanalis);
		
		$mod_datesp3k=strtotime($tglupload."+90day");
		$newdatesp3k=date("Y-m-d",$mod_datesp3k);
		
		$sqlentry = "select b.status,b.tgl_upload from detail_transaksirumah dt, transaksi_perumahan t, berkas_kons b
		where t.id_transaksi=dt.id_transaksi and b.id_transaksi=dt.id_transaksi
		and dt.id_transaksi='$idt' and b.status='entry'";
		$resulentry=mysqli_query($mysqli,$sqlentry);
		$entry = mysqli_num_rows($resulentry);
		
		$sqlan = "select b.status,b.tgl_upload from detail_transaksirumah dt, transaksi_perumahan t, berkas_kons b
		where t.id_transaksi=dt.id_transaksi and b.id_transaksi=dt.id_transaksi
		and dt.id_transaksi='$idt' and b.status='analis'";
		$resulanl=mysqli_query($mysqli,$sqlan);
		$analis = mysqli_num_rows($resulanl);
		
		$sqlsp = "select b.status,b.tgl_upload from detail_transaksirumah dt, transaksi_perumahan t, berkas_kons b
		where t.id_transaksi=dt.id_transaksi and b.id_transaksi=dt.id_transaksi
		and dt.id_transaksi='$idt' and b.status='sp3k'";
		$resulsp=mysqli_query($mysqli,$sqlsp);
		$sp3k = mysqli_num_rows($resulsp);
		
		$sqlakad = "select b.status,b.tgl_upload from detail_transaksirumah dt, transaksi_perumahan t, berkas_kons b
		where t.id_transaksi=dt.id_transaksi and b.id_transaksi=dt.id_transaksi
		and dt.id_transaksi='$idt' and b.status='akad'";
		$resulakad=mysqli_query($mysqli,$sqlakad);
		$akad = mysqli_num_rows($resulakad);
		
		if(empty($status)){
			echo "<option value='entry'>Entry</option>";
		}
		else if($entry > 0 AND $tglsekarang>=$newdateentry){
			echo "<option value='analis'>Analis</option>";
		}
		else if($analis > 0 AND $tglsekarang>=$newdateanalis){
			echo "<option value='sp3k'>Sp3k</option>";
		}
		else if($sp3k > 0 AND $tglsekarang>=$newdatesp3k){
			echo "<option value='akad'>Akad</option>";
		}
		else {
			echo "<option value='serah terima'>Serah Terima</option>";
			echo "<option value='closing'>Closing</option>";
		}
	}
?>