<?php   
	include '../../../config/connectdb.php';
	error_reporting(0);
	
	echo "<p><h3 align=center> Laporan Kas Giro</h3>
			
    <table id='DLseluruh' class='table table-bordered table-hover'>
        <thead>
            <tr>
                <th width='5%' align='center'>No</th>
				<th width='15%'>Debet (Rp)</th>
				<th width='35%'>Kredit (Rp)</th>
                <th width='25%'>Saldo (Rp)</th>
            </tr>
        </thead>
        <tbody>
        ";

		$sql = mysqli_query($mysqli,"select sum(dk.nominal) as nominal from data_keuangan dk,giro g where g.no_giro=dk.no_giro and g.jenis_giro='marketing'");
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
		$nom=$r['nominal'];
		
		$sql6 = mysqli_query($mysqli,"select sum(dk.nominal) as nominal from data_keuangan dk,giro g where g.no_giro=dk.no_giro and g.jenis_giro='Dan Lain-Lain' and dk.giro_asal='Marketing'");
        $row = mysqli_fetch_array($sql6);
		$kredit = $row['nominal'];
		
		$saldo=$saldo+$nom-$kredit;
		
		?>
			<tr align='left'>
            <td>
                <?php echo  $no;?>
            </td>
            <td><div align='right'>
				<?php echo number_format(($nom),0,',','.');  
				?>
				</div>
            </td>
            <td> 
				<div align='right'>
				<?php echo number_format(($kredit),0,',','.');  
				?>
				</div>
            </td>
			<td>
				<div align='right'>
                <?php
					$s= $saldo;
					echo number_format(($s),0,',','.');  
				?>
				</div>
            </td>
        </tr>
		
		<?php
            $no++;
        }
        ?>
	<tr>
		<td colspan="9" align="right"><button type="button" class="btn btn-primary" onclick="print_dc()" >
				<span class="glyphicon glyphicon-print"></span> Print</button</td>
	</tr>
	</tbody>
</table>
	

<script>
	function print_dc(){
		<?php
			$tgl1=  $_GET['tgl_1'];
			$tgl2 = $_GET['tgl_2'];
		
		echo "window.open('../action/marketing/printLpemasukan.php?id=$tgl1&id2=$tgl2','_blank');";
		?>
	}
</script>