<html>
<head>
	<title>Print Data Konsumen</title>
	<link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
	<script src="../../assets/js/jquery.min.js"></script>
	<script src="../../assets/js/bootstrap.min.js"></script>
	<link href="../../../images/pavicon.png" rel="icon" type="image/x-icon" />

	<?php
		$fullmode=$_GET['mod'];
		if($fullmode=='full'){
			echo "<link href='style.css' type='text/css' rel='stylesheet' />";
		}
		else{
			echo "<style>
				body{
					color:white !important;
				}
				table, tr, th, td, h3,hr{
					border:1px solid white !important;
					color:white !important;
				}
				.data{
					color:black !important;
				}
				.logo{
					display :hidden;
				}

			</style>";
		}
	?>
	
	</head>

	<body><br>
	<?php
	error_reporting(0);
	include '../../../config/connectdb.php';

	if($fullmode=='full'){
		echo "	<img src='../../../images/logo.png' align='right'><br>";
		?>
		<h3>Kartu Bukti Setoran Angsuran Konsumen</h3><br><hr>
			<?php
			$id=$_GET['id'];
			
			$sql = mysqli_query($mysqli,"SELECT p.nama_pembeli,ty.type_rumah, p.notelp_pembeli, r.nama_perumahan, ty.harga_kavling, dt.no_kavling,dt.total_hrgarumah, dt.harga_tambahtanah,dt.biaya_tambahan,dt.hook, dt.nominal_kpr, dt.jlh_DP1, t.id_transaksi
										FROM data_pembeli p, data_perumahan r,detail_transaksirumah dt,transaksi_perumahan t,data_kavling dk,type_rumah ty
										WHERE t.nik_ktp=p.nik_ktp and ty.id_type=dk.id_type
										AND dt.id_perumahan=r.id_perumahan
										AND dt.no_kavling=dk.no_kavling
										and dt.status_batal=''
										AND dt.id_transaksi=t.id_transaksi AND dt.id_transaksi='$id'");

			$no = 1;
			while ($r = mysqli_fetch_array($sql)) {
			$hrg = $r['harga_kavling'];
			$tanah = $r['harga_tambahtanah'];
			$hook = $r['hook'];
			$biayalain = $r['biaya_tambahan'];
			$tombak2 = $hrg + $tanah + $hook + $biayalain;
        	$totalharga= $r['total_hrgarumah'];
        
			?>
			<table width="100%" align="center">   
				<thead style="font-size:12px; text-align:right;" >
					<tr>
					<th>Nama Pemilik</th><th>:</th>
					<th><?php echo $r[nama_pembeli]; ?></th>
					<th>Harga Rumah</th><th>: Rp.</th>
					<th class='pull-right' style="margin-right:10%">
						<?php 
							$rupiah=number_format($r['harga_kavling'],0,',','.'); 
							echo $rupiah; 
						?>
					</th>
					<th >Jumlah KPR</th><th>: Rp.</th>
					<th class='pull-right' style="margin-right:10%">
						<?php 
                        $rupiah=number_format($r['nominal_kpr'],0,',','.'); 
                        echo $rupiah; 
						?>
					</th>
				</tr>
				<tr>
					<th>Type Rumah</th><th>:</th>
					<th>  <?php echo $r[type_rumah]; ?></th>
					<th>Penambahan Tanah</th><th>: Rp.</th>
					<th  class='pull-right' style="margin-right:10%"> 
					<?php 
						$rupiah=number_format($r['harga_tambahtanah'],0,',','.'); 
						echo $rupiah; 
					?>
					</th>
					<th>Jumlah DP</th><th>: Rp.</th>
					<th  class='pull-right' style="margin-right:10%"> 
					<?php 
						$rupiah=number_format($r['jlh_DP1'],0,',','.'); 
						echo $rupiah; 
					?>
					</th>
				</tr>
				<tr>
					<th>Kavling</th><th>:</th>
					<th>  <?php echo $r[no_kavling]; ?></th>
					<th>Hook</th><th>: Rp.</th>
					<th class='pull-right' style="margin-right:10%"> 
					<?php 
						$rupiah=number_format($r['hook'],0,',','.'); 
						echo $rupiah; 
					?>
					</th>
					<th>Total</th><th>: Rp.</th>
						<th class='pull-right' style="margin-right:10%"> 
						<?php
							$kpr = $r['nominal_kpr'];
							$dp = $r['jlh_DP1'];
							$total = $kpr + $dp;
							echo number_format($total,0,',','.'); 
						?>
						</th>
					</tr>
				<tr>
					<th>Lokasi</th><th>:</th>
					<th> <?php echo $r[nama_perumahan]; ?></th>
					<th>Penambahan Lain</th><th>: Rp. </th>
					<th class='pull-right' style="margin-right:10%">
					<?php 
						$rupiah=number_format($r['biaya_tambahan'],0,',','.'); 
						echo $rupiah; 
					?>
					</th>
					<th></th>
					<th></th>
				</tr>
				<tr>
					<th>HP</th><th>:</th>
					<th> <?php echo $r[notelp_pembeli]; ?></th>
					<th>Total Harga Rumah</th><th>: Rp.</th>
					<th class='pull-right' style="margin-right:10%">
					<?php
						$hrg = $r['harga_kavling'];
						$tanah = $r['harga_tambahtanah'];
						$hook = $r['hook'];
						$biayalain = $r['biaya_tambahan'];
						$tombak = $hrg + $tanah + $hook + $biayalain;
						echo number_format($tombak,0,',','.'); 
					?>
					</th>
					<th></th>
					<th></th>
				</tr>
				<tr>
					<th>Minimal Angsuran/Bln</th><th>:</th>
					<th>  </th>
					<th>Total harga rumah setelah diskon</th>
	                <th>:Rp</th>
	                <th  class='pull-right' style="margin-right:10%"><?php echo number_format($r[total_hrgarumah],0,',','.');  ?></th>
					<th></th>
				</tr>
			</thead>
		</table><hr>

    <?php
		$no++;
    }
	?>
		<table id='setoran' border='1' width=100%>
		<thead>
			<tr>
				<th>No</th>
				<th width=10%> Tanggal</th>
				<th width=15%> Uraian</th>
				<th> Jumlah Setoran (Rp)</th>
				<th> Total Setoran (Rp)</th>
				<th> Sisa Setoran (Rp)</th>
				<th>TTD/Paraf Penyetor</th>
				<th>TTD/Paraf Marketing</th>
				<th>TTD/Paraf Keuangan</th>
			</tr>
		</thead>
		<tbody>
		<?php
			
	    	$no=1;
			while($no<=15){
		?>
        <tr>
            <td><?php echo $no; ?></td>
            <td></td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
			<td> </td>
			<td> </td>
			<td> </td>
        </tr>
		<?php
			$no++;
			}
		?>
    </tbody>

    </table>  
	
			<table width="100%" align="center" style="margin-top:140px;">   
				<thead style="font-size:15px; text-align:right;" >
				<tr>
				<th></th>
				<th></th>
				<th>Mengetahui :</th>
				</tr>
				<tr>
				<th><b>Pj.Konsumen</b></th>
				<th><b>Konsumen</b></th>
				<th><b>Asisten Manager Marketing</b></th>
				<th><b>Manager Keuangan</b></th>
				<th><b>Direktur Utama</b></th>
				</tr>
			</thead>
		</table>
		<?php
	}
	/*hafl*/
	else{
			echo "";
			$id=$_GET['id'];
			$sql = mysqli_query($mysqli,"SELECT p.nama_pembeli,ty.type_rumah, p.notelp_pembeli, r.nama_perumahan, ty.harga_kavling, dt.no_kavling,dt.total_hrgarumah, dt.harga_tambahtanah,dt.biaya_tambahan,dt.hook, dt.nominal_kpr, dt.jlh_DP1, t.id_transaksi
										FROM data_pembeli p, data_perumahan r,detail_transaksirumah dt,transaksi_perumahan t,data_kavling dk,type_rumah ty
										WHERE t.nik_ktp=p.nik_ktp and ty.id_type=dk.id_type
										AND dt.id_perumahan=r.id_perumahan
										AND dt.no_kavling=dk.no_kavling
										and dt.status_batal=''
										AND dt.id_transaksi=t.id_transaksi AND dt.id_transaksi='$id'");

			$no = 1;
			while ($r = mysqli_fetch_array($sql)) {
			$hrg = $r['harga_kavling'];
			$tanah = $r['harga_tambahtanah'];
			$hook = $r['hook'];
			$biayalain = $r['biaya_tambahan'];
			$tombak2 = $hrg + $tanah + $hook + $biayalain;
			$totalharga=$r['total_hrgarumah'];

			?>
			<table width="100%" align="center">   
				<thead style="font-size:12px; text-align:right;" >
					<tr>
					<th>Nama Pemilik</th><th>:</th>
					<th><?php echo $r[nama_pembeli]; ?></th>
					<th>Harga Rumah</th><th>: Rp.</th>
					<th class='pull-right' style="margin-right:10%">
						<?php 
							$rupiah=number_format($r['harga_kavling'],0,',','.'); 
							echo $rupiah; 
						?>
					</th>
						<th >Jumlah KPR</th><th>: Rp.</th>
						<th class='pull-right' style="margin-right:10%">
							<?php 
								$rupiah=number_format($r['nominal_kpr'],0,',','.'); 
								echo $rupiah; 
							?>
						</th>
					</tr>
					<tr>
						<th>Type Rumah</th><th>:</th>
						<th>  <?php echo $r[type_rumah]; ?></th>
						<th>Penambahan Tanah</th><th>: Rp.</th>
						<th  class='pull-right' style="margin-right:10%"> 
						<?php 
							$rupiah=number_format($r['harga_tambahtanah'],0,',','.'); 
							echo $rupiah; 
						?>
						</th>
						<th>Jumlah DP</th><th>: Rp.</th>
						<th  class='pull-right' style="margin-right:10%"> 
						<?php 
							$rupiah=number_format($r['jlh_DP1'],0,',','.'); 
							echo $rupiah; 
						?>
						</th>
					</tr>
					<tr>
						<th>Kavling</th><th>:</th>
						<th>  <?php echo $r[no_kavling]; ?></th>
						<th>Hook</th><th>: Rp.</th>
						<th class='pull-right' style="margin-right:10%"> 
						<?php 
							$rupiah=number_format($r['hook'],0,',','.'); 
							echo $rupiah; 
						?>
						</th>
						<th>Total</th><th>: Rp.</th>
						<th class='pull-right' style="margin-right:10%"> 
						<?php
							$kpr = $r['nominal_kpr'];
							$dp = $r['jlh_DP1'];
							$total = $kpr + $dp;
							echo number_format($total,0,',','.'); 
						?>
						</th>
					</tr>
					<tr>
						<th>Lokasi</th><th>:</th>
						<th> <?php echo $r[nama_perumahan]; ?></th>
						<th>Penambahan Lain</th><th>: Rp. </th>
						<th class='pull-right' style="margin-right:10%">
						<?php 
							$rupiah=number_format($r['biaya_tambahan'],0,',','.'); 
							echo $rupiah; 
						?>
						</th>
						<th></th>
						<th></th>
					</tr>
					<tr>
						<th>HP</th><th>:</th>
						<th> <?php echo $r[notelp_pembeli]; ?></th>
						<th>Total Harga Rumah</th><th>: Rp.</th>
						<th class='pull-right' style="margin-right:10%">
						<?php
							$hrg = $r['harga_kavling'];
							$tanah = $r['harga_tambahtanah'];
							$hook = $r['hook'];
							$biayalain = $r['biaya_tambahan'];
							$tombak = $hrg + $tanah + $hook + $biayalain;
							echo number_format($tombak,0,',','.'); 
						?>
						</th>
						<th></th>
						<th></th>
					</tr>
					<tr>
						<th>Minimal Angsuran/Bln</th><th>:</th>
						<th>  </th>
						<th>Total harga rumah setelah diskon</th>
		                <th>:Rp</th>
		                <th><?php echo number_format($r[total_hrgarumah],0,',','.');  ?></th>
						<th></th>
					</tr>
				</thead>
			</table><hr>
    <?php
		$no++;
    }
	?>
	
	<table border='1' width='100%'>
			<thead>
			<tr>
				<th>No</th>
				<th> Tanggal</th>
				<th> Uraian</th>
				<th> Jumlah Setoran (Rp)</th>
				<th> Total Setoran (Rp)</th>
				<th> Sisa Setoran (Rp)</th>
				<th> TTD/Paraf Penyetor</th>
				<th> TTD/Paraf Marketing</th>
				<th> TTD/Paraf Keuangan</th>
			</tr>
			</thead>
	<?php
		//QUERY UNTUK MENCARI NIK_KTP,ID_PERUMAHAN,NO_KAVLING
		$query2 = mysqli_query($mysqli,"SELECT tr.nik_ktp,dt.id_perumahan,dt.no_kavling FROM transaksi_perumahan tr,detail_transaksirumah dt 
		where tr.id_transaksi=dt.id_transaksi and tr.id_transaksi='".$_GET['id']."'");
		$data2 = mysqli_fetch_array($query2);

		$nik = $data2['nik_ktp'];
		$idrumah = $data2['id_perumahan'];
		$no_kavling = $data2['no_kavling'];
		
		$tampil = mysqli_query($mysqli,"select b.jenis_bayar,b.id_bayarrumah,b.nik_ktp,k.jenis_transaksi,b.tgl_setoran,b.jlh_setoran
	            from kategori_jenistransaksi k, bayar_rumah b
	            where k.id_jenisbayar=b.id_jenisbayar
	            AND b.nik_ktp='$nik' AND b.id_perumahan='$idrumah' AND b.no_kavling='$no_kavling'");

		$setor= 0;
	    $row = mysqli_fetch_array($tampil);
	        
		//QUERY BAYAR RUMAH UNTUK SETORAN KONSUMEN
        $sql2 = mysqli_query($mysqli,"select b.id_bayarrumah,b.nik_ktp,k.jenis_transaksi,b.tgl_setoran,b.jlh_setoran
            from kategori_jenistransaksi k, bayar_rumah b
            where k.id_jenisbayar=b.id_jenisbayar
            AND b.nik_ktp='$nik' AND b.id_perumahan='$idrumah' AND b.no_kavling='$no_kavling' order by b.tgl_setoran DESC limit 1");

		//QUERY MENCARI JUMLAH BERAPA KALI KONSUMEN MENYETOR RUMAH
		$sqljlh = mysqli_query($mysqli,"select count(b.nik_ktp) as jlh
            from bayar_rumah b
            where b.nik_ktp='$nik' AND b.id_perumahan='$idrumah' AND b.no_kavling='$no_kavling'");
			
		$datajlh = mysqli_fetch_array($sqljlh);
		$jlhdata=$datajlh['jlh'];
		$temp=1;
		
		$sql22 = mysqli_query($mysqli,"select b.jenis_bayar,b.id_bayarrumah,b.nik_ktp,k.jenis_transaksi,b.tgl_setoran,sum(b.jlh_setoran) as total,dt.total_hrgarumah
            from kategori_jenistransaksi k, bayar_rumah b,detail_transaksirumah dt,transaksi_perumahan t
            where k.id_jenisbayar=b.id_jenisbayar and dt.id_transaksi=t.id_transaksi and t.nik_ktp=b.nik_ktp and dt.id_perumahan=b.id_perumahan and dt.no_kavling=b.no_kavling
            AND b.nik_ktp='$nik' AND b.id_perumahan='$idrumah' AND b.no_kavling='$no_kavling' group by b.nik_ktp,b.id_perumahan,b.no_kavling");
        $r22 = mysqli_fetch_array($sql22);
      
        $jlhsetor=$r22['total'];
		$totalhrga=$r22['total_hrgarumah'];
        
        $r2 = mysqli_fetch_array($sql2);
			$id = $r2['id_bayarrumah'];
			while($temp<=$jlhdata){
			 if($temp == $jlhdata){
			?>	
			<tbody>
			<tr>
				<td class='data'></td>
				<td class='data'>
					<?php echo  $r2['tgl_setoran']; ?>
				</td>
				<td class='data'>
					<?php echo  $r2['jenis_transaksi']; ?>
				</td>
				<td class='data'>
				<?php 
                    $rupiah=number_format($r2['jlh_setoran'],0,',','.'); 
                    echo $rupiah; 
                ?>
				</td>
				<td class='data'>
					<?php
                    echo number_format($jlhsetor,0,',','.'); 
                ?>
				</td>
				<td class='data'>
					<?php
						$totals+=$jlhsetor;
						$sisa1=$totalharga;
                        $sisa= $sisa1-$totals;
	                    echo number_format($sisa,0,',','.'); 
	                ?>
				</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<?php
			}
			else{
			?>
				<tr>
				<td><?php echo $temp;?></td>
				<td class='data'>
				</td>
				<td class='data'>
				</td>
				<td class='data'>
				</td>
				<td class='data'>
				</td>
				<td class='data'>
				</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			</tbody>
			<?php
			}
            $temp++;
			}
    ?>

    </table>  
	<?php
		}
	?>
	<script>
		window.load = print_d();
		function print_d(){
			window.print();
		}
	</script>