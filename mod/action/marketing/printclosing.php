<html>
<head>
	<title>Print Laporan</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="../../assets/css/bootstrap-theme.min.css">
	<link href="../../../images/pavicon.png" rel="icon" type="image/x-icon" />
</head>
<body><br><br>
	<img src="../../../images/logo.png" align="right">
	<h3>Laporan Closing</h3><hr>
	<table border="1" width="100%" style="border-collapse:collapse;" align="center">
	    <thead>
	    	<tr class="tableheader" style="font-size:14px;">
	    		<th>No</th>
				<th>ID Transaksi</th>
	            <th>Nama Konsumen</th>
				<th>Perumahan</th>
	            <th>Kavling</th>
				<th>Harga Total Rumah (Rp)</th>
	            <th>Total Setoran (Rp)</th>
	            <th>Tanggal Lunas</th>
				<th>Upload Kartu Konsumen</th>
	        </tr>
	    </thead>
	    <tbody>
			<?php
                error_reporting(0);
				include '../../../config/connectdb.php';
					
                $id = $_GET['id'];
					
				$sql = mysqli_query($mysqli,"SELECT p.nama_pembeli, r.nama_perumahan,dt.total_hrgarumah, dt.no_kavling,t.id_transaksi,b.tgl_setoran,sum(b.jlh_setoran) as jlh_setoran
        FROM data_pembeli p, data_perumahan r,detail_transaksirumah dt,transaksi_perumahan t,data_kavling dk, bayar_rumah b
        WHERE t.nik_ktp=p.nik_ktp and b.nik_ktp=p.nik_ktp
        AND dt.id_perumahan=r.id_perumahan
        AND dt.no_kavling=dk.no_kavling
        AND dt.id_transaksi=t.id_transaksi GROUP BY t.id_transaksi ORDER BY b.tgl_setoran  DESC");

				$no = 1;
        while ($r = mysqli_fetch_array($sql)) {
		$id = $r['id_transaksi'];
        $htr= $r['total_hrgarumah'];
        $setr=$r['jlh_setoran'];
		if($htr==$setr){
        ?>
		<tr align='left'>
            <td><?php echo  $no;?></td>
			<td><?php echo  $id;?></td>
            <td><?php echo  $r['nama_pembeli']; ?></td>
            <td><?php echo  $r['nama_perumahan']; ?></td>
			<td><?php echo  $r['no_kavling']; ?></td>
			<td align="right"><?php $rupiah=number_format($htr,0,',','.'); 
                    echo $rupiah; 
                ?>  
            </td>
            <td align="right"><?php $rupiah=number_format($r['jlh_setoran'],0,',','.'); 
                    echo $rupiah; 
                ?> 
            </td>
            <td><?php echo $r['tgl_setoran']; ?></td>
            <td><?php echo"" ; ?></td>
        </tr>
		<?php

            $no++;
        }else{
            echo" "; 
        }

        }
    
        ?>
               
    	</tbody>
    </table>
</body>
</html>
		
	<script>
		window.load = print_dc();
		function print_dc(){
			window.print();
		}
	</script>