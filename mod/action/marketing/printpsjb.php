<html>
<head>
	<title>Print Perjanjian Sementara Jual Beli-PSJB</title>
</head>
<body>
<?php
	include '../../../config/connectdb.php';
	error_reporting(0);
?>
<h3>PERJANJIAN SEMENTARA JUAL BELI � PSJB</h3>
<p>
No: 
<br>
Pada hari ini <?php 
$day = date ("D");
switch ($day) {
case 'Sun' : $hari = "Minggu"; break;
case 'Mon' : $hari = "Senin"; break;
case 'Tue' : $hari = "Selasa"; break;
case 'Wed' : $hari = "Rabu"; break;
case 'Thu' : $hari = "Kamis"; break;
case 'Fri' : $hari = "Jum'at"; break;
case 'Sat' : $hari = "Sabtu"; break;
default : $hari = "Kiamat";
}
echo $hari;?>, 
<?php echo "tanggal ".date('d')." ".date('M')." tahun ".date('Y');?> yang bertanda tangan di bawah ini dengan diketahui para saksi yang akan turut menandatangani perjanjian ini:<br>
<br>PIHAK 1 :<br>
<table>
<tr><td>Nama </td><td> : </td><td><?php echo $_GET['nama_pebeli']; ?></td></tr> 
<tr><td>Jabatan	</td><td> : </td></tr> 
<tr><td>Alamat	</td><td> : </td></tr> 
<tr><td>Nomor telepon/HP  </td><td> : </td></tr> 
</table>
<p>Dalam hal ini selaku penjual bertindak atas nama Direktur Utama PT. Matahari Cipta, Rahmad Yadi, SE., selanjutnya disebut sebagai : <b>PIHAK PERTAMA</b> </p>
<br>
PIHAK 2 :
<table>
<tr><td>Nama </td><td> : </td><td><?php echo $_GET['namapembeli']; ?></td></tr>
<tr><td>Alamat Sekarang di	</td><td> : </td><td><?php echo $_GET['alamat']; ?></td></tr>
<tr><td>Nomor telepon/HP	</td><td> : </td><td><?php echo $_GET['notelp']; ?></td></tr>
</table>
<br>
<p>Selanjutnya disebut sebagai : <b>PIHAK KEDUA</b></p>
<p>Bahwa PIHAK PERTAMA dengan ini mengikatkan diri untuk menjual, memindahkan dan mengalihkan serta menyerahkan kepada PIHAK KEDUA, dan PIHAK KEDUA dengan ini pula mengikatkan diri dalam perjanjian ini untuk membeli, menerima pemindahan dan pengalihan serta penyerahan dari PIHAK PERTAMA sebuah rumah � 36 m^2 (lebih kurang tiga puluh enam meter persegi). Dan berdiri diatas sebidang tanah seluas � 150 m^2 (kurang lebih seratus limapuluh meter persegi) dengan data sertifikat terletak di :
</p>
<br>
<table>
<tr><td>Desa/Kelurahan	</td><td> : </td><td>Gue</td></tr>
<tr><td>Kecamatan		</td><td> : </td><td>Kota Baru</td></tr>
<tr><td>Kabupaten		</td><td> : </td><td>Aceh Besar</td></tr>
<tr><td>Provinsi		</td><td> : </td><td>Aceh</td></tr>
</table>
<p>Yang dikenal sebagai perumahan <?php 
		$querykavrum="SELECT nama_perumahan FROM data_perumahan where id_perumahan='$_GET[idr]'";
		$datakavrum= mysqli_query($mysqli,$querykavrum);
		$r3 = mysqli_fetch_array($datakavrum);
		$narum=$r3['nama_perumahan'];
		echo $narum;
 ?> nomor kavling <?php
		$querykavrum="SELECT type_rumah,luas_tanah FROM type_rumah where id_type='$_GET[ty]'";
		$datakavrum= mysqli_query($mysqli,$querykavrum);
		$r3 = mysqli_fetch_array($datakavrum);
		$type=$r3['type_rumah'];
		$luas=$r3['luas_tanah'];
		
		echo $_GET['kav'];
		echo " dengan type ".$type." Lb/Lt (".$luas.") ," 
 ?>spesifikasi dan gambar terlampir yang akan disetujui dan ditanda tangani antara kedua belah pihak pada saat akad jual beli dilakukan lebih kurang 14 hari setelah perjanjian ini. Dengan demikian kedua belah pihak telah bersepakat mengikatkan dirinya masing-masing untuk mengadakan Perjanjian Sementara Jual Beli (BJPS) dengan syarat-syarat dan ketentuan sebagai berikut.
</p><br>
<center>
<p>PASAL 1<br>
HARGA JUAL
</p></center>
<p>1. PIHAK PERTAMA mengikatkan untuk menjual, memindahkan dan mengalihkan kepada PIHAK KEDUA dan PIHAK KEDUA membeli, menerima pemindahan serta penyerahan diri PIHAK PERTAMA atas tanah dan bangunan tersebut dengan harga kesepakatan sebesar:</p><br>
		<table>
		<tr><td>Harga Rumah </td><td> Rp </td><td><?php echo $_GET['hargarumah']?></td></tr>
		<tr><td>Penambahan Tanah </td><td> Rp </td><td><?php echo $_GET['tambahtanah']?></td></tr>
		<tr><td>Hook			</td><td>Rp</td><td><?php echo $_GET['hook']?></td></tr>
		<tr><td>Total			</td><td>Rp</td><td><?php echo $_GET['totalrmh']?></td></tr>
		</table>
		<table>
		<tr><td>Total</td><td>:</td> </td><td>Rp</td><td><?php echo $_GET['totalrmh']?></td></tr>
		<tr><td>Terbilang</td><td>:</td></td><td></td><td></td></tr>
		</table>
<br>
<p>2. Harga tersebut belum termasuk Biaya Balik Nama (BBN) dan Biaya Perolehan Ha katas Tanah dan Bangunan (BPHTP) sesuai dengan peraturan perundangan yang berlaku yang harus dibayarkan sebelum penanda tanganan Akte Jual Beli di Notaris.</p> 
<br>
<center>
<p>PASAL 2<br>
TATA CARA PEMBAYARAN
</p></center>
<br>
<p>1. PIHAK KEDUA sanggup melunasi pembayaran tersebut dalam pasal 1 dengan system dan cara pembayaran menggunakan fasilitas kredit bank dengan tahapan pembayaran sbb:</p><br>
	<table>
	<tr><td>No</td><td>Tahapan Pembayaran</td><td>Tanggal</td><td>Jumlah Pembayaran</td></tr>
	<tr><td>1.</td><td>Uang Tanda Jadi</td><td><?php echo date('d')." ".date('M')." ".date('Y');?></td><td> Rp. </td></tr>
	<tr><td>2.</td><td>Down Payment(DP)</td><td> </td><td> Rp. </td></tr>
	<tr><td>3.</td><td>Sisa Pembayaran dengan fasilitas kredit bank</td><td> </td><td> Rp. </td></tr>
	</table>
	<table>
	<tr><td>Total</td><td>:</td> </td><td>Rp</td><td></td></tr>
	<tr><td>Terbilang</td><td>:</td></td><td></td><td></td></tr>
	</table>

<p>2. Down Payment pertama harus dibayarkan selambat-lambatnya 2 (dua) minggu setelah Booking Free. Down Payment dapat dibayarkan penuh saat 2 (dua) minggu setelah Booking, ataupun dapat diangsur sampai selama masa pembangunan (�4 bulan). Dengan ketentuan nominal perbulannya tidak kurang dari Total DP dibagi masa pembangunan.
</p><p>3. PIHAK KEDUA menjamin bahwa tahapan pembayaran angsuran ini dilaksanakan oleh PIHAK KEDUA sebelum dan sesudah hari dan tanggal perjanjian ini ditandatangani
</p><br>
<p>Untuk tiap � tiap pembayaran ( angsuran, denda dan bunga ) yang dilakukan PIHAK KEDUA kepada PIHAK PERTAMA, harus dilakukan ke alamat PIHAK PERTAMA, atau transfer bank ke rekening PIHAK PERTAMA. Pembayaran melalui cek atau transfer baru dianggap sah diterima setelah dana yang bersangkutan efektif diterima oleh PIHAK PERTAMA dan akan diberikan tanda terima berupa kwitansi oleh PIHAK PERTAMA yang merupakan bagian yang tidak terpisahkan.
</p>
<br>
<center>
PASAL 3<br>
PEMBELIAN DENGAN FASILITAS KREDIT BANK
</center><br>
<p>1. Apabila pelunasan pembayaran dilakukan melalui fasilitas Kredit Pemilikan Rumah (KPR), maka PIHAK KEDUA bersedia memenuhi segala persyaratan dan biaya � biaya yang diminta oleh pihak bank pemberi kredit. Bank pemberi KPR adalah hak yang ditunjuk oleh PIHAK PERTAMA.
<br><p>2. PIHAK KEDUA bersedia melaksanakan pelunasan pembayaran dengan melakukan akad kredit dengan pihak bank selambat � lambatnya 14 (empat belas) hari sejak disetujuinya Kredit Pemilikan Rumah (KPR) oleh pihak Bank. Jika ternyata PIHAK KEDUA membatalkan pembelian dengan menggunakan fasilitas Kredit Pemilikan Rumah (KPR), atau Pihak bank tidak menyetujui, maka PIHAK KEDUA sanggup melunasi pembayaran secara tunai. Apabila PIHAK KEDUA tidak dapat memenuhi hal tersebut di atas, maka perjanjian ini batal dan untuk selanjutnya PPIHAK KEDUA dikenakan denda sesuai dengan pasal 4 perjanjian ini.
</p><br><p>3. Masa untuk pengumpulan berkas Kredit Pemilihan Rumah (KPR) yang diberikan oleh PIHAK PERTAMA adalah 5 (lima) hari.
</p><br><p>4. Jika pengajuan kredit PIHAK KEDUA disetujui oleh bank, maka Akad Kredit akan dilakukan pada saat kondisi rumah 80%. Dimana kondisi rumah tersebut sudah beratap, lantai sudah dicor, dinding sudah diplater dan cat dasar.
</p>
<br>
<center>PASAL 4<br>
PEMBATALAN
</center><br>
<p>Dalam hal terjadi pembatalan jual beli yang dilakukan oleh PIHAK KEDUA, kedua belah pihak sepakat untuk mengecualikan ketentuan pasal 1266 dan 1267 Kitab Undang-Undang Hukum Perdata, sehingga haltersebut tidaklah diperlukan suatu keputusan atau ketetapan Pengadilan Negeri, dan selanjutnya PIHAK KEDUA setuju untuk membayar biaya administrasi pembatalan kepada PIHAK PERTAMA dengan perincian sebagai berikut:
	Apabila terjadi pembatalan dari PIHAK KEDUA, ataupun PIHAK PERTAMA melakukan pembatalan secara sepihhak dikarenakan pihak KEDUA TIDAK mengikuti prosedur yang berjalan, maka uang tanda jadi hangus sebesar Rp. 2.000.000,-. 
	Pada saat pembangunan rumah sudah berjalan maka PIHAK KEDUA hanya akan menerima pengembalian untuk rumah standart sebesar 50% dari total uang yang telah dibayarkan kepada PIHAK PERTAMA dan pengembalian sebesar 25% untuk rumah tidak standart.
	Setelah fisik nagunan rumah sudah selesai. Maka semua uang yang sudah terbayar dianggap hangus dan menjadi milik PIHAK PERTAMA.</p>
	<ul>
	<li>Apabila pembatalan terjadi karena berkas tidak disetujui oleh bank, maka DP yang telah dibayar PIHAK KEDUA kepada PIHAK PERTAMA akan dikembalikan dengan potongan sejumlah Rp. 500.000,-.
	</li><li>Apabila PIHAK KEDUA tidak melakukan pembayaran Down Payment (DP) pertama dalam jangka waktu selambat-lambatnya 14 (empat belas) hari sejak pembayaran Booking Fee, maka secara otomatis dianggap telah melakukan pembatalan, dengan uang tanda jadi (Booking Fee) hangus/tidak dikembalikan.
	</li><li>Apabila PIHAK KEDUA menghambat jalannya proses jual beli rumah, maka PIHaK PERTAMA berhak membatalkan secara sepihak tanpa persetujuan dan konfirmasi dari PIHAK KEDUA, serta Bookig Fee yang telah dibayarkan hangus (tidak dikembalikan).
	</li>
	</ul>
<br>
<center>
PASAL 5 <br>
PEMBANGUNAN<br>
</center>
	<p>1. PIHAK PERTAMA akan melaksanakan pembangunan fisik rumah dimulai atas kesepakatan para pihak dengan melihat kesiapan dilapangan atau selama-lamanya 30 (tiga puluh) hari semenjak dikeluarkannya izin mendirikan bangunan (IMB), PIHAK KEDUA membayar angsuran mencapai nilai 10% (sepuluh persen) dari harga jual yang telah disepakati.
	<br></p>
	<p>2. PIHAK PERTAMA berkewajiban menyelesaikan pembangunan rumah milik PIHAK KEDUA dalam jangka waktu kurang lebih 120 (seratus dua puluh) hari atau 4 (empat) bulan dihitung sejak awal mulai pembangunan.
	</p><br>
	<p>3. PIHAK PERTAMA menjamin bahwa pembangunan rumah sesuai dengan prediksi awal, jika dalam pelaksanaannya tidak terjadi keadaan kahar (diliar kemampuan manusia) atau force majeure.
	</p><br>
	<p>4. Dalam hal terjadinya keterlambatan masa pembangunan keua belah pihak sepakat untuk menyelesaikannya secara musyawarah mufakat.
	</p><br>
<center>
PASAL 6<br>
SERAH TERIMA BANGUNAN
</center>
<br>
	<p>1. PIHAK KEDUA menerima dan setuju penyerahan bangunan rumah (serah terima kunci) dari PIHAK PERTAMA dilaksanakan, apabila PIHAK PERTAMA seperti tercantum dalam pasal 2 perjanjian ini. Sebelum di adakan serah terima dari PIHAK PERTAMA kepada PIHAK KEDUA, maka PIHAK KEDUA tidak diperkenankan melakukan hal-hal sebagai berikut:
</p><br>
	<ul>  
	<li>PIHAK KEDUA tidak diperlukan untuk melaksanakan pembangunan, mengubah maupun menambah bangunan, baik yang dilaksanakan sendiri maupun melalui pihak ketiga
	</li> <li>  PIHAK KEDUA tidak diperkenankan untuk menempati bangunan atau menempatkan pihak ketiga dengan alas an apapun di lokasi pembangunan.
	</li> <li>  PIHAK KEDUA tidak diperkenankan untuk memasukkan dan/ atau menempatkan barang apapun juga di lokasi pembangunan.
	</li>
	</ul>
<br><p>2. Penyerahan kunci rumah akan dibuatkan dengan Berita Acara Serah Terima Rumah tersendiri yang merupakan bagian yang tidak terpisahkan dari perjanjian ini. Sejak diserahkannya bangunan dari PIHAK PERTAMA kepada PIHAK KEDUA, maka segala biaya-biaya yang berkaitan dengan fasilitas pada bangunan tersebut menjadi tanggung jawab PIHAK KEDUA.<br>
</p><br>
<center>
	PASAL 7
</center><br>
	<p>1. Setelah pembangunan rumah yang dilakukan selesai, maka PIHAK PERTAMA berkewajiban untuk mengalihkan ha katas tanah dimana rumah tersebut berdiri kepada PIHAK KEDUA dan untuk biaya akte jual beli (AJB), biaya balik nama (BBN) serta biaya perolehan ha katas tanah dan bangunan (BPHTB) akan di bayar masing-masing pihak atas perundang-undangan yang berlaku.
	</p><br><p>2. Status hak atas tanah sepenuhnya menyesuaikan dengan ketentuan peraturan yang berlaku dimana lokasi rumah ini berada dan Badan Pertanahan Nasional (BPN) setempat.
</p><br>
<center>
PASAL 8
</center>
<br>
<p>Hal � hal yang belum diatur dalam perjanjian ini oleh PIHAK PERTAMA dan PIHAK KEDUA akan diatur dan ditetapkan dikemudian hari, dengan syarat disetujui dan si tanda tangani bersama oleh kedua belah pihak dan merupakan bagian yang tidak terpisahkan dari perjanjian ini. Apabila dikemudian hari terdapat kesalahan dan kekelituan dalam perjanjian ini akan diadakan perubahan dan pembetulan sebagaimana mestinya.
</p><br>
<center>
PASAL 9<br>
PENUTUP
</center>
<br>
<p>PIHAK PERTAMA dan PIHAK KEDUA menatakan dengan sunguh-sungguh bahwa perjanjian pendahuluan tentang pengikatan jual beli ini dibuat dengan tanpa adanya paksaan dari pihak manapun. Demikian perjanjian ini dibuat rangkap 2 dimana masing-masing bermaterai cukup dan mempunyai kekuatan hokum yang sama.

<table align="center">
<tr><td>PIHAK PERTAMA<td><td>PIHAK KEDUA</td></tr>
<tr><td></td><td></td></tr>
<tr><td colspan='2'><br></td></tr>
<tr><td colspan='2'><br></td></tr>
<tr><td colspan='2'><br></td></tr>
<tr>
<td>(YULIANTI NATALIA)</td><td>&nbsp &nbsp &nbsp </td><td> (<?php echo $_GET['namapembeli'];?>)<td>
</tr>
</table>
<br><center>Saksi � saksi :</center>
<br>
<br>
<table align="center">
<tr><td>(_____________________)<td><td>  (_____________________)</td></tr>
</table>
</p>
</body>
</html>
<script>
	window.load = print_d();
	function print_d(){
		window.print();
	}
</script>