<link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../../assets/css/dataTables.bootstrap.css"/>

<?php
	//error_reporting(0);
	include '../../config/connectdb.php';
	$tglsekarang=date('Y-m-d');
	echo"
		<form method='post' action='../action/adm/act_keuangan.php?mod=keuangan_giro&act=tambahkeuangan'>
			<b style='font-size:20px;'><center>Input Data</center></b><br><br>
			<table border=0 width='80%'>
				<tr>
					<td><label class='control-label' style='padding:5%;'>Tanggal Sekarang: </label></td>
					<td><input type='date' name='tgl' required='' value='$tglsekarang' class='form-control' disabled></td>
					<td width='20%'><label class='control-label' style='padding:5%;'>Tanggal Cair: </label></td>
					<td><input type='date' name='tgl_cek' required=''  class='form-control'></td>
				</tr>
				<tr>
					<td width='20%'><label class='control-label' style='padding:5%;'>Pilih : </label> </td>
					<td><select name='jenis' id='jenis' class='form-control' required=''>
							<option value='' selected disabled=''>- Pilih -</option>";
							$getdata="SELECT * FROM jenis_bayar";
							$tampil=mysqli_query($mysqli,$getdata);
							while($r=mysqli_fetch_array($tampil))
							{
								echo "<option value=$r[id_jeniskeuangan]>
								$r[jenis_keuangan]</option>";
							}
				echo"	</select>
					</td>
				</tr>
				<tr>
					<td><label for='giro' class='control-label' style='padding:5%;'>Giro: </label></td>
					<td><select name='giro' id='giro' class='form-control' required=''>
								<option value='' selected disabled=''>- Pilih -</option>";
							$getdata="SELECT * FROM giro WHERE jenis_giro !='ADM'";
							$tampil=mysqli_query($mysqli,$getdata);
							while($r=mysqli_fetch_array($tampil))
							{
								echo "<option value=$r[no_giro]>
								$r[jenis_giro]</option>";
							}
				echo"	</select></td>
					<td>
				</tr>
				<tr>
					<td><label for='no_cek' class='control-label' style='padding:5%;'>No Cek: </label>
					<td><input type='text' name='no_cek' id='no_cek' class='form-control'><td>
				</tr>

				<tr><div class='form-group'>
					<td><label class='control-label' style='padding:5%;'>Keterangan : </label></td>
					<td><textarea name='keterangan' class='form-control'></textarea></td>
					</div>
				</tr>
				<tr><div class='form-group'>
					<td><label class='control-label' style='padding:5%;'>Nominal : </label></td>
					<td><input type='text' name='nominal' id='nominal' class='form-control'>";
					?>
						<script type='text/javascript'>
							$(document).ready(function(){
								$('#nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
							});
						</script>						
					<?php
				echo"
					</td>
					</div>
				</tr>
				<tr>
					<td><br><input type='submit' value='Input' class='btn btn-primary btn-sm'>&nbsp;
					<input type='reset' value='Reset' class='btn btn-primary btn-sm'></td>
				</tr>
			</table>
		</form>
		";	

	echo "<br><br>
		";
		?>
		<table id='Dkeuangan' class='table table-bordered table-hover'>
			<tr bgcolor='#cfcfcf' class="tableheader">
				<th width="5%">No</th>
				<th width="13%">Tanggal Cek/Bg</th>
				<th width="17%">Keterangan</th>
				<th width="10%">Cek/Bg</th>
				<th width="10%">No.Cek</th>
				<th width="15%">Debit (Rp)</th>
				<th width="15%">Kredit (Rp)</th>
				<th width="15%">Saldo (Rp)</th>    	
			</tr>
			<tbody>
			<?php
			$p      = new Paging;
	        $batas  = 5;
	        $posisi = $p->cariPosisi($batas);

			$tampil = mysqli_query($mysqli,"SELECT g.jenis_giro,g.no_giro,k.giro_asal,k.tgl_sekarang,k.tgl_cek,k.no_cek,k.nominal,k.keterangan,j.jenis_keuangan,j.id_jeniskeuangan
                    FROM giro g, data_keuangan k,jenis_bayar j
                    WHERE g.no_giro=k.no_giro AND k.id_jeniskeuangan=j.id_jeniskeuangan
                    ORDER BY k.tgl_sekarang LIMIT $posisi,$batas");

			$no=1;
			while($r = mysqli_fetch_array($tampil)){
	 			$id=$r[id_keuangan];

	        if($r[jenis_giro]=='ADM'){
	        	$debit=$r[nominal];
	        	$saldod+=$debit;
	        	$saldoa+=$debit;
	        ?>
	        <tr align='center'>
	            <td><?php echo $no;?></td>
	            <td><?php echo  $r['tgl_sekarang']; ?></td>
	            <td align='left'><?php echo  $r['keterangan']; ?></td>
				<td><?php echo  $r['giro_asal']; ?></td>
	            <td><?php echo  $r['no_cek']; ?></td>
	            <td align='right'><?php echo number_format($debit,0,',','.'); ?></td>
	            <td align='right'></td>
	            <td align='right'><?php echo number_format($saldoa,0,',','.'); ?></td>
	        </tr>
	        <?php	
	        } else {
	        	$kredit=$r[nominal];
	            $saldok+=$kredit;
	            $saldoa=$saldod-$saldok;
	        ?>
	        <tr align='center'>
	            <td><?php echo $no;?></td>
	            <td><?php echo  $r['tgl_sekarang']; ?></td>
	            <td align='left'><?php echo  $r['keterangan']; ?></td>
				<td><?php echo  $r['jenis_giro']; ?></td>
	            <td><?php echo  $r['no_cek']; ?></td>
	            <td align='right'></td>
	            <td align='right'><?php echo  number_format($kredit,0,',','.'); ?></td>
	            <td align='right'><?php echo number_format($saldoa,0,',','.'); ?></td>
	        </tr>
	        <?php
	        }  
	        	$saldos=$saldod-$saldok;
	        $no++;
	        }
	        ?>
			<tr>
				<th colspan="5">TOTAL</th>
				<td align="right"><?php echo number_format($saldod,0,',','.');  ?></td>
				<td align="right"><?php echo number_format($saldok,0,',','.');  ?></td>
				<td align="right"><?php echo number_format($saldos,0,',','.');  ?></td>
				
			</tr>
			<tr>
				<td colspan="9" align="right"><button type="button" class="btn btn-primary" onclick="print_d()" >
				<span class="glyphicon glyphicon-print"></span> Print</button></td>
			</tr>
			</tbody>
		</table>
		<table>
			<tr><td>
				<form action='index.php?mod=keuangan_giro' method='post'>
					<td><input name='back' type='submit' value='Back' class='btn btn-primary '></td>
				</form>
				</td>
				<td width="10%"><?php
					$jmldata = mysqli_num_rows(mysqli_query($mysqli,"SELECT g.jenis_giro,g.no_giro,k.giro_asal,k.tgl_sekarang,k.tgl_cek,k.no_cek,k.nominal,k.keterangan,j.jenis_keuangan,j.id_jeniskeuangan
                    FROM giro g, data_keuangan k,jenis_bayar j
                    WHERE g.no_giro=k.no_giro AND k.id_jeniskeuangan=j.id_jeniskeuangan"));
				    $jmlhalaman  = $p->jumlahHalaman($jmldata, $batas);
				    $linkHalaman = $p->navHalaman($_GET[halaman], $jmlhalaman);

					echo "<div class='btn'> $linkHalaman</div>";
					?></td>
			</tr>
			<tr>

			</tr>
		</table>

<!-- Modal for Edit Giro-->
    <div id="edit_nominal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Data</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function(){
            $(document).on('click','.edit-nominal',function(e){
                e.preventDefault();
                $("#edit_nominal").modal('show');
                $.post('../action/adm/edit_nominal.php',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
                $('#edit_nominal').on('hidden.bs.modal', function () {
                 location.reload();
                })
            });
        });
    </script>

<!--     <script type="text/javascript">
        $('#jenis_keluar').change(function(){
            if( $(this).val() == '1'){
                $('#giro').show();
                $('#no_cek').hide();
            }else{
                $('#giro').hide();
                $('#no_cek').show();
            }
        });
 	</script>
 -->		        
<script>
	function print_d(){
		<?php
		
		echo "window.open('../action/adm/printLaporan.php','_blank');";
		?>
	}
</script>


<script src="../../assets/js/jquery-1.11.0.js"></script>
<script src="../../assets/js/bootstrap.min.js"></script>
<script src="../../assets/datatables/jquery.dataTables.js"></script>
<script src="../../assets/datatables/dataTables.bootstrap.js"></script>
