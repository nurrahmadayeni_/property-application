<?php
	include '../../../config/connectdb.php';
	error_reporting(0);
	$id = $_POST["id"]; //escape the string if you like

        echo "
            <table id='DGiro' width='100%' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Giro</th>
                    <th>Div.Penanggung Jawab</th>
                    <th>No.Rek</th>
                    <th>Bank</th>
                    <th>Specimen</th>
                    <th>Saldo Awal Giro (Rp)</th>
                </tr>
            </thead>
            <tbody>
        ";
       
             
        include '../../../config/connectdb.php';

        $sql = mysqli_query($mysqli,"SELECT g.no_giro,g.penaggungjawab,g.no_rek,g.nama_bank,g.spesimen,k.nominal,k.giro_asal,j.id_jeniskeuangan 
                    FROM giro g, data_keuangan k,jenis_bayar j
                    WHERE g.no_giro=k.no_giro AND k.id_jeniskeuangan=j.id_jeniskeuangan
                    AND j.id_jeniskeuangan='2' AND g.no_giro !='5' AND k.giro_asal='ADM'");
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
            $id = $r['no_giro'];
        ?>

        <tr align='left'>
            <td><?php echo $no;?></td>
            <td>
                <?php echo  $r['jenis_giro']; ?>
            </td>
            <td>
                <?php echo  $r['penaggungjawab']; ?>
            </td>
            <td>
                <?php echo  $r['no_rek']; ?>
            </td>
            <td>
                <?php echo  $r['nama_bank']; ?>
            </td>
            <td>
                <?php echo  $r['spesimen']; ?>
            </td>
            <td align='right'>
                <?php echo number_format($r[nominal],0,',','.'); ?>
            </td>
        </tr>
        <?php
            $no++;
        }
        ?>

        </tbody>

    </table>  
<script type="text/javascript">
    $(function() {
        $("#DGiro").DataTable();
    });
    </script>
