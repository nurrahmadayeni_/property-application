<html>
<head>
	<title>Print Laporan</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="../../assets/css/bootstrap-theme.min.css">
	<link href="../../../images/pavicon.png" rel="icon" type="image/x-icon" />
</head>
<body><br>
	<img src="../../../images/logo.png" align="right"><br>
	<b style="font-size:17px;">Laporan Alokasi</b><hr><br>
	<table border="1" width="100%" style="border-collapse:collapse;" align="center">
    	<tr class="tableheader"> 
			<th width='5%'><center>No</center></th>
			<th width='15%'>Nama Alokasi</th>
			<th width='10%'><center>Quantity</center></th>
			<th width='15%'><center>Nilai/Harga (Rp)</center></th>
			<th width='15%'><center>Jumlah Alokasi (Rp)</center></th>
        </tr>
     </thead>
            <tbody>
            <?php
    			error_reporting(0);
				include '../../../config/connectdb.php';

		        $sql = mysqli_query($mysqli,'SELECT nama_alokasi,jml_alokasi,harga_alokasi,jumlah_keluar FROM pengajuan_alokasi');
		        $no = 1;
		        while ($r = mysqli_fetch_array($sql)) {
		            $jml=$r['harga_alokasi'];
		            $jk= $r['jumlah_keluar'];
		            $total = $r['jml_alokasi'] * $r['harga_alokasi'];
		            $totals += $total;
		    ?>
		<tr>
	        <td align='center'><?php echo $no;?></td>
	        <td>
	        	<?php echo  $r['nama_alokasi']; ?>
	        </td>
	        <td align='center'>
	        	<?php echo  $r['jml_alokasi']; ?>
	        </td>
	        <td align='right'>
	            <?php 
	                $rupiah=number_format($r['harga_alokasi'],0,',','.'); 
	                echo $rupiah; 
	            ?>
	        </td>
	        <td align='right'>
	            <?php 
	                echo number_format($total,0,',','.');
	            ?>
	        </td>
	    </tr>
        <?php
        	$no++;
		    }
        ?>
        <tr>
	        <td colspan='4'>TOTAL</td>
	        <td align="right">
	            <?php 
	                echo number_format($totals,0,',','.'); 
	            ?>
	        </td>
        </tr>

        </tbody>
    </table>
</body>
</html>

	<script>
		window.load = print_d();
		function print_d(){
			window.print();
		}
	</script>