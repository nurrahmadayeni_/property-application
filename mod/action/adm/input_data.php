<?php
	error_reporting(0);
	include '../../config/connectdb.php';
	$tglsekarang=date('Y-m-d');
	echo"
		<form method='post' action='../action/adm/act_keuangan.php?mod=keuangan_giro&act=tambahkeuangan'>
			<b style='font-size:20px;'><center>Input Data</center></b><br><br>
			<table border=0 width='1000%'>
				<tr>
					<td><label class='control-label' style='padding:5%;'>Tanggal Sekarang: </label></td>
					<td><input type='date' name='tgl' required='' value='$tglsekarang' class='form-control' disabled></td>
					<td width='20%'><label class='control-label' style='padding:5%;'>Tanggal Cair: </label></td>
					<td><input type='date' name='tgl_cek' required=''  class='form-control'></td>
				</tr>
				<tr>
					<td width='20%'><label class='control-label' style='padding:5%;'>Pilih : </label> </td>
					<td><select name='jenis' id='jenis' class='form-control' required=''>
							<option value='' selected disabled=''>- Pilih -</option>";
							$getdata="SELECT * FROM jenis_bayar";
							$tampil=mysqli_query($mysqli,$getdata);
							while($r=mysqli_fetch_array($tampil))
							{
								echo "<option value=$r[id_jeniskeuangan]>
								$r[jenis_keuangan]</option>";
							}
				echo"	</select>
					</td>
				</tr>
				<tr>
					<td width='20%'><label for='jenis_keluar' class='control-label' style='padding:5%;'>Cek/Bg : </label> </td>
					<td><select name='jenis_keluar' id='jenis_keluar' class='form-control' required=''>
							<option value='' selected disabled=''>- Pilih -</option>
							<option value='1'>Giro</option>
							<option value='0'>Cek</option>
						</select>
					</td>
				</tr>
				<tr>
				<td colspan='2'>
				<div id='giro' style='display:none;'>
					<div style='float:left;width:15%;padding-left:2%;'>
						<label for='giro' class='control-label'>Giro: </label></div>
					<div style='float:right;width:62%;'>
						<select name='giro' id='giro' class='form-control' required=''>
							<option value='' selected disabled=''>- Pilih -</option>";
							$getdata="SELECT * FROM giro WHERE jenis_giro !='ADM'";
							$tampil=mysqli_query($mysqli,$getdata);
							while($r=mysqli_fetch_array($tampil))
							{
								echo "<option value=$r[no_giro]>
								$r[jenis_giro]</option>";
							}
				echo"	</select></div>
					</div>
					<td>
				</tr>

				<tr>
				<td colspan='2'>
				<div id='no_cek' style='display:none;'>
					<div style='float:left;width:20%;padding-left:2%;'>
						<label for='no_cek' class='control-label'>No Cek: </label></div>
					<div style='float:right;width:62%;'>
						<input type='text' name='no_cek' id='no_cek' class='form-control'>
						</div>
					</div>
					<td>
				</tr>

				<tr><div class='form-group'>
					<td><label class='control-label' style='padding:5%;'>Keterangan : </label></td>
					<td><textarea name='keterangan' class='form-control'></textarea></td>
					</div>
				</tr>
				<tr><div class='form-group'>
					<td><label class='control-label' style='padding:5%;'>Nominal : </label></td>
					<td><input type='text' name='nominal' id='nominal' class='form-control'>";
					?>
						<script type='text/javascript'>
							$(document).ready(function(){
								$('#nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
							});
						</script>						
					<?php
				echo"
					</td>
					</div>
				</tr>
				<tr>
					<td><br><input type='submit' value='Input' class='btn btn-primary btn-sm'>&nbsp;
					<input type='reset' value='Reset' class='btn btn-primary btn-sm'></td>
				</tr>
			</table>
		</form>
		";	
?>

    <script type="text/javascript">
        $('#jenis_keluar').change(function(){
            if( $(this).val() == '1'){
                $('#giro').show();
                $('#no_cek').hide();
            }else{
                $('#giro').hide();
                $('#no_cek').show();
            }
        });
 	</script>