<?php
    error_reporting(0);
    switch($_GET[act]){
        default:
        echo "<h3 align=center> LAPORAN ALOKASI </h3>";
?>
	<div class="panel-heading"><p></div>
	<div class="navbar">
		<ul class="nav nav-pills">
			<li class="active"><a href="#tab1" data-toggle="tab">Laporan Pengajuan Alokasi</a></li>
			<li><a href="#tab2" data-toggle="tab">Laporan Utama</a></li>
			<!--<li><a href="#tab3" data-toggle="tab">Laporan Per Alokasi</a></li> -->
		</ul>
	</div>

	<div class="tab-content">
		<div class="tab-pane active" id="tab1">
			<div class="form-group">
				<h4 align=center>Laporan Pengajuan </h4>
			    <table id='alokasi' class='table table-bordered table-hover'>
			    <thead>
			    	<tr>
			        	<th width='5%'><center>No</center></th>
			            <th width='15%'>Nama Alokasi</th>
			            <th width='10%'><center>Quantity</center></th>
			            <th width='15%'><center>Nilai/Harga (Rp)</center></th>
			            <th width='15%'><center>Jumlah Alokasi (Rp)</center></th>
			        </tr>
			    </thead>
			    <tbody>
			        <?php   
			        include '../../config/connectdb.php';

			        $sql = mysqli_query($mysqli,'SELECT * FROM pengajuan_alokasi');
			        $no = 1;
			        while ($r = mysqli_fetch_array($sql)) {
			            $id = $r['no_alokasi'];
			            $jk = $r['jumlah_keluar'];
			            $total = $r['jml_alokasi'] * $r['harga_alokasi'];
			            $sisa=$total-$jk;
			            $totalS+=$total;
			            $totalk+=$jk;
			            $totalsisa+=$sisa;
			        ?>

			        <tr >
			            <td align='center'><?php echo $no;?></td>
			            <td>
			                <?php echo  $r['nama_alokasi']; ?>
			            </td>
						<td align='center'>
			                <?php echo  $r['jml_alokasi']; ?>
			            </td>
			            <td align='right'>
			                <?php 
			                    $rupiah=number_format($r['harga_alokasi'],0,',','.'); 
			                    echo $rupiah; 
			                ?>
			            </td>
			            <td align='right'>
			                <?php
			                    echo number_format($total,0,',','.'); 
			                ?>
			            </td>
			        </tr>
			        <?php
			            $no++;
			        }
			        ?>
			        <tr>
				        <td colspan='4'><b>TOTAL</b></td>
				        <td align="right"><b>
				            <?php 
				                echo number_format($totalS,0,',','.'); 
				            ?></b>
				        </td>
			        </tr>
			        <tr>
			        	<td colspan='5' align='right'><button type="button" class="btn btn-primary" onclick="print_d()" ><span class="glyphicon glyphicon-print"></span> Print</button></td>
			        </tr>			

			    </tbody>
			    </table>  
			</div>
		</div>

		<div class="tab-pane" id="tab2">
			<div class="form-group">
				<h4 align=center>Laporan Utama </h4>
			    <table id='alokasi' class='table table-bordered table-hover'>
			    <thead>
			    	<tr>
			        	<th width='5%'><center>No</center></th>
			            <th width='15%'>Nama Alokasi</th>
			            <th width='10%'><center>Quantity</center></th>
			            <th width='15%'><center>Nilai/Harga (Rp)</center></th>
			            <th width='15%'><center>Jumlah Alokasi (Rp)</center></th>
			            <th width='15%'><center>Jumlah Keluar (Rp)</center></th>
			            <th width='15%'><center>Sisa (Rp)</center></th>
			        </tr>
			    </thead>
			    <tbody>
			        <?php   
			        include '../../config/connectdb.php';

			        $sql = mysqli_query($mysqli,'SELECT * FROM pengajuan_alokasi');
			        $no = 1;
			        while ($r = mysqli_fetch_array($sql)) {
			            $id = $r['no_alokasi'];
			            $jk = $r['jumlah_keluar'];
			            $total = $r['jml_alokasi'] * $r['harga_alokasi'];
			            $sisa=$total-$jk;
			            $totalS+=$total;
			            $totalk+=$jk;
			            $totalsisa+=$sisa;
			        ?>

			        <tr >
			            <td align='center'><?php echo $no;?></td>
			            <td>
			                <?php echo  $r['nama_alokasi']; ?>
			            </td>
						<td align='center'>
			                <?php echo  $r['jml_alokasi']; ?>
			            </td>
			            <td align='right'>
			                <?php 
			                    $rupiah=number_format($r['harga_alokasi'],0,',','.'); 
			                    echo $rupiah; 
			                ?>
			            </td>
			            <td align='right'>
			                <?php
			                    echo number_format($total,0,',','.'); 
			                ?>
			            </td>
			            <td align='right'>
			                <?php 
			                    $rupiah=number_format($r['jumlah_keluar'],0,',','.'); 
			                    echo $rupiah; 
			                ?>
			            </td>
			            <td align='right'>
			                <?php
			                    echo number_format($sisa,0,',','.'); 
			                ?>
			            </td>
			        </tr>
			        <?php
			            $no++;
			        }
			        ?>
			        <tr>
				        <td colspan='4'><b>TOTAL</b></td>
				        <td align="right"><b>
				            <?php 
				                echo number_format($totalS,0,',','.'); 
				            ?></b>
				        </td>
				        <td align="right"><b>
				            <?php 
				                echo number_format($totalk,0,',','.'); 
				            ?></b>
				        </td>
				        <td align="right"><b>
				            <?php 
				                echo number_format($totalsisa,0,',','.'); 
				            ?></b>
				        </td>
			        </tr>
			        <tr>
			        	<td colspan='7' align='right'><button type="button" class="btn btn-primary" onclick="print_b()" ><span class="glyphicon glyphicon-print"></span> Print</button></td>
			        </tr>			

			    </tbody>
			    </table>  
			</div>
		</div>
		
		<div class="tab-pane" id="tab3">
			<div class="form-group">
				<table class='table table-bordered table-hover'>
					<tr align='center'>
						<td>
							<select name='no_alokasi' id='kalokasi' class='form-control' required=''>
			                    <option value='' selected disabled="">- Pilih Jenis Alokasi -</option>
			                    <?php
			                        $getdata="SELECT jenis_alokasi FROM pengajuan_alokasi ";
			                        $tampil=mysqli_query($mysqli,$getdata);
			                        while($r=mysqli_fetch_array($tampil)){
			                            echo "<option value=$r[no_alokasi]>
			                            $r[jenis_alokasi]</option>";
			                        }
			                    ?>
			                </select>
						</td>
					</tr>
				</table>
			</div>
			<div id="Lalokasi"></div>
		</div>

	</div>
	 
	<?php } ?>
	
<script>
    $(document).on('change','#kalokasi',function(){
            var val = $(this).val();
			$.ajax({

                url: '../action/adm/Laporanper_alokasi.php',
                data: {no_alokasi:val},
                type: 'GET',
                dataType: 'html',
                success: function(result){
                    $('#Lalokasi').html();  
                    $('#Lalokasi').html(result); 
                }
            });
    });
</script>

<script>
    function print_d(){
        window.open("../action/adm/printPengajuan.php","_blank");
    }
</script>
<script>
    function print_b(){
        window.open("../action/adm/printLaporan_utama.php","_blank");
    }
</script>