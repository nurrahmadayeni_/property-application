<link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../../assets/css/dataTables.bootstrap.css"/>

<script>
        	 $(document).on('change','#tipe_mtr',function(){
		             var val = $(this).val();
		             $.ajax({
		                   url: '../action/adm/show_material.php',
		                   data: {tipe_mtr:val},
		                   type: 'GET',
		                   dataType: 'html',
		                   success: function(result){
		                        $('#nama_mtr').html();  
		                        $('#nama_mtr').html(result); 
		                   }
		              });
		       });
</script>


<?php
	error_reporting(0);

	$dth = mysqli_query($mysqli,"SELECT tgl_terdaftar FROM temp_order");
	$dtglh = mysqli_fetch_array($dth);
	$tglh = $dtglh['tgl_terdaftar'];
	
	$idsup = mysqli_query($mysqli,"SELECT id_supplier FROM temp_order");
	$sup= mysqli_fetch_array($idsup);
	$idsupplier = $sup['id_supplier'];
	
	echo"
		<form method='post' action='../action/adm/aksi_addneworder.php?mod=supplier&act=tambahneworder'>
			<h3 align=center>Form Order Material</h3>
			<table>
			<tr>
			";
			$query3 = mysqli_query($mysqli,"SELECT tgl_order FROM temp_order");
			$ort= mysqli_fetch_array($query3);
			$tgl=$ort['tgl_order'];
			$data3 = mysqli_num_rows($query3);
			if($data3==0){
				echo"<td><label class='control-label'>Tanggal Order : </label></td>
				<td><input type='date' name='tgl_order' required='' class='form-control'></td>";
			}
			else{
				echo"<td><label class='control-label'>Tanggal Order :&nbsp </label></td>
				<td>".$tgl."</td>";
			}
			$data4 = mysqli_num_rows($dth);
			if($data4!=0){
				echo"<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td>
				
				<td><label class='control-label'>Tanggal Hari Ini :&nbsp </label></td>
				<td>".$tglh."</td>
				";
			}
			else{
				echo"<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td>
				<td></td><td>
				</td>";
			}
			echo"</tr><br><br>
			<br>
			<tr>
				<td><label class='control-label'>Nama Supplier : </label> </td>";
				$data5 = mysqli_num_rows($idsup);
				if($data5!=0){	
					echo"<td><input type='text' name='nama_supplier' class='form-control' value='";
						echo $idsupplier;
						echo"' disabled='disabled'>
						<input type='hidden' class='form-control' name='nama_supplier' value='";
						echo $idsupplier;
					echo"'></td>";
				}else{
					echo"
					<td><select name='nama_supplier' class='form-control' id='nama_supplier' required=''>
					<option value='' selected>- Pilih Supplier -</option>";
					$getdata="SELECT s.id_supplier,s.nama_perusahaan FROM supplier s left join bon b on s.id_supplier=b.id_supplier where b.id_supplier IS NULL";
					$tampil=mysqli_query($mysqli,$getdata);
					while($r=mysqli_fetch_array($tampil))
					{
						echo "<option value=$r[id_supplier]>
						$r[nama_perusahaan]</option>";
					}
					echo "</select>
					</td>";
				}
				echo"
				<td>&nbsp</td>
				<td><label class='control-label'>Pilih Gudang: </label></td>
				<td><select name='gudang' id='gudang' class='form-control' required=''>
					<option value='' selected>- Pilih Gudang -</option>";
					$getdata="SELECT * FROM data_gudang";
					$tampil=mysqli_query($mysqli,$getdata);
					while($r=mysqli_fetch_array($tampil))
					{
						echo "<option value=$r[id_gudang]>
						$r[nama_gudang]</option>";
					}
					echo "</select>
				</td>
			</tr>
			<tr>
				<td><label class='control-label'>Tipe Material : </label></td>
				<td><select name='tipe_mtr' id='tipe_mtr' class='form-control' required=''>
					<option value='' selected>- Pilih Kategori -</option>";
					$getkategori="SELECT * FROM kategori_material";
				    $tampil=mysqli_query($mysqli,$getkategori);
					while($r=mysqli_fetch_assoc($tampil))
					{
						echo "<option value=$r[id_kategorimaterial]>
						$r[kategori_material]</option>";
					}
					echo "</select>
				</td>
			</tr>
			<tr>
				<td><label class='control-label'>Nama Material : </label></td>
				<td><select name='nama_mtr' id='nama_mtr' class='form-control' required=''>
					<option value='' selected>- Pilih Nama Material -</option>";
					echo "</select>
				</td>
			</tr>
			<tr><div class='form-group'>
				<td><label class='control-label'>Jumlah Order : </label></td>
				<td><input type='text' name='jlh_order' class='form-control'></td>
				</div>
			</tr>
			<tr><div class='form-group'>
				<td><label class='control-label'>Harga Material : </label></td>
				<td><input type='text' name='harga' id='harga' class='form-control'>
					";?>
					<script type='text/javascript'>
						$(document).ready(function(){
							$('#harga').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
						});
					</script>
				<?php
				echo"
				</td>
				</div>
			</tr>
			<tr>
				<td><br><input type='submit' value='save' class='btn btn-primary'></td>
				<td><br><input type='reset' value='reset' class='btn btn-primary'></td>
			</tr>
		</table></form>
		";	

	echo "<br><br><table class='table table-bordered table-hover'>";
			echo "<tr bgcolor=#cfcfcf align='center'>";
				echo "<td>No</td>";
				echo "<td>Tanggal order</td>";
				echo "<td>Nama Material</td>";
				echo "<td>Jumlah Barang</td>";
				echo "<td>Harga (Rp)</td>";
				echo "<td>Total Harga (Rp)</td>";
				echo "<td>order ke gudang</td>";
				echo "<td>Aksi</td>";
			echo "</tr>";
			$total=0;
			$no=1;
			
			$queryid = mysqli_query($mysqli,"SELECT max(id_order) AS idMAX FROM order_material");
			$data = mysqli_fetch_array($queryid);
			$idorder = $data['idMAX'];

			$noUrut = (int) substr($idorder, 4, 5);
			$noUrut++;

			$chartetap = 'ORD-';
			$id2 = $chartetap.sprintf("%05s", $noUrut);
			
			$tampil = "select o.id_order as id_order,d.id_material as id_material,o.tgl_order,d.id_detailorder as id_detailorder,m.nama_material as nama_material,d.jumlah_brg as jumlah_brg,d.harga_material as harga_material,g.nama_gudang as nama_gudang from temp_order o,temp_detailorder d,material m,data_gudang g where g.id_gudang=d.id_gudang and m.id_material=d.id_material and d.id_order=o.id_order and o.id_order='$id2'";
			$queri=mysqli_query($mysqli,$tampil);
			while($row = mysqli_fetch_array($queri)){
			$total=$row[harga_material] * $row[jumlah_brg];
			$rupiah=number_format($total,0,',','.'); 
			$rupiah2=number_format($row['harga_material'],0,',','.'); 
            
			echo "<tr align='center'>
				<td>$no</td>
				<td>$row[tgl_order]</td>
				<td>$row[nama_material]</td>
				<td>$row[jumlah_brg]</td>
				<td align='right'>$rupiah2</td>
				<td align='right>$rupiah</td>
				<td>$row[nama_gudang]</td>
				<td><a href=../action/adm/aksi_batalorder.php?mod=supplier&act=hapusneworder&idd=$row[id_detailorder]>
					<button class='btn btn-primary'>hapus</button></a></td>
			</tr>";
			$no++;
			}
		echo "
		</table>";
		echo"
		<table>
		<tr>
		<form action='../action/adm/aksi_addneworder.php?mod=supplier&act=finishtambahneworder' method='post'>
		<td>
		<input name='order' type='submit' value='Order' class='btn btn-primary'>
		</td></form>
		<td>&nbsp &nbsp</td>
		<form action='../action/adm/aksi_batalorder.php?mod=supplier&act=batalneworder' method='post'>
		<td><input name='batal' type='submit' value='Batal Order' class='btn btn-primary'>
		</td></form>
		<td>&nbsp &nbsp</td>
		<form action='index.php?mod=supplier' method='post'>
		<td><input name='back' type='submit' value='Back' class='btn btn-primary'>
		</td></form>
		</tr>
		</table></form>";
		
		?>
		
        <script src="../../assets/js/jquery-1.11.0.js"></script>
        <script src="../../assets/js/bootstrap.min.js"></script>
        <script src="../../assets/datatables/jquery.dataTables.js"></script>
        <script src="../../assets/datatables/dataTables.bootstrap.js"></script>
        
		<script type="text/javascript">
            $(function() {
                $("#order").dataTable();
            });
        </script>