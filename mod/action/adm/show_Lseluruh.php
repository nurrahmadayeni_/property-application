<?php   
	include '../../../config/connectdb.php';
	error_reporting(0);
	
	echo "<p><h3 align=center> Laporan Gudang Keseluruhan</h3>
		<p><h5 align=center> Dari Tanggal <font color='red'><b>".$_GET['tgl_1']."</b></font> sampai <font color='red'><b>".$_GET['tgl_2']."</b></font> </h5> 		
    <table id='DLseluruh' class='table table-bordered table-hover'>
        <thead>
            <tr>
                <th width='5%'>No</th>
                <th width='15%'>Tanggal Masuk</th>
                <th width='14%'>Gudang</th>
                <th width='15%'>Material</th>
                <th width='15%'>Jumlah Material</th>
                <th width='18%'>Harga Material (Rp)</th>
                <th width='18%'>Total Harga (Rp)</th>
            </tr>
        </thead>
        <tbody>
        ";

    $a=$_GET['tgl_1'];
	$b=$_GET['tgl_2'];
	
	$sql = mysqli_query($mysqli,"SELECT t.tgl_masuk as tanggal, l.id_laporangdg as id_laporangdg,l.jumlah as jumlah,m.nama_material,m.harga_material,g.nama_gudang 
								FROM material_masuk t, material_gudang l,material m,data_gudang g 
								WHERE l.id_gudang=g.id_gudang 
								AND m.id_material=l.id_material 
								AND t.id_gudang=g.id_gudang
								AND m.id_material=t.id_material
								AND t.tgl_masuk between '$a' AND '$b'
								ORDER BY t.tgl_masuk ASC");
	$no = 1;
	while ($r = mysqli_fetch_array($sql)) {
		$tgl_masuk= $r['tanggal'];
		$gdg= $r['nama_gudang'];
		$mat= $r['nama_material'];
		$hrg= $r['harga_material'];
		$jl= $r['jumlah'];
		$idg = $r['id_gudang'];

		$total = $jl * $hrg;
		$totalseluruh+=$total;
 	?>
			<tr align='left'>
				
				<td align="center"> <?php echo  $no;?> </td>
				<td><?php echo $tgl_masuk; ?></td>		
				<td><?php echo $gdg; ?></td>
				<td><?php echo $mat;  ?></td>
				<td align='center'><?php echo $jl; ?></td>
				<td align='right'>
					<?php $rupiah=number_format($hrg,0,',','.'); 
                    	echo $rupiah; 
                    ?>
                </td>
				<td align='right'>
					<?php
	                    echo number_format($total,0,',','.'); 
                	?>
                </td>		
			</tr>
		   
	<?php
		$no++;
	} 
	?>
			<tr>
				<td colspan='6'><b>Total Harga Keseluruhan (Rp)</b></td>
				<td align="right"><b><?php echo number_format($totalseluruh, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan);?></b></td>
			</tr>	
			<tr align="right">
				<td colspan='7'><button type="button" class="btn btn-primary" onclick="print_d()" >
				<span class="glyphicon glyphicon-print"></span> Print</button></td>
			</tr>
	</tbody>
	</table>
	

<script>
	function print_d(){
		<?php
			$tgl1=  $_GET['tgl_1'];
			$tgl2 = $_GET['tgl_2'];
		
		echo "window.open('../action/adm/printlaporan1.php?id=$tgl1&id2=$tgl2','_blank');";
		?>
	}
</script>