<html>
<head>
	<title>Print Document</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body><br>
	<img src="../../../images/logo.png" align="right"><br>
	<b style="font-size:17px;">Bukti Order</b><br>
	<b style="font-size:12px;">ID Order : <font color="red"> <?php echo $_GET["id"] ;?> </font></b><hr>
	<table border="1" width="100%" style="border-collapse:collapse;" align="center">
    	<tr class="tableheader"> 
			<th width="15%">Tanggal Pemesanan</th>
			<th width="15%">Tanggal Order</th>
			<th width="12%">Gudang</th>
            <th width="15%">Nama Material</th>
			<th width="12%">Jumlah Material</th>
			<th width="15%">Harga Material (Rp)</th>
			<th width="21%">Total Harga (Rp)</th>
        </tr>
     </thead>
            <tbody>
				<?php
                    error_reporting(0);
					include '../../../config/connectdb.php';
					$id = $_GET["id"];
                    //Data mentah yang ditampilkan ke tabel 
					$jumlah_desimal ="2";
					$pemisah_desimal =",";
					$pemisah_ribuan =".";
					$total=0;
					
                    $sql = mysqli_query($mysqli,"SELECT o.tgl_terdaftar,o.id_order as id_order,o.tgl_order,o.status_pembayaran as status_pembayaran,d.jumlah_brg as jumlah_brg,m.nama_material,g.nama_gudang,d.harga_material FROM detail_order d,material m,order_material o,data_gudang g where o.id_order=d.id_order and m.id_material=d.id_material and g.id_gudang=d.id_gudang and o.id_order='$id' ");
                    while ($r = mysqli_fetch_array($sql)) {
                    $id = $r['id_order'];
					$tgl_kini= $r['tgl_terdaftar'];
					$tgl_order= $r['tgl_order'];
					$gdg= $r['nama_gudang'];
					$mat= $r['nama_material'];
					$jl= $r['jumlah_brg'];
					$hhr=$r['harga_material'];
					
					$totalmat=$jl*$hhr;
					$total=$total+$totalmat;
					echo"
                    <tr align='left' style='font-size:15px;'>
						<td>$tgl_kini</td>
						<td>$tgl_order</td>
                        <td>$gdg</td>
						<td>$mat</td>
                        <td align='center'>$jl</td>
						<td align='right'>";
							$rupiah=number_format($hhr,0,',','.'); 
			                echo "$rupiah
						</td>
						<td align='right'>".number_format($totalmat, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan)."</td>
                    </tr>";
					}
					echo"
					<tr >
					<td colspan='6'>Total Order</td>
					<td align='right' style='font-size:15px;'>".number_format($total, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan)."</td>
					</tr>";

                    ?>
                </tbody>
            </table>
		
	<script>
		window.load = print_d();
		function print_d(){
			window.print();
		}
	</script>