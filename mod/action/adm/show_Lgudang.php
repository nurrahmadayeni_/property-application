<?php   
	include '../../../config/connectdb.php';
	error_reporting(0);
	
	echo "<p><h3 align=center> Laporan Gudang</h3>
    <table id='Dgudang' class='table table-bordered table-hover'>
        <thead>
            <tr>
                <th width='5%'>No</th>
                <th width='15%'>Tanggal Masuk</th>
                <th width='14%'>Gudang</th>
                <th width='15%'>Material</th>
                <th width='15%'>Jumlah Material</th>
                <th width='18%'>Harga Material (Rp)</th>
                <th width='18%'>Total Harga (Rp)</th>
            </tr>
        </thead>
        <tbody>
        ";
	
 	$id = $_GET['laporan_gudang'];
	$sql = mysqli_query($mysqli,"SELECT t.tgl_masuk as tanggal, l.id_laporangdg as id_laporangdg,l.jumlah as jumlah,m.nama_material,m.harga_material,g.nama_gudang 
								FROM material_masuk t, material_gudang l,material m,data_gudang g 
								WHERE l.id_gudang=g.id_gudang 
								AND m.id_material=l.id_material 
								AND t.id_gudang=g.id_gudang
								AND m.id_material=t.id_material
								AND g.id_gudang='$id' ORDER BY m.nama_material");
	$no = 1;
	while ($r = mysqli_fetch_array($sql)) {
		$gd= $r['nama_gudang'];
		$mat= $r['nama_material'];
		$hrg= $r['harga_material'];
		$jl= $r['jumlah'];
		$tgl= $r['tanggal'];
		$id = $r['id_laporangdg'];

		$total = $jl * $hrg;
		$totalseluruh+=$total;
 	?>
			<tr align='left'>
				
				<td align="center"> <?php echo  $no;?> </td>
				<td><?php echo $tgl; ?></td>		
				<td><?php echo $gd; ?></td>
				<td><?php echo $mat;  ?></td>
				<td align='center'><?php echo $jl; ?></td>
				<td align='right'>
					<?php $rupiah=number_format($hrg,0,',','.'); 
                    	echo $rupiah; 
                    ?>
                </td>
				<td align='right'>
					<?php
	                    echo number_format($total,0,',','.'); 
                	?>
                </td>		
			</tr>
		   
	<?php
		$no++;
	} 
	?>
			<tr>
				<td colspan='6'><b>Total Harga Keseluruhan (Rp)</b></td>
				<td align="right"><b><?php echo number_format($totalseluruh, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan);?></b></td>
			</tr>		
			<tr align="right">
				<td colspan='7'><button type="button" class="btn btn-primary" onclick="print_d()" >
				<span class="glyphicon glyphicon-print"></span> Print</button></td>
			</tr>

        </tbody>
    </table>  
	

<script>
	function print_d(){
		<?php
			$id=  $_GET['laporan_gudang'];
		
		echo "window.open('../action/adm/printlaporan2.php?id=$id','_blank');";
		?>
	}
</script>