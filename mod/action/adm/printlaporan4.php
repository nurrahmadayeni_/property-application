<html>
<head>
	<title>Print Laporan</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="../../assets/css/bootstrap-theme.min.css">
	<link href="../../../images/pavicon.png" rel="icon" type="image/x-icon" />
</head>
<?php
include '../../../config/connectdb.php';
error_reporting(0);

$sql1 = mysqli_query($mysqli,"SELECT * FROM kategori_material WHERE id_kategorimaterial='$_GET[id]'");
		while($r3=mysqli_fetch_array($sql1)){
		$nm= $r3['kategori_material'];
	}
	$sql2 = mysqli_query($mysqli,"SELECT * FROM data_gudang WHERE id_gudang='$_GET[id2]'");
		while($r2=mysqli_fetch_array($sql2)){
		$ng= $r2['nama_gudang'];
	}

$prev_day = 7;
$preV_hari = mktime(0,0,0,date("m"),date("d") -$prev_day,date("Y"));
$tgl=date('Y-m-d',$preV_hari);
 
echo "
<body><br><br>
	<img src='../../../images/logo.png' align='right' width='27%' height='5%'><br>
	<font size='4%'>Laporan Gudang Periodik</font><br>
	<font size='2%'>Tipe Material : <font color='red'><b>".$nm."</b></font><br>
			Gudang : <font color='red'><b>".$ng."</b></font><br>
			Tanggal : <font color='red'><b>".$tgl." </b></font> 
			s/d <font color='red'><b>".$_GET['id3']." </b></font>
	</font><hr>";
 ?>
 	<table border="1" width="100%" style="border-collapse:collapse;" align="center">
    <thead>
        <tr style="font-size:14px;">
			<th>Material</th>
			<th>Satuan</th>
			<th>Harga (Rp)</th>
			<th>Tgl Masuk</th>
			<th>Jml Masuk</th>
			<th>Tgl Keluar</th>
			<th>Jml Keluar</th>
			<th>Tgl Retur</th>
			<th>Jml Retur</th>
			<th>Total Stok</th>
			<th>Total Harga Masuk (Rp)</th>
        </tr>
    </thead>
        <tbody>
		<?php
			include '../../../config/connectdb.php';
            error_reporting(0);
            
			$a=$_GET['id'];
			$b=$_GET['id2'];
			$c=$_GET['id3'];

			$prev_day = 7;
			$preV_hari = mktime(0,0,0,date("m"),date("d") -$prev_day,date("Y"));
			$tgl=date('Y-m-d',$preV_hari);
				
			$sql = mysqli_query($mysqli,"SELECT rt.id_perumahan, rt.no_kavling,t.kategori_material,m.nama_material,m.harga_material, m.satuan_material, g.nama_gudang, mg.jumlah AS total_Stok, mm.jumlah_masuk AS jlh_masuk, mm.tgl_masuk, 
				IF((
				SELECT dk.jumlah_keluar
				FROM material_gudang mg
				INNER JOIN data_detailkeluar dk ON dk.id_gudang = mg.id_gudang
				AND dk.id_material = mg.id_material
				AND dk.id_gudang IS NULL 
				AND dk.id_material IS NULL
				), 0, dk.jumlah_keluar) AS jlh_keluar,dk.tgl_pengeluaran,
				IF((
				SELECT rt.jumlah_kembali
				FROM material_gudang mg
				INNER JOIN retur_detail rt ON rt.id_gudang = mg.id_gudang
				AND rt.id_material = mg.id_material
				AND rt.id_gudang IS NULL 
				AND rt.id_material IS NULL
				), 0, rt.jumlah_kembali) AS jlh_retur,rt.tgl_retur
				FROM material_gudang mg
				INNER JOIN material_masuk mm ON mm.id_gudang = mg.id_gudang
				AND mm.id_material = mg.id_material
				AND mm.tgl_masuk between '$tgl' AND '$c' 
				INNER JOIN material m ON mg.id_material = m.id_material
				INNER JOIN data_gudang g ON mg.id_gudang = g.id_gudang
				AND g.id_gudang='$b'
				LEFT JOIN data_detailkeluar dk ON dk.id_material = mg.id_material
				AND dk.id_gudang = mg.id_gudang
				LEFT JOIN retur_detail rt ON rt.id_material = mg.id_material
				AND rt.id_gudang = mg.id_gudang
				LEFT JOIN kategori_material t ON mg.id_material=m.id_material
				WHERE m.id_kategorimaterial=t.id_kategorimaterial
				AND t.id_kategorimaterial='$a'
				ORDER BY t.id_kategorimaterial");

			while ($r = mysqli_fetch_array($sql)) {
				$hrg=$r['harga_material'];
				$jk=$r['jlh_masuk'];
				$total = $jk * $hrg;
				$totalseluruh+=$total;

			if($r['tgl_pengeluaran']==NULL && $r['tgl_retur']==NULL){
			echo"
			<tr align='center' style='font-size:13px;'>
                <td>$r[nama_material]</td>
				<td>$r[satuan_material]</td>
				<td align='right'>";
					$rupiah=number_format($hrg,0,',','.'); 
			        echo $rupiah; 
			echo"</td>
				<td>$r[tgl_masuk]</td>
				<td>$r[jlh_masuk]</td>
				<td> - </td>
				<td> - </td>
				<td> - </td>
				<td> - </td>
				<td>$r[total_Stok]</td>
				<td align='right'>";
				    echo number_format($total,0,',','.');
			echo"</td>
			</tr>";
			}
			else{
			echo"
            <tr align='center' style='font-size:13px;'>
                <td>$r[nama_material]</td>
				<td>$r[satuan_material]</td>
				<td align='right'>";
					$rupiah=number_format($hrg,0,',','.'); 
			        echo $rupiah; 
			echo"</td>
				<td>$r[tgl_masuk]</td>
				<td>$r[jlh_masuk]</td>
				<td>$r[tgl_pengeluaran]</td>
				<td>$r[jlh_keluar]</td>
				<td>$r[tgl_retur]</td>
				<td>$r[jlh_retur]</td>
				<td>$r[total_Stok]</td>
				<td align='right'>";
				    echo number_format($total,0,',','.');
			echo"</td>
			</tr>";
			}
            }
        ?>
        	<tr>
				<td colspan='10'><b>Total Harga Keseluruhan (Rp)</b></td>
				<td align="right"><b><?php echo number_format($totalseluruh, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan);?></b></td>
			</tr>
        </tbody>
    </table>

</body>
</html>
		
	<script>
		window.load = print_d();
		function print_d(){
			window.print();
		}
	</script>