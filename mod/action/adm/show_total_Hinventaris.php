<?php   
	include '../../../config/connectdb.php';

	echo "
            <table id='inventaris' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Merk/Type</th>
                    <th>Warna</th>
                    <th>Thn Perolehan</th>
                    <th>Jlh</th>
                    <th>Kondisi</th>
                    <th>Harga (Rp)</th>
                    <th>Total (Rp)</th>
                    <th>PTJ</th>
                    <th>Lokasi</th>
                    <th>Foto</th>
                </tr>
            </thead>
            <tbody width='100%'>
        ";

 	$id = $_GET['kategori_inventaris'];
 	
 	$get_inv = "SELECT *FROM INVENTARIS WHERE id_kategori=$id";

	$tampil=mysqli_query($mysqli,$get_inv);

	$no = 1;
	while ($r = mysqli_fetch_array($tampil)) {
            $id = $r['id_inventaris'];
            $idk = $r['id_kategori'];
?>
        <tr align='left' style="font-size:15px;">
            <td align="center"> <?php echo  $no;?> </td>
            <td> <?php echo  $r['nama_barang']; ?> </td>
            <td> <?php echo  $r['merk']; ?> </td>
            <td> <?php echo  $r['warna']; ?> </td>
            <td> <?php echo  $r['thn_peroleh']; ?> </td>
            <td> <?php echo  $r['jumlah']; ?> </td>
            <td> <?php echo  $r['kondisi']; ?></td>
            <td align='right'> 
                <?php 
                    $rupiah=number_format($r['harga_barang'],0,',','.'); 
                    echo $rupiah; 
                ?>
            </td>
            <td align='right'>
                <?php
                    $jumlah = $r['jumlah'];
                    $hrg = $r['harga_barang'];
                    $total = $jumlah * $hrg;
                    echo number_format($total,0,',','.'); 
                ?>
            </td>
            <td> <?php echo  $r['ptj']; ?> </td>
            <td> <?php echo  $r['lokasi']; ?> </td>
            <td> <img src="../mod_adm/gbr_inventaris/<?php echo  $r['foto']; ?>" width='100%'> </td>
        </tr>
		<?php
       		$no++;
		}
            
        ?>
        <tr>
	        <td></td>
	        <td colspan="7" align="center"> <b> <h4>TOTAL (Rp)</h4> </b> </td>
	        <td colspan="4" style="font-size:17px;">
	        	<?php
	        	
					$sql_total = "SELECT SUM(jumlah * harga_barang) as total FROM inventaris where id_kategori=$_GET[kategori_inventaris]";
					$qur = $mysqli->query($sql_total);
					$r3 = mysqli_fetch_assoc($qur);

					$total3 = $r3['total'];
					        		
					echo "<b>".number_format($total3,0,',','.')."</b>";  		
	        	?>
	        </td>
        </tr>

        </tbody>
    </table>  
	<script type="text/javascript">
	        $(function() {
	            $("#inventaris").dataTable();
	        });
	</script>
 