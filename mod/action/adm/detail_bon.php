<?php
	error_reporting(0);
	include '../../../config/connectdb.php';

	$id = $_POST["id"]; //escape the string if you like
	$jumlah_desimal ="2";
	$pemisah_desimal =",";
	$pemisah_ribuan =".";
	$total=0;
	$tampil = "select id_order from order_material where id_order='$id'";
	$queri = mysqli_query($mysqli,$tampil);
	$row = mysqli_fetch_array($queri);
	$idor= $row['id_order'];
	echo "
	ID Order	: <b>".$idor."</b>
	<table class='table table-bordered table-hover'>
       <thead>
           <tr align='center'>
				<th width='15%'>Tanggal Pemesanan</th>
				<th width='15%'>Tanggal Order</th>
				<th width='12%'>Gudang</th>
				<th width='15%'>Nama Material</th>
				<th width='12%'>Jumlah Material</th>
				<th width='15%'>Harga Material (Rp)</th>
				<th width='21%'>Total Harga (Rp)</th>

           </tr>
       </thead>
   
        ";
	
	$sql = mysqli_query($mysqli,"SELECT o.tgl_terdaftar,o.id_order as id_order,o.tgl_order,o.status_pembayaran as status_pembayaran,d.jumlah_brg as jumlah_brg,m.nama_material,g.nama_gudang,d.harga_material FROM detail_order d,material m,order_material o,data_gudang g where o.id_order=d.id_order and m.id_material=d.id_material and g.id_gudang=d.id_gudang and o.id_order='$id' ");
    
	while($q = mysqli_fetch_array($sql)){
		$totalmat=$q['jumlah_brg']*$q['harga_material'];
		echo "
				<tr align='left'>
					<td> <b>".$q['tgl_terdaftar']." </b> </td>
					<td> <b>".$q['tgl_order']."</b> </td>
					<td> <b>".$q['nama_gudang']." </b> </td>
					<td> <b>".$q['nama_material']."</b> </td>
					<td> <b>".$q['jumlah_brg']."</b> </td>
					<td align='right'> <b>".$rupiah=number_format($q['harga_material'],0,',','.')."</b> </td>
					<td align='right'> <b>".number_format($totalmat, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan)."</b> </td>
				</tr>
		";
	}
	echo"</table>";

?>
<div class="modal-footer">
	<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
	<button type="button" class="btn btn-primary" onclick="print_d()"><span class="glyphicon glyphicon-print"></span> Print</button></a>
</div>
<script>
	function print_d(){
		window.open("../action/adm/print.php?id=<?php echo $id;?>","_blank");
	}
</script>

