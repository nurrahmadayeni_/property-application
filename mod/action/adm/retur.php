<link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../../assets/css/dataTables.bootstrap.css"/>
<script>
        	 $(document).on('change','#tipe_mtr',function(){
		             var val = $(this).val();
		             $.ajax({
		                   url: '../action/adm/show_material.php',
		                   data: {tipe_mtr:val},
		                   type: 'GET',
		                   dataType: 'html',
		                   success: function(result){
		                        $('#nama_mtr').html();  
		                        $('#nama_mtr').html(result); 
		                   }
		              });
		       });
</script>

<script>
        	 $(document).on('change','#tipe_rmh',function(){
		             var val = $(this).val();
		             $.ajax({
		                   url: '../action/adm/show_perumahan.php',
		                   data: {tipe_rmh:val},
		                   type: 'GET',
		                   dataType: 'html',
		                   success: function(result){
		                        $('#nama_prm').html();  
		                        $('#nama_prm').html(result); 
		                   }
		              });
		       });
</script>


<?php
	error_reporting(0);

	$dth = mysqli_query($mysqli,"SELECT tgl_sekarang FROM temp_retur");
	$dtglh = mysqli_fetch_array($dth);
	$tglh = $dtglh['tgl_sekarang'];
	
	echo"
		<form method='post' action='../action/adm/aksi_addretur.php?mod=gudang&act=tambahnewretur'>
			<h3 align=center>Form Laporan Retur Barang</h3>
			<table>
			<tr>
				<td><label class='control-label'>Tanggal Kembali : </label></td>
				<td><input type='date' name='tgl_pengeluaran' required='' class='form-control'></td>";
				
				$data4 = mysqli_num_rows($dth);
				if($data4!=0){
					echo"<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td>
					<td><label class='control-label'>Tanggal Hari Ini :&nbsp </label></td>
					<td>".$tglh."</td>
					";
				}
				else{
					echo"<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td>
					<td></td><td>
					</td>";
				}
				echo"</tr><br><br>
				<br>
				<tr>
				<td><label class='control-label'>Pilih Gudang: </label></td>
				<td><select name='gudang' id='gudang' class='form-control' required=''>
					<option value='' selected>- Pilih Gudang -</option>";
					$getdata="SELECT * FROM data_gudang";
					$tampil=mysqli_query($mysqli,$getdata);
					while($r=mysqli_fetch_array($tampil))
					{
						echo "<option value=$r[id_gudang]>
						$r[nama_gudang]</option>";
					}
					echo "</select>
				</td>
				</tr>
				<tr>
				<td><label class='control-label'>Tipe Material : </label></td>
				<td><select name='tipe_mtr' id='tipe_mtr' class='form-control' required=''>
					<option value='' selected>- Pilih Kategori -</option>";
					$getkategori="SELECT * FROM kategori_material";
				    $tampil=mysqli_query($mysqli,$getkategori);
					while($r=mysqli_fetch_assoc($tampil))
					{
						echo "<option value=$r[id_kategorimaterial]>
						$r[kategori_material]</option>";
					}
					echo "</select>
				</td>
				</tr>
				<tr>
				<td><label class='control-label'>Nama Material : </label></td>
				<td><select name='nama_mtr' id='nama_mtr' class='form-control' required=''>
					<option value='' selected>- Pilih Nama Material -</option>";
					echo "</select>
				</td>
			</tr>
			
			<tr>
				<td><label class='control-label'>Pilih Perumahan : </label></td>
				<td><select name='tipe_rmh' id='tipe_rmh' class='form-control' required=''>
					<option value='' selected>- Pilih Perumahan -</option>";
					$getkategori="SELECT * FROM data_perumahan";
				    $tampil=mysqli_query($mysqli,$getkategori);
					while($r=mysqli_fetch_assoc($tampil))
					{
						echo "<option value=$r[id_perumahan]>
						$r[nama_perumahan]</option>";
					}
					echo "</select>
				</td>
				</tr>
				<tr>
				<td><label class='control-label'>Pilih Kavling : </label></td>
				<td><select name='nama_prm' id='nama_prm' class='form-control' required=''>
					<option value='' selected>- Pilih Kavling -</option>";
					echo "</select>
				</td>
			</tr>
			
			<tr><div class='form-group'>
				<td><label class='control-label'>Jumlah Kembali : </label></td>
				<td><input type='text' name='jlh_kluar' class='form-control'></td>
				</div>
			</tr>
			<tr>
				<td><br><input type='submit' value='save' class='btn btn-primary'></td>
				<td><br><input type='reset' value='reset' class='btn btn-primary'></td>
			</tr>
		</table></form>
		";	

	echo "<br><br><table class='table table-bordered table-hover'>";
			echo "<tr bgcolor=#cfcfcf>";
				echo "<td>No</td>";
				echo "<td>Tanggal Kembali</td>";
				echo "<td>ID Material</td>";
				echo "<td>Nama Material</td>";
				echo "<td>Jumlah Material</td>";
				echo "<td>Perumahan</td>";
				echo "<td>Kavling</td>";
				echo "<td>Gudang</td>";
				echo "<td>Aksi</td>";
			echo "</tr>";
			$total=0;
			$no=1;
			
			$queryid = mysqli_query($mysqli,"SELECT max(id_retur) AS idMAX FROM retur_barang");
			$data = mysqli_fetch_array($queryid);
			$idkeluar = $data['idMAX'];
			$idkeluar=$idkeluar+1;
			
			$tampil = "select td.id_detailretur,t.id_retur,m.id_material,m.nama_material,td.jumlah_kembali,p.nama_perumahan,k.no_kavling,td.tgl_retur,g.nama_gudang from material m,temp_retur t,temp_returdetail td,data_perumahan p,data_kavling k,data_gudang g where t.id_retur=td.id_retur and td.id_material=m.id_material and td.id_gudang=g.id_gudang and td.id_perumahan=p.id_perumahan and td.no_kavling=k.no_kavling and k.id_perumahan=p.id_perumahan and td.id_retur='$idkeluar' group by td.id_detailretur  ";
			$queri=mysqli_query($mysqli,$tampil);
			while($row = mysqli_fetch_array($queri)){
			$total=$total+($row[harga_material] * $row[jumlah_kembali]);
			echo "<tr align='left'>
				<td>$no</td>
				<td>$row[tgl_retur]</td>
				<td>$row[id_material]</td>
				<td>$row[nama_material]</td>
				<td>$row[jumlah_kembali]</td>
				<td>$row[nama_perumahan]</td>
				<td>$row[no_kavling]</td>
				<td>$row[nama_gudang]</td>
				<td><a href=../action/adm/aksi_batalorder.php?mod=gudang&act=hapusdetailretur&idd=$row[id_detailretur]&ido=$row[id_retur]>
					<button class='btn btn-primary'>hapus</button></a></td>
			</tr>";
			$no++;
			}
		echo "
		</table>";
		echo"
		<table>
		<tr>
		<form action='../action/adm/aksi_addretur.php?mod=gudang&act=finishtambahnewretur' method='post'>
		<td>
		<input name='order' type='submit' value='Laporkan' class='btn btn-primary'>
		</td></form>
		<td>&nbsp &nbsp</td>
		<form action='../action/adm/aksi_batalorder.php?mod=gudang&act=batalnewretur' method='post'>
		<td><input name='batal' type='submit' value='Batal Laporkan' class='btn btn-primary'>
		</td></form>
		<td>&nbsp &nbsp</td>
		<form action='index.php?mod=data_retur' method='post'>
		<td><input name='back' type='submit' value='Back' class='btn btn-primary'>
		</td></form>
		</tr>
		</table></form>";
		
		?>
		
        <script src="../../assets/js/jquery-1.11.0.js"></script>
        <script src="../../assets/js/bootstrap.min.js"></script>
        <script src="../../assets/datatables/jquery.dataTables.js"></script>
        <script src="../../assets/datatables/dataTables.bootstrap.js"></script>
        
		<script type="text/javascript">
            $(function() {
                $("#order").dataTable();
            });
        </script>