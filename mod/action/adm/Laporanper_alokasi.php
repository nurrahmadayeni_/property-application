<?php   
	include '../../../config/connectdb.php';
	error_reporting(0);
	
	echo "<p><h4 align=center> Laporan Per Alokasi</h4>
    <table id='alokasi' class='table table-bordered table-hover'>
        <thead>
            <tr>
               	<th width='5%'><center>No</center></th>
			    <th width='15%'>Nama Alokasi</th>
			    <th width='10%'><center>Quantity</center></th>
			    <th width='15%'><center>Nilai/Harga (Rp)</center></th>
			    <th width='15%'><center>Jumlah Alokasi (Rp)</center></th>
			    <th width='15%'><center>Jumlah Keluar (Rp)</center></th>
			    <th width='15%'><center>Sisa (Rp)</center></th>
            </tr>
        </thead>
        <tbody>
        ";

        $id = $_GET['no_alokasi'];
		$sql1 = mysqli_query($mysqli,"SELECT jenis_alokasi,no_alokasi FROM pengajuan_alokasi WHERE no_alokasi='$id'");
 		$r2 = mysqli_fetch_array($sql1);

		$sql = mysqli_query($mysqli,"SELECT nama_alokasi,jml_alokasi,jenis_alokasi,harga_alokasi,jumlah_keluar,no_alokasi FROM pengajuan_alokasi WHERE jenis_alokasi='$r2[jenis_alokasi]'");
		$no = 1;
		while ($r = mysqli_fetch_array($sql)) {
			            //$id = $r['no_alokasi'];
			            $jk = $r['jumlah_keluar'];
			            $total = $r['jml_alokasi'] * $r['harga_alokasi'];
			            $sisa=$total-$jk;
			            $totalS+=$total;
			            $totalk+=$jk;
			            $totalsisa+=$sisa;
			        ?>

			        <tr >
			            <td align='center'><?php echo $no;?></td>
			            <td>
			                <?php echo  $r['nama_alokasi']; ?>
			            </td>
						<td align='center'>
			                <?php echo  $r['jml_alokasi']; ?>
			            </td>
			            <td align='right'>
			                <?php 
			                    $rupiah=number_format($r['harga_alokasi'],0,',','.'); 
			                    echo $rupiah; 
			                ?>
			            </td>
			            <td align='right'>
			                <?php
			                    echo number_format($total,0,',','.'); 
			                ?>
			            </td>
			            <td align='right'>
			                <?php 
			                    $rupiah=number_format($r['jumlah_keluar'],0,',','.'); 
			                    echo $rupiah; 
			                ?>
			            </td>
			            <td align='right'>
			                <?php
			                    echo number_format($sisa,0,',','.'); 
			                ?>
			            </td>
			        </tr>
			        <?php
			            $no++;
			        }
			        ?>
			        <tr>
				        <td colspan='4'><b>TOTAL</b></td>
				        <td align="right"><b>
				            <?php 
				                echo number_format($totalS,0,',','.'); 
				            ?></b>
				        </td>
				        <td align="right"><b>
				            <?php 
				                echo number_format($totalk,0,',','.'); 
				            ?></b>
				        </td>
				        <td align="right"><b>
				            <?php 
				                echo number_format($totalsisa,0,',','.'); 
				            ?></b>
				        </td>
			        </tr>
			        <tr>
			        	<td colspan='7' align='right'><button type="button" class="btn btn-primary" onclick="print_b()" ><span class="glyphicon glyphicon-print"></span> Print</button></td>
			        </tr>			


        </tbody>
    </table>  
	

<script>
    function print_d(){
        window.open("../action/adm/printPengajuan.php","_blank");
    }
</script>