<?php   
	include '../../../config/connectdb.php';
	error_reporting(0);
	
	echo "<p><h3 align=center> Laporan Kas (Pemasukan)</h3>
		<p><h5 align=center> Dari Tanggal <font color='red'><b>".$_GET['tgl_1']."</b></font> sampai <font color='red'><b>".$_GET['tgl_2']."</b></font> </h5> 		
    <table id='DLseluruh' class='table table-bordered table-hover'>
        <thead>
            <tr>
                <th width='5%'>No</th>
	            <th width='10%'>Tanggal Cek</th>
	            <th width='15%'>Keterangan</th>
	            <th width='15%'>Cek/Bg</th>
	            <th width='15%'>No.Cek</th>
	            <th width='15%'>Debit (Rp)</th>
            </tr>
        </thead>
        <tbody>
        ";

    $a=$_GET['tgl_1'];
	$b=$_GET['tgl_2'];
	
	$sql = mysqli_query($mysqli,"SELECT g.jenis_giro,g.no_giro,k.giro_asal,k.tgl_sekarang,k.tgl_cek,k.no_cek,k.nominal,k.keterangan,j.jenis_keuangan,j.id_jeniskeuangan 
                    FROM giro g, data_keuangan k,jenis_bayar j
                    WHERE g.no_giro=k.no_giro AND k.id_jeniskeuangan=j.id_jeniskeuangan
					AND k.tgl_sekarang between '$a' AND '$b'
                    AND g.jenis_giro='ADM'");
	$no = 1;
	while ($r = mysqli_fetch_array($sql)) {
            $id=$r[id_kenuangan];
            $rp=number_format($r[nominal],0,',','.');
            $nominal=$r[nominal];
            $saldo+=$nominal;
        ?>
        <tr align='left'>
            <td><?php echo $no;?></td>
            <td><?php echo  $r['tgl_sekarang']; ?></td>
            <td><?php echo  $r['giro_asal']; ?></td>
            <td><?php echo  $r['no_cek']; ?></td>
            <td><?php echo  $r['keterangan']; ?></td>
            <td align='right'><?php echo $rp; ?></td>
        </tr>
    
        <?php    
        $no++;
        }
        ?>

		<tr>
	    	<td colspan='5'><b>TOTAL</b></td>
			<td align="right"><b><?php echo number_format($saldo,0,',','.'); ?></b></td>
		</tr>
		<tr>
			<td colspan='8' align='right'><button type="button" class="btn btn-primary" onclick="print_d()" ><span class="glyphicon glyphicon-print"></span> Print</button></td>
		</tr>			

	</tbody>
</table>  

<script>
	function print_d(){
		<?php
			$tgl1=  $_GET['tgl_1'];
			$tgl2 = $_GET['tgl_2'];
		
		echo "window.open('../action/adm/printLaporankas.php?id=$tgl1&id2=$tgl2','_blank');";
		?>
	}
</script>