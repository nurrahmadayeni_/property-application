<?php   
	include '../../../config/connectdb.php';
	error_reporting(0);
	
	echo "<p><h3 align=center> Laporan Giro (Pemasukan dan Pengeluaran)</h3>
		<p><h5 align=center> Dari Tanggal <font color='red'><b>".$_GET['tgl_1']."</b></font> sampai <font color='red'><b>".$_GET['tgl_2']."</b></font> </h5> 		
    <table id='DLseluruh' class='table table-bordered table-hover'>
        <thead>
            <tr>
                <th width='5%'>No</th>
				<th width='13%'>Tanggal Cek/Bg</th>
				<th width='17%'>Keterangan</th>
				<th width='10%'>Cek/Bg</th>
				<th width='10%'>No.Cek</th>
				<th width='15%'>Debit (Rp)</th>
				<th width='15%'>Kredit (Rp)</th>
				<th width='15%'>Saldo (Rp)</th> 
            </tr>
        </thead>
        <tbody>
        ";

    $a=$_GET['tgl_1'];
	$b=$_GET['tgl_2'];
	
	$sql = mysqli_query($mysqli,"SELECT g.jenis_giro,g.no_giro,k.giro_asal,k.tgl_sekarang,k.tgl_cek,k.no_cek,k.nominal,k.keterangan,j.jenis_keuangan,j.id_jeniskeuangan
                    FROM giro g, data_keuangan k,jenis_bayar j
                    WHERE g.no_giro=k.no_giro AND k.id_jeniskeuangan=j.id_jeniskeuangan
                    AND k.tgl_sekarang between '$a' AND '$b'
                    ORDER BY k.tgl_sekarang");
	$no=1;
	while($r = mysqli_fetch_array($sql)){
		$id=$r[id_keuangan];

	if($r[jenis_giro]=='ADM'){
		$debit=$r[nominal];
	   	$saldod+=$debit;
	   	$saldoa+=$debit;
    ?>
        <tr align='center'>
            <td><?php echo $no;?></td>
            <td><?php echo  $r['tgl_sekarang']; ?></td>
            <td align='left'><?php echo  $r['keterangan']; ?></td>
			<td><?php echo  $r['giro_asal']; ?></td>
            <td><?php echo  $r['no_cek']; ?></td>
            <td align='right'><?php echo number_format($debit,0,',','.'); ?></td>
	        <td align='right'></td>
	        <td align='right'><?php echo number_format($saldoa,0,',','.'); ?></td>
	    </tr>
    <?php	
	} else {
     	$kredit=$r[nominal];
        $saldok+=$kredit;
        $saldoa=$saldod-$saldok;
	?>
		<tr align='center'>
	        <td><?php echo $no;?></td>
	        <td><?php echo  $r['tgl_sekarang']; ?></td>
	        <td align='left'><?php echo  $r['keterangan']; ?></td>
	  		<td><?php echo  $r['jenis_giro']; ?></td>
	        <td><?php echo  $r['no_cek']; ?></td>
	        <td align='right'></td>
	        <td align='right'><?php echo  number_format($kredit,0,',','.'); ?></td>
	        <td align='right'><?php echo number_format($saldoa,0,',','.'); ?></td>
	    </tr>		
	<?php
	}  
    	$saldos=$saldod-$saldok;
        $no++;
    }
	?>
		<tr>
			<th colspan="5">TOTAL</th>
			<td align="right"><?php echo number_format($saldod,0,',','.');  ?></td>
			<td align="right"><?php echo number_format($saldok,0,',','.');  ?></td>
			<td align="right"><?php echo number_format($saldos,0,',','.');  ?></td>		
		</tr>
		<tr>
			<td colspan='9' align='right'><button type="button" class="btn btn-primary" onclick="print_d()" ><span class="glyphicon glyphicon-print"></span> Print</button></td>
		</tr>			

	</tbody>
</table>  

<script>
	function print_d(){
		<?php
			$tgl1=  $_GET['tgl_1'];
			$tgl2 = $_GET['tgl_2'];
		
		echo "window.open('../action/adm/printlaporangiro.php?id=$tgl1&id2=$tgl2','_blank');";
		?>
	}
</script>