<?php
	include '../../../config/connectdb.php';
	//error_reporting(0);

	$mod=$_GET["mod"];
	$act=$_GET["act"];
	
	if(isset($mod) AND $act=='tambah'){
		$id_k = $_POST['kategori_material'];
		$nama = $_POST['nama_material'];
		$satuan = $_POST['satuan_material'];
		$spesifikasi = $_POST['spesifikasi'];
		$fileName = $_FILES['gambar_material']['name'];
		$harga_material = $_POST['harga_material'];
		
		//auto increment utk id
		$query = mysqli_query($mysqli,"SELECT max(id_material) AS idMAX FROM material");
		$data = mysqli_fetch_array($query);

		$id = $data['idMAX'];

		$noUrut = (int) substr($id, 4, 5); 
		$noUrut++;

		$chartetap = 'MT-';
		$id_m = $chartetap.sprintf("%02s", $noUrut);

		$sql_cek = $mysqli->query(
			"SELECT nama_material FROM material
			WHERE nama_material='$nama'"
		);

		$cek = mysqli_num_rows($sql_cek);

		if($cek > 0){
			echo "
					<script>
					window.alert('Material is exist');					
					javascript:history.go(-1);
				</script>
			";
						
			//header("Location: ../mod_adm/index.php?mod=".$mod);
			
		}elseif(isset($_POST['submit'])){
				
			// Simpan di Folder Gambar
			move_uploaded_file($_FILES['gambar_material']['tmp_name'], "../../mod_adm/gbr_material/".$_FILES['gambar_material']['name']);

			$hrgs = substr($harga_material,4,11);     
			$hrg = str_replace('.', '', $hrgs);
				
				$sql_insert = $mysqli->query(
				"INSERT INTO material (id_material,id_kategorimaterial, nama_material, satuan_material, spesifikasi, harga_material, gambar_material )
				VALUES('$id_m','$id_k', '$nama', '$satuan', '$spesifikasi','$hrg', '$fileName')"
				);

				// $sql_insert = mysqli_query($mysqli, $sql);

				if($sql_insert){
					echo "
						<script type='text/javascript'>
							windows.alert('success');
						</script>
					";
				}
		}
		echo "<script type='text/javascript'>
		document.location='../../mod_adm/index.php?mod=material&ids=$id_k' 
		</script>";
	}		
	else if(isset($mod) AND $act=='edit'){
		$id_k = $_POST['id_kategori_material'];
		$nama = $_POST['nama_material'];
		$satuan = $_POST['satuan_material'];
		$spesifikasi = $_POST['spesifikasi'];
		$fileName = $_FILES['gambar_material']['name'];
		$hrg = $_POST['harga_material'];

		if(empty($fileName)){
			$sql_update = $mysqli->query(
				"UPDATE material SET nama_material = '$nama', 
							   		satuan_material = '$satuan', 
							   		spesifikasi = '$spesifikasi', 
							   		harga_material= '$hrg'
							   WHERE id_material   = '$_POST[id]'
							   AND id_kategorimaterial ='$id_k'
							   ")or die("query gagal");
		}elseif(!empty($fileName)){
			move_uploaded_file($_FILES['gambar_material']['tmp_name'], "../../mod_adm/gbr_material/".$_FILES['gambar_material']['name']);
			
			$sql_update = $mysqli->query(
				"UPDATE material SET nama_material = '$nama', 
							   		satuan_material = '$satuan', 
							   		spesifikasi = '$spesifikasi', 
							   		harga_material= '$hrg',
							   		gambar_material = '$fileName'
							   WHERE id_material   = '$_POST[id]' 
							   AND id_kategorimaterial ='$id_k'") or die("query gagal");
		}
		
		// $sql_insert = mysqli_query($mysqli, $sql);

		if($sql_update){
			
			echo "
				<script type='text/javascript'>
					windows.alert('success');
				</script>
			";
		}
		echo "<script type='text/javascript'>
		document.location='../../mod_adm/index.php?mod=material&ids=$id_k' 
		</script>";
	}
	else if(isset($mod) AND $act=='hapus'){
			$id=$_GET['id'];
			$sql_insert = $mysqli->query("DELETE FROM material WHERE id_material='$id'");

		echo "<script type='text/javascript'>
		document.location='../../mod_adm/index.php?mod=data_material'
		</script>";
	}	
?>


