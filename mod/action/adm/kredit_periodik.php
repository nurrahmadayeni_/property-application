<?php   
	include '../../../config/connectdb.php';
	error_reporting(0);
	
	echo "<p><h3 align=center> Laporan Kredit (Pengeluaran)</h3>
		<p><h5 align=center> Dari Tanggal <font color='red'><b>".$_GET['tgl_1']."</b></font> sampai <font color='red'><b>".$_GET['tgl_2']."</b></font> </h5> 		
    <table id='DLseluruh' class='table table-bordered table-hover'>
        <thead>
            <tr>
                <th width='5%'>No</th>
                <th width='15%'>Tanggal Keluar</th>
                <th width='15%'>Tujuan Cek/Bg</th>
                <th width='15%'>No.Cek</th>
                <th width='20%'>Keterangan</th>
                <th width='20%'><center>Kredit (Rp)</center></th>
            </tr>
        </thead>
        <tbody>
        ";

    $a=$_GET['tgl_1'];
	$b=$_GET['tgl_2'];
	
	$sql = mysqli_query($mysqli,"SELECT g.jenis_giro,g.no_giro,k.tgl_sekarang,k.id_jeniskeuangan,k.nominal,k.keterangan,k.id_keuangan,jb.jenis_keuangan
                        FROM data_keuangan k, giro g,jenis_bayar jb
                        WHERE k.no_giro=g.no_giro  AND jb.id_jeniskeuangan=k.id_jeniskeuangan
						AND k.tgl_sekarang between '$a' AND '$b'
						AND k.giro_asal='ADM' AND k.id_jeniskeuangan='2'");

		$no = 1;
        while ($r = mysqli_fetch_array($sql)) {
            $id=$r[id_kenuangan];
            $row1=number_format($r[nominal],0,',','.');
            $nominal=$r[nominal];
            $total+=$nominal;
        ?>

        <tr align='left'>
            <td><?php echo $no;?></td>
            <td><?php echo  $r['tgl_sekarang']; ?></td>
            <td><?php echo  $r['jenis_giro']; ?></td>
            <td><?php echo  $r['no_cek']; ?></td>
            <td><?php echo  $r['keterangan']; ?></td>
            <td align='right'><?php echo  $row1; ?></td>
		</tr>
		<?php 

			$no++;
		}
		?>
		<tr>
	    	<td colspan='5'><b>TOTAL</b></td>
			<td align="right"><b><?php echo number_format($total,0,',','.'); ?></b></td>
		</tr>
		<tr>
			<td colspan='8' align='right'><button type="button" class="btn btn-primary" onclick="print_d()" ><span class="glyphicon glyphicon-print"></span> Print</button></td>
		</tr>			

	</tbody>
</table>  

<script>
	function print_d(){
		<?php
			$tgl1=  $_GET['tgl_1'];
			$tgl2 = $_GET['tgl_2'];
		
		echo "window.open('../action/adm/printLaporankredit.php?id=$tgl1&id2=$tgl2','_blank');";
		?>
	}
</script>