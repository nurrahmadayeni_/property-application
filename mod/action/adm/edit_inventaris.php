<?php
	//database connection 
	error_reporting(0);
	include '../../../config/connectdb.php';

	$id = $_POST["id"]; //escape the string if you like
	//$id = 1;
	
	$sql = mysqli_query($mysqli,"SELECT * FROM inventaris WHERE id_inventaris='$id'");
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
            
	?>
	
        <form method="post" action='../action/adm/act_edit_inventaris.php?mod=inventaris&act=edit' enctype="multipart/form-data">
			<input type=hidden name='id' value="<?php echo $r[id_inventaris]; ?>">
			<div class="form-group">
				<label for="nama-barang" class="control-label">Nama Barang : </label>
					<input type="text" class="form-control" id="nama-barang" name="nama_barang" value="<?php echo $r[nama_barang]; ?>">
            </div>
            <?php
            	$sql2 = mysqli_query($mysqli,"SELECT inventaris.id_kategori, kategori_inventaris.kategori_inventaris FROM inventaris, kategori_inventaris WHERE inventaris.id_kategori = kategori_inventaris.id_kategori and inventaris.id_inventaris='$r[id_inventaris]'");

            	$r2 = mysqli_fetch_array($sql2);

            	echo "
            	<div class='form-group'>
		            <label for='kategori' class='control-label'>Kategori Inventaris :</label>
                    <input type='text' class='form-control' value='$r2[kategori_inventaris]' name='kategori_inventaris' disabled>
                    <input type='hidden' class='form-control' value='$r2[id_kategori]' name='kategori'>";
            ?>
                    <?php
                        $getdata="SELECT * FROM kategori_inventaris WHERE id_inventaris='$_GET[id]'";
                        $tampil=mysqli_query($mysqli,$getdata);
                        while($r3=mysqli_fetch_array($tampil)){
                            echo "<option value=$r3[id_kategori]>
                            $r3[kategori_inventaris]</option>";
                        }
                    ?>
                          
		            </div>
            
            <div class="form-group">
                <label for="merk/type" class="control-label">Merk/Type :</label>
                <input type="text" class="form-control" id="merk" name="merk" value="<?php echo $r[merk]; ?>">
            </div>
            <div class="form-group">
                <label for="warna" class="control-label">Warna :</label>
                <input type="text" class="form-control" id="warna" name="warna" value="<?php echo $r[warna]; ?>">
            </div>
            <div class="form-group">
                <label for="thn" class="control-label">Tahun Perolehan :</label>
                <input type="text" class="form-control" id="thn_peroleh" name="thn_peroleh" value="<?php echo $r[thn_peroleh]; ?>">
            </div>
            <div class="form-group">
                <label for="jumlah" class="control-label">Jumlah :</label>
                <input type="text" class="form-control" id="jumlah" name="jumlah" value="<?php echo $r[jumlah]; ?>">
            </div>
            <div class="form-group">
                <label for="kondisi" class="control-label">Kondisi :</label>
                <input type="text" class="form-control" id="kondisi" name="kondisi" value="<?php echo $r[kondisi]; ?>">
            </div>
            <div class="form-group">
                <label for="hrg" class="control-label">Harga :</label>
                <input type="text" class="form-control" name="harga_barang" id="harga_barang" value="<?php echo $r[harga_barang]; ?>">
                <script type="text/javascript">
                    $(document).ready(function(){
                        $('#harga_barang').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});
                    });
                </script>
                
            </div>
            <div class="form-group">
                <label for="ptj" class="control-label">Penganggung Jawab :</label>
                <input type="text" class="form-control" id="ptj" name="ptj" value="<?php echo $r[ptj]; ?>">
            </div>
            <div class="form-group">
                <label for="lokasi" class="control-label">Lokasi :</label>
                <input type="text" class="form-control" id="lokasi" name="lokasi" value="<?php echo $r[lokasi]; ?>">
            </div>
            <div class="form-group">
                <label for="foto" class="control-label">Upload Foto :</label>
                <input type="file" id="foto" class="form-control" name="foto"  /> *Kosongkan jika gambar tidak diubah
            </div>
         
          <div class="modal-footer">
            <input type="Submit" class="btn btn-primary" value="Submit" name="submit">
            <input type="reset" class="btn btn-default" value="Reset">
        </div>
        </form>
	
	<?php
	$no++;
        }
	?>