<link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../../assets/css/dataTables.bootstrap.css"/>
<h3 align=center> DETAIL PENGELUARAN ASET GUDANG </h3>
<table id="order" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th width="10%">No</th>
			<th width="15%">Tanggal Keluar</th>
            <th width="15%">Nama Material</th>
            <th width="15%">Gudang</th>
			<th width="15%">Perumahan</th>
			<th width="15%">Kavling</th>
			<th width="15%">Jumlah Keluar</th>
			<th width="15%">Aksi</th>
        </tr>
     </thead>
            <tbody>
				<?php
                    error_reporting(0);
                    //Data mentah yang ditampilkan ke tabel   
                    $sql = mysqli_query($mysqli,"SELECT d.id_detailkeluar,p.id_pengeluaran,d.jumlah_keluar,d.tgl_pengeluaran,m.nama_material,g.nama_gudang,pr.nama_perumahan,k.no_kavling from data_pengeluaran p,material m,data_gudang g,data_perumahan pr,data_kavling k,data_detailkeluar d where p.id_pengeluaran=d.id_pengeluaran and d.id_material=m.id_material and d.id_gudang=g.id_gudang and pr.id_perumahan=d.id_perumahan and pr.id_perumahan=k.id_perumahan and d.no_kavling=k.no_kavling group by d.id_detailkeluar");
                    $no = 1;
                    while ($r = mysqli_fetch_array($sql)) {
                    $id = $r['id_pengeluaran'];
                    $idd=$r['id_detailkeluar'];
					echo"
                    <tr align='left'>
                        <td>$no</td>
                        <td>$r[tgl_pengeluaran]</td>
						<td>$r[nama_material]</td>
						<td>$r[nama_gudang]</td>
						<td>$r[nama_perumahan]</td>
						<td>$r[no_kavling]</td>
						<td>$r[jumlah_keluar]</td>";
				?>
						<td><a href="../action/adm/aksi_hapuskeluar.php?mod=gudang&act=hapus&id= <?php echo $idd; ?>" title="hapus" onclick="return confirm('Anda ingin menghapus data ini?')"> 
						<button class='btn btn-danger btn-sm' alt='Hapus'><span class='glyphicon glyphicon-trash'></span></button> </a></td>
                    </tr>
				<?php
                    $no++;
                   }
                    ?>
                </tbody>
            </table>
		<table><tr>
		<td>
			<a href='index.php?mod=pengeluaran' class='btn btn-primary' ><span class="glyphicon glyphicon-plus"></span>  Pengeluaran</a>
		</td>
		</tr>
		
		</table>
    <script src="../../assets/js/jquery-1.11.0.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>        
	<script src="../../assets/datatables/jquery.dataTables.js"></script>
	<script src="../../assets/datatables/dataTables.bootstrap.js"></script>
    <script type="text/javascript">
      $(function() {
             $("#order").dataTable();
      });
    </script>