
<h3 align=center> Laporan Gudang Periodik </h3>
<?php
include '../../../config/connectdb.php';
error_reporting(0);

	$sql1 = mysqli_query($mysqli,"SELECT * FROM kategori_material WHERE id_kategorimaterial='$_GET[km]'");
		while($r3=mysqli_fetch_array($sql1)){
		$nm= $r3['kategori_material'];
	}
	$sql2 = mysqli_query($mysqli,"SELECT * FROM data_gudang WHERE id_gudang='$_GET[kg]'");
		while($r2=mysqli_fetch_array($sql2)){
		$ng= $r2['nama_gudang'];
	}

	$prev_day = 7;
	$preV_hari = mktime(0,0,0,date("m"),date("d") -$prev_day,date("Y"));
	$tgl=date('Y-m-d',$preV_hari);

	echo"	<h5 align=center> Tipe Material <font color='red'><b>".$nm."</b></font>
			Gudang <font color='red'><b>".$ng."</b></font>
			Tanggal <font color='red'><b>".$tgl." </b></font> 
			s/d <font color='red'><b>".$_GET['tg']." </b></font> </h5>";
?>
<table id="DLperiodik" class="table table-bordered table-hover">
	<thead>
        <tr>
            <th>No</th>
			<th>Gudang</th>
			<th>Material</th>
			<th>Satuan</th>
			<th>Harga (Rp)</th>
			<th>Tgl Masuk</th>
			<th>Jml Masuk</th>
			<th>Tgl Keluar</th>
			<th>Jml Keluar</th>
			<th>Tgl Retur</th>
			<th>Jml Retur</th>
			<th>Total Stok</th>
			<th>Total Harga Masuk (Rp)</th>
        </tr>
    </thead>
    <tbody>
		<?php
			include '../../../config/connectdb.php';
            error_reporting(0);
            
			$a=$_GET['km'];
			$b=$_GET['kg'];
			$c=$_GET['tg'];

			$prev_day = 7;
			$preV_hari = mktime(0,0,0,date("m"),date("d") -$prev_day,date("Y"));
			$tgl=date('Y-m-d',$preV_hari);
				
			$sql = mysqli_query($mysqli,"SELECT t.kategori_material,m.nama_material,m.harga_material, m.satuan_material, g.nama_gudang, mg.jumlah AS total_Stok, mm.jumlah_masuk AS jlh_masuk, mm.tgl_masuk, 
				IF((
				SELECT dk.jumlah_keluar
				FROM material_gudang mg
				INNER JOIN data_detailkeluar dk ON dk.id_gudang = mg.id_gudang
				AND dk.id_material = mg.id_material
				AND dk.id_gudang IS NULL 
				AND dk.id_material IS NULL
				), 0, dk.jumlah_keluar) AS jlh_keluar,dk.tgl_pengeluaran,
				IF((
				SELECT rt.jumlah_kembali
				FROM material_gudang mg
				INNER JOIN retur_detail rt ON rt.id_gudang = mg.id_gudang
				AND rt.id_material = mg.id_material
				AND rt.id_gudang IS NULL 
				AND rt.id_material IS NULL
				), 0, rt.jumlah_kembali) AS jlh_retur,rt.tgl_retur
				FROM material_gudang mg
				INNER JOIN material_masuk mm ON mm.id_gudang = mg.id_gudang
				AND mm.id_material = mg.id_material
				AND mm.tgl_masuk between '$tgl' AND '$c' 
				INNER JOIN material m ON mg.id_material = m.id_material
				INNER JOIN data_gudang g ON mg.id_gudang = g.id_gudang
				AND g.id_gudang='$b'
				LEFT JOIN data_detailkeluar dk ON dk.id_material = mg.id_material
				AND dk.id_gudang = mg.id_gudang
				LEFT JOIN retur_detail rt ON rt.id_material = mg.id_material
				AND rt.id_gudang = mg.id_gudang
				LEFT JOIN kategori_material t ON mg.id_material=m.id_material
				WHERE m.id_kategorimaterial=t.id_kategorimaterial
				AND t.id_kategorimaterial='$a'
				ORDER BY t.id_kategorimaterial");

			$no = 1;
			while ($r = mysqli_fetch_array($sql)) {
				
				$hrg=$r['harga_material'];
				$jk=$r['jlh_masuk'];
				$total = $jk * $hrg;
				$totalseluruh+=$total;	

			if($r['tgl_pengeluaran']==NULL && $r['tgl_retur']==NULL){
			echo"
			<tr align='left'>
                <td>$no</td>
				<td>$r[nama_gudang]</td>
                <td>$r[nama_material]</td>
				<td>$r[satuan_material]</td>
				<td align='right'>";
					$rupiah=number_format($hrg,0,',','.'); 
			        echo $rupiah; 
			echo"</td>
				<td>$r[tgl_masuk]</td>
				<td align='center'>$r[jlh_masuk]</td>
				<td> - </td>
				<td align='center'> - </td>
				<td> - </td>
				<td align='center'> - </td>
				<td>$r[total_Stok]</td>
				<td align='right'>";
				    echo number_format($total,0,',','.');
			echo"</td>
			</tr>";
			}
			else{
			echo"
            <tr align='left'>
                <td>$no</td>
				<td>$r[nama_gudang]</td>
                <td>$r[nama_material]</td>
				<td>$r[satuan_material]</td>
				<td align='right'>";
					$hrg=$r['harga_material'];
					$rupiah=number_format($hrg,0,',','.'); 
			        echo $rupiah; 
			echo"</td>
				<td>$r[tgl_masuk]</td>
				<td align='center'>$r[jlh_masuk]</td>
				<td>$r[tgl_pengeluaran]</td>
				<td align='center'>$r[jlh_keluar]</td>
				<td>$r[tgl_retur]</td>
				<td align='center'>$r[jlh_retur]</td>
				<td>$r[total_Stok]</td>
				<td align='right'>";
				    echo number_format($total,0,',','.');
			echo"</td>
			</tr>";
			}
            $no++;
            }
        ?>
        	<tr>
				<td colspan='12'><b>Total Harga Keseluruhan (Rp)</b></td>
				<td align="right"><b><?php echo number_format($totalseluruh, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan);?></b></td>
			</tr>
            <tr align="right">
				<td colspan='13'><button type="button" class="btn btn-primary" onclick="print_d()" >
				<span class="glyphicon glyphicon-print"></span> Print</button></td>
			</tr>
    </tbody>
</table>
 
 <script>
	function print_d(){
		<?php
		$a=$_GET['km'];
		$b=$_GET['kg'];
		$c=$_GET['tg'];

		$prev_day = 7;
		$preV_hari = mktime(0,0,0,date("m"),date("d") -$prev_day,date("Y"));
		$tgl=date('Y-m-d',$preV_hari);
		
		echo "window.open('../action/adm/printlaporan4.php?id=$a&id2=$b&id3=$c','_blank');";
		?>
	}
</script>