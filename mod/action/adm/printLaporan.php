<html>
<head>
	<title>Print Document</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body><br><br>
	<img src="../../../images/logo.png" align="right"><br>
	<font size="5%"><b>Laporan Pemasukan dan Pengeluaran</b></font><br><hr>
	<table border="1" width="100%" style="border-collapse:collapse;" align="center">
    	<tr class="tableheader">
				<th width="5%">No</th>
				<th width="10%">Tanggal Cek/Bg</th>
				<th width="20%">Keterangan</th>
				<th width="10%">Cek/Bg</th>
				<th width="10%">No.Cek</th>
				<th width="15%">Debit (Rp)</th>
				<th width="15%">Kredit (Rp)</th>
				<th width="15%">Saldo (Rp)</th>    	
        </tr>
     </thead>
            <tbody>
				<?php
                error_reporting(0);
				include '../../../config/connectdb.php';
					
				$sql = mysqli_query($mysqli,"SELECT g.jenis_giro,g.no_giro,k.giro_asal,k.tgl_sekarang,k.tgl_cek,k.no_cek,k.nominal,k.keterangan,j.jenis_keuangan,j.id_jeniskeuangan
                    FROM giro g, data_keuangan k,jenis_bayar j
                    WHERE g.no_giro=k.no_giro AND k.id_jeniskeuangan=j.id_jeniskeuangan
                    ORDER BY k.tgl_sekarang");

				$no = 1;
				while ($r = mysqli_fetch_array($sql)) {
				    $id=$r['id_keuangan'];

				if($r[jenis_giro]=='ADM'){
	        	$debit=$r[nominal];
	        	$saldod+=$debit;
	        	$saldoa+=$debit;
	        ?>
	        <tr align='center'>
	            <td ><?php echo $no;?></td>
	            <td><?php echo  $r['tgl_sekarang']; ?></td>
	            <td align='left'><?php echo  $r['keterangan']; ?></td>
				<td><?php echo  $r['giro_asal']; ?></td>
	            <td><?php echo  $r['no_cek']; ?></td>
	            <td align='right'><?php echo number_format($debit,0,',','.'); ?></td>
	            <td align='right'></td>
	            <td align='right'><?php echo number_format($saldoa,0,',','.'); ?></td>
	        </tr>
	        <?php	
	        } else {
	        	$kredit=$r[nominal];
	            $saldok+=$kredit;
	            $saldoa=$saldod-$saldok;
	        ?>
	        <tr align='center'>
	            <td ><?php echo $no;?></td>
	            <td><?php echo  $r['tgl_sekarang']; ?></td>
	            <td align='left'><?php echo  $r['keterangan']; ?></td>
				<td><?php echo  $r['jenis_giro']; ?></td>
	            <td><?php echo  $r['no_cek']; ?></td>
	            <td align='right'></td>
	            <td align='right'><?php echo  number_format($kredit,0,',','.'); ?></td>
	            <td align='right'><?php echo number_format($saldoa,0,',','.'); ?></td>
	        </tr>
	        <?php
	        }  
	        	$saldos=$saldod-$saldok;
	        $no++;
	        }
	        ?>
			<tr>
				<td colspan="5"><b>TOTAL</b></td>
				<td align="right"><?php echo number_format($saldod,0,',','.');  ?></td>
				<td align="right"><?php echo number_format($saldok,0,',','.');  ?></td>
				<td align="right"><?php echo number_format($saldos,0,',','.');  ?></td>
				
			</tr>

		</tbody>
		</table>  <br>
	<div style="float:right; margin-right:5%;">
		<table bgcolor="#E6E6FA">
			<tr>
				<td>Total Debit</td><td width="8%">Rp</td><td align="right" ><?php echo number_format($saldod,0,',','.'); ?></td>
			</tr>
			<tr>
				<td>Total Kredit</td><td>Rp</td><td align="right"><?php echo number_format($saldok,0,',','.'); ?></td>
			</tr>
			<tr>
				<td>Sisa Saldo</td><td>Rp</td><td align="right"><?php echo number_format($saldos,0,',','.'); ?></td>
			</tr>
		</table>
	</div>  
</body>
</html>
		
	<script>
		window.load = print_d();
		function print_d(){
			window.print();
		}
	</script>