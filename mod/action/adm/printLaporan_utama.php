<html>
<head>
	<title>Print Laporan</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="../../assets/css/bootstrap-theme.min.css">
	<link href="../../../images/pavicon.png" rel="icon" type="image/x-icon" />
</head>
<body><br>
	<img src="../../../images/logo.png" align="right"><br>
	<b style="font-size:17px;">Laporan Alokasi</b><hr><br>
	<table border="1" width="100%" style="border-collapse:collapse;" align="center">
    	<tr class="tableheader"> 
			<th width='5%'><center>No</center></th>
            <th>Nama Alokasi</th>
            <th><center>Jumlah Alokasi (Rp)</center></th>
            <th><center>Jumlah Keluar (Rp)</center></th>
            <th><center>Sisa (Rp)</center></th>
        </tr>
     </thead>
            <tbody>
            <?php
    			error_reporting(0);
				include '../../../config/connectdb.php';

		        $sql = mysqli_query($mysqli,'SELECT nama_alokasi,jml_alokasi,harga_alokasi,jumlah_keluar FROM pengajuan_alokasi');
		        $no = 1;
		        while ($r = mysqli_fetch_array($sql)) {
		            $jml=$r['harga_alokasi'];
		            $jk= $r['jumlah_keluar'];
		            $total = $r['jml_alokasi'] * $r['harga_alokasi'];
		            $sisa=$total-$jk;
		            $totala += $jml;
		            $totalk += $jk;
		            $totals +=$sisa;
		    ?>
		<tr>
	        <td align='center'><?php echo $no;?></td>
	        <td>
	        	<?php echo  $r['nama_alokasi']; ?>
	        </td>
	        <td align='right'>
	            <?php 
	                $rupiah=number_format($r['harga_alokasi'],0,',','.'); 
	                echo $rupiah; 
	            ?>
	        </td>
	        <td align='right'>
	            <?php 
	                $rupiah=number_format($r['jumlah_keluar'],0,',','.'); 
	                echo $rupiah; 
	            ?>
	        </td>
	        <td align='right'>
	            <?php 
	                echo number_format($sisa,0,',','.');
	            ?>
	        </td>
	    </tr>
        <?php
        	$no++;
		    }
        ?>
        <tr>
	        <td colspan='2'>TOTAL</td>
	        <td align="right">
	            <?php 
	                echo number_format($totala,0,',','.'); 
	            ?>
	        </td>
	        <td align="right">
	            <?php 
	                echo number_format($totalk,0,',','.'); 
	            ?>
	        </td>
	        <td align="right">
	            <?php 
	                echo number_format($totals,0,',','.'); 
	            ?>
	        </td>
        </tr>

        </tbody>
    </table>
</body>
</html>

	<script>
		window.load = print_b();
		function print_b(){
			window.print();
		}
	</script>