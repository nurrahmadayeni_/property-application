<html>
<head>
	<title>Print Laporan</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="../../assets/css/bootstrap-theme.min.css">
	<link href="../../../images/pavicon.png" rel="icon" type="image/x-icon" />
</head>
<body>
	<br><br>
	<img src="../../../images/logo.png" align="right"><br>
	<font size="3%">Laporan Gudang</font><br><hr>
	<table border="1" width="100%" style="border-collapse:collapse;" align="center">
    	<tr class="tableheader">
    		<th width="5%">No</th>
			<th width='15%'>Tanggal Masuk</th>
            <th width='15%'>Gudang</th>
            <th width='15%'>Material</th>
            <th width='15%'>Jumlah Material</th>
            <th width='17%'>Harga Material (Rp)</th>
            <th width='18%'>Total (Rp)</th>
        </tr>
     </thead>
            <tbody>
				<?php
                    error_reporting(0);
					include '../../../config/connectdb.php';
					
					$id = $_GET['id'];
					
                    $sql = mysqli_query($mysqli,"SELECT t.tgl_masuk as tanggal, l.id_laporangdg as id_laporangdg,l.jumlah as jumlah,m.nama_material,m.harga_material,g.nama_gudang 
										FROM material_masuk t, material_gudang l,material m,data_gudang g 
										WHERE l.id_gudang=g.id_gudang 
										AND m.id_material=l.id_material 
										AND t.id_gudang=g.id_gudang
										AND m.id_material=t.id_material
										AND g.id_gudang=$id ORDER BY m.nama_material ");
					$no=1;
                    while ($r = mysqli_fetch_array($sql)) {
						$id= $r['id_laporangd'];
						$gdg= $r['nama_gudang'];
						$mat= $r['nama_material'];
						$hrg=$r['harga_material'];
						$jl= $r['jumlah'];
						$tgl_masuk= $r['tanggal'];

						$total = $jl * $hrg;
						$totalseluruh+=$total;
						
					echo"
                    <tr align='left'>
                    	<td align='center'>$no</td>
						<td>$tgl_masuk</td>
                        <td>$gdg</td>
						<td>$mat</td>
                        <td align='center'>$jl</td>
                        <td align='right'>";
							$rupiah=number_format($hrg,0,',','.'); 
			                echo $rupiah; 
			            echo"    
			            </td>
						<td align='right'>";
				            echo number_format($total,0,',','.');
			            echo"</td>
                    </tr>";
                    $no++;
					}
                    ?>
                    <tr>
						<td colspan='6'><b>Total Harga Keseluruhan (Rp)</b></td>
						<td align="right"><b><?php echo number_format($totalseluruh, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan);?></b></td>
					</tr>		
                </tbody>
            </table>
		
	<script>
		window.load = print_d();
		function print_d(){
			window.print();
		}
	</script>