<?php
	include '../../../config/connectdb.php';
	error_reporting(0);
	session_start();
	$id = $_GET['v1'];
	$id2 = $_GET['v2'];
	$id_k = $_GET['v3'];
	
	$sql = "SELECT *FROM kontrak_pekerjaan 
	WHERE id_perumahan='$id' 
	AND no_kavling='$id2' 
	AND id_kategorikerja='$id_k' order BY id_kontrak DESC LIMIT 1";
	
	$tampil=mysqli_query($mysqli,$sql);

    $q = mysqli_fetch_array($tampil);
		
	?>
	
		<table class='table table-bordered table-hover'>
            <tr align='center'>
                <td><b>No</b></td>
                <td><b>Kontraktor</b></td>
                <td><b>Jenis Pekerjaan</b></td>
                <td><b>Penyelesaian Terakhir</b></td>
                <td><b>Nilai Kontrak(Rp.)</b></td>
                <td><b>Nilai Ambil Max</b></td>
                <td><b>Sudah Cair</b></td>
				<td><b>Sisa</b></td>
				<td><b>Aksi</b></td>
            </tr>
		<?php
			if(isset($q)){
		?>
			<tr>
				<td align='center'>1</td>
				<td align='center'><?php echo $q['kontraktor'];?></td>
				<?php
					$nama_kategorikerja = $mysqli->query(
                            "SELECT nama_kategorikerja
                            FROM kategori_pekerjaan
                            WHERE id_kategorikerja = '$id_k'
                            ")->fetch_object()->nama_kategorikerja;
				?>
				<td align='center'><?= $nama_kategorikerja; ?></td>
				<?php
					$ps = $mysqli->query("
					SELECT d.nama_pekerjaan 
					FROM data_pekerjaan d, progress_pembangunan pp 
					WHERE d.id_kategorikerja in 
					(select id_kategorikerja from kontrak_pekerjaan ) 
					and pp.id_pekerjaan=d.id_pekerjaan 
					and pp.progress='1' 
					and pp.id_perumahan='$id' 
					AND pp.no_kavling='$id2' AND d.id_kategorikerja='$id_k' 
					order by pp.id_progress desc limit 1
					")->fetch_object()->nama_pekerjaan;
				?>
				<td align='center'><?= $ps; ?></td>
				<td align='center'><?= number_format($q['nilai_kontrak'],0,',',','); ?></td>
				<?php
					$kesiapan = "SELECT pp.id_pekerjaan, d.nama_pekerjaan from data_pekerjaan d, progress_pembangunan pp 
					WHERE d.id_kategorikerja in (select id_kategorikerja from kontrak_pekerjaan ) 
					and pp.id_pekerjaan=d.id_pekerjaan and pp.progress='1' 
					and pp.id_perumahan='$id' AND pp.no_kavling='$id2' AND d.id_kategorikerja='$id_k' order by pp.id_progress";

					$t = mysqli_query($mysqli,$kesiapan);
					$siap = mysqli_num_rows($t);
					
					$tot = "SELECT pp.id_pekerjaan, d.nama_pekerjaan 
					from data_pekerjaan d, progress_pembangunan pp 
					WHERE d.id_kategorikerja in (select id_kategorikerja from kontrak_pekerjaan ) 
					and pp.id_pekerjaan=d.id_pekerjaan and pp.id_perumahan='$id' AND pp.no_kavling='$id2' 
					AND d.id_kategorikerja='$id_k'";

					$total = mysqli_query($mysqli,$tot);
					$siap_t = mysqli_num_rows($total);

					$persen = round(($siap / $siap_t) *100);
					
					$biaya_progress = ($persen/100) * $q['nilai_kontrak'];
					$nilai = number_format($biaya_progress,0,',',',');
				?>
				<td align='center'><?= $nilai; ?></td>
				<?php
					$tot = $mysqli->query(
					"SELECT total_ambilan FROM progress_pembayaran_kontrak 
					WHERE id_kontrak=".$q['id_kontrak']."
					ORDER BY id_progress_pembayaran DESC limit 1
					")->fetch_object()->total_ambilan;

					$upah_tot = number_format($tot,0,',','.');
				?>
				
				<td align='center'><?= $upah_tot; ?></td>
				<?php
					$sisa = $q['nilai_kontrak'] - $tot ;
				?>
				<td align='center'><?= number_format($sisa,0,',',','); ?></td>
				<?php
					$id_kontrak = $q['id_kontrak'];
					$kontraktor = $q['kontraktor'];
					echo"
                        <td><a href='?mod=bayar_upah&v1=$id&v2=$id2&v3=$id_kontrak&v4=$kontraktor&v5=$nama_kategorikerja'>
						<i class='btn btn-primary material-icons'>payment</i></a></td>
                    ";
				?>
				
		<?php
			}
		?>
	