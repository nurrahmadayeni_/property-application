<?php
	//database connection 

	include '../../../config/connectdb.php';

	$id = $_POST["v1"]; // perumahan
	$id2 = $_POST["v2"]; // kavling
	$id3 = $_POST["v3"]; // pengawas

?>

<form method="post" action='../action/teknikal/act_kontrak.php?mod=teknikal&act=tambahkontrakbaru'>
    <div class="form-group">
        <label for="perumahan" class="control-label">Perumahan : </label>
        <input type="text" name="perumahan2" class="form-control" value=<?= $id; ?> disabled>
        <input type="hidden" name="perumahan" class="form-control" value=<?= $id; ?> >
    </div>
    <div class="form-group">
        <label for="no_kavling" class="control-label">Pilih Kavling :</label>
        <input type="text" name="no_kavling2" class="form-control" value= <?= $id2; ?> disabled>
        <input type="hidden" name="no_kavling" class="form-control" value= <?= $id2; ?> >
    </div>      
    <div class="form-group">
        <label for="kontraktor" class="control-label">Kontraktor :</label>
        <input type="text" class="form-control" id="kontraktor" name="kontraktor" required=''>
        </script>
      </div>
    <div class="form-group">
        <label for="nama_pegawai" class="control-label">Pengawas :</label>
        <input type="text" name ="nama_pegawai" class="form-control" >
    </div>
    
      <div class="form-group">
        <label for="tipe-pekerjaan" class="control-label">Tipe Pekerjaan : </label>
		<select name='tipe_krj' id='tipe_krj' class='form-control' required=''>
		<option value='' selected>- Pilih Tipe Pekerjaan -</option>
		<?php
			$getkategori="SELECT * FROM kategori_pekerjaan";
			$tampil=mysqli_query($mysqli,$getkategori);
			while($r=mysqli_fetch_assoc($tampil)){
				
				$kategori = "SELECT id_kategorikerja from kontrak_pekerjaan where id_kategorikerja='$r[id_kategorikerja]' and id_perumahan='$id' and no_kavling='$id2'";
				$t = mysqli_query($mysqli,$kategori);
				$s = mysqli_num_rows($t);

				if($s>0){
					echo "<option value=$r[id_kategorikerja] disabled>
				$r[nama_kategorikerja]</option>";
				}else{
					echo "<option value=$r[id_kategorikerja]>
				$r[nama_kategorikerja]</option>";
				}					
			}
		?>
		</select>
	  </div>
	  <div class="form-group">
        <label for="nilai" class="control-label">Nilai Kontrak :</label>
        <input type="text" class="form-control" id="nilai" name="nilai" required=''>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#nilai').maskMoney({prefix:'Rp. ', thousands:',', decimal:',', precision:0});
			});
		</script>
	  </div>
      <div class="form-group">
        <label for="tanggal" class="control-label">Tanggal Siap :</label>
        <input type="date" class="form-control" id="tgl" name="tgl" required=''>
      </div>
     <div class="modal-footer">
                <input type="Submit" class="btn btn-primary" value="Submit">
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
   </form>