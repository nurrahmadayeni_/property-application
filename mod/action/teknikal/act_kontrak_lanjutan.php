<?php
	//database connection 

	include '../../../config/connectdb.php';

	$id = $_POST["v1"]; // perumahan
	$id2 = $_POST["v2"]; // kavling
	$id3 = $_POST["v3"]; // id_kontrak
	$id4 = $_POST["v4"]; // kategori kerja
	$id5 = $_POST["v5"]; // sisa

?>
<script>
	function formatThousands(n, dp) {
		  var s = ''+(Math.floor(n)), d = n % 1, i = s.length, r = '';
		  while ( (i -= 3) > 0 ) { r = ',' + s.substr(i, 3) + r; }
		  return s.substr(0, i + 3) + r + (d ? '.' + Math.round(d * Math.pow(10,dp||2)) : '');
	}
	$(document).ready(function() {
		$('#tambahan').keyup(function(){
	    	var harga = $('#tambahan').val();
		    var harga2=harga.replace('Rp. ','');
		    var harga3=parseInt(harga2.replace(/,/g,''), 10);
			
			var nilai_kon=parseInt($('#nilai').val());

			var total=harga3+nilai_kon;
		   	var hargatmabh = formatThousands(total);

		   	$('#kontrak_baru2').val(hargatmabh);
		    $('#kontrak_baru').val(total);

		    var harga2 = parseInt($('#kontrak_baru').val());

			var upah_cair=parseInt($('#jlh_upah_cair').val());

			var total2=harga2-upah_cair;
		   	var hargasisa = formatThousands(total2);

		   	$('#sisa_kontrak_baru2').val(hargasisa);
		    $('#sisa_kontrak_baru').val(total2);
	    });
	    $('#kontrak_baru2').keyup(function(){
	    	
	    });
	});
</script>
<form method="post" action='../action/teknikal/act_kontrak.php?mod=teknikal&act=tambahkontraklanjutan'>
    <div class="form-group">
        <label for="perumahan" class="control-label">Perumahan : </label>
        <input type="text" name="perumahan2" class="form-control" value=<?= $id; ?> disabled>
        <input type="hidden" name="perumahan" class="form-control" value=<?= $id; ?> >
        <input type="hidden" name="id_kontrak" class="form-control" value=<?= $id3; ?> >
    </div>

    <div class="form-group">
        <label for="no_kavling" class="control-label">Pilih Kavling :</label>
        <input type="text" name="no_kavling2" class="form-control" value= <?= $id2; ?> disabled>
        <input type="hidden" name="no_kavling" class="form-control" value= <?= $id2; ?> >
    </div>      
    <div class="form-group">
        <label for="kontraktor" class="control-label">Kontraktor :</label>
        <input type="text" class="form-control" id="kontraktor" name="kontraktor" required=''>
        </script>
      </div>
    <div class="form-group">
        <label for="nama_pegawai" class="control-label">Pengawas :</label>
        <input type="text" name ="nama_pegawai" class="form-control" >
    </div>
    <?php
	  	$sql = "SELECT *FROM kontrak_pekerjaan 
		WHERE id_perumahan='$id' 
		AND no_kavling='$id2' 
		AND id_kontrak='$id3'";
		
		$tampil=mysqli_query($mysqli,$sql);

	    $q = mysqli_fetch_array($tampil);
	  ?>
    
      <div class="form-group">
        <label for="tipe-pekerjaan" class="control-label">Tipe Pekerjaan : </label>
		<input type="text" name ="tipe_krj2" class="form-control" value=<?= $id4; ?> disabled>
		<input type="hidden" name ="tipe_krj" class="form-control" value=<?= $q['id_kategorikerja']; ?>>
	  </div>

	  <div class="form-group">
		<label for="tipe-pekerjaan" class="control-label">Nilai Kontrak Awal : </label>
		<?php $p = number_format($q['nilai_kontrak'],0,',',','); ?>
		<input type="text" id="nilai2" name ="nilai" class="form-control" value=<?= $p; ?> disabled>
		<input type="hidden" id="nilai" name ="nilai" class="form-control" value=<?= $q['nilai_kontrak']; ?>>
	  </div>
	  <div class="form-group">
		<label for="tipe-pekerjaan" class="control-label">Sisa Kontrak Awal : </label>
		<input type="text" name ="sisa_kontrak2" class="form-control" value=<?= number_format($id5,0,',',','); ?> disabled>
		<input type="hidden" name ="sisa_kontrak" class="form-control" value=<?= $id5; ?>>
	  </div>

	  <div class="form-group">
        <label for="tambha" class="control-label"> Penambahan Kontrak :</label>
        <input type="text" class="form-control" id="tambahan" name="tambahan" required=''>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#tambahan').maskMoney({prefix:'Rp. ',thousands:',', decimal:',', precision:0});
			});
		</script>
	  </div>
	  <div class="form-group">
		<label for="tipe-pekerjaan" class="control-label">Kontrak Baru : </label>
		<input type="text" name ="kontrak_baru2" id="kontrak_baru2" class="form-control" value='' disabled>
		<input type="hidden" name ="kontrak_baru" id="kontrak_baru" class="form-control" value='' >
	  </div>
	  <?php
	  	$upah = $mysqli->query(
                "SELECT sum(harga_upah) as 'upah' FROM progress_pembayaran_kontrak
                WHERE id_kontrak = '$id3' 
                ")->fetch_object()->upah;
	  ?>
	  <input type="hidden" name ="jlh_upah_cair" id="jlh_upah_cair" class="form-control" value=<?= $upah; ?> >

	  <div class="form-group">
		<label for="tipe-pekerjaan" class="control-label">Sisa Kontrak Baru : </label>
		<input type="text" id="sisa_kontrak_baru2" name ="sisa_kontrak_baru2" class="form-control" value='' disabled>
		<input type="hidden" id="sisa_kontrak_baru" name ="sisa_kontrak_baru" class="form-control" value='' >
	  </div>

      <div class="form-group">
        <label for="tanggal" class="control-label">Tanggal Siap :</label>
        <input type="date" class="form-control" id="tgl" name="tgl" required=''>
      </div>
     <div class="modal-footer">
                <input type="Submit" class="btn btn-primary" value="Submit">
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
   </form>
