<?php   
	include '../../../config/connectdb.php';

	echo "<h3 align=center> Data Inventaris</h3>
            <table id='inventaris' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Merk/Type</th>
                    <th>Warna</th>
                    <th>Thn Perolehan</th>
                    <th>Jlh</th>
                    <th>Kondisi</th>
                    <th>Harga</th>
                    <th>Total</th>
                    <th>PTJ</th>
                    <th>Lokasi</th>
                    <th>Foto</th>
                </tr>
            </thead>
            <tbody>
        ";

 	$id = $_GET['kategori_inventaris'];
 	$get_inv = "SELECT *FROM INVENTARIS WHERE id_kategori=$id ORDER BY harga_barang ASC";
	$tampil=mysqli_query($mysqli,$get_inv);

	$no = 1;
	while ($r = mysqli_fetch_array($tampil)) {
            $id = $r['id_inventaris'];
            $idk = $r['id_kategori'];
?>
        <tr align='left'>
            <td align="center"> <?php echo  $no;?> </td>
            <td> <?php echo  $r['nama_barang']; ?> </td>
            <td> <?php echo  $r['merk']; ?> </td>
            <td> <?php echo  $r['warna']; ?> </td>
            <td> <?php echo  $r['thn_peroleh']; ?> </td>
            <td> <?php echo  $r['jumlah']; ?> </td>
            <td> <?php echo  $r['kondisi']; ?></td>
            <td> 
                <?php 
                    $rupiah=number_format($r['harga_barang'],0,',','.'); 
                    echo 'Rp.'. $rupiah; 
                ?>
            </td>
            <td>
                <?php
                    $jumlah = $r['jumlah'];
                    $hrg = $r['harga_barang'];
                    $total = $jumlah * $hrg;
                    echo "Rp.". number_format($total,0,',','.'); 
                ?>
            </td>
            <td> <?php echo  $r['ptj']; ?> </td>
            <td> <?php echo  $r['lokasi']; ?> </td>
            <td> <img src="../mod_adm/gbr_inventaris/<?php echo  $r['foto']; ?>" width='100%'> </td>
        </tr>
		<?php
       		$no++;
		}
            
        ?>
        <tr align="right">
                <td colspan='12'><button type="button" class="btn btn-primary" onclick="print_d()" >
                <span class="glyphicon glyphicon-print"></span> Print</button></td>
            </tr>

        </tbody>
    </table>  

<script>
    function print_d(){
        <?php
            $id = $_GET['kategori_inventaris'];
            echo "window.open('../action/teknikal/printInventaris.php?id=$id','_blank');";
        ?>
    }
</script>