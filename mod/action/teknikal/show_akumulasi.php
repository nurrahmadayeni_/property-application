<?php   
	include '../../../config/connectdb.php';
    //error_reporting(0);
    
    $sql_per = "SELECT pp.id_perumahan, 
                count(pp.progress)/(select count(id_pekerjaan) from data_pekerjaan)  as 'total' from progress_pembangunan pp
                where pp.progress='1'
                group by pp.id_perumahan";

    $tampil_per=mysqli_query($mysqli,$sql_per);

    
    while($h_per = mysqli_fetch_array($tampil_per)){
        $kvl = "SELECT count(kav_id) as 'jlh_kvlg'
        from data_kavling where id_perumahan='".$h_per['id_perumahan']."'";

        $kvling=mysqli_query($mysqli,$kvl);

        $h = mysqli_fetch_array($kvling);

        $total = $h_per['total']/$h['jlh_kvlg'];
        $totall = (number_format($total, 2, '.', ''))*100;

        $perumahan = $mysqli->query(
                "SELECT nama_perumahan FROM data_perumahan
                WHERE id_perumahan= '$h_per[id_perumahan]'
            ")->fetch_object()->nama_perumahan;

        echo $perumahan;
        
        echo "
            <div class='progress' style='height:100%'>
                <div class='progress-bar progress-bar-striped active' role='progressbar' aria-valuenow='".$totall."' aria-valuemin='10' aria-valuemax='100' style='width:".$totall."%'>
                    Progress:".$totall."%
                </div>
            </div>
        ";
    }

    $sql_pern = "SELECT p.id_perumahan, p.nama_perumahan from data_perumahan p where id_perumahan NOT IN(select id_perumahan from progress_pembangunan) ";

    $tampil_pern=mysqli_query($mysqli,$sql_pern);

    while($h_pern = mysqli_fetch_array($tampil_pern)){
        echo $h_pern['nama_perumahan'];
        
        echo "
            <div class='progress'>
                <div class='progress-bar progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:0'>
                    Progress:0%
                </div>
            </div>
        ";
    }
	

?>