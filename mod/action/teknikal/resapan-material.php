<?php
	include '../../../config/connectdb.php';
	error_reporting(0);

	$id = $_POST['v1'];
    $id2 = $_POST['v2'];
    
    echo "Perumahan : <b>".$id."</b><br/>";
    echo "Kavling : <b>".$id2."</b><br/>";

    $total_ambilan = $mysqli->query(
        "SELECT pp.total_ambilan
        from progress_pembayaran_kontrak pp, kontrak_pekerjaan kp 
        where kp.id_perumahan='$id' and kp.no_kavling='$id2'
        and kp.id_kontrak=pp.id_kontrak
        order by pp.id_progress_pembayaran desc limit 1 
        ")->fetch_object()->total_ambilan;

    echo "Total Pembayaran Upah Kerja: <b>". number_format($total_ambilan,0,',',',')."</b><br/><br/>";

        echo "
            <table id='Bkavling' class='table table-bordered table-hover'>
            <thead>
                <tr>
					<th width='10%'>No.</th>
                    <th width='20%'>Nama Material</th>
                    <th width='20%'>Jlh Material Dikeluarkan</th>
                    <th width='20%'>Jlh Return Material</th>
                    <th width='20%'>Jlh Material Habis Pakai</th>
                    <th width='15%''>Harga Material(Rp.)</th>
                    <th width='15%''>Total Harga(Rp.) </th>
					<th width='5%'>Tanggal Kebutuhan Material</th>
                </tr>
            </thead>
            <tbody>
        ";
       
             
        include '../../../config/connectdb.php';
        $sql = mysqli_query($mysqli,"SELECT d.jumlah_keluar,m.nama_material, m.harga_material, d.tgl_pengeluaran, d.id_material
            from data_pengeluaran p,material m,data_perumahan pr,data_kavling k,data_detailkeluar d
            where p.id_pengeluaran=d.id_pengeluaran 
            and d.id_material=m.id_material 
            and pr.id_perumahan=d.id_perumahan 
            and pr.id_perumahan=k.id_perumahan 
            and d.no_kavling=k.no_kavling 
            and pr.id_perumahan='$id'
            AND k.no_kavling ='$id2'
            group by d.id_detailkeluar");

        $no = 1;

        $keluar = "";
        $masuk = "";

        $tot="";
        $pakai = "";
        $tpakai = "";

        while ($q = mysqli_fetch_array($sql)) {
        ?>
        <tr align='center'>
			<td><?php echo  $no;?></td>
            <td><?php echo $q['nama_material']; ?></td>
            <td>
                <?php 
                    echo  $q['jumlah_keluar']; 
                    $keluar += $q['jumlah_keluar'];
                ?>
                        
            </td>
                <?php
                    $kembali = $mysqli->query(
                        "SELECT jumlah_kembali as 'kembali' FROM retur_detail
                        WHERE id_material = '$q[id_material]' 
                        ")->fetch_object()->kembali;
                    echo "<td>$kembali</td>";

                    $masuk += $kembali;

                    if(isset($kembali)){
                        echo "<td>";   
                        $pakai = $q['jumlah_keluar'] - $kembali;
                        echo $pakai."</td>";
                    } else{
                        echo "<td align='right'>";
                        $pakai = $q[jumlah_keluar];
                        echo "</td>";    
                    }
                ?>
            <td align='right'><?php echo  number_format($q['harga_material'],0,',',','); ?></td>
            <td align='right'>
                <?php 
                    $total =""; 
                    $total = $pakai * $q['harga_material'];
                    $tot+=$total;
                    echo number_format($total,0,',',',');
                ?>
            </td>
			<td>
                <?php echo  $q['tgl_pengeluaran']; ?>
            </td>
        </tr>
        <?php
            $no++;
        }
        ?>

        </tbody>
        <tr align="center">
            <td colspan="2" align="center"> Total Resapan Dana Material</td>
            <td> <?= $keluar ?> </td>
            <td> <?= $masuk ?> </td>
            <td> <?= $tpakai ?></td>
            <td></td>
            <td align='right'><?php echo "<b>".number_format($tot,0,',','.')."</b>"; ?></td>
            <td></td>
        </tr>
        <tr align="center" style="font-size:18px;">
            <td colspan="4" align="center"><b> Total Resapan Keseluruhan Dana = Total Upah + Total Dana Material</b></td>

            <td colspan="2"><b> <?= number_format($total_ambilan,0,',','.') ?> + <?= number_format($tot,0,',','.') ?> = </b></td>

            <?php $resapan_dana = $tot + $total_ambilan ?>

            <td align='right' colspan="2"><?php echo "<b>".number_format($resapan_dana,0,',','.')."</b>"; ?></td>
            
        </tr>
    </table>  
<script type="text/javascript">
$(function() {
	$("#Bkavling").DataTable();
});
</script>

