<?php
	include '../../../config/connectdb.php';
	error_reporting(0);

	$mod=$_GET["mod"];
	$act=$_GET["act"];

	if(isset($mod) AND $act=='tambahkontrak'){
		$id_kontrak=$_GET['id'];
		$perumahan=$_POST['perumahan'];
		$no_kav=$_POST['no_kavling'];
		$pegawai=$_POST['nama_pegawai'];
		$tglselesai = $_POST['tgl'];
		$tipe = $_POST['tipe_krj'];
		$nilai = $_POST['nilai'];
		$kontraktor = $_POST['kontraktor'];

		$hrgs = substr($nilai,4,11);     
		$hrg = str_replace(',', '', $hrgs);
		
		$sql_insert = $mysqli->query(
				"INSERT INTO kontrak_pekerjaan (id_kontrak,pengawas,id_perumahan,no_kavling,id_kategorikerja,nilai_kontrak,tgl_selesai, kontraktor)
				VALUES('','$pegawai','$perumahan','$no_kav','$tipe','$hrg','$tglselesai','$kontraktor')"
				);

		$sql_p = $mysqli->query("SELECT id_pekerjaan
								FROM data_pekerjaan 
								WHERE id_kategorikerja='$tipe'");
		$row = array();
		while($r = mysqli_fetch_assoc($sql_p)) {
		    $row[] = $r;
		}
		$h1 = json_encode($row);

		$h2 = json_decode($h1);

		foreach ($h2 as $data) {
			$sql_insert2 = $mysqli->query(
			"INSERT INTO progress_pembangunan (pengawas, id_perumahan, no_kavling, id_pekerjaan)
			VALUES('$pegawai','$perumahan','$no_kav','$data->id_pekerjaan' )"
			);
			if($sql_insert2){
			?>
				<script type='text/javascript'>
						windows.alert('success');
					</script>
				<?php
			}
		}
		
		echo "<script type='text/javascript'>
			document.location='../../mod_teknikal/index.php?mod=kontrak_pekerjaan' 
			</script>";

	}else if(isset($mod) AND $act=='tambahkontrakbaru'){
		$id_kontrak=$_GET['id'];	
		$perumahan=$_POST['perumahan'];
		$no_kav=$_POST['no_kavling'];
		$pegawai=$_POST['nama_pegawai'];
		$tglselesai = $_POST['tgl'];
		$tipe = $_POST['tipe_krj'];
		$nilai = $_POST['nilai'];
		$kontraktor = $_POST['kontraktor'];

		$hrgs = substr($nilai,4,11);     
		$hrg = str_replace(',', '', $hrgs);

		$sql_insert2 = $mysqli->query(
				"INSERT INTO kontrak_pekerjaan (pengawas,id_perumahan,no_kavling,id_kategorikerja,nilai_kontrak,tgl_selesai, kontraktor)
				VALUES('$pegawai','$perumahan','$no_kav','$tipe','$hrg','$tglselesai','$kontraktor')"
				);

		$sql_p = $mysqli->query("SELECT id_pekerjaan
								FROM data_pekerjaan 
								WHERE id_kategorikerja='$tipe'");
		$row = array();
		while($r = mysqli_fetch_assoc($sql_p)) {
		    $row[] = $r;
		}
		$h1 = json_encode($row);

		$h2 = json_decode($h1);

		foreach ($h2 as $data) {
			$sql_insert2 = $mysqli->query(
			"INSERT INTO progress_pembangunan (pengawas, id_perumahan, no_kavling, id_pekerjaan)
			VALUES('$pegawai','$perumahan','$no_kav','$data->id_pekerjaan' )"
			);
			if($sql_insert2){
			?>
				<script type='text/javascript'>
						windows.alert('success');
					</script>
				<?php
			}
		}
		
		echo "<script type='text/javascript'>
		document.location='../../mod_teknikal/index.php?mod=detail_kontrak&idp=$perumahan&idk=$no_kav&idpp=$pegawai' 
		</script>";
	}else if(isset($mod) AND $act=='tambahkontraklanjutan'){
		$id_kontrak=$_POST['id_kontrak'];	
		$pegawai=$_POST['nama_pegawai'];
		$kontraktor = $_POST['kontraktor'];
		$sisa_kontrak = $_POST['sisa_kontrak'];
		$tambahan =$_POST['tambahan'];
		$nilai_baru = $_POST['sisa_kontrak_baru'];
		$tglselesai = $_POST['tgl'];

		$sql_insert2 = $mysqli->query(
				"INSERT INTO kontrak_lanjutan (id_kontrak,pengawas, kontraktor, sisa_kontrak_lama, nilai_tambah, nilai_kontrak, tgl_selesai)
				VALUES('$id_kontrak','$pegawai','$kontraktor','$sisa_kontrak','$tambahan','$nilai_baru','$tglselesai')"
				);

		$sql_p = $mysqli->query("SELECT id_pekerjaan
								FROM data_pekerjaan 
								WHERE id_kategorikerja='$tipe'");
		$row = array();
		while($r = mysqli_fetch_assoc($sql_p)) {
		    $row[] = $r;
		}
		$h1 = json_encode($row);

		$h2 = json_decode($h1);

		foreach ($h2 as $data) {
			$sql_insert2 = $mysqli->query(
			"INSERT INTO progress_pembangunan (pengawas, id_perumahan, no_kavling, id_pekerjaan)
			VALUES('$pegawai','$perumahan','$no_kav','$data->id_pekerjaan' )"
			);
			
			if($sql_insert2){
			?>
				<script type='text/javascript'>
						windows.alert('success');
					</script>
				<?php
			}
		}
		
		echo "<script type='text/javascript'>
		document.location='../../mod_teknikal/index.php?mod=detail_kontrak&idp=$perumahan&idk=$no_kav&idpp=$pegawai' 
		</script>";
	}else if(isset($mod) AND $act=='bayar_upah'){

		$upahs = $_POST['upah_bayar'];
		$id_kontrak = $_POST['id_kontrak'];
		$progress = $_POST['progress'];
		$biaya_progress = $_POST['biaya_progress'];
		$keterangan = $_POST['keterangan'];
		$id_perumahan = $_POST['id_perumahan'];
		$no_kavling = $_POST['no_kavling'];
		$uang_progress = $_POST['uang_progress'];

		$upahss = substr($upahs,4,11);     
		$upah = str_replace(',', '', $upahss);

		$tgl_cek = $mysqli->query(
				"SELECT tgl_laporan from progress_pembangunan pp where progress='1' and id_perumahan='$id_perumahan' and no_kavling='$no_kavling' order by id_progress desc LIMIT 1
			")->fetch_object()->tgl_laporan;

		$tot = $mysqli->query(
                "SELECT total_ambilan FROM progress_pembayaran_kontrak 
                WHERE id_kontrak='$id_kontrak' 
                ORDER BY id_progress_pembayaran DESC limit 1
                ")->fetch_object()->total_ambilan;

		$nilai_kontrak = $mysqli->query(
                "SELECT nilai_kontrak FROM kontrak_pekerjaan
                WHERE id_kontrak = '$id_kontrak'")->fetch_object()->nilai_kontrak;

		$temp = $upah+$tot;
		
		$sisa2 = $mysqli->query(
                "SELECT sisa_saldo
                from  saldo_teknikal
                order by id desc limit 1 
                ")->fetch_object()->sisa_saldo;

		if($temp > $sisa2){
			echo "
			<div class='alert alert-danger fade in form-group' >
		    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
		    	<strong>Upah yang dibayar melebihi Saldo</strong> Silahkan Klik Reset Untuk Input ulang Harga Upah
		    </div>
		  ";
		}elseif($temp > $uang_progress){
			echo "
			<div class='alert alert-warning fade in form-group' >
		    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
		    	<strong>Upah yang dibayar melebihi upah sesuai dengan progress</strong> Silahkan Klik Reset Untuk Input ulang Harga Upah
		    </div>
		  ";
		}
		elseif($temp > $nilai_kontrak){
			echo "
			<div class='alert alert-danger fade in form-group' >
		    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
		    	<strong>Upah yang dibayar melebihi nilai kontrak</strong> Silahkan Klik Reset Untuk Input ulang Harga Upah
		    </div>
		  ";
		}
		else{
			$query = mysqli_query($mysqli,"SELECT max(id_progress_pembayaran) 
				AS idMAX FROM progress_pembayaran_kontrak");
			$data = mysqli_fetch_array($query);

			$id = $data['idMAX'];

			$noUrut = (int) substr($id, 4, 5); 
			$noUrut++;

			$chartetap = 'KWU-';
			$id2 = $chartetap.sprintf("%05s", $noUrut);

			$total_ambilan = $mysqli->query(
				"SELECT total_ambilan FROM progress_pembayaran_kontrak
				WHERE id_progress_pembayaran = '$id' 
				")->fetch_object()->total_ambilan;

			$total_a = "";
			if($total_ambilan==0){
				$total_a=$upah;
			}else{
				$total_a = $total_ambilan+$upah;
			}

			$time = date("Y-m-d");

			$sql_insert = $mysqli->query("INSERT INTO progress_pembayaran_kontrak (id_progress_pembayaran,id_kontrak,harga_upah, tanggal_pembayaran, progress, nilai_progress, keterangan, total_ambilan, tgl_cek_progress)
			VALUES('$id2','$_POST[id_kontrak]','$upah','$time', '$progress','$biaya_progress','$keterangan', '$total_a','$tgl_cek')"
			);

			$to = $sisa2-$upah;

			$sql_inserttt = $mysqli->query("INSERT INTO saldo_teknikal (tgl, debit, kredit, sisa_saldo, keterangan )
			VALUES('$time', '0','$upah', '$to', '$keterangan')");

			echo "
				<div class='alert alert-success fade in form-group' >
					<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
			    	<strong>Pembayaran Sukses</strong> Silahkan Print Kwintansi
	    		</div>
			    
			  ";
		
			}
			
	}


?>