<html>
<head>
	<title>Print Upah Kerja</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="../../assets/css/dataTables.bootstrap.css"/>
	<link rel="stylesheet" href="../../assets/css/bootstrap-theme.min.css">
	<link href="../../../images/pavicon.png" rel="icon" type="image/x-icon" />
</head>
<body>
	<br><br>
	<img src="../../../images/logo.png" align="right"><br>
	<font size="5%"><b>BUKTI AMBILAN TUKANG</b></font><br><hr>
	<style>
		.table.no-border tr td, .table.no-border tr th {
		  border-width: 0;
		}
	</style>
	<table class="table no-border" border='0'>
		<?php
			include '../../../config/connectdb.php';
			$id = $_GET['id'];
			$id2 = $_GET['id2']; // pekerjaan
			$id3 = $_GET['id3']; // perumahan
			$id4 = $_GET['id4']; // no kavling

			$id_max = $mysqli->query(
            "SELECT max(id_progress_pembayaran) as 'id_max'
            FROM progress_pembayaran_kontrak
            WHERE id_kontrak = '$id'
            ")->fetch_object()->id_max;

			$kontraktor = "";

			$max = "SELECT kp.kontraktor, pk.tanggal_pembayaran, pk.id_progress_pembayaran, pk.id_kontrak, pk.harga_upah, pk.progress, pk.nilai_progress, pk.keterangan from progress_pembayaran_kontrak pk, kontrak_pekerjaan kp where pk.id_progress_pembayaran='$id_max' order by pk.id_progress_pembayaran DESC limit 1";

            $t = mysqli_query($mysqli,$max);
            
            while ($r = mysqli_fetch_array($t)) {
                echo "
                	<tr><td> Tanggal </td><td> : </td> <td> $r[tanggal_pembayaran]</td><td></td> </tr>
                	<tr><td> Nomor </td><td> : </td> <td> $id_max </td><td> KAVLING</td> </tr>
                	<tr><td> Sudah diterima oleh </td><td> : </td> </td> <td> $r[kontraktor]</td> <td> <b>$id4</b> </td> </tr>
                	<tr><td> Untuk Pembayaran </td><td> : </td> <td> $r[keterangan]</td> <td>PERUMAHAN</td> </tr>
                	<tr><td> Pekerjaan </td><td> : </td><td> $id2 </td> <td> <b>$id3</b></td> </tr>
                	<tr><td> Jumlah Ambilan </td><td> : </td> <td> Rp. ".number_format($r['harga_upah'],0,',','.')."</td> <td></td> </tr>
                ";
                $kontraktor = $r['kontraktor'];
            }

		?>
	</table> <hr>

	<div class="row">
	    <div class="col-xs-4" >
	      	<br>
	      	<br>
	    </div>
	    <div class="col-xs-4" >
	      	<br>
	      	<br>
	    </div>
	    <div class="col-xs-4" >
	      	<br>
	      	<br>
	    </div>
	  </div>
	</div>

	<div class="container">
	  <div class="row">
	    <div class="col-xs-4" >
	      Diterima Oleh
	    </div>
	    <div class="col-xs-4" >
	      Dibuat Oleh
	    </div>
	    <div class="col-xs-4" >
	      Disetujui Oleh
	    </div>
	  </div>
	  <div class="row">
	    <div class="col-xs-4" >
	      	<br>
	      	<br>
	      	<br>
	      	<br>
	    </div>
	    <div class="col-xs-4" >
	      	<br>
	      	<br>
	      	<br>
	      	<br>
	    </div>
	    <div class="col-xs-4" >
	      	<br>
	      	<br>
	      	<br>
	      	<br>
	    </div>
	  </div>
	
	<div class="row">
	    <div class="col-xs-4" >
	      <?= $kontraktor ?>
	    </div>
	    <div class="col-xs-4" >
	      Muhammad Khadafi
	    </div>
	    <div class="col-xs-4" >
	      Roni Hariza
	    </div>
	  </div>

	  <div class="row">
	    <div class="col-xs-4" >
	      Kontraktor
	    </div>
	    <div class="col-xs-4" >
	      Staff Teknikal
	    </div>
	    <div class="col-xs-4" >
	      Manager Teknikal
	    </div>
	  </div>
	</div>


	<script>
		window.load = print_u();
		function print_u(){
			window.print();
		}
	</script>
</body>
</html>