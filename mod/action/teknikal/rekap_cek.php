<link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../../assets/css/bootstrap-theme.min.css">
<?php

    include '../../../config/connectdb.php';
    error_reporting(0);

	$a=$_GET['tgl_awal'];
	$b=$_GET['tgl_akhir'];
?>
	<div class="container-fluid">
        <div class="row">
            <b style="font-size:15px;">LAPORAN PENARIKAN CEK/GIRO</b>
        </div>
        <div class="row">
            <b>Tanggal : <font color="red"><?= $a ?></font> s/d <font color="red"><?= $b ?></font></b>
        </div>
    </div>
    <br>
    <br>
<?php
    echo "<table id='DLseluruh' class='table table-bordered table-hover'>
        <thead>
            <tr align='center'>
                <th width='5%'>No</th>
                <th width='15%'>Tanggal Penarikan</th>
                <th width='14%'>Jumlah</th>
                <th width='15%'>Keterangan</th>
            </tr>
        </thead>
        <tbody>
        ";

    
	
	$sql = mysqli_query($mysqli,"SELECT * FROM penarikan_cek
								WHERE tanggal_penarikan between '$a' AND '$b'
								ORDER BY tanggal_penarikan ASC");
	$no = 1;
	$tot ="";
	while ($r = mysqli_fetch_array($sql)) {
		$tanggal_penarikan= $r['tanggal_penarikan'];
		$jumlah= $r['jumlah'];
		$keterangan= $r['keterangan'];
 	?>
			<tr align='center'>
				<td align="center"> <?php echo  $no;?> </td>
				<td><?php echo $tanggal_penarikan; ?></td>		
				<td align='right'><?php echo number_format($jumlah,0,',','.') ?></td>
				<td><?php echo $keterangan;  ?></td>
				<?php $tot+= $jumlah; ?>
			</tr>
		   
	<?php
		$no++;
	} 
	?>
			
	</tbody>
			<tr>
				<td colspan='2'><b>Total Penarikan (Rp)</b></td>
				<td align="right"><b><?php echo number_format($tot,0,',','.' );?></b></td>
				<td></td>
			</tr>	
			<tr align="right">
				<td colspan='4'><i class="material-icons btn btn-primary" onclick="print_d()">local_printshop</i></td>
			</tr>
	</table>
	

<script>
	function print_d(){
		<?php
			$a=$_GET['tgl_awal'];
			$b=$_GET['tgl_akhir'];
		
		echo "window.open('../action/teknikal/print_rekap_cek.php?id=$a&id2=$b','_blank');";
		?>
	}
</script>
<script>
$(function() {
         $("#DLseluruh").dataTable();
     });
</script>
