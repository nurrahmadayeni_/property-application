<html>
<head>
	<title>Print Upah Kerja</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="../../assets/css/bootstrap-theme.min.css">
	<link href="../../../images/pavicon.png" rel="icon" type="image/x-icon" />
</head>
<body>
	<hr>
	<img src="../../../images/logo.png" align="right"><br>
	<font size="3%"><center><b>KARTU BUKTI PEMBAYARAN PROGRESS PEMBANGUNAN DENGAN KONTRAKTOR</b></center></font><hr>


	<style>
		.table.no-border tr td, .table.no-border tr th {
		  border-width: 0;
		}
	</style>
	<table class="table no-border" border='0' >
		<?php
			include '../../../config/connectdb.php';

			$id = $_GET['id'];

			$tampil = "select d.nama_perumahan, k.no_kavling, k.kontraktor, kp.nama_kategorikerja,k.nilai_kontrak from kontrak_pekerjaan k, data_perumahan d, data_pekerjaan dp, kategori_pekerjaan kp WHERE k.id_perumahan=d.id_perumahan and dp.id_kategorikerja=kp.id_kategorikerja and k.id_kontrak='$id' GROUP BY k.id_kontrak";

			$queri = mysqli_query($mysqli,$tampil);
			$row = mysqli_fetch_array($queri);
			
			$time = date("Y-m-d");
		
			?>

			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-6 col-sm-3" >Lokasi </div>
					<div class="col-xs-6 col-sm-3" >: <?=$row['nama_perumahan']?> </div>
					<div class="col-xs-6 col-sm-3" >Kontraktor </div>
					<div class="col-xs-6 col-sm-3" >: <?=$row['kontraktor']?> </div>
				</div>
				<div class="row">
					<div class="col-xs-6 col-sm-3" >Kavling </div>
					<div class="col-xs-6 col-sm-3" >: <?=$row['no_kavling']?> </div>
					<div class="col-xs-6 col-sm-3" >Jenis Pekerjaan </div>
					<div class="col-xs-6 col-sm-3" >: <?=$row['nama_kategorikerja']?> </div>
				</div>
				<div class="row">
					<div class="col-xs-6 col-sm-3" >Ukuran/Tipe/Kavling </div>
					<div class="col-xs-6 col-sm-3" >: <?=$row['no_kavling']?> </div>
					<div class="col-xs-6 col-sm-3" >Nilai Borongan </div>
					<div class="col-xs-6 col-sm-3" >: <?=number_format($row['nilai_kontrak'],0,',','.')?> </div>
				</div>
				<div class="row">
					<div class="col-xs-6 col-sm-3" >Tanggal </div>
					<div class="col-xs-6 col-sm-3" >: <?= $time ?> </div>
				</div>
			</div>

		    <table class='table table-bordered table-hover'>
		        <thead align="center">
		            <tr > 
		                 <th width='5%'>No</th>
		                 <th width='27%'>Tgl Cek Progress   &nbsp; </th>
		                 <th width='20%'>Tgl Pengambilan Uang</th>
		                 <th width='5%'>Kemajuan Progress</th>
		                 <th width='0%'>Progress</th>
		                 <th width='15%'>Nilai Uang Sesuai Progress(Rp.)</th>
		                 <th width='13%'>Ambilan Kontraktor(Rp.)</th>
		                 <th width='15%'>Total Ambilan(Rp.)</th>
		                 <th width='10%'>Paraf Kontraktor</th>
		 					<th width='15%'>Paraf Manager Teknikal</th>
		 					<th width='15%'>Paraf Manager ADM & Keungan</th>

		            </tr>
		        </thead>
		       <?php
		   
				    $sql = mysqli_query($mysqli,"SELECT * from progress_pembayaran_kontrak pk where id_kontrak='$id'");
				    
				    $no=1;
				    while($q = mysqli_fetch_array($sql)){
				        echo "
				            <tr align='center'>
				                <td> $no </td>
				                <td width='25%'> ".$q['tgl_cek_progress']."  </td>
				                <td> ".$q['tanggal_pembayaran']."  </td>
				                <td> ".$q['keterangan']."  </td>
				                <td> ".$q['progress']."% </td>        
				                <td align='right'> ".number_format($q['nilai_progress'],0,',','.')."  </td>
				                <td align='right'> ".number_format($q['harga_upah'],0,',','.')." </td>
				                <td align='right'> ".number_format($q['total_ambilan'],0,',','.')." </td>
				 					<td></td>
				 					<td></td>
				 					<td></td>
				            </tr>
				        ";
				        $no++;
				    }
				    
					?>
			</table>

	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-6 col-sm-3" >Kontraktor </div>
			<div class="col-xs-6 col-sm-3" >Staff Teknikal</div>
			<div class="col-xs-6 col-sm-3" >Manager Teknikal </div>
			<div class="col-xs-6 col-sm-3" >Direktur Utama</div>
		</div>

		<div class="row">
			<div class="col-xs-4" ><br><br><br><br></div>
			<div class="col-xs-4" ><br><br><br><br></div>
			<div class="col-xs-4" ><br><br><br><br></div>
		</div>
		<div class="row">
			<div class="col-xs-6 col-sm-3" ><?=$row['kontraktor']?></div>
			<div class="col-xs-6 col-sm-3" ></div>
			<div class="col-xs-6 col-sm-3" ></div>
			<div class="col-xs-6 col-sm-3" ></div>
		</div>

	</div>

			<style type="text/css" media="print">
			  @page { size: landscape; }
			</style>
	<script>
		window.load = print_t();
		function print_t(){
			window.print();
		}
	</script>
</body>
</html>