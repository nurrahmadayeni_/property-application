<html>
<head>
	<title>Print Rekapitulasi Penarikan Cek</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="../../assets/css/bootstrap-theme.min.css">
	<link href="../../../images/pavicon.png" rel="icon" type="image/x-icon" />
</head>
<body><br><br>
	<img src="../../../images/logo.png" align="right"><br>


	<?php

		error_reporting(0);
		include '../../../config/connectdb.php';

		$id = $_GET['v1'];
		$id3 = $_GET['v2'];
		$id4 = $_GET['v3'];

		 $perumahan = $mysqli->query(
        "SELECT nama_perumahan FROM data_perumahan
        WHERE id_perumahan = '$id' 
        ")->fetch_object()->nama_perumahan;
	?>
	<div class="container-fluid">
        <div class="row"><h4>
            REKAPITULASI PENGELUARAN DANA PERUMAHAN <?php echo "<font style='text-transform: capitalize;'>".$perumahan."</font>"; ?> <br></h4>
        </div>
        <div class="row">
            <h4>DIVISI TEKNIKAL PT. MATAHARI CIPTA</h4>
        </div>
        <div class="row">
            <h5>Tanggal : <?= $id3 ?> s/d <?= $id4 ?></h5>
        </div>
    </div>
    <br>
	
	 <table id='detail' class='table table-bordered table-hover' >
       <thead>
           <tr> 
                <th width='5%'>No</th>
                <th width='20%'>Hari / Tanggal </th>
                <th width='12%'>Uraian</th>
                <th width='10%'>Kavling</th>
                <th width='15%'>Pekerjaan</th>
                <th width='10%'>Kontraktor</th>
                <th width='15%'>Jumlah(Rp.)</th>
           </tr>
       </thead>
				<?php
					
                   $sql = mysqli_query($mysqli,"SELECT pp.tanggal_pembayaran, pp.keterangan, kp.no_kavling, k.nama_kategorikerja,
				        kp.kontraktor, pp.harga_upah
				        FROM kontrak_pekerjaan kp,
				        progress_pembayaran_kontrak pp,
				        data_perumahan d,
				        kategori_pekerjaan k
				        WHERE k.id_kategorikerja=kp.id_kategorikerja
				        and kp.id_perumahan=d.id_perumahan
				        AND pp.id_kontrak=kp.id_kontrak
				        AND kp.id_perumahan='$id'
				        AND tanggal_pembayaran BETWEEN '$id3' AND '$id4'
				");
    
			    $no=1;
			    $total="";
			    while($q = mysqli_fetch_array($sql)){
			        echo "
			            <tr align='left'>
			                <td> $no </td>
			                <td> ".$q['tanggal_pembayaran']."  </td>
			                <td> ".$q['keterangan']."  </td>
			                <td> ".$q['no_kavling']."</td>
			                <td> ".$q['nama_kategorikerja']." </td>
			                <td> ".$q['kontraktor']." </td>
			                <td align='right'> ".number_format($q['harga_upah'],0,',','.')." </td>
			            </tr>

			        ";
			         $total+=$q['harga_upah'];

			        $no++;
			    }
			    echo "<tr> 
		                <td colspan=6 align='center'><b> TOTAL </td>
		                <td align='right'><b>".number_format($total,0,',','.')."</b></td>
		           </tr>";

			    echo"</table>";
			    ?>

			</body>
			</html>
			<style type="text/css" media="print">
			  @page { size: landscape; }
			</style>
				<script>
					window.load = print_t();
					function print_t(){
						window.print();
					}
				</script>