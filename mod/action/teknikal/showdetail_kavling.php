<?php
		error_reporting(0);
		include '../../../config/connectdb.php';
		
		$id= $_GET['id'];

		$perumahan = $mysqli->query(
                "SELECT nama_perumahan 
                FROM data_perumahan
                WHERE id_perumahan = '$id'
                ")->fetch_object()->nama_perumahan;
		?>
		
		
		<h4 align=center>DETAIL KAVLING PERUMAHAN</h4>
		<table id="detail" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th width="5%">No</th>
				<th width="5%">Tipe Kavling</th>  
				<th width="5%">Nomor Kavling</th>  
				<th width="10%">Luas Tanah</th>
				<th width="15%">Harga Tanah Per Kavling (Rp)</th>
				<th width="15%">Kebutuhan Material</th>
				<th width="12%">Return Material</th>
				<th width="17%">Resapan Dana Material</th>
			</tr>
		 </thead>
		<tbody>
		<?php

		echo "<h5><b>".$perumahan."</b></h5>";

		$sql = mysqli_query($mysqli,"SELECT dk.kav_id, tr.type_rumah, dk.no_kavling, tr.luas_tanah, tr.harga_kavling, dk.mapping, dp.id_perumahan, dp.id_perumahan from type_rumah tr, data_kavling dk, data_perumahan dp where tr.id_type=dk.id_type and dk.id_perumahan=dp.id_perumahan and dp.id_perumahan='$id'");
		
		$no = 1;
		while ($r = mysqli_fetch_array($sql)) {
			$id_perumahan = $r['id_perumahan'];
			$no_kvl = $r['no_kavling'];
			echo"
				<tr align='center'>	
					<td> $no </td>
					<td> $r[type_rumah] </td>
					<td> $r[no_kavling] </td>
					<td> $r[luas_tanah] </td>
					<td align='right'> ".number_format($r[harga_kavling],0,',',',') ."</td>

					<td> <i data-id='$id_perumahan' data-id2='$no_kvl' class='kebutuhan-material material-icons btn btn-primary' >details</i> </td>
					<td>
						<i class='material-icons return-material btn btn-primary' data-id='$id_perumahan' data-id2='$no_kvl'>details</i>
					</td>
					<td> <i data-id='$id_perumahan' data-id2='$no_kvl' class='resapan-material material-icons btn btn-primary' >details</i> </td>
				</tr>
			";
			$no++;
		}
		?>
	</tbody>
</table>

<div class="modal fade" id="kebutuhan-material" tabindex="-1" role="dialog" aria-labelledby="detail" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Kebutuhan Material</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="return-material" tabindex="-1" role="dialog" aria-labelledby="detail" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Return Material</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="resapan-material" tabindex="-1" role="dialog" aria-labelledby="detail" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Resapan Material</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
  $(function() {
		 $("#detail").dataTable();
  });

  $(document).on('click','.kebutuhan-material',function(e){
        e.preventDefault();

        var val1 = $(this).attr('data-id');
        var val2 = $(this).attr('data-id2');
       
        $("#kebutuhan-material").modal('show');

        $.post('../action/teknikal/kebutuhan_material.php',
            {v1: val1, v2: val2},
            function(html){
                $(".modal-body").html(html);
            }   
        );
    });

  $(document).on('click','.return-material',function(e){
        e.preventDefault();

        var val1 = $(this).attr('data-id');
        var val2 = $(this).attr('data-id2');
       
        $("#return-material").modal('show');

        $.post('../action/teknikal/return_material.php',
            {v1: val1, v2: val2},
            function(html){
                $(".modal-body").html(html);
            }   
        );
    });

  $(document).on('click','.resapan-material',function(e){
        e.preventDefault();

        var val1 = $(this).attr('data-id');
        var val2 = $(this).attr('data-id2');
       
        $("#resapan-material").modal('show');

        $.post('../action/teknikal/resapan-material.php',
            {v1: val1, v2: val2},
            function(html){
                $(".modal-body").html(html);
            }   
        );
    });

</script>