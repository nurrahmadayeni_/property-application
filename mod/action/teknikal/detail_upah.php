<link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../../assets/css/bootstrap-theme.min.css">
<?php

	include '../../../config/connectdb.php';

	$id = $_POST["v1"]; // perumahan
	$id2 = $_POST["v2"]; // kavling
	$id3 = $_POST["v3"]; // id_kontrak
	$id4 = $_POST["v4"]; // kontraktor
	$id5 = $_POST["v5"]; // pekerjaan

    $perumahan = $mysqli->query(
        "SELECT nama_perumahan FROM data_perumahan
        WHERE id_perumahan = '$id' 
        ")->fetch_object()->nama_perumahan;

    $nilai_kontrak = $mysqli->query(
        "SELECT nilai_kontrak FROM kontrak_pekerjaan
        WHERE id_kontrak = '$id3' 
        ")->fetch_object()->nilai_kontrak;

    date_default_timezone_set("Asia/Bangkok");
    $time = date('Y-m-d H:i:s');
?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6 col-sm-3" >Lokasi </div>
            <div class="col-xs-6 col-sm-3" >: <?= $perumahan ?> </div>
            <div class="col-xs-6 col-sm-3" >Kontraktor </div>
            <div class="col-xs-6 col-sm-3" >: <?=$id4 ?> </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-3" >Kavling </div>
            <div class="col-xs-6 col-sm-3" >: <?=$id2?> </div>
            <div class="col-xs-6 col-sm-3" >Jenis Pekerjaan </div>
            <div class="col-xs-6 col-sm-3" >: <?=$id5?> </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-3" >Ukuran/Tipe/Kavling </div>
            <div class="col-xs-6 col-sm-3" >: <?= $id2 ?> </div>
            <div class="col-xs-6 col-sm-3" >Nilai Borongan </div>
            <div class="col-xs-6 col-sm-3" >: <?=number_format($nilai_kontrak,0,',','.')?> </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-3" >Tanggal </div>
            <div class="col-xs-6 col-sm-3" >: <?= $time ?> </div>
            <div class="col-xs-6 col-sm-3" >Pekerjaan yang telah selesai </div>
            <div class="col-xs-6 col-sm-3" >
                <?php
                    $id_k = $mysqli->query(
                        "SELECT id_kategorikerja 
                        FROM kategori_pekerjaan
                        WHERE nama_kategorikerja = '$id5'
                        ")->fetch_object()->id_kategorikerja;

                    $kesiapan = "SELECT pp.id_pekerjaan, d.nama_pekerjaan from data_pekerjaan d, progress_pembangunan pp WHERE d.id_kategorikerja in (select id_kategorikerja from kontrak_pekerjaan ) and pp.id_pekerjaan=d.id_pekerjaan and pp.progress='1' and pp.id_perumahan='$id' AND pp.no_kavling='$id2' AND d.id_kategorikerja='$id_k'";

                    $t = mysqli_query($mysqli,$kesiapan);
                    $siap = mysqli_num_rows($t);
                    
                    if(!empty($siap)){
                        while ($r = mysqli_fetch_array($t)) {
                            echo "<li>".$r['nama_pekerjaan']."</li>";
                        }
                    }else{
                        echo "<li> - </li>";
                    }
                    
                ?>
            </div>
        </div>
    </div>
    <br>
    <?php

    echo "
    <table id='detail' class='table table-bordered table-hover' >
       <thead>
           <tr> 
                <th width='5%'>No</th>
                <th width='20%'>Tgl Pengambilan Uang</th>
                <th width='12%'>Kemajuan Progress</th>
                <th width='10%'>Progress</th>
                <th width='20%'>Nilai Uang Sesuai Progress</th>
                <th width='13%'>Ambil Kontraktor</th>
                <th width='15%'>Total Ambilan</th>
           </tr>
       </thead>
   
        ";

    $sql = mysqli_query($mysqli,"SELECT * from progress_pembayaran_kontrak pk where id_kontrak='$id3'");
    
    $no=1;
    while($q = mysqli_fetch_array($sql)){
        echo "
            <tr align='left'>
                <td> $no </td>
                <td> ".$q['tanggal_pembayaran']."  </td>
                <td> ".$q['keterangan']."  </td>
                <td> ".$q['progress']."% </td>
                <td align='right'> ".number_format($q['nilai_progress'],0,',','.')."  </td>
                <td align='right'> ".number_format($q['harga_upah'],0,',','.')." </td>
                <td align='right'> ".number_format($q['total_ambilan'],0,',','.')." </td>
            </tr>
        ";
        $no++;
    }
    echo"</table>";

?>
<div class="modal-footer">
    <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
    <i class="material-icons btn btn-primary" onclick="print_t()">local_printshop</i></a>
</div>
<script>
    function print_t(){
        window.open("../action/teknikal/print_upah_total.php?id=<?php echo $id3;?>","_blank");
    }
    $(function() {
         $("#detail").dataTable();
     });
</script>

