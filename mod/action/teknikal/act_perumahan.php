<?php
	include '../../../config/connectdb.php';
	//error_reporting(0);

	$mod=$_GET["mod"];
	$act=$_GET["act"];
	

	if(isset($mod) AND $act=='tambahteknikal'){
		$nama = $_POST['nama_perumahan'];
		$desk = $_POST['deskripsi'];
		$jlhrmh = $_POST['jlh_rumah'];
		$spek =$_POST['spekteknikrmh'];
		$ltanah = $_POST['ltanah'];
		$hrga = $_POST['hrga'];
		$fileName = $_FILES['foto']['name'];

		//auto increment utk id
		$query = mysqli_query($mysqli,"SELECT max(id_perumahan) AS idMAX FROM data_perumahan");
		$data = mysqli_fetch_array($query);

		$id = $data['idMAX'];

		$noUrut = (int) substr($id, 4, 5); //'4' menandakan karakter yang tetap ('SPL-'), '5' menandakan karakter auto increment
		$noUrut++;

		$chartetap = 'PRM-';
		$id2 = $chartetap.sprintf("%05s", $noUrut);

		//batas akhir deklarasi $id
		$sql_cek = $mysqli->query(
			"SELECT nama_perumahan FROM data_perumahan
			WHERE nama_perumahan='$nama'"
		);

		$cek = mysqli_num_rows($sql_cek);

		if($cek > 0){
			echo "
					<script>
					window.alert('Perumahan dengan nama tersebut sudah ada');					
					javascript:history.go(-1);
				</script>
			";
		}else{
			move_uploaded_file($_FILES['foto']['tmp_name'], "../../mod_teknikal/gbr_perumahan/".$_FILES['foto']['name']);

			$sql_insert = $mysqli->query(
					"INSERT INTO data_perumahan (id_perumahan,nama_perumahan,deskripsi,spesifikasi_teknik,jumlah_kavling,harga_rumah)
					VALUES('$id2', '$nama', '$desk', '$spek', '$jlhrmh','$hrga')"
					);
					
			$sql_insert2 = $mysqli->query(
			"INSERT INTO foto_perumahan(id_perumahan,foto_rumah)
			VALUES('$id2','$fileName')"
			);
			
			if($sql_insert){
				$no_kavling = chr(rand(65,90));
				$sql_c = $mysqli->query("SELECT no_kavling FROM
										data_kavling
										WHERE id_perumahan='$id2'"
									);

				$cek2 = mysqli_num_rows($sql_c);
				if($cek2>0){
					$no_kavling2 = chr(rand(65,90));
					for($i=1; $i<=$jlhrmh; $i++){
						$mysqli->query(
						"INSERT INTO data_kavling (no_kavling, id_perumahan, luas_tanah)
						VALUES('$no_kavling2$i','$id2','$ltanah')"
						);
					}
				} else{
					for($i=1; $i<=$jlhrmh; $i++){
						$mysqli->query(
						"INSERT INTO data_kavling (no_kavling, id_perumahan , luas_tanah)
						VALUES('$no_kavling$i','$id2','$ltanah')"
						);
					}
				}				
			}
		}
		
		echo "<script type='text/javascript'>
			document.location='../../mod_teknikal/index.php?mod=data_teknis' 
			</script>";
	} 
	
	if(isset($mod) AND $act=='tambahpengawas'){
		$nama = $_POST['nama_pengawas'];
		$alamat = $_POST['alamat'];
		$telp = $_POST['telp'];
		$email = $_POST['email'];


		//auto increment utk id
		$query = mysqli_query($mysqli,"SELECT max(id_pengawas) AS idMAX FROM data_pengawas");
		$data = mysqli_fetch_array($query);

		$id = $data['idMAX'];

		$noUrut = (int) substr($id, 4, 5); //'4' menandakan karakter yang tetap ('SPL-'), '5' menandakan karakter auto increment
		$noUrut++;

		$chartetap = 'PGW-';
		$id2 = $chartetap.sprintf("%05s", $noUrut);

		$sql_insert = $mysqli->query(
					"INSERT INTO data_pengawas(id_pengawas, nama_pengawas, alamat_pengawas,no_telp,email)
					VALUES('$id2', '$nama', '$alamat','$telp ','$email')"
					);

		if($sql_insert){
				echo "
				<script type='text/javascript'>
					windows.alert('success');
				</script>
			";
		}
		
		echo "<script type='text/javascript'>
		document.location='../../mod_teknikal/index.php?mod=data_pengawas' 
		</script>";
	} 
	
	if(isset($mod) AND $act=='tambahpekerjaan'){
		$nama = $_POST['nama'];
		$tipe_krj = $_POST['tipe_krj'];
		$harga = $_POST['hrg'];

		$hrgH = substr($harga,4,11);     
		$hrgpkerjaan= str_replace('.', '', $hrgH);
		
		$sql_insert = $mysqli->query(
					"INSERT INTO data_pekerjaan(id_pekerjaan,id_kategorikerja,nama_pekerjaan,harga_pekerjaan)
					VALUES(' ','$tipe_krj','$nama','$hrgpkerjaan')"
					);
		
		echo "<script type='text/javascript'>
		document.location='../../mod_teknikal/index.php?mod=data_pekerjaan' 
		</script>";
	} 
?>


