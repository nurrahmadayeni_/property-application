<?php   
    include '../../../config/connectdb.php';
    error_reporting(0);
    $id = $_GET['id_perumahan'];
    $id2 = $_GET['no_kavling'];

    $sql1 = "SELECT pp.id_perumahan, p.nama_perumahan, pp.no_kavling, pp.id_pekerjaan, k.id_kategorikerja, k.nama_kategorikerja, pp.tgl_laporan, COUNT(pp.progress) as 'siap' 
            FROM data_perumahan p, progress_pembangunan pp, kategori_pekerjaan k , data_pekerjaan d 
            WHERE p.id_perumahan=pp.id_perumahan 
            AND k.id_kategorikerja=d.id_kategorikerja 
            AND d.id_pekerjaan=pp.id_pekerjaan 
            AND pp.id_perumahan='$id' 
            AND pp.no_kavling='$id2' 
            AND pp.progress='1' 
            GROUP BY k.id_kategorikerja
    ";

    $tampil=mysqli_query($mysqli,$sql1);

    $ro = mysqli_num_rows($tampil);
    $no = 1;

    $t ="";

    $img = $mysqli->query(
                "SELECT foto 
            from progress_pembangunan where id_perumahan ='$id' and no_kavling='$id2' and progress='1' order by id_progress desc limit 1
            ")->fetch_object()->foto;
    echo "<a href='#detail_img' data-toggle='modal' ><img src='img_progress/".$img."' width='20%'></a>";

    echo "<h4>Jenis Tipe Pekerjaan</h4>";
    while($r=mysqli_fetch_array($tampil)){
        $sql2 = "SELECT id_pekerjaan FROM data_pekerjaan WHERE id_kategorikerja=$r[id_kategorikerja]";
        $result=mysqli_query($mysqli,$sql2);
        $h = mysqli_num_rows($result);

        $persen = round(($r['siap'] / $h) *100);
        $persens = "$persen"."%";

        $kategori = $r['nama_kategorikerja'];
        $string="";

        ?>
        <div class='row'>
            <div class='col-md-4'>
                <?= $kategori ?>
            </div>
            <div class='col-md-8' align='right'>
                <i>Tanggal Selesai: <?= $r['tgl_laporan'] ?> </i>
            </div>
        </div>
        <?php

          echo "
            <span class='progressText' > ".$string."</span>
            <div class='progress'>
                <div class='progress-bar progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='".$persen."' aria-valuemin='10' aria-valuemax='100' style='width:".$persen."%'>
                </span><span class='progressText' >  ".$persens."
                </span></div>
            </div>
          ";
        $no++;
        $t += $persen;
    }

    $sql3 = "SELECT id_perumahan, no_kavling, count(progress)/(select count(id_pekerjaan) from data_pekerjaan) as 'persen'
        from progress_pembangunan where progress='1' and no_kavling='$id2' and id_perumahan='$id' group by no_kavling";
    $result2=mysqli_query($mysqli,$sql3);
    $h2 = mysqli_fetch_array($result2);
    $h = $h2['persen'];
    $persen_t = $h ;

    $sql3="SELECT nama_kategorikerja, id_kategorikerja FROM kategori_pekerjaan WHERE id_kategorikerja NOT IN(SELECT k.id_kategorikerja FROM progress_pembangunan pp, kategori_pekerjaan k , data_pekerjaan d WHERE k.id_kategorikerja=d.id_kategorikerja and d.id_pekerjaan=pp.id_pekerjaan AND pp.id_perumahan='$id' AND pp.no_kavling='$id2' AND pp.progress='1' GROUP BY k.id_kategorikerja)";

    $tampil2=mysqli_query($mysqli,$sql3);
    $no2 = 1;

    while($r2=mysqli_fetch_array($tampil2)){
        $kategori = $r2['nama_kategorikerja'];
        $kategori1 = "";
        echo "
            <span class='progressText' > ".$kategori1."</span>
            <div class='progress'>
                <div class='progress-bar progress-bar-warning progress-bar-striped active' role='progressbar' aria-valuenow='13' aria-valuemin='10' aria-valuemax='100'>
                <span class='progressText' > ".$kategori." 0% 
                </span></div>
            </div>
          ";
        $no2++;
    }

    $sql = mysqli_query($mysqli,"SELECT d.jumlah_keluar,d.id_material, m.harga_material
            from data_pengeluaran p,material m,data_perumahan pr,data_kavling k,data_detailkeluar d
            where p.id_pengeluaran=d.id_pengeluaran 
            and d.id_material=m.id_material 
            and pr.id_perumahan=d.id_perumahan 
            and pr.id_perumahan=k.id_perumahan 
            and d.no_kavling=k.no_kavling 
            and pr.id_perumahan='$id'
            AND k.no_kavling ='$id2'
            group by d.id_detailkeluar");

   
        $tot="";
        $pakai = "";
        $tpakai = "";

        while ($q = mysqli_fetch_array($sql)) {

           $kembali = $mysqli->query(
            "SELECT jumlah_kembali as 'kembali' FROM retur_detail
            WHERE id_material = '$q[id_material]' 
            ")->fetch_object()->kembali;

            if(isset($kembali)){
                $pakai = $q['jumlah_keluar'] - $kembali;
            } else{
                $pakai = $q['jumlah_keluar'];
                
            }
           
                $total =""; 
                $total = $pakai * $q['harga_material'];
                $tot+=$total;
                $no++;
        }        

        $total_ambilan = $mysqli->query(
            "SELECT pp.total_ambilan
            from progress_pembayaran_kontrak pp, kontrak_pekerjaan kp 
            where kp.id_perumahan='$id' and kp.no_kavling='$id2'
            and kp.id_kontrak=pp.id_kontrak
            order by pp.id_progress_pembayaran desc limit 1 
            ")->fetch_object()->total_ambilan;

        $resapan_dana = $tot + $total_ambilan;
        echo "<tr width='100%' style='font-size:15px;'>
            <td> Resapan Dana : <b>Rp. ". number_format($resapan_dana,0,',','.')."</b></td>
            <td align='right' style='font-size:15px;'> Total persen kesiapan pembangunan: <b>".$persen_t."%</b></td>";
?>

<div id="detail_img" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

            <img src="img_progress/<?=$img?>" width='100%'>
            
    </div>
  </div>
</div>
<script type="text/javascript"></script>