<html>
<head>
	<title>Print Rekapitulasi Penarikan Cek</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="../../assets/css/bootstrap-theme.min.css">
	<link href="../../../images/pavicon.png" rel="icon" type="image/x-icon" />
</head>
<body><br><br>
	<img src="../../../images/logo.png" align="right"><br>
	<font size="4%">REKAPITULASI PENCAIRAN CEK GIRO TEKNIKAL</font><br><br>
	<font size="3%">TANGGAL : <font color='red'><b><?php echo $_GET['id'] ; ?></b></font> 
						s/d <font color='red'><b><?php echo $_GET['id2']; ?></b></font></font><hr>
	
	<table class='table table-bordered table-hover'>
        <thead>
            <tr align='center'>
                <th width='5%'>No</th>
                <th width='15%'>Tanggal Penarikan</th>
                <th width='14%'>Jumlah</th>
                <th width='15%'>Keterangan</th>
            </tr>
        </thead>
        <tbody>
				<?php
                    error_reporting(0);
					include '../../../config/connectdb.php';
					$a=$_GET['id'];
					$b=$_GET['id2'];
					
                   $sql = mysqli_query($mysqli,"SELECT * FROM penarikan_cek
								WHERE tanggal_penarikan between '$a' AND '$b'
								ORDER BY tanggal_penarikan ASC");
	$no = 1;
	$tot ="";
	while ($r = mysqli_fetch_array($sql)) {
		$tanggal_penarikan= $r['tanggal_penarikan'];
		$jumlah= $r['jumlah'];
		$keterangan= $r['keterangan'];
 	?>
			<tr align='center'>
				<td align="center"> <?php echo  $no;?> </td>
				<td><?php echo $tanggal_penarikan; ?></td>		
				<td align='right'><?php echo number_format($jumlah,0,',','.') ?></td>
				<td><?php echo $keterangan;  ?></td>
				<?php $tot+= $jumlah; ?>
			</tr>
		   
	<?php
		$no++;
	} 
	?>
			<tr>
				<td colspan='2'><b>Total Penarikan (Rp)</b></td>
				<td align="right"><b><?php echo number_format($tot,0,',','.' );?></b></td>
				<td>
			</tr>	
	</tbody>
	</table>

	<div class="container-fluid">
		<div class="row" align="right">
		    <div class="col-md-4" >
		    	<?php $hari = date('d M Y'); ?>
		      	Banda Aceh, <?= $hari; ?>
		    </div>
		</div>

		<div class="row" align="right">
		    <div class="col-md-6" >
		      Divisi Teknikal
		    </div>
		</div>

		<div class="row" align="right">
		    <div class="col-md-6" >
		      PT. Matahari Cipta
		    </div>
		</div>
		<br/><br/><br/><br/>
		<div class="row" align="right">
		    <div class="col-md-6">
		      _____________
		    </div>
		</div>
		<div class="row" align="right">
		    <div class="col-md-6" >
		      Manager
		    </div>
		</div>
	</div>


</body>
</html>
		
	<script>
		window.load = print_d();
		function print_d(){
			window.print();
		}
	</script>