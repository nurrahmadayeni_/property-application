<html>
<head>
	<title>Print Document</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
	<br><br>
	<img src="../../../images/logo.png" align="right"><br>
	<font size="3%">Data Inventaris</font><br><hr>
	<table border="1" width="100%" style="border-collapse:collapse;" align="center">
     <thead>	
    	<tr class="tableheader" style="font-size: 12px;">
            <th>Nama Barang</th>
            <th>Merk/Type</th>
            <th>Warna</th>
            <th>Thn Perolehan</th>
            <th>Jlh</th>
            <th>Kondisi</th>
            <th>Harga</th>
            <th>Total</th>
            <th>PTJ</th>
            <th>Lokasi</th>
            <th>Foto</th>
        </tr>
     </thead>
    <tbody>
				<?php
                    error_reporting(0);
					include '../../../config/connectdb.php';
					
					$id = $_GET['id'];
				 	$sql = mysqli_query($mysqli,"SELECT * FROM INVENTARIS WHERE id_kategori=$id ORDER BY harga_barang ASC");
					
					$no = 1;
					while ($r = mysqli_fetch_array($sql)) {
				           $id = $r['id_inventaris'];
				            $idk = $r['id_kategori'];
				?>

            <tr align='left'>
	            <td> <?php echo  $r['nama_barang']; ?> </td>
	            <td> <?php echo  $r['merk']; ?> </td>
	            <td> <?php echo  $r['warna']; ?> </td>
	            <td> <?php echo  $r['thn_peroleh']; ?> </td>
	            <td> <?php echo  $r['jumlah']; ?> </td>
	            <td> <?php echo  $r['kondisi']; ?></td>
	            <td> 
	                <?php 
	                    $rupiah=number_format($r['harga_barang'],0,',','.'); 
	                    echo 'Rp.'. $rupiah; 
	                ?>
	            </td>
	            <td>
	                <?php
	                    $jumlah = $r['jumlah'];
	                    $hrg = $r['harga_barang'];
	                    $total = $jumlah * $hrg;
	                    echo "Rp.". number_format($total,0,',','.'); 
	                ?>
	            </td>
	            <td> <?php echo  $r['ptj']; ?> </td>
	            <td> <?php echo  $r['lokasi']; ?> </td>
	            <td> <img src="../../mod_adm/gbr_inventaris/<?php echo  $r['foto']; ?>" width='100%'> </td>
        	</tr>
				<?php
		       		$no++;
				}
				?>
    </tbody>
    </table>

</body>
</html>
		
	<script>
		window.load = print_d();
		function print_d(){
			window.print();
		}
	</script>