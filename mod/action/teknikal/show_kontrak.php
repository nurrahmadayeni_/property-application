<?php   
	include '../../../config/connectdb.php';

	$id1 = $_GET['id1']; // perumahan
	$id2 = $_GET['id2'];  // kavling

	$sql = "SELECT k.id_perumahan, p.nama_perumahan, k.no_kavling, sum(k.nilai_kontrak) 
		as 'total', k.tgl_selesai,  k.id_kontrak , k.pengawas
		FROM kontrak_pekerjaan k, data_perumahan p WHERE k.id_perumahan = p.id_perumahan AND
		k.id_perumahan='$id1' and k.no_kavling='$id2'
		GROUP BY k.id_perumahan, k.no_kavling";

	$tampil=mysqli_query($mysqli,$sql);

    $q = mysqli_fetch_array($tampil);
    if(isset($q)){

    ?>
			<table id='' class='table table-bordered table-hover'>
            <tr align='center'>
                <td><b>No</b></td>
                <td><b>Perumahan</b></td>
                <td><b>Kavling</b></td>
                <td><b>Pengawas</b></td>
                <td><b>Nilai Kontrak(Rp.)</b></td>
                <td><b>Tanggal Siap</b></td>
                <td><b>Menu</b></td>
            </tr>
            <tr id='kontrak'>

              
	        <td align='center'>1</td>
	        <td align='center'><?php echo $q['nama_perumahan'];?></td>
	        <td align='center'><?php echo $q['no_kavling'];?></td>
	        <td align='center'><?php echo $q['pengawas'];?></td>
	        <td align="right">
	            <?php 
	                $rupiah=number_format($q['total'],0,',',','); 
	                echo $rupiah; 
	            ?>
	        </td>
	        <td align='center'><?php echo  $q['tgl_selesai']; ?></td>
	        <td align="center">
	            <?php
	                $idp = $q['id_perumahan'];
	                $idk = $q['no_kavling'];
	                $idpp = $q['pengawas'];
	                echo "<a href='?mod=detail_kontrak&idp=$idp&idk=$idk&idpp=$idpp'>
	                    <i class='material-icons btn btn-primary btn-sm'>details</i>
	                    </a>
	                ";
	            ?>
	        </td>
			</tr>
    </table> 
	   
	<?php
	}
	else{
		$rumah = $mysqli->query(
                        "SELECT nama_perumahan 
                        FROM data_perumahan
                        WHERE id_perumahan = '$id1'
                        ")->fetch_object()->nama_perumahan;
		echo "
		
				<td colspan='7'>Tidak ada kontrak pada ".$rumah." dan Kavling ".$id2."</td>
			
		";
	}
	?>
