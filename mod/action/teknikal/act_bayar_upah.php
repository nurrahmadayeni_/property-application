<?php
	//database connection 

	include '../../../config/connectdb.php';

	$id = $_POST["v1"]; // perumahan
	$id2 = $_POST["v2"]; // kavling
	$id3 = $_POST["v3"]; // id_kontrak
	$id4 = $_POST["v4"]; // kontraktor
	$id5 = $_POST["v5"]; // pekerjaan

?>
<script src="../jquery.price_format.2.0.min.js"></script>
<script src="../jquery.price_format.2.0.js"></script>
	<form method="post" action='../action/teknikal/act_kontrak.php?mod=teknikal&act=bayar_upah'>
    <div class="form-group">
        <label for="kontraktor" class="control-label">Kontraktor :</label>
        <input type="text" class="form-control" id="kontraktor" value=<?= $id4;?> name="kontraktor" disabled>
        <input type="hidden" name="id_perumahan" class="form-control"
         value= <?= $id; ?> >
        <input type="hidden" name="no_kavling" class="form-control"
         value= <?= $id2; ?> >
        <input type="hidden" name="kontraktor" class="form-control"
         value= <?= $id4; ?> >
         <input type="hidden" name="id_kontrak" class="form-control
         " value= <?= $id3; ?> >
      </div>
  
    <div class="form-group">
        <label for="pekerjaan" class="control-label">Pekerjaan :</label>
        <input type="text" name="pekerjaan2" class="form-control" value= <?= $id5; ?> disabled>
        <input type="hidden" name="pekerjaan" class="form-control" value= <?= $id5; ?> >
    </div>

    <div class="form-group">
        <label for="pekerjaan_siap" class="control-label">Pekerjaan Siap :</label>
        <?php

        	$id_k = $mysqli->query(
                "SELECT id_kategorikerja 
                FROM kategori_pekerjaan
                WHERE nama_kategorikerja = '$id5'
                ")->fetch_object()->id_kategorikerja;

        	$kesiapan = "SELECT pp.id_pekerjaan, d.nama_pekerjaan from data_pekerjaan d, progress_pembangunan pp WHERE d.id_kategorikerja in (select id_kategorikerja from kontrak_pekerjaan ) and pp.id_pekerjaan=d.id_pekerjaan and pp.progress='1' and pp.id_perumahan='$id' AND pp.no_kavling='$id2' AND d.id_kategorikerja='$id_k'";

			$t = mysqli_query($mysqli,$kesiapan);
			$siap = mysqli_num_rows($t);
        	
        	while ($r = mysqli_fetch_array($t)) {
        		echo "<li>".$r['nama_pekerjaan']."</li>";
        	}
        ?>
    </div>
    
	<div class="form-group">
        <label for="progress" class="control-label">Progress Pembangunan :</label>
        <?php
        	$tot = "SELECT pp.id_pekerjaan, d.nama_pekerjaan from data_pekerjaan d, progress_pembangunan pp WHERE d.id_kategorikerja in (select id_kategorikerja from kontrak_pekerjaan ) and pp.id_pekerjaan=d.id_pekerjaan and pp.id_perumahan='$id' AND pp.no_kavling='$id2' AND d.id_kategorikerja='$id_k'";

			$total = mysqli_query($mysqli,$tot);
			$siap_t = mysqli_num_rows($total);

			$persen = round(($siap / $siap_t) *100);
			$persens = "$persen"."%";
        ?>
        <input type="text" class="form-control" name="progress2" value=<?= $persens ?> disabled="">
        <input type="hidden" name="progress" value=<?= $persen ?> >
	</div>

	<div class="form-group">
        <label for="progress" class="control-label">Nilai Kontrak :</label>
        <?php
	        $nilai_kontrak = $mysqli->query(
			"SELECT nilai_kontrak FROM kontrak_pekerjaan
			WHERE id_perumahan = '$id' 
			AND no_kavling = '$id2' AND id_kategorikerja='$id_k'
			")->fetch_object()->nilai_kontrak;

			$nilai_k = number_format($nilai_kontrak,0,',','.');
        ?>
        <input type="text" class="form-control" name="nilai_kontrak2" value= <?= $nilai_k ?> disabled>
        <input type="hidden" name="nilai_kontrak" value=<?= $nilai_kontrak ?> >
	</div>

	<div class="form-group">
        <label for="progress" class="control-label">Uang sesuai progress :</label>
        <?php
			$biaya_progress = ($persen/100) * $nilai_kontrak;
			$nilai = number_format($biaya_progress,0,',','.');
        ?>
        <input type="text" class="form-control" name="biaya_progress2" value= <?= $nilai ?> disabled>
        <input type="hidden" name="biaya_progress" value=<?= $biaya_progress ?> >
	</div>

	<div class="form-group">
        <label for="keterangan" class="control-label">Keterangan/Kemajuan Pekerjaan</label>
        <textarea class="form-control" name="keterangan"></textarea>
	</div>	

    <div class="form-group">
        <label for="nilai" class="control-label">Bayar Upah</label>
        <input type="text" class="form-control" id="nilai" name="upah_bayar" required=''>
		<script type="text/javascript">
			$(document).ready(function(){
        $('#nilai').priceFormat({
            clearPrefix: true
        });
			});
		</script>
	</div>			  
    
   	<div class="modal-footer">
                <input type="Submit" class="btn btn-primary" value="Bayar" id='bayar'>
                <input type="reset" class="btn btn-default" value="Reset" id='batal'>
            </div>
   </form>