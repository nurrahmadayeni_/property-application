<?php

    include '../../../config/connectdb.php';
    //error_reporting(0);
    session_start();

    $id1 = $_POST['v1']; // kategori kerja
    $id2 = $_POST['v2']; // perumahan
    $id3 = $_POST['v3']; // kavling

    $sql = mysqli_query($mysqli,"select pp.foto, pp.id_perumahan, d.nama_pekerjaan, k.id_kategorikerja, pp.no_kavling, k.nama_kategorikerja from progress_pembangunan pp, kategori_pekerjaan k, data_pekerjaan d where d.id_pekerjaan=pp.id_pekerjaan and d.id_kategorikerja=k.id_kategorikerja and pp.id_perumahan='$id2' and pp.no_kavling='$id3' and k.id_kategorikerja='$id1' and pp.progress='1' GROUP BY pp.id_pekerjaan");

      $rows = array();
      while($r=$sql->fetch_assoc()){ 
          $rows[] = $r; 
      }
    ?>

    <div id='carousel-example-generic' class='carousel slide' data-ride='carousel'>
         <div class='carousel-inner'>
            <?php
            $i = 1; 
            foreach ($rows as $r): 
            $item_class = ($i == 1) ? 'item active' : 'item'; 
            ?>              
            <div class="<?php echo $item_class; ?>"> 
                <img src="img_progress/<?php echo $r['foto'];?>" class='img-responsive'>
                <div class='carousel-caption'>
                        <font color="black" size="15pt;">
                          <?php echo $r['nama_pekerjaan'];?>
                        </font>
                </div>
            </div>
            <?php $i++; ?>
            <?php endforeach; ?> 
          </div>
    

      <a class='left carousel-control' href='#carousel-example-generic' role='button' data-slide='prev'>
          <i class="material-icons">keyboard_arrow_left</i>
      </a>
      <a class='right carousel-control' href='#carousel-example-generic' role='button' data-slide='next'>
          <i class="material-icons">keyboard_arrow_right</i>
      </a>
     
    </div>
    
