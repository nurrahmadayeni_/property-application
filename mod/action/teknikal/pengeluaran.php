<link rel="stylesheet" href="../../assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../../assets/css/bootstrap-theme.min.css">
<?php

    include '../../../config/connectdb.php';
    error_reporting(0);

    $a = $_GET["prm1"]; 
    $b = $_GET["kav1"];
    $c = $_GET["tgl_awal"];
    $d = $_GET["tgl_akhir"];
    $e = $_GET["month"];
    $f = $_GET["day"];
    $g = $_GET["year"];

    $perumahan = $mysqli->query(
        "SELECT nama_perumahan FROM data_perumahan
        WHERE id_perumahan = '$a' 
        ")->fetch_object()->nama_perumahan;

?>
    <div class="container-fluid">
        <div class="row">
            <b style="font-size:16px;">LAPORAN PENGELUARAN</b>
        </div>
        <div class="row">
            <b>Perumahan : <font style="text-transform: capitalize; color:red; "><?= $perumahan ?> </font> </b>
        </div>
        <div class="row">
            <b>Tanggal : <font color="red"><?= $c ?></font> s/d <font color="red"><?= $d ?></font> </b>
        </div>
    </div>
    <br>
    <?php

    echo "
    <table id='detail' class='table table-bordered table-hover' >
       <thead>
           <tr> 
                <th width='5%'>No</th>
                <th width='20%'>Hari / Tanggal </th>
                <th width='12%'>Uraian</th>
                <th width='10%'>Kavling</th>
                <th width='20%'>Pekerjaan</th>
                <th width='10%'>Kontraktor</th>
                <th width='10%'>Jumlah</th>
           </tr>
       </thead>
       <tbody>
   
        ";

    $sql = mysqli_query($mysqli,"SELECT pp.tanggal_pembayaran, pp.keterangan, kp.no_kavling, k.nama_kategorikerja,
        kp.kontraktor, pp.harga_upah
        FROM kontrak_pekerjaan kp,
        progress_pembayaran_kontrak pp,
        data_perumahan d,
        kategori_pekerjaan k
        WHERE k.id_kategorikerja=kp.id_kategorikerja
        and kp.id_perumahan=d.id_perumahan
        AND pp.id_kontrak=kp.id_kontrak
        AND kp.id_perumahan='$a' AND kp.no_kavling='$b'
        AND pp.tanggal_pembayaran BETWEEN '$c' AND '$d'
");
    
    $no=1;
    while($q = mysqli_fetch_array($sql)){
        echo "
            <tr align='left'>
                <td> $no </td>
                <td> ".$q['tanggal_pembayaran']."  </td>
                <td> ".$q['keterangan']."  </td>
                <td> ".$q['no_kavling']."</td>
                <td> ".$q['nama_kategorikerja']." </td>
                <td> ".$q['kontraktor']." </td>
                <td align='right'> ".number_format($q['harga_upah'],0,',','.')." </td>
            </tr>
        ";
        $no++;
    }
?>

    </tbody>
        <tr align="right">
            <td colspan='7'><i class="material-icons btn btn-primary" onclick="print_d()">local_printshop</i></td>
         </tr>
    </table>


<script>
    function print_d(){
        window.open("../action/teknikal/print_pengeluaran.php?v1=<?php echo $id;?>&v2=<?php echo $id3; ?>&v3=<?php echo $id4; ?>","_blank");
    }
    $(function() {
         $("#detail").dataTable();
     });
</script>

