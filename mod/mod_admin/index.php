<link rel="stylesheet" href="../assets/css/bootstrap.min.css"/>
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<link href="../../images/logo.png" rel="icon" type="image/x-icon" />

<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
if(!isset($_SESSION["username"])){
	?>
    <!--Modal Session -->
		<script type="text/javascript">
		    $(document).ready(function(){
		        $("#login").modal('show');
		    });
		</script>

		<div id="login" class="modal fade">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header alert-warning">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                <h4 class="modal-title">Information</h4>
		            </div>
		            <div class="modal-body">
					  Please Login First
					</div>
		            <div class = "modal-footer">
			            <a href="../../index.php"><button type = "button" class = "btn btn-warning" >OK</button> </a>
			         </div>
		        </div>
		    </div>
		</div>
    <?php
}else{

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">  <head>
    <title>ADMIN</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="../../css/main.css" rel="stylesheet">
    <link href="../../images/logo.png" rel="icon" type="image/x-icon" />
   	
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="../assets/css/dataTables.bootstrap.css"/>
	<link rel="stylesheet" href="../assets/css/bootstrap-theme.min.css">

	<script src="../assets/js/jquery-1.11.0.js"></script>
	<script src="../assets/js/bootstrap.min.js"></script>
	<script src="../assets/js/jquery.min.js"></script>
	<script src="../assets/js/jquery.maskMoney.min.js"></script>
	<script src="../assets/datatables/jquery.dataTables.js"></script>
	<script src="../assets/datatables/dataTables.bootstrap.js"></script>
	
	<script>
	//copyright
		function copyDate() {
  		var cpyrt = document.getElementById("copyright")
  		if (cpyrt) {
     	cpyrt.firstChild.nodeValue = (new Date()).getFullYear();
  		}}
		window.onload = copyDate;
	</script>
	
  </head>
  <body>

  	<div class="header" style="background-color: #303F9F">
	      <div class="container">
	        <div class="row">
	           <div class="col-md-5">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="#">ADMIN</a></h1>
	              </div>
	           </div>
	           <div class="col-md-5">
	              <div class="row">
	                <div class="col-lg-12">
	                  <div class="input-group form"></div>
	                </div>
	              </div>
	           </div>
	           <div class="col-md-2">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo"$_SESSION[username]"; ?></b></a>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

    <div class="page-content">
    	<div class="row">
		  <div class="col-md-2">
		  	<div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <?php
				        $page = $_SERVER['QUERY_STRING'];
				    ?>
                    <li <?php if($page=='mod=history') echo "class='current'" ?> >
                    	<a href="?mod=history"><i class="glyphicon glyphicon-record"></i> History User</a>
                    </li>
                    <li><a href="#" data-toggle = "modal" data-target = "#logout"><i class="glyphicon glyphicon-record"></i> Logout</a></li>
                </ul>
             </div>
		  </div>
		  <div class="col-md-10">
			<div class="content-box-large">
				<?php
					include '../../config/connectdb.php';

					if($_GET['mod']=='history'){
						include "history_user.php"; 
					}
					elseif ($_GET['mod']=='logout') {
						?>
						<a href="#" data-toggle = "modal" data-target = "#logout"> Logout</a>
						<?php
					}					
				?>
				<script language="Javascript">
					function deleteask(){
					  if (confirm('Are you sure you want to logout?')){
					    return true;
					  }else{
					    return false;
					  }
					}
				</script>
			</div>
		  </div>
		</div>
    </div>
    <!-- Modal LOGOUT-->
	<div class = "modal fade" id = "logout" tabindex = "-1" role = "dialog" 
	   aria-labelledby = "myModalLabel" aria-hidden = "true">
	   
	   <div class = "modal-dialog">
	      <div class = "modal-content">
	         
	         <div class = "modal-header">
	            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
	                  &times;
	            </button>
	            
	            <h4 class = "modal-title" id = "myModalLabel">
	               CONFIRM LOGOUT
	            </h4>
	         </div>
	         
	         <div class = "modal-body">
	            Are you sure you want to logout?
	         </div>
	         
	         <div class = "modal-footer">
	            <button type = "button" class = "btn btn-default" data-dismiss = "modal">
	               NO
	            </button>
	            
	            <a href="logout.php"><button type = "button" class = "btn btn-primary" >YES</button> </a>
	         </div>
	         
	      </div><!-- /.modal-content -->
	   </div><!-- /.modal-dialog -->
	  
	</div><!-- /.modal -->
    <footer style="background-color: #303F9F">
         <div class="container">
            <div class="copy text-center">
               Copyright&copy; <span id="copyright">...</span> <a href='#'>Website</a>
            </div>          
         </div>
    </footer>

   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../js/custom.js"></script>
  </body>
</html>
<?php
} ?>