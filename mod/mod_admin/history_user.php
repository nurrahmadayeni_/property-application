<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);

        echo "<h3 align=center> HISTORY USER </h1>";
        echo "
            <table id='history' class='table table-bordered table-hover'>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Pegawai</th>
                    <th>Divisi</th>
                    <th>Waktu Login</th>
                    <th>Waktu Logout</th>
                </tr>
            </thead>
            <tbody>
        ";
       
             
        include '../../config/connectdb.php';

        $sql = mysqli_query($mysqli,'SELECT l.id_loging, p.nama_pegawai, p.level, l.waktu_login, l.waktu_logout from data_pegawai p, loging_user l where p.id_user=l.id_user');
        $no = 1;
        while ($r = mysqli_fetch_array($sql)) {
            $id = $r['id_loging'];
        ?>
            <tr align='left'>
                <td>
                    <?php echo  $no;?>
                </td>
                <td>
                    <?php echo  $r['nama_pegawai']; ?>
                </td>
                <td>
                    <?php echo  $r['level']; ?>
                </td>
    			<td>
                    <?php echo  $r['waktu_login']; ?>
                </td>
                <td>
                    <?php echo  $r['waktu_logout']; ?>
                </td>
            </tr>
			<?php
             $no++;
			}
           
        ?>

        </tbody>

    </table>
	
    <script type="text/javascript">
    $(function() {
        $("#history").dataTable();
    });
    </script>
