<!DOCTYPE html>
<html>
	<head>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
		
		<title>Login User MCKons</title>
		
		<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Roboto:300,400,500,700'>
		<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/icon?family=Material+Icons'>

		<!-- Bootstrap -->
		<link rel='stylesheet' type='text/css' href='css/bootstrap.min.css'>
		<link rel='stylesheet' type='text/css' href='css/style.css'>

		<!-- Bootstrap Material Design -->
		<link rel='stylesheet' type='text/css' href='css/bootstrap-material-design.min.css'>
		<link rel='stylesheet' type='text/css' href='css/ripples.css'>

		<link href='images/pavicon.png' rel='icon' type='image/x-icon' />
</head>
<body>
	
<div class='wrapper'>
<div class='col-lg-4 col-md-offset-4 well well-lg paper'>
	<div class="col-lg-12 text-center">
		<img class="img img-circle" width='150px' src='images/no-photo.jpg'>
	</div>
	<div class='col-lg-12'>	
		<form class='form-horizontal' action='config/sign_in.php' method='POST'>
			<div class="form-group label-floating">
			  <label class="control-label" for="focusedInput1">Username</label>
			  <input class="form-control" id="focusedInput1" type="text" name='username' required>
			  <p class="help-block">Isi dengan Username anda</p>
			</div>
		    <div class='form-group label-floating'>
			  <label class='control-label' for='focusedInput2'>Password</label>
			  <input class='form-control' id='focusedInput2' name='password' type='password' required>
			</div>
		    <div class='form-group'>
		       	<input type='submit' class='btn btn-lg btn-block btn-raised btn-danger' value='Login'/>
		    </div>
		</form>						
	</div>
	<div class='row'>
		<div class='col-lg-12 col-md-12 col-sm-12'>
			<p class='text-left'>&copy; 2016 PT Matahari Cipta</p>
			<p class='text-right dev'>Developed by : <a href='http://lunata.co.id'> Lunata IT & Consultant </a></p>
		</div>
	</div>
</div>

	<script src='js/jquery.min.js'></script>
	<script src='js/material.js'></script>
	<script src='js/ripples.js'></script>
	
 	<script>
		$.material.init();
	</script>
</body>

</html>