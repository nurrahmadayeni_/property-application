-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 03. Juni 2016 jam 08:12
-- Versi Server: 5.5.8
-- Versi PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_proyek1`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `alokasi_teknikal`
--

CREATE TABLE IF NOT EXISTS `alokasi_teknikal` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tgl` date DEFAULT NULL,
  `debit` int(100) DEFAULT NULL,
  `kredit` int(100) DEFAULT NULL,
  `sisa_saldo` int(100) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `alokasi_teknikal`
--

INSERT INTO `alokasi_teknikal` (`id`, `tgl`, `debit`, `kredit`, `sisa_saldo`, `keterangan`) VALUES
(1, '2016-06-30', 20000000, 0, 20000000, 'Dana Marketing');

-- --------------------------------------------------------

--
-- Struktur dari tabel `batal_pembelian`
--

CREATE TABLE IF NOT EXISTS `batal_pembelian` (
  `id_pembatalan` int(100) NOT NULL AUTO_INCREMENT,
  `tgl_pembatalan` date NOT NULL,
  `tgl_sekarang` date NOT NULL,
  `id_transaksi` varchar(100) NOT NULL,
  `nominal_pengembalian` int(100) NOT NULL,
  `dana_hangus` int(100) NOT NULL,
  `foto_kartukonsumen` varchar(200) NOT NULL,
  `foto_kwitansikembali` varchar(100) NOT NULL,
  PRIMARY KEY (`id_pembatalan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `batal_pembelian`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `batal_transaksi`
--

CREATE TABLE IF NOT EXISTS `batal_transaksi` (
  `id_bataltransaksi` int(100) NOT NULL AUTO_INCREMENT,
  `id_transaksi` varchar(100) NOT NULL,
  `nik_ktp` varchar(100) NOT NULL,
  `id_perumahan` varchar(100) NOT NULL,
  `no_kavling` varchar(100) NOT NULL,
  `alasan_batal` varchar(255) NOT NULL,
  `tgl_pembatalan` date NOT NULL,
  PRIMARY KEY (`id_bataltransaksi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `batal_transaksi`
--

INSERT INTO `batal_transaksi` (`id_bataltransaksi`, `id_transaksi`, `nik_ktp`, `id_perumahan`, `no_kavling`, `alasan_batal`, `tgl_pembatalan`) VALUES
(1, 'TRS-00001', '12345678', 'PRM-00004', 'A1', 'hahshashasha', '2016-06-03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bayar_rumah`
--

CREATE TABLE IF NOT EXISTS `bayar_rumah` (
  `id_bayarrumah` int(100) NOT NULL AUTO_INCREMENT,
  `nik_ktp` varchar(100) NOT NULL,
  `id_perumahan` varchar(100) NOT NULL,
  `id_transaksi` varchar(100) NOT NULL,
  `no_kavling` varchar(100) NOT NULL,
  `id_jenisbayar` int(10) NOT NULL,
  `jlh_setoran` int(100) NOT NULL,
  `tgl_setoran` date NOT NULL,
  `tgl_input` date NOT NULL,
  `jenis_bayar` enum('Cash','Transfer Bank') NOT NULL,
  `no_rekeningkons` varchar(100) NOT NULL,
  PRIMARY KEY (`id_bayarrumah`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `bayar_rumah`
--

INSERT INTO `bayar_rumah` (`id_bayarrumah`, `nik_ktp`, `id_perumahan`, `id_transaksi`, `no_kavling`, `id_jenisbayar`, `jlh_setoran`, `tgl_setoran`, `tgl_input`, `jenis_bayar`, `no_rekeningkons`) VALUES
(1, '12345678', 'PRM-00004', 'TRS-00001', 'A1', 2, 12000000, '2016-06-03', '2016-06-03', 'Cash', ''),
(2, '12345678', 'PRM-00004', 'TRS-00001', 'A1', 3, 10000000, '2016-06-02', '2016-06-03', 'Transfer Bank', 'db_proyek1-7.sql'),
(3, '12345678', 'PRM-00004', 'TRS-00001', 'A1', 26, 190000000, '2016-06-01', '2016-06-03', 'Cash', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `berkas_kons`
--

CREATE TABLE IF NOT EXISTS `berkas_kons` (
  `id_berkaskons` int(100) NOT NULL AUTO_INCREMENT,
  `id_transaksi` varchar(100) NOT NULL,
  `id_jenispembayaran` int(100) NOT NULL,
  `entry_file` varchar(100) NOT NULL,
  `entry` varchar(100) NOT NULL,
  `analis` varchar(100) NOT NULL,
  `OTS` varchar(100) NOT NULL,
  `sp3k` varchar(100) NOT NULL,
  `akad` varchar(100) NOT NULL,
  `serah_terima` varchar(100) NOT NULL,
  `closing` varchar(100) NOT NULL,
  `tgl_upload` date NOT NULL,
  `tgl_jatuhtempo` date NOT NULL,
  PRIMARY KEY (`id_berkaskons`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `berkas_kons`
--

INSERT INTO `berkas_kons` (`id_berkaskons`, `id_transaksi`, `id_jenispembayaran`, `entry_file`, `entry`, `analis`, `OTS`, `sp3k`, `akad`, `serah_terima`, `closing`, `tgl_upload`, `tgl_jatuhtempo`) VALUES
(1, 'TRS-00001', 2, '1464934189.pdf', '1', '', '', '', '', '', '', '2016-06-03', '2016-06-18');

-- --------------------------------------------------------

--
-- Stand-in structure for view `bon`
--
CREATE TABLE IF NOT EXISTS `bon` (
`id_supplier` varchar(100)
,`nama_supplier` varchar(100)
,`sisa` bigint(21)
,`total_order` bigint(21)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `bon_2`
--
CREATE TABLE IF NOT EXISTS `bon_2` (
`id_supplier` varchar(100)
,`nama_supplier` varchar(100)
,`sisa` bigint(21)
,`total_order` bigint(21)
,`total_harga` decimal(65,0)
);
-- --------------------------------------------------------

--
-- Struktur dari tabel `data_detailkeluar`
--

CREATE TABLE IF NOT EXISTS `data_detailkeluar` (
  `id_detailkeluar` int(100) NOT NULL AUTO_INCREMENT,
  `id_pengeluaran` int(100) NOT NULL,
  `id_material` varchar(30) NOT NULL,
  `id_gudang` int(200) NOT NULL,
  `jumlah_keluar` int(100) NOT NULL,
  `id_perumahan` varchar(100) NOT NULL,
  `no_kavling` varchar(100) NOT NULL,
  `tgl_pengeluaran` date NOT NULL,
  PRIMARY KEY (`id_detailkeluar`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data untuk tabel `data_detailkeluar`
--

INSERT INTO `data_detailkeluar` (`id_detailkeluar`, `id_pengeluaran`, `id_material`, `id_gudang`, `jumlah_keluar`, `id_perumahan`, `no_kavling`, `tgl_pengeluaran`) VALUES
(9, 1, 'MT-01', 1, 10, 'PRM-00001', 'M2', '2016-05-31'),
(10, 2, 'MT-02', 2, 6, 'PRM-00001', 'M2', '2016-06-03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_gudang`
--

CREATE TABLE IF NOT EXISTS `data_gudang` (
  `id_gudang` int(200) NOT NULL AUTO_INCREMENT,
  `nama_gudang` varchar(100) NOT NULL,
  `penanggungjwb_gudang` varchar(100) NOT NULL,
  `alamat_gudang` varchar(100) NOT NULL,
  `tlp_gudang` varchar(100) NOT NULL,
  PRIMARY KEY (`id_gudang`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `data_gudang`
--

INSERT INTO `data_gudang` (`id_gudang`, `nama_gudang`, `penanggungjwb_gudang`, `alamat_gudang`, `tlp_gudang`) VALUES
(1, 'gudang 1', 'hahah', 'alala', '121212'),
(2, 'gudang baru', 'sas', 'asa1', '12'),
(3, 'gudang 2', 'amir', 'jln.helvetia', '0618456663');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_kavling`
--

CREATE TABLE IF NOT EXISTS `data_kavling` (
  `kav_id` int(100) NOT NULL AUTO_INCREMENT,
  `no_kavling` varchar(100) NOT NULL,
  `id_perumahan` varchar(100) NOT NULL,
  `id_type` int(100) NOT NULL,
  `mapping` varchar(255) NOT NULL,
  `status` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`kav_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `data_kavling`
--

INSERT INTO `data_kavling` (`kav_id`, `no_kavling`, `id_perumahan`, `id_type`, `mapping`, `status`) VALUES
(3, 'A1', 'PRM-00004', 1, '181,132,278,122,278,217,216,219,203,217,188,216,177,216,169,209', 0),
(4, 'A2', 'PRM-00004', 1, '362,245,467,249,465,342,367,336', 0),
(5, 'B1', 'PRM-00004', 2, '168,243,251,244,252,341,152,340', 1),
(6, 'B2', 'PRM-00004', 2, '486,100,569,87,580,225,483,220', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_keuangan`
--

CREATE TABLE IF NOT EXISTS `data_keuangan` (
  `id_keuangan` int(100) NOT NULL AUTO_INCREMENT,
  `giro_asal` varchar(100) NOT NULL,
  `no_giro` int(100) NOT NULL,
  `id_jeniskeuangan` int(10) NOT NULL,
  `no_cek` varchar(100) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `nominal` int(100) NOT NULL,
  `tgl_sekarang` date NOT NULL,
  `tgl_cek` date NOT NULL,
  PRIMARY KEY (`id_keuangan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `data_keuangan`
--

INSERT INTO `data_keuangan` (`id_keuangan`, `giro_asal`, `no_giro`, `id_jeniskeuangan`, `no_cek`, `keterangan`, `nominal`, `tgl_sekarang`, `tgl_cek`) VALUES
(1, 'ADM', 1, 2, '', 'Dana Marketing', 20000000, '2016-07-02', '2016-06-30'),
(2, 'ADM', 2, 2, '', 'Dana Teknikal', 20000000, '2016-07-02', '2016-06-30');

-- --------------------------------------------------------

--
-- Stand-in structure for view `data_konsumenrefund`
--
CREATE TABLE IF NOT EXISTS `data_konsumenrefund` (
`id_transaksi` varchar(100)
);
-- --------------------------------------------------------

--
-- Struktur dari tabel `data_pegawai`
--

CREATE TABLE IF NOT EXISTS `data_pegawai` (
  `id_pegawai` varchar(10) NOT NULL,
  `id_user` int(100) NOT NULL,
  `nama_pegawai` varchar(255) NOT NULL,
  `nope` varchar(20) NOT NULL,
  `jenis_kelamin` enum('pria','wanita') NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `level` enum('ADM','Teknikal','Marketing') NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_pegawai`
--

INSERT INTO `data_pegawai` (`id_pegawai`, `id_user`, `nama_pegawai`, `nope`, `jenis_kelamin`, `email`, `alamat`, `level`) VALUES
('PGW-00001', 24, 'Nurrahmadayeni', '082171478564', 'wanita', ' 1214022yeni@gmail.com', 'Jln Jamin Ginting gg Haji Arif No 2G ', 'Teknikal'),
('PGW-00002', 25, 'Endang Windarsih', '082192819384', 'wanita', '1214022yeni@gmail.com', 'Jln Helvetia no 28', 'ADM'),
('PGW-00003', 26, 'Ranti Ramadhiana', '08217147798', 'wanita', '1214022yeni@gmail.com', 'Jln Pardede no 98 ', 'Marketing');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_pekerjaan`
--

CREATE TABLE IF NOT EXISTS `data_pekerjaan` (
  `id_pekerjaan` int(100) NOT NULL AUTO_INCREMENT,
  `id_kategorikerja` int(100) NOT NULL,
  `nama_pekerjaan` varchar(100) NOT NULL,
  `harga_pekerjaan` int(100) NOT NULL,
  PRIMARY KEY (`id_pekerjaan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data untuk tabel `data_pekerjaan`
--

INSERT INTO `data_pekerjaan` (`id_pekerjaan`, `id_kategorikerja`, `nama_pekerjaan`, `harga_pekerjaan`) VALUES
(1, 2, 'bowplank', 14000000),
(2, 1, 'pondasi', 100000),
(3, 1, 'sloof', 25000),
(4, 1, 'bt di bawah kusen', 30000),
(5, 1, 'kusen', 20000),
(6, 2, 'rakit rangka', 30000),
(7, 2, 'rangka siap', 10000),
(8, 2, 'seng terpasang', 34000),
(9, 2, 'lisplank', 40000),
(11, 4, 'keramik lantai', 12000),
(12, 3, 'rangka plafond', 23000),
(13, 3, 'plafond dalam', 10000),
(14, 4, 'pas kamar mandi', 33000),
(15, 4, 'pemipaan', 10000),
(16, 5, 'tanam pipa', 44000),
(17, 5, 'instalasi', 45000),
(18, 5, 'finishing', 33300),
(19, 6, 'cat dasar', 20000),
(20, 6, 'cat dinding luar', 23000),
(21, 7, 'septitank', 10000),
(22, 7, 'sumur bor', 10000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_pembeli`
--

CREATE TABLE IF NOT EXISTS `data_pembeli` (
  `nik_ktp` varchar(100) NOT NULL,
  `nama_pembeli` varchar(200) NOT NULL,
  `alamat_pembeli` varchar(200) NOT NULL,
  `notelp_pembeli` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `catatan_khusus` varchar(200) NOT NULL,
  PRIMARY KEY (`nik_ktp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_pembeli`
--

INSERT INTO `data_pembeli` (`nik_ktp`, `nama_pembeli`, `alamat_pembeli`, `notelp_pembeli`, `email`, `catatan_khusus`) VALUES
('12121212asasasq', 'ttret', 'uqywuq', '10281281021280', '23@gmail.com', 'sdsdd'),
('12345678', 'yeni', 'jln', '082171478564', 'nurrahmadayeni.11@gmail.com', 'h'),
('321', 'udin', 'kjhk', '1234', 'nurrahmadayeni.11@gmail.com', 'jhkj'),
('hfjh', 'fjj', 'jhfj', 'fjhf', 'nurrahmadayeni.11@gmail.com', 'hjf'),
('nuhuh', 'gjg', 'asaas', 'jhgj', 'nurrahmadayeni.11@gmail.com', 'hvjhv');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_pengeluaran`
--

CREATE TABLE IF NOT EXISTS `data_pengeluaran` (
  `id_pengeluaran` int(100) NOT NULL AUTO_INCREMENT,
  `tgl_sekarang` date NOT NULL,
  PRIMARY KEY (`id_pengeluaran`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `data_pengeluaran`
--

INSERT INTO `data_pengeluaran` (`id_pengeluaran`, `tgl_sekarang`) VALUES
(1, '2016-05-29'),
(2, '2016-05-30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_perumahan`
--

CREATE TABLE IF NOT EXISTS `data_perumahan` (
  `id_perumahan` varchar(100) NOT NULL,
  `nama_perumahan` varchar(100) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `spesifikasi_teknik` varchar(500) NOT NULL,
  `jumlah_kavling` int(100) NOT NULL,
  `harga_rumah` int(100) NOT NULL,
  PRIMARY KEY (`id_perumahan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_perumahan`
--

INSERT INTO `data_perumahan` (`id_perumahan`, `nama_perumahan`, `deskripsi`, `spesifikasi_teknik`, `jumlah_kavling`, `harga_rumah`) VALUES
('PRM-00004', 'Perumahan Nenek Moyang', 'Perumahan ini merupakan kepunyaan nenek moyang yang secara turun temurun diwariskan ke cucu-cucu yang baru lahir. ', ' Mempunya dua lantai, dan berlantaikan keramin', 0, 500000000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_saldodivisi`
--

CREATE TABLE IF NOT EXISTS `data_saldodivisi` (
  `id_saldo` int(100) NOT NULL AUTO_INCREMENT,
  `no_giro` int(100) NOT NULL,
  `saldo_divisi` int(100) NOT NULL,
  PRIMARY KEY (`id_saldo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `data_saldodivisi`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_order`
--

CREATE TABLE IF NOT EXISTS `detail_order` (
  `id_detailorder` int(100) NOT NULL AUTO_INCREMENT,
  `id_order` varchar(100) NOT NULL,
  `id_material` varchar(30) NOT NULL,
  `id_gudang` int(200) NOT NULL,
  `jumlah_brg` int(100) NOT NULL,
  `harga_material` int(100) NOT NULL,
  PRIMARY KEY (`id_detailorder`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `detail_order`
--

INSERT INTO `detail_order` (`id_detailorder`, `id_order`, `id_material`, `id_gudang`, `jumlah_brg`, `harga_material`) VALUES
(1, 'ORD-00001', 'MT-01', 1, 20, 250000),
(2, 'ORD-00001', 'MT-05', 1, 15, 2000000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_transaksirumah`
--

CREATE TABLE IF NOT EXISTS `detail_transaksirumah` (
  `id_detailtransaksi` int(100) NOT NULL AUTO_INCREMENT,
  `id_transaksi` varchar(100) NOT NULL,
  `id_perumahan` varchar(100) NOT NULL,
  `no_kavling` varchar(100) NOT NULL,
  `harga_tambahtanah` int(100) NOT NULL,
  `luas_tambahtanah` varchar(100) NOT NULL,
  `hook` varchar(100) NOT NULL,
  `biaya_tambahan` int(100) NOT NULL,
  `id_jenispembayaran` int(100) NOT NULL,
  `jlh_bayarbooking` int(100) NOT NULL,
  `jlh_DP1` int(100) NOT NULL,
  `nominal_kpr` int(100) NOT NULL,
  `keterangan_transaksi` varchar(200) NOT NULL,
  `total_hrgarumah` int(100) NOT NULL,
  `status_batal` varchar(100) NOT NULL,
  PRIMARY KEY (`id_detailtransaksi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `detail_transaksirumah`
--

INSERT INTO `detail_transaksirumah` (`id_detailtransaksi`, `id_transaksi`, `id_perumahan`, `no_kavling`, `harga_tambahtanah`, `luas_tambahtanah`, `hook`, `biaya_tambahan`, `id_jenispembayaran`, `jlh_bayarbooking`, `jlh_DP1`, `nominal_kpr`, `keterangan_transaksi`, `total_hrgarumah`, `status_batal`) VALUES
(1, 'TRS-00001', 'PRM-00004', 'A1', 3000000, '3', '10000000', 1000000, 2, 0, 12000000, 200000000, 'test', 212000000, 'batal transaksi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `foto_perumahan`
--

CREATE TABLE IF NOT EXISTS `foto_perumahan` (
  `id_foto` int(100) NOT NULL AUTO_INCREMENT,
  `id_perumahan` varchar(100) NOT NULL,
  `foto_rumah` varchar(200) NOT NULL,
  PRIMARY KEY (`id_foto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `foto_perumahan`
--

INSERT INTO `foto_perumahan` (`id_foto`, `id_perumahan`, `foto_rumah`) VALUES
(4, 'PRM-00004', 'ffc16084-4e17-4f1b-9b43-e8a2f15762d4.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `giro`
--

CREATE TABLE IF NOT EXISTS `giro` (
  `no_giro` int(100) NOT NULL AUTO_INCREMENT,
  `jenis_giro` varchar(100) NOT NULL,
  `no_rek` varchar(100) NOT NULL,
  `nama_bank` varchar(100) NOT NULL,
  `spesimen` varchar(100) NOT NULL,
  `penaggungjawab` varchar(100) NOT NULL,
  PRIMARY KEY (`no_giro`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `giro`
--

INSERT INTO `giro` (`no_giro`, `jenis_giro`, `no_rek`, `nama_bank`, `spesimen`, `penaggungjawab`) VALUES
(1, 'Marketing', '106002823934908', 'Mandiri', '-', 'Keuangan Marketing'),
(2, 'Teknikal', '106002823934909', 'Mandiri', '-', 'Keuangan Teknikal');

-- --------------------------------------------------------

--
-- Struktur dari tabel `inventaris`
--

CREATE TABLE IF NOT EXISTS `inventaris` (
  `id_inventaris` varchar(10) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `merk` varchar(255) NOT NULL,
  `warna` varchar(100) NOT NULL,
  `thn_peroleh` varchar(5) NOT NULL,
  `jumlah` int(10) NOT NULL,
  `kondisi` varchar(50) NOT NULL,
  `harga_barang` int(50) NOT NULL,
  `ptj` varchar(100) NOT NULL,
  `lokasi` varchar(100) NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `id_kategori`, `nama_barang`, `merk`, `warna`, `thn_peroleh`, `jumlah`, `kondisi`, `harga_barang`, `ptj`, `lokasi`, `foto`) VALUES
('INV-00001', 0, 'Mobil 10o', 'Avanza Sei G 010', 'hitam kecoklatan', '2016', 10, 'Baik', 286000000, 'Dewi kumalasari 2', 'Operasional Kantor 2', 'histogram_sample (1).jpg'),
('INV-00002', 2, 'Mobil 2', 'L 300', 'Putih', '2011', 1, 'Baik', 135000000, 'Nurin', 'Operasi Lapangan', 'l300-dalam.jpg'),
('INV-00003', 1, 'Mobil 3', 'L300', 'Putih', '2015', 2, 'Baik', 250000000, 'Samina', 'Lokasi Lapangan', 'L300.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_bayar`
--

CREATE TABLE IF NOT EXISTS `jenis_bayar` (
  `id_jeniskeuangan` int(10) NOT NULL AUTO_INCREMENT,
  `jenis_keuangan` varchar(100) NOT NULL,
  PRIMARY KEY (`id_jeniskeuangan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `jenis_bayar`
--

INSERT INTO `jenis_bayar` (`id_jeniskeuangan`, `jenis_keuangan`) VALUES
(1, 'Debit'),
(2, 'Kredit');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_pembayaranrumah`
--

CREATE TABLE IF NOT EXISTS `jenis_pembayaranrumah` (
  `id_jenispembayaran` int(100) NOT NULL AUTO_INCREMENT,
  `jenis_pembayaran` enum('Tunai(cash)','Kredit(KPR)') NOT NULL,
  PRIMARY KEY (`id_jenispembayaran`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `jenis_pembayaranrumah`
--

INSERT INTO `jenis_pembayaranrumah` (`id_jenispembayaran`, `jenis_pembayaran`) VALUES
(1, 'Tunai(cash)'),
(2, 'Kredit(KPR)');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_inventaris`
--

CREATE TABLE IF NOT EXISTS `kategori_inventaris` (
  `id_kategori` int(100) NOT NULL AUTO_INCREMENT,
  `kategori_inventaris` varchar(100) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `kategori_inventaris`
--

INSERT INTO `kategori_inventaris` (`id_kategori`, `kategori_inventaris`) VALUES
(1, 'Kendaraan'),
(2, 'Elektronik'),
(3, 'm');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_jenistransaksi`
--

CREATE TABLE IF NOT EXISTS `kategori_jenistransaksi` (
  `id_jenisbayar` int(10) NOT NULL AUTO_INCREMENT,
  `jenis_transaksi` varchar(50) NOT NULL,
  PRIMARY KEY (`id_jenisbayar`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data untuk tabel `kategori_jenistransaksi`
--

INSERT INTO `kategori_jenistransaksi` (`id_jenisbayar`, `jenis_transaksi`) VALUES
(2, 'DP1'),
(3, 'DP2'),
(4, 'DP3'),
(5, 'DP4'),
(6, 'DP5'),
(7, 'DP6'),
(8, 'DP7'),
(9, 'DP8'),
(10, 'DP9'),
(11, 'DP10'),
(25, 'Pelunasan DP'),
(26, 'Pelunasan Rumah'),
(27, 'KPR'),
(28, 'Pencairan Retensi1'),
(29, 'Pencairan Retensi2'),
(30, 'Pencairan Retensi3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_material`
--

CREATE TABLE IF NOT EXISTS `kategori_material` (
  `id_kategorimaterial` int(10) NOT NULL AUTO_INCREMENT,
  `kategori_material` varchar(10) NOT NULL,
  PRIMARY KEY (`id_kategorimaterial`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `kategori_material`
--

INSERT INTO `kategori_material` (`id_kategorimaterial`, `kategori_material`) VALUES
(1, 'atap'),
(2, 'plafon');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_pekerjaan`
--

CREATE TABLE IF NOT EXISTS `kategori_pekerjaan` (
  `id_kategorikerja` int(100) NOT NULL AUTO_INCREMENT,
  `nama_kategorikerja` varchar(100) NOT NULL,
  PRIMARY KEY (`id_kategorikerja`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data untuk tabel `kategori_pekerjaan`
--

INSERT INTO `kategori_pekerjaan` (`id_kategorikerja`, `nama_kategorikerja`) VALUES
(1, 'Struktur'),
(2, 'Atap'),
(3, 'Plafond'),
(4, 'Keramik Lantai'),
(5, 'Kamar Mandi'),
(6, 'Listrik'),
(7, 'Cat'),
(8, 'Septictank'),
(9, 'Sumur Bor'),
(10, 'Meja Dapur'),
(11, 'Pagar'),
(12, 'Pintu & Jendela'),
(13, 'Lainnya');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kontrak_pekerjaan`
--

CREATE TABLE IF NOT EXISTS `kontrak_pekerjaan` (
  `id_kontrak` int(10) NOT NULL AUTO_INCREMENT,
  `id_kategorikerja` int(100) NOT NULL,
  `nilai_kontrak` int(100) NOT NULL,
  `tgl_selesai` date NOT NULL,
  `id_perumahan` varchar(100) NOT NULL,
  `no_kavling` varchar(100) NOT NULL,
  `pengawas` varchar(100) NOT NULL,
  `kontraktor` varchar(100) NOT NULL,
  PRIMARY KEY (`id_kontrak`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `kontrak_pekerjaan`
--


-- --------------------------------------------------------

--
-- Stand-in structure for view `laporan_gudang`
--
CREATE TABLE IF NOT EXISTS `laporan_gudang` (
`nama_material` varchar(100)
,`satuan_material` varchar(100)
,`nama_gudang` varchar(100)
,`total_Stok` int(100)
,`jlh_masuk` int(100)
,`tgl_masuk` date
,`jlh_keluar` bigint(100)
,`tgl_pengeluaran` date
,`jlh_retur` bigint(100)
,`tgl_retur` date
);
-- --------------------------------------------------------

--
-- Struktur dari tabel `loging_user`
--

CREATE TABLE IF NOT EXISTS `loging_user` (
  `id_loging` int(100) NOT NULL AUTO_INCREMENT,
  `id_user` int(100) NOT NULL,
  `waktu_login` datetime NOT NULL,
  `waktu_logout` datetime NOT NULL,
  PRIMARY KEY (`id_loging`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data untuk tabel `loging_user`
--

INSERT INTO `loging_user` (`id_loging`, `id_user`, `waktu_login`, `waktu_logout`) VALUES
(1, 1, '2016-06-01 08:08:32', '2016-06-01 08:41:11'),
(2, 24, '2016-06-01 08:41:20', '2016-06-01 08:41:46'),
(3, 24, '2016-06-01 08:41:52', '2016-06-01 08:42:00'),
(4, 24, '2016-06-01 08:42:08', '2016-06-01 08:42:37'),
(5, 24, '2016-06-01 08:42:41', '2016-06-01 08:47:39'),
(6, 24, '2016-06-01 08:47:43', '2016-06-01 08:47:54'),
(7, 24, '2016-06-01 08:47:58', '2016-06-01 08:48:00'),
(8, 1, '2016-06-01 08:48:15', '2016-06-01 08:49:46'),
(9, 24, '2016-06-01 08:50:32', '2016-06-01 09:21:16'),
(10, 25, '2016-06-01 09:22:04', '2016-06-01 09:27:09'),
(11, 25, '2016-06-30 09:40:30', '0000-00-00 00:00:00'),
(12, 25, '2016-06-30 09:54:18', '0000-00-00 00:00:00'),
(13, 25, '2016-06-30 09:57:55', '0000-00-00 00:00:00'),
(14, 25, '2016-06-30 10:02:29', '0000-00-00 00:00:00'),
(15, 26, '2016-06-01 16:04:14', '0000-00-00 00:00:00'),
(16, 25, '2016-06-01 16:04:44', '2016-06-01 17:10:41'),
(17, 26, '2016-06-01 17:10:47', '0000-00-00 00:00:00'),
(18, 26, '2016-06-02 00:33:23', '0000-00-00 00:00:00'),
(19, 25, '2016-06-02 10:35:25', '0000-00-00 00:00:00'),
(20, 24, '2016-06-02 15:36:05', '2016-06-02 15:36:10'),
(21, 26, '2016-06-02 15:36:17', '0000-00-00 00:00:00'),
(22, 26, '2016-06-02 16:01:58', '0000-00-00 00:00:00'),
(23, 24, '2016-06-02 17:36:05', '0000-00-00 00:00:00'),
(24, 24, '2016-06-02 23:08:20', '0000-00-00 00:00:00'),
(25, 24, '2016-06-03 02:26:13', '0000-00-00 00:00:00'),
(26, 25, '2016-06-03 03:02:32', '0000-00-00 00:00:00'),
(27, 24, '2016-06-03 03:23:17', '0000-00-00 00:00:00'),
(28, 24, '2016-06-03 07:27:34', '0000-00-00 00:00:00'),
(29, 24, '2016-06-03 11:25:12', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `material`
--

CREATE TABLE IF NOT EXISTS `material` (
  `id_material` varchar(30) NOT NULL,
  `id_kategorimaterial` int(10) NOT NULL,
  `nama_material` varchar(100) NOT NULL,
  `satuan_material` varchar(100) NOT NULL,
  `spesifikasi` varchar(200) NOT NULL,
  `gambar_material` varchar(200) NOT NULL,
  `harga_material` int(100) NOT NULL,
  PRIMARY KEY (`id_material`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `material`
--

INSERT INTO `material` (`id_material`, `id_kategorimaterial`, `nama_material`, `satuan_material`, `spesifikasi`, `gambar_material`, `harga_material`) VALUES
('MT-01', 1, 'atap rumah', 'buah', 'warna hijau', 'gambar-secangkir-kopi.jpg', 1212),
('MT-02', 1, 'atap timbul', 'buah', 'warna biru', 'adadad', 12133),
('MT-03', 1, 'besi', 'sa', 'sdsd', 'asds', 1212),
('MT-05', 2, 'Plafon\r\n', 'sak', 'asa', 'asas', 1212);

-- --------------------------------------------------------

--
-- Struktur dari tabel `material_gudang`
--

CREATE TABLE IF NOT EXISTS `material_gudang` (
  `id_laporangdg` int(100) NOT NULL AUTO_INCREMENT,
  `id_gudang` int(200) NOT NULL,
  `id_material` varchar(30) NOT NULL,
  `jumlah` int(100) NOT NULL,
  PRIMARY KEY (`id_laporangdg`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data untuk tabel `material_gudang`
--

INSERT INTO `material_gudang` (`id_laporangdg`, `id_gudang`, `id_material`, `jumlah`) VALUES
(11, 1, 'MT-01', 87),
(12, 3, 'MT-01', 202),
(13, 1, 'MT-05', 118),
(14, 2, 'MT-02', 100),
(15, 3, 'MT-05', 12),
(16, 2, 'MT-01', 13),
(17, 1, 'MT-03', 13),
(18, 2, 'MT-05', 32);

-- --------------------------------------------------------

--
-- Struktur dari tabel `material_masuk`
--

CREATE TABLE IF NOT EXISTS `material_masuk` (
  `id_masuk` int(100) NOT NULL AUTO_INCREMENT,
  `id_gudang` int(200) NOT NULL,
  `id_material` varchar(30) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `jumlah_masuk` int(100) NOT NULL,
  PRIMARY KEY (`id_masuk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data untuk tabel `material_masuk`
--

INSERT INTO `material_masuk` (`id_masuk`, `id_gudang`, `id_material`, `tgl_masuk`, `jumlah_masuk`) VALUES
(1, 2, 'MT-01', '2016-05-20', 13),
(2, 1, 'MT-03', '2016-05-20', 13),
(3, 1, 'MT-01', '2016-05-16', 2),
(4, 1, 'MT-05', '2016-05-03', 2),
(5, 2, 'MT-02', '2016-04-07', 3),
(6, 3, 'MT-01', '2016-03-16', 2),
(8, 3, 'MT-05', '2016-05-04', 2),
(9, 2, 'MT-05', '2016-05-16', 30),
(10, 2, 'MT-05', '2016-05-18', 2),
(11, 3, 'MT-01', '2016-05-21', 12),
(12, 1, 'MT-01', '2016-06-01', 20),
(13, 1, 'MT-05', '2016-06-01', 15);

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_material`
--

CREATE TABLE IF NOT EXISTS `order_material` (
  `id_order` varchar(100) NOT NULL,
  `id_supplier` varchar(100) NOT NULL,
  `tgl_terdaftar` date NOT NULL,
  `tgl_order` date NOT NULL,
  `status_pembayaran` enum('lunas','belum lunas') NOT NULL,
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `order_material`
--

INSERT INTO `order_material` (`id_order`, `id_supplier`, `tgl_terdaftar`, `tgl_order`, `status_pembayaran`) VALUES
('ORD-00001', 'SPL-00002', '2016-06-01', '2016-06-01', 'belum lunas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran_order`
--

CREATE TABLE IF NOT EXISTS `pembayaran_order` (
  `id_pembayaran` int(100) NOT NULL AUTO_INCREMENT,
  `id_order` varchar(100) NOT NULL,
  `id_keuangan` int(100) NOT NULL,
  `tgl_cek` date NOT NULL,
  `tgl_terdaftar` date NOT NULL,
  `no_cek` varchar(100) NOT NULL,
  PRIMARY KEY (`id_pembayaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `pembayaran_order`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran_rumah`
--

CREATE TABLE IF NOT EXISTS `pembayaran_rumah` (
  `id_jenisbayar` int(100) NOT NULL AUTO_INCREMENT,
  `jenis_pembayaran` varchar(20) NOT NULL,
  PRIMARY KEY (`id_jenisbayar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `pembayaran_rumah`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `penarikan_cek`
--

CREATE TABLE IF NOT EXISTS `penarikan_cek` (
  `id_giro` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_penarikan` date NOT NULL,
  `jumlah` int(20) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  PRIMARY KEY (`id_giro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `penarikan_cek`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `pengajuan_alokasi`
--

CREATE TABLE IF NOT EXISTS `pengajuan_alokasi` (
  `no_alokasi` int(100) NOT NULL AUTO_INCREMENT,
  `jenis_alokasi` varchar(255) NOT NULL,
  `nama_alokasi` varchar(255) NOT NULL,
  `jml_alokasi` int(100) NOT NULL,
  `harga_alokasi` int(100) NOT NULL,
  `jumlah_keluar` int(100) NOT NULL,
  `waktu` date NOT NULL,
  PRIMARY KEY (`no_alokasi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `pengajuan_alokasi`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `pengawas_perumahan`
--

CREATE TABLE IF NOT EXISTS `pengawas_perumahan` (
  `id_pengawasrmh` int(100) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(10) NOT NULL,
  `id_perumahan` varchar(100) NOT NULL,
  PRIMARY KEY (`id_pengawasrmh`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `pengawas_perumahan`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `progress_pembangunan`
--

CREATE TABLE IF NOT EXISTS `progress_pembangunan` (
  `id_progress` int(100) NOT NULL AUTO_INCREMENT,
  `pengawas` varchar(100) NOT NULL,
  `id_perumahan` varchar(100) NOT NULL,
  `no_kavling` varchar(100) NOT NULL,
  `id_pekerjaan` int(100) NOT NULL,
  `progress` varchar(100) NOT NULL DEFAULT '0',
  `tgl_laporan` date NOT NULL,
  `foto` varchar(255) NOT NULL,
  PRIMARY KEY (`id_progress`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `progress_pembangunan`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `progress_pembayaran_kontrak`
--

CREATE TABLE IF NOT EXISTS `progress_pembayaran_kontrak` (
  `id_progress_pembayaran` varchar(15) NOT NULL,
  `id_kontrak` int(11) NOT NULL,
  `harga_upah` int(20) NOT NULL,
  `progress` varchar(10) NOT NULL,
  `nilai_progress` int(15) NOT NULL DEFAULT '0',
  `total_ambilan` int(20) NOT NULL DEFAULT '0',
  `keterangan` varchar(255) NOT NULL,
  `tanggal_pembayaran` date NOT NULL,
  `tgl_cek_progress` date NOT NULL,
  PRIMARY KEY (`id_progress_pembayaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `progress_pembayaran_kontrak`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `retur_barang`
--

CREATE TABLE IF NOT EXISTS `retur_barang` (
  `id_retur` int(100) NOT NULL AUTO_INCREMENT,
  `tgl_sekarang` date NOT NULL,
  PRIMARY KEY (`id_retur`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `retur_barang`
--

INSERT INTO `retur_barang` (`id_retur`, `tgl_sekarang`) VALUES
(1, '2016-05-29'),
(2, '2016-05-30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `retur_detail`
--

CREATE TABLE IF NOT EXISTS `retur_detail` (
  `id_detailretur` int(100) NOT NULL AUTO_INCREMENT,
  `id_retur` int(100) NOT NULL,
  `id_material` varchar(30) NOT NULL,
  `id_gudang` int(100) NOT NULL,
  `jumlah_kembali` int(100) NOT NULL,
  `id_perumahan` varchar(100) NOT NULL,
  `no_kavling` varchar(100) NOT NULL,
  `tgl_retur` date NOT NULL,
  PRIMARY KEY (`id_detailretur`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `retur_detail`
--

INSERT INTO `retur_detail` (`id_detailretur`, `id_retur`, `id_material`, `id_gudang`, `jumlah_kembali`, `id_perumahan`, `no_kavling`, `tgl_retur`) VALUES
(5, 1, 'MT-01', 1, 2, 'PRM-00001', 'M2', '2016-06-01'),
(7, 2, 'MT-02', 2, 2, 'PRM-00001', 'M2', '2016-06-04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `saldo_teknikal`
--

CREATE TABLE IF NOT EXISTS `saldo_teknikal` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tgl` date DEFAULT NULL,
  `debit` int(100) DEFAULT NULL,
  `kredit` int(100) DEFAULT NULL,
  `sisa_saldo` int(100) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data untuk tabel `saldo_teknikal`
--

INSERT INTO `saldo_teknikal` (`id`, `tgl`, `debit`, `kredit`, `sisa_saldo`, `keterangan`) VALUES
(7, '2016-05-30', 100000, 0, 100000, ''),
(8, '2016-05-30', 200000, 0, 300000, ''),
(9, '2016-05-30', 200000, 0, 500000, 'saldo'),
(10, '2016-06-30', 20000000, 0, 20500000, 'Dana Marketing');

-- --------------------------------------------------------

--
-- Stand-in structure for view `sisa`
--
CREATE TABLE IF NOT EXISTS `sisa` (
`id_supplier` varchar(100)
,`total` bigint(21)
);
-- --------------------------------------------------------

--
-- Struktur dari tabel `supplier`
--

CREATE TABLE IF NOT EXISTS `supplier` (
  `id_supplier` varchar(100) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `nama_pemilik` varchar(100) NOT NULL,
  `alamat_supplier` varchar(255) NOT NULL,
  `norek_supplier` varchar(100) NOT NULL,
  `tlp_supplier` varchar(20) NOT NULL,
  PRIMARY KEY (`id_supplier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `supplier`
--

INSERT INTO `supplier` (`id_supplier`, `nama_perusahaan`, `nama_pemilik`, `alamat_supplier`, `norek_supplier`, `tlp_supplier`) VALUES
('SPL-00001', 'PT. Global Union', 'PT. Global Union', 'Haery I Building . Jl. Kemang Selatan No.151 Jakarta Selatan 12560, Jakarta, Indonesia', '5447-6919-02-2', '(+62) 217812372'),
('SPL-00002', 'Pioneer Genteng Beton', 'Pioneer Genteng Beton', 'Jl Teuku Umar 304-306,Neusu Jaya,Baiturrahman, Banda Aceh, Indonesia', '5447-902-22-2', '0651 42038');

-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_detailkeluar`
--

CREATE TABLE IF NOT EXISTS `temp_detailkeluar` (
  `id_detailkeluar` int(100) NOT NULL AUTO_INCREMENT,
  `id_pengeluaran` int(100) NOT NULL,
  `id_material` varchar(30) NOT NULL,
  `id_gudang` int(200) NOT NULL,
  `jumlah_keluar` int(100) NOT NULL,
  `id_perumahan` varchar(100) NOT NULL,
  `no_kavling` varchar(100) NOT NULL,
  `tgl_pengeluaran` date NOT NULL,
  PRIMARY KEY (`id_detailkeluar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `temp_detailkeluar`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_detailorder`
--

CREATE TABLE IF NOT EXISTS `temp_detailorder` (
  `id_detailorder` int(100) NOT NULL AUTO_INCREMENT,
  `id_order` varchar(100) NOT NULL,
  `id_material` varchar(30) NOT NULL,
  `id_gudang` int(200) NOT NULL,
  `jumlah_brg` int(100) NOT NULL,
  `harga_material` int(100) NOT NULL,
  PRIMARY KEY (`id_detailorder`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `temp_detailorder`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_order`
--

CREATE TABLE IF NOT EXISTS `temp_order` (
  `id_order` varchar(100) NOT NULL,
  `id_supplier` varchar(100) NOT NULL,
  `tgl_terdaftar` date NOT NULL,
  `tgl_order` date NOT NULL,
  `status_pembayaran` enum('lunas','belum lunas') NOT NULL,
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `temp_order`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_pengeluaran`
--

CREATE TABLE IF NOT EXISTS `temp_pengeluaran` (
  `id_pengeluaran` int(100) NOT NULL AUTO_INCREMENT,
  `tgl_sekarang` date NOT NULL,
  PRIMARY KEY (`id_pengeluaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `temp_pengeluaran`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_retur`
--

CREATE TABLE IF NOT EXISTS `temp_retur` (
  `id_retur` int(100) NOT NULL AUTO_INCREMENT,
  `tgl_sekarang` date NOT NULL,
  PRIMARY KEY (`id_retur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `temp_retur`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `temp_returdetail`
--

CREATE TABLE IF NOT EXISTS `temp_returdetail` (
  `id_detailretur` int(100) NOT NULL AUTO_INCREMENT,
  `id_retur` int(100) NOT NULL,
  `id_material` varchar(30) NOT NULL,
  `id_gudang` int(100) NOT NULL,
  `jumlah_kembali` int(100) NOT NULL,
  `id_perumahan` varchar(100) NOT NULL,
  `no_kavling` varchar(100) NOT NULL,
  `tgl_retur` date NOT NULL,
  PRIMARY KEY (`id_detailretur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `temp_returdetail`
--


-- --------------------------------------------------------

--
-- Stand-in structure for view `total_hargabon`
--
CREATE TABLE IF NOT EXISTS `total_hargabon` (
`id_supplier` varchar(100)
,`id_order` varchar(100)
,`total_bon` decimal(65,0)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `total_order`
--
CREATE TABLE IF NOT EXISTS `total_order` (
`id_supplier` varchar(100)
,`total_order` bigint(21)
);
-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_perumahan`
--

CREATE TABLE IF NOT EXISTS `transaksi_perumahan` (
  `id_transaksi` varchar(100) NOT NULL,
  `tgl_booking` date NOT NULL,
  `nik_ktp` varchar(100) NOT NULL,
  `tgl_booking2` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi_perumahan`
--

INSERT INTO `transaksi_perumahan` (`id_transaksi`, `tgl_booking`, `nik_ktp`, `tgl_booking2`) VALUES
('TRS-00001', '2016-06-03', '12345678', '2016-06-02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `type_rumah`
--

CREATE TABLE IF NOT EXISTS `type_rumah` (
  `id_type` int(100) NOT NULL AUTO_INCREMENT,
  `type_rumah` varchar(100) NOT NULL,
  `harga_kavling` int(100) NOT NULL,
  `luas_tanah` varchar(100) NOT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `type_rumah`
--

INSERT INTO `type_rumah` (`id_type`, `type_rumah`, `harga_kavling`, `luas_tanah`) VALUES
(1, 'A', 200000000, '200 '),
(2, 'B', 400000000, '260 '),
(3, 'C', 250000000, '4 meter x 5 meter');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(100) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level_user` enum('ADM','Teknikal','Marketing','Supervisor','Admin') NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `level_user`) VALUES
(24, 'nurrahmadayeni32', '827ccb0eea8a706c4c34a16891f84e7b', 'Teknikal'),
(25, 'endangwi14', '827ccb0eea8a706c4c34a16891f84e7b', 'ADM'),
(26, 'rantira24', '827ccb0eea8a706c4c34a16891f84e7b', 'Marketing');

-- --------------------------------------------------------

--
-- Structure for view `bon`
--
DROP TABLE IF EXISTS `bon`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `bon` AS select `su`.`id_supplier` AS `id_supplier`,`su`.`nama_perusahaan` AS `nama_supplier`,if((select `s`.`total` from ((`supplier` `su` join `order_material` `o` on((`o`.`id_supplier` = `su`.`id_supplier`))) join `sisa` `s` on((`s`.`id_supplier` = `su`.`id_supplier`))) where isnull(`s`.`id_supplier`)),0,`s`.`total`) AS `sisa`,if((select `t`.`total_order` from ((`supplier` `su` join `order_material` `o` on((`o`.`id_supplier` = `su`.`id_supplier`))) join `total_order` `t` on((`t`.`id_supplier` = `su`.`id_supplier`))) where isnull(`t`.`id_supplier`)),0,`t`.`total_order`) AS `total_order` from (((`supplier` `su` join `order_material` `o` on((`o`.`id_supplier` = `su`.`id_supplier`))) left join `sisa` `s` on((`s`.`id_supplier` = `su`.`id_supplier`))) left join `total_order` `t` on((`t`.`id_supplier` = `su`.`id_supplier`))) group by `su`.`id_supplier`;

-- --------------------------------------------------------

--
-- Structure for view `bon_2`
--
DROP TABLE IF EXISTS `bon_2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `bon_2` AS select `su`.`id_supplier` AS `id_supplier`,`su`.`nama_perusahaan` AS `nama_supplier`,if((select `s`.`total` from ((`supplier` `su` join `order_material` `o` on((`o`.`id_supplier` = `su`.`id_supplier`))) join `sisa` `s` on((`s`.`id_supplier` = `su`.`id_supplier`))) where isnull(`s`.`id_supplier`)),0,`s`.`total`) AS `sisa`,if((select `t`.`total_order` from ((`supplier` `su` join `order_material` `o` on((`o`.`id_supplier` = `su`.`id_supplier`))) join `total_order` `t` on((`t`.`id_supplier` = `su`.`id_supplier`))) where isnull(`t`.`id_supplier`)),0,`t`.`total_order`) AS `total_order`,sum(`th`.`total_bon`) AS `total_harga` from ((((`supplier` `su` join `order_material` `o` on((`o`.`id_supplier` = `su`.`id_supplier`))) left join `sisa` `s` on((`s`.`id_supplier` = `su`.`id_supplier`))) left join `total_order` `t` on((`t`.`id_supplier` = `su`.`id_supplier`))) left join `total_hargabon` `th` on((`th`.`id_supplier` = `su`.`id_supplier`))) group by `su`.`id_supplier`;

-- --------------------------------------------------------

--
-- Structure for view `data_konsumenrefund`
--
DROP TABLE IF EXISTS `data_konsumenrefund`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `data_konsumenrefund` AS select `bt`.`id_transaksi` AS `id_transaksi` from `batal_transaksi` `bt` where (not(`bt`.`id_transaksi` in (select `batal_pembelian`.`id_transaksi` from `batal_pembelian`)));

-- --------------------------------------------------------

--
-- Structure for view `laporan_gudang`
--
DROP TABLE IF EXISTS `laporan_gudang`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `laporan_gudang` AS select `m`.`nama_material` AS `nama_material`,`m`.`satuan_material` AS `satuan_material`,`g`.`nama_gudang` AS `nama_gudang`,`mg`.`jumlah` AS `total_Stok`,`mm`.`jumlah_masuk` AS `jlh_masuk`,`mm`.`tgl_masuk` AS `tgl_masuk`,if((select sum(`dk`.`jumlah_keluar`) from (`material_gudang` `mg` join `data_detailkeluar` `dk` on(((`dk`.`id_gudang` = `mg`.`id_gudang`) and (`dk`.`id_material` = `mg`.`id_material`) and isnull(`dk`.`id_gudang`) and isnull(`dk`.`id_material`)))) group by `dk`.`id_gudang`,`dk`.`id_material`),0,`dk`.`jumlah_keluar`) AS `jlh_keluar`,`dk`.`tgl_pengeluaran` AS `tgl_pengeluaran`,if((select sum(`rt`.`jumlah_kembali`) from (`material_gudang` `mg` join `retur_detail` `rt` on(((`rt`.`id_gudang` = `mg`.`id_gudang`) and (`rt`.`id_material` = `mg`.`id_material`) and isnull(`rt`.`id_gudang`) and isnull(`rt`.`id_material`)))) group by `rt`.`id_gudang`,`rt`.`id_material`),0,`rt`.`jumlah_kembali`) AS `jlh_retur`,`rt`.`tgl_retur` AS `tgl_retur` from (((((`material_gudang` `mg` join `material_masuk` `mm` on(((`mm`.`id_gudang` = `mg`.`id_gudang`) and (`mm`.`id_material` = `mg`.`id_material`)))) join `material` `m` on((`mg`.`id_material` = `m`.`id_material`))) join `data_gudang` `g` on((`mg`.`id_gudang` = `g`.`id_gudang`))) left join `data_detailkeluar` `dk` on(((`dk`.`id_material` = `mg`.`id_material`) and (`dk`.`id_gudang` = `mg`.`id_gudang`)))) left join `retur_detail` `rt` on(((`rt`.`id_material` = `mg`.`id_material`) and (`rt`.`id_gudang` = `mg`.`id_gudang`)))) order by `mg`.`id_gudang`;

-- --------------------------------------------------------

--
-- Structure for view `sisa`
--
DROP TABLE IF EXISTS `sisa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sisa` AS select `order_material`.`id_supplier` AS `id_supplier`,count(`order_material`.`id_order`) AS `total` from `order_material` where (`order_material`.`status_pembayaran` = 'belum lunas') group by `order_material`.`id_supplier`;

-- --------------------------------------------------------

--
-- Structure for view `total_hargabon`
--
DROP TABLE IF EXISTS `total_hargabon`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `total_hargabon` AS select `o`.`id_supplier` AS `id_supplier`,`d`.`id_order` AS `id_order`,sum((`d`.`jumlah_brg` * `d`.`harga_material`)) AS `total_bon` from (`detail_order` `d` join `order_material` `o`) where (`o`.`id_order` = `d`.`id_order`) group by `d`.`id_order`;

-- --------------------------------------------------------

--
-- Structure for view `total_order`
--
DROP TABLE IF EXISTS `total_order`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `total_order` AS select `order_material`.`id_supplier` AS `id_supplier`,count(`order_material`.`id_order`) AS `total_order` from `order_material` group by `order_material`.`id_supplier`;
